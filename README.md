# Create new module

## Package.json
1) Create new folder in packages
2) Create package.json
3) !!!Set name in package.json
4) Add workspace in root package.json

```
{
  "name": "@frontend-shared/{name}",
  "version": "1.0.2",
  "main": "./index.ts",
  "license": "MIT",
  "typings": "./index.ts",
}
```

## Publish module

1)  Commit all changes
2)  Launch the publication command from package.json that is located in the root

## Pre use command
1) Registration https://www.npmjs.com/
2) Use npm login for sign up in terminal
3) Use command

## Command
```
   "scripts": {
    "release-patch": "lerna publish patch --npm-tag latest", // 1.0.0 -> 1.0.1
    "release-minor": "lerna publish minor --npm-tag latest", // 1.0.0 -> 1.1.0
    "release-major": "lerna publish major --npm-tag latest", // 1.0.0 -> 2.0.0
    "alpha-patch": "lerna publish prepatch --npm-tag next",  //1.0.0 -> 1.0.1-alpha.0
    "alpha-minor": "lerna publish preminor --npm-tag next",  //1.0.0 -> 1.1.0-alpha.0
    "alpha-major": "lerna publish premajor --npm-tag next"   //1.0.0 -> 2.0.0-alpha.0
  }
```
## Command to publish one module
```
    lerna publish --force-publish [package-name]
    Example: lerna publish --force-publish common
```