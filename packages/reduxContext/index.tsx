import React from 'react';
import { ReactReduxContextValue } from "react-redux";
export const createContext = <T extends object>(initialValue?:T): React.Context<ReactReduxContextValue<T>> => React.createContext<ReactReduxContextValue<T>>({ ...initialValue } as ReactReduxContextValue<T>);