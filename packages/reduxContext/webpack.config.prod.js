'use strict';
const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');

module.exports = {
  entry: path.resolve(__dirname, '/index.tsx'),
  output: {
    path: path.join(__dirname, '/lib'),
    filename: 'index.js',
    library: 'redux-context',
    libraryTarget: 'umd',
    clean: true,
    publicPath: '/',
    globalObject: 'this',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  optimization: {
    minimize: false
  },
  target: 'web',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: [/node_modules/],
        use: ['babel-loader', 'ts-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
    ]
  },
  plugins: [
    /**
     * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
     * the following package: sudo apt-get install libpng16-dev
     */
    // new ModuleFederationPlugin({
    //   name: 'redux-context',
    //   filename: 'remoteEntry.js',
    //   remotes: {},
    //   exposes: {},
    //   shared: {
    //     react: {
    //       singleton: true,
    //     },
    //   },
    // }),
  ],
  externals: {
    react: {
      commonjs: 'React',
      commonjs2: 'react',
      amd: 'react'
    },
  },
};
