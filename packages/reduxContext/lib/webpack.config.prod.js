'use strict';
var webpack = require('webpack');
var path = require('path');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
module.exports = {
    entry: path.resolve(__dirname, '/index.tsx'),
    output: {
        path: path.join(__dirname, '/lib'),
        filename: 'index.js',
        library: 'redux-context',
        libraryTarget: 'umd',
        clean: true,
        publicPath: '/',
        globalObject: 'this',
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    optimization: {
        minimize: false
    },
    target: 'web',
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: [/node_modules/],
                use: ['babel-loader', 'ts-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
        ]
    },
    plugins: [],
    externals: {
        react: {
            commonjs: 'React',
            commonjs2: 'react',
            amd: 'react'
        },
    },
};
//# sourceMappingURL=webpack.config.prod.js.map