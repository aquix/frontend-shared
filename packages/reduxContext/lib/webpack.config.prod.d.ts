export const entry: any;
export namespace output {
    const path: any;
    const filename: string;
    const library: string;
    const libraryTarget: string;
    const clean: boolean;
    const publicPath: string;
    const globalObject: string;
}
export namespace resolve {
    const extensions: string[];
}
export namespace optimization {
    const minimize: boolean;
}
export const target: string;
export const mode: string;
export namespace module {
    const rules: ({
        test: RegExp;
        exclude: RegExp[];
        use: string[];
        loader?: undefined;
    } | {
        test: RegExp;
        exclude: RegExp;
        loader: string;
        use?: undefined;
    })[];
}
export const plugins: never[];
export namespace externals {
    namespace react {
        const commonjs: string;
        const commonjs2: string;
        const amd: string;
    }
}
