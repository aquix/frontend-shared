import { Design } from "react-email-editor";

export interface IMailTemplate {
   id: number,
   title: string,
   content: string,
   config: Design,
}

export interface IMailTemplateReducer {
   list: IMailTemplate[],
   item: IMailTemplate,
}