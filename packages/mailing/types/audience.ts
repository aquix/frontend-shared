export interface IAudience {
   id: number,
   title: string,
   url: string,
}

export interface IAudienceReducer {
   list: IAudience[],
   item: IAudience,
}