import { IAudience, IAudienceReducer } from "../../types/audience";
import {
   AudienceAction,
   CREATE_AUDIENCE,
   CreateAudienceAction,
   LOAD_AUDIENCE, LOAD_AUDIENCE_ITEM,
   LoadAudienceAction,
   LoadAudienceItemAction,
   REMOVE_AUDIENCE,
   RemoveAudienceAction
} from "../action/audience";

const defaultState: IAudienceReducer = {
   list: [] as IAudience[],
   item: {} as IAudience,
}

const AudienceReducer = (
   state: IAudienceReducer = defaultState,
   action: AudienceAction,
): IAudienceReducer => {
   if (action.type === LOAD_AUDIENCE) {
      return {
         ...state,
         list: (action as LoadAudienceAction).payload,
      }
   }

   if (action.type === REMOVE_AUDIENCE) {
      const item = (action as RemoveAudienceAction).payload;
      const index = state.list.findIndex(i => i.id === item);

      return {
         ...state,
         list: [
            ...state.list.slice(0, index),
            ...state.list.slice(index + 1),
         ]
      }
   }

   if (action.type === CREATE_AUDIENCE) {
      const item = (action as CreateAudienceAction).payload;

      return {
         ...state,
         list: [
            item,
            ...state.list
         ]
      }
   }

   if (action.type === LOAD_AUDIENCE_ITEM) {
      return {
         ...state,
         item: (action as LoadAudienceItemAction).payload,
      }
   }

   return state;
};

export default AudienceReducer;
