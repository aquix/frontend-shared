import { IMailTemplate, IMailTemplateReducer } from "../../types/template";
import {
   CREATE_OR_UPDATE_TEMPLATE,
   CreateOrUpdateTemplateAction, GET_TEMPLATE, GetTemplateAction,
   LOAD_TEMPLATES, LoadTemplatesAction,
   MailTemplateAction, REMOVE_TEMPLATE, RemoveTemplateAction
} from "../action/template";

const defaultState: IMailTemplateReducer = {
   list: [],
   item: {} as IMailTemplate,
}

const MailTemplateReducerMethod = (
   state: IMailTemplateReducer = defaultState,
   action: MailTemplateAction,
): IMailTemplateReducer => {

   if (action.type === CREATE_OR_UPDATE_TEMPLATE) {
      const item = (action as CreateOrUpdateTemplateAction).payload;
      const index = state.list.findIndex(i => i.id === item.id);

      if (!!~index) {
         return {
            ...state,
            list: [
               ...state.list.slice(0, index),
               item,
               ...state.list.slice(index + 1),
            ]
         }
      }
   }

   if (action.type === LOAD_TEMPLATES) {
      const list = (action as LoadTemplatesAction).payload;
      return {
         ...state,
         list
      }
   }

   if (action.type === REMOVE_TEMPLATE) {
      const id = (action as RemoveTemplateAction).payload;
      const index = state.list.findIndex(i => i.id === id);

      return {
         ...state,
         list: [
            ...state.list.slice(0, index),
            ...state.list.slice(index + 1),
         ]
      }
   }

   if (action.type === GET_TEMPLATE) {
      const item = (action as GetTemplateAction).payload;
      return {
         ...state,
         item,
      }
   }

   return state
}

export default MailTemplateReducerMethod