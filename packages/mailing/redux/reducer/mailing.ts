import { IMailing, IMailReducer } from "../../types/mailing";
import {
   CREATE_MAILING,
   CreateMailingAction,
   LOAD_MAILING, LOAD_MAILING_ITEM,
   LoadMailingAction, LoadMailingItemAction,
   MailingAction, REMOVE_MAILING, RemoveMailingAction, SEND_MAILING, SendMailingAction
} from "../action/mailing";

const defaultState: IMailReducer = {
   list: [],
   item: {} as IMailing,
}

const MailingReducerMethod = (
   state: IMailReducer = defaultState,
   action: MailingAction,
): IMailReducer => {
   if (action.type === LOAD_MAILING) {
      return {
         ...state,
         list: (action as LoadMailingAction).payload
      }
   }

   if (action.type === CREATE_MAILING) {
      const item = (action as CreateMailingAction).payload;

      return {
         ...state,
         list: [
            item,
            ...state.list,
         ]
      };
   }

   if (action.type === SEND_MAILING) {
      const item = (action as SendMailingAction).payload;
      const index = state.list.findIndex(i => i.id === item.id);

      if (!!~index) {
         return {
            ...state,
            list: [
               ...state.list.slice(0, index),
               { ...state.list[index], ...item },
               ...state.list.slice(index + 1),
            ]
         }
      }
   }

   if (action.type === REMOVE_MAILING) {
      const item = (action as RemoveMailingAction).payload;
      const index = state.list.findIndex(i => i.id === item);

      return {
         ...state,
         list: [
            ...state.list.slice(0, index),
            ...state.list.slice(index + 1),
         ]
      }
   }

   if (action.type === LOAD_MAILING_ITEM) {
      return {
         ...state,
         item: (action as LoadMailingItemAction).payload,
      }
   }

   return state;
};

export default MailingReducerMethod;
