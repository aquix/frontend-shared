import { combineReducers } from 'redux';
import {IMailingReducer} from '../../types/reducer';
import AudienceReducer from "./audience";
import MailingReducerMethod from "./mailing";
import MailTemplateReducerMethod from "./template";

export const MailingReducer = combineReducers<IMailingReducer>({
   audience: AudienceReducer,
   mail: MailingReducerMethod,
   template: MailTemplateReducerMethod,
});