import {IMailing} from "../../types/mailing";
import { CommonAction } from "@frontend-shared/common";

export const LOAD_MAILING = 'LOAD_MAILING';
export const CREATE_MAILING = 'CREATE_MAILING';
export const SEND_MAILING = 'SEND_MAILING';
export const REMOVE_MAILING = 'REMOVE_MAILING';
export const LOAD_MAILING_ITEM = 'LOAD_MAILING_ITEM';

export const loadMailingAction = CommonAction<IMailing[]>(LOAD_MAILING);
export const createMailingAction = CommonAction<IMailing>(CREATE_MAILING);
export const sendMailingAction = CommonAction<IMailing>(SEND_MAILING);
export const removeMailingAction = CommonAction<number>(REMOVE_MAILING);
export const loadMailingItemAction = CommonAction<IMailing>(LOAD_MAILING_ITEM);

export type LoadMailingAction = ReturnType<typeof loadMailingAction>;
export type CreateMailingAction = ReturnType<typeof createMailingAction>;
export type SendMailingAction = ReturnType<typeof sendMailingAction>;
export type RemoveMailingAction = ReturnType<typeof removeMailingAction>;
export type LoadMailingItemAction = ReturnType<typeof loadMailingItemAction>;

export type MailingAction = LoadMailingAction | CreateMailingAction | SendMailingAction | RemoveMailingAction | LoadMailingItemAction;