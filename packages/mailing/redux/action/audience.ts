import { IAudience } from "../../types/audience";
import { CommonAction } from "@frontend-shared/common";

export const CREATE_AUDIENCE = 'CREATE_AUDIENCE';
export const LOAD_AUDIENCE = 'LOAD_AUDIENCE';
export const REMOVE_AUDIENCE = 'REMOVE_AUDIENCE';
export const LOAD_AUDIENCE_ITEM = 'LOAD_AUDIENCE_ITEM';

export const createAudienceAction = CommonAction<IAudience>(CREATE_AUDIENCE);
export const loadAudienceAction = CommonAction<IAudience[]>(LOAD_AUDIENCE);
export const removeAudienceAction = CommonAction<number>(REMOVE_AUDIENCE);
export const loadAudienceItemAction = CommonAction<IAudience>(LOAD_AUDIENCE_ITEM);

export type CreateAudienceAction = ReturnType<typeof createAudienceAction>;
export type LoadAudienceAction = ReturnType<typeof loadAudienceAction>;
export type RemoveAudienceAction = ReturnType<typeof removeAudienceAction>;
export type LoadAudienceItemAction = ReturnType<typeof loadAudienceItemAction>;

export type AudienceAction = CreateAudienceAction | CreateAudienceAction | LoadAudienceAction | RemoveAudienceAction | LoadAudienceItemAction;
