import {IMailTemplate} from "../../types/template";
import { CommonAction } from "@frontend-shared/common";

export const LOAD_TEMPLATES = 'LOAD_TEMPLATES';
export const CREATE_OR_UPDATE_TEMPLATE = 'CREATE_OR_UPDATE_TEMPLATE';
export const REMOVE_TEMPLATE = 'REMOVE_TEMPLATE';
export const GET_TEMPLATE = 'GET_TEMPLATE';

export const loadTemplatesAction = CommonAction<IMailTemplate[]>(LOAD_TEMPLATES);
export const createOrUpdateTemplateAction = CommonAction<IMailTemplate>(CREATE_OR_UPDATE_TEMPLATE);
export const removeTemplateAction = CommonAction<number>(REMOVE_TEMPLATE);
export const getTemplateAction = CommonAction<IMailTemplate>(GET_TEMPLATE);

export type LoadTemplatesAction = ReturnType<typeof loadTemplatesAction>;
export type CreateOrUpdateTemplateAction = ReturnType<typeof createOrUpdateTemplateAction>;
export type RemoveTemplateAction = ReturnType<typeof removeTemplateAction>;
export type GetTemplateAction = ReturnType<typeof getTemplateAction>;

export type MailTemplateAction = LoadTemplatesAction | CreateOrUpdateTemplateAction | RemoveTemplateAction | GetTemplateAction;