import { createStore, applyMiddleware, AnyAction } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import {MailingReducer} from './reducer/MailingReducer';
import { IMailingReducer } from '../types/reducer';

const loggerMiddleware = createLogger();
const initialState = {} as IMailingReducer;

let middleware;

if (process.env.NODE_ENV === 'production') {
   middleware = applyMiddleware(thunkMiddleware);
} else {
   middleware = applyMiddleware(thunkMiddleware, loggerMiddleware);
}

export default createStore<IMailingReducer, AnyAction, {}, {}>(MailingReducer, initialState, middleware);
