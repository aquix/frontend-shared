import {IAudience} from "../types/audience";
import {
  createAudienceAction,
  loadAudienceAction,
  loadAudienceItemAction,
  removeAudienceAction,
} from "../redux/action/audience";
import { BaseCommandProps, ResultItemRes, ResultListRes } from "@frontend-shared/common";
import { apiCall } from "@frontend-shared/common";
import { AppDispatch } from "@frontend-shared/common";

export const createAudience = (value: IAudience) => (dispatch: AppDispatch<IAudience>) => {
  return dispatch(apiCall<object, ResultItemRes<IAudience> & BaseCommandProps>('/v1/mailing/audience', value, { method: 'post', context: 'common'}))
    .then(({ item }) => dispatch(createAudienceAction({ ...value, ...item })));
}

export const loadAudience = () => (dispatch: AppDispatch<IAudience>) => {
  return dispatch(apiCall<object, ResultListRes<IAudience[]> & BaseCommandProps>('/v1/mailing/audience', undefined, { method: 'get' }))
    .then(({ list }) => dispatch(loadAudienceAction(list)));
}

export const removeAudience = (id: number) => (dispatch: AppDispatch<number>) => {
  return dispatch(apiCall<object, BaseCommandProps>('/v1/mailing/audience', { id }, { method: 'delete' }))
    .then((res) => dispatch(removeAudienceAction(id)));
}

export const loadAudienceItem = (id: number) => (dispatch: AppDispatch<IAudience>) => {
  return dispatch(apiCall<object, ResultItemRes<IAudience> & BaseCommandProps>(`/v1/mailing/audience/item/${id}`, undefined, { method: 'get' }))
  .then(({ item }) => dispatch(loadAudienceItemAction(item)));
}