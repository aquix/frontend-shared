import {IMailing} from "../types/mailing";
import {
   createMailingAction,
   loadMailingAction,
   loadMailingItemAction, removeMailingAction,
   sendMailingAction,
} from "../redux/action/mailing";
import { AppDispatch } from "@frontend-shared/common";
import { BaseCommandProps, ResultItemRes, ResultListRes } from "@frontend-shared/common";
import { apiCall } from "@frontend-shared/common";

export const createMailing = (value: IMailing) => (dispatch: AppDispatch<IMailing>) => {
  return dispatch(apiCall<object, ResultItemRes<IMailing> & BaseCommandProps>('/v1/mailing/project', value, { method: 'post', context: 'common' }))
    .then(({ item: createdItem }) => {
      dispatch(createMailingAction({ ...value, ...createdItem, status: value.status === false ? false : null }));
      if (value.status === false) {
        dispatch(sendMailingAction({ id: value.id, status: false } as IMailing));
        dispatch(apiCall<object, ResultItemRes<IMailing> & BaseCommandProps>('/v1/mailing/project/send', { projectId: createdItem.id }, { method: 'post' }))
          .then(({ item }) => dispatch(sendMailingAction({ ...value, ...item, id: createdItem.id })));
      }
    });
}

export const loadMailing = () => (dispatch: AppDispatch<IMailing[]>) => {
  dispatch(apiCall<object, ResultListRes<IMailing[]> & BaseCommandProps>('/v1/mailing/project', undefined, { method: 'get' }))
    .then(({ list }) => dispatch(loadMailingAction(list)));
}

export const sendMailing = (value: IMailing) => (dispatch: AppDispatch<IMailing[]>) => {
  dispatch(sendMailingAction({ id: value.id, status: undefined } as IMailing));
  return dispatch(apiCall<object, ResultItemRes<IMailing> & BaseCommandProps>('/v1/mailing/project', { ...value }, { method: 'post', context: 'common' }))
    .then(({ item }) => {
      dispatch(sendMailingAction({ id: value.id, status: false } as IMailing));
      dispatch(apiCall<object, ResultItemRes<IMailing> & BaseCommandProps>('/v1/mailing/project/send', { projectId: value.id }, { method: 'post' }))
        .then(({ item }) => dispatch(sendMailingAction({ ...value, ...item })));
    });
}

export const pauseMailing = (value: IMailing) => (dispatch: AppDispatch<IMailing>) => {
  dispatch(sendMailingAction({ ...value, status: null } as IMailing));
  return dispatch(apiCall<object, ResultItemRes<IMailing> & BaseCommandProps>('/v1/mailing/project/pause', { projectId: value.id }, { method: 'post' }))
    .then(({ item }) => dispatch(sendMailingAction({ ...value, status: null })));
}

export const removeMailing = (id: number) => (dispatch: AppDispatch<number>) => {
  return dispatch(apiCall<object, BaseCommandProps>('/v1/mailing/project', { id }, { method: 'delete' }))
  .then((res) => dispatch(removeMailingAction(id)));
}

export const loadMailingItem = (id: number) => (dispatch: AppDispatch<IMailing>) => {
  return dispatch(apiCall<object, ResultItemRes<IMailing> & BaseCommandProps>(`/v1/mailing/project/item/${id}`, undefined, { method: 'get' }))
  .then(({ item }) => dispatch(loadMailingItemAction(item)));
}