import {IMailTemplate} from "../types/template";
import {
  createOrUpdateTemplateAction,
  getTemplateAction,
  loadTemplatesAction,
  removeTemplateAction,
} from "../redux/action/template";
import { ApiClientOptions } from "@frontend-shared/common";
import { BaseCommandProps, ResultItemRes, ResultListRes } from "@frontend-shared/common";
import { AppDispatch } from "@frontend-shared/common";
import { apiCall } from "@frontend-shared/common";

export const createOrUpdateTemplate = (value: IMailTemplate) => (dispatch: AppDispatch<BaseCommandProps>) => {
  const options = <ApiClientOptions>{method: (value.id) ? 'put' : 'post'};
  return dispatch(apiCall<object, ResultItemRes<IMailTemplate> & BaseCommandProps>('/v1/mailing/template', value, options))
    .then(({ item }) => dispatch(createOrUpdateTemplateAction({ ...value, ...item })));
}

export const removeTemplate = (id: number) => (dispatch: AppDispatch<BaseCommandProps>) => {
  return dispatch(apiCall<object, BaseCommandProps>('/v1/mailing/template', { id }, { method: "delete" }))
    .then(() => dispatch(removeTemplateAction(id)));
}

export const loadTemplates = () => (dispatch: AppDispatch<BaseCommandProps>) => {
  return dispatch(apiCall<object, ResultListRes<IMailTemplate[]> & BaseCommandProps>('/v1/mailing/template',  undefined, { method: "get" }))
    .then(({ list }) => dispatch(loadTemplatesAction(list)));
}

export const getTemplate = (id: number) => (dispatch: AppDispatch<BaseCommandProps>) => {
  return dispatch(apiCall<object, ResultItemRes<IMailTemplate> & BaseCommandProps>(`/v1/mailing/template/item/${id}`,  undefined, { method: "get" }))
  .then(({ item }) => dispatch(getTemplateAction(item)));
}