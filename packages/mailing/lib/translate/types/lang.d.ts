import { BaseCommandProps } from "@frontend-shared/common";
export interface ILang {
    id: number;
    code: string;
    sort: number;
    isoCode: string;
}
export declare type ILangObject = {
    [key: string]: string;
};
export declare type ILangObjectFormTranslate = {
    [key: string]: {
        value: string;
    };
};
export interface LangObjectRes extends BaseCommandProps {
    list: ILang[];
}
export interface LangObjectEditRes extends BaseCommandProps {
    lang: ILang;
}
export interface UserLangRes extends BaseCommandProps {
    code: string;
}
export interface ExportTranslateRes extends BaseCommandProps {
    list: string;
}
