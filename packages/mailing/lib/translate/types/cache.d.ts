import { BaseCommandProps } from "@frontend-shared/common";
export interface CacheObjectRes extends BaseCommandProps {
    cache: number;
}
