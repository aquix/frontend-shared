import { ITranslateReducer } from "../types/reducer";
import { TranslateListAdminObjectRes, TranslateListItemAdminObjectRes } from "../types/translateListAdmin";
import { ExportTranslateRes, ILang, LangObjectEditRes, LangObjectRes } from "../types/lang";
import { ITranslate, TranslateObjectReq } from "../types/translate";
import { CacheObjectRes } from "../types/cache";
import { AppDispatch } from "@frontend-shared/common";
export declare const loadCache: () => (dispatch: AppDispatch<CacheObjectRes>) => Promise<{
    type: string;
    payload: {
        cache: number;
    };
}>;
export declare const loadTranslateAdmin: (currentPage: number, pageSize: number) => (dispatch: AppDispatch<ITranslateReducer>, getState: () => ITranslateReducer) => void;
export declare const loadLangsListWithSource: () => (dispatch: AppDispatch<LangObjectRes>, getState: () => ITranslateReducer) => Promise<void>;
export declare const createOrUpdateTranslate: (value: TranslateObjectReq) => (dispatch: AppDispatch<TranslateListItemAdminObjectRes>, getState: () => ITranslateReducer) => Promise<{
    type: string;
    payload: {
        item: import("../types/translateListAdmin").ITranslateListAdmin;
    };
}>;
export declare const createOrUpdateLang: (value: ILang) => (dispatch: AppDispatch<LangObjectEditRes>, getState: () => ITranslateReducer) => Promise<{
    type: string;
    payload: {
        item: ILang;
    };
}>;
export declare const importTranslate: (value: object) => (dispatch: AppDispatch<any>, getState: () => ITranslateReducer) => void;
export declare const exportTranslate: () => (dispatch: AppDispatch<ExportTranslateRes>, getState: () => ITranslateReducer) => void;
export declare const searchTranslate: (value: string, currentPage: number, pageSize: number) => (dispatch: AppDispatch<TranslateListAdminObjectRes>, getState: () => ITranslateReducer) => void;
export declare const removeTranslate: (value: number) => (dispatch: AppDispatch<any>, getState: () => ITranslateReducer) => Promise<{
    payload: number;
    type: string;
}>;
export declare const saveLanguageUser: (code: string) => (dispatch: AppDispatch<any>) => Promise<any>;
export declare const loadTransInputValue: (key: string) => (dispatch: AppDispatch<object>) => Promise<import("../types/transObject").TransObject<string>>;
export declare const exportTranslateAll: () => (dispatch: AppDispatch<object>, getState: () => ITranslateReducer) => void;
export declare const loadTranslates: (lang: string | undefined, cache: number) => (dispatch: AppDispatch<any>) => Promise<{
    type: string;
    payload: {
        translates: ITranslate;
    };
}>;
