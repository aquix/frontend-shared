"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailingReducer = void 0;
var redux_1 = require("redux");
var audience_1 = __importDefault(require("./audience"));
var mailing_1 = __importDefault(require("./mailing"));
var template_1 = __importDefault(require("./template"));
exports.MailingReducer = (0, redux_1.combineReducers)({
    audience: audience_1.default,
    mail: mailing_1.default,
    template: template_1.default,
});
//# sourceMappingURL=MailingReducer.js.map