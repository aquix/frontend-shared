"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
var audience_1 = require("../action/audience");
var defaultState = {
    list: [],
    item: {},
};
var AudienceReducer = function (state, action) {
    if (state === void 0) { state = defaultState; }
    if (action.type === audience_1.LOAD_AUDIENCE) {
        return __assign(__assign({}, state), { list: action.payload });
    }
    if (action.type === audience_1.REMOVE_AUDIENCE) {
        var item_1 = action.payload;
        var index = state.list.findIndex(function (i) { return i.id === item_1; });
        return __assign(__assign({}, state), { list: __spreadArray(__spreadArray([], state.list.slice(0, index), true), state.list.slice(index + 1), true) });
    }
    if (action.type === audience_1.CREATE_AUDIENCE) {
        var item = action.payload;
        return __assign(__assign({}, state), { list: __spreadArray([
                item
            ], state.list, true) });
    }
    if (action.type === audience_1.LOAD_AUDIENCE_ITEM) {
        return __assign(__assign({}, state), { item: action.payload });
    }
    return state;
};
exports.default = AudienceReducer;
//# sourceMappingURL=audience.js.map