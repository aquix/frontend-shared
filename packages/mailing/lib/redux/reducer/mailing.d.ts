import { IMailReducer } from "../../types/mailing";
import { MailingAction } from "../action/mailing";
declare const MailingReducerMethod: (state: IMailReducer | undefined, action: MailingAction) => IMailReducer;
export default MailingReducerMethod;
