"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
var mailing_1 = require("../action/mailing");
var defaultState = {
    list: [],
    item: {},
};
var MailingReducerMethod = function (state, action) {
    if (state === void 0) { state = defaultState; }
    if (action.type === mailing_1.LOAD_MAILING) {
        return __assign(__assign({}, state), { list: action.payload });
    }
    if (action.type === mailing_1.CREATE_MAILING) {
        var item = action.payload;
        return __assign(__assign({}, state), { list: __spreadArray([
                item
            ], state.list, true) });
    }
    if (action.type === mailing_1.SEND_MAILING) {
        var item_1 = action.payload;
        var index = state.list.findIndex(function (i) { return i.id === item_1.id; });
        if (!!~index) {
            return __assign(__assign({}, state), { list: __spreadArray(__spreadArray(__spreadArray([], state.list.slice(0, index), true), [
                    __assign(__assign({}, state.list[index]), item_1)
                ], false), state.list.slice(index + 1), true) });
        }
    }
    if (action.type === mailing_1.REMOVE_MAILING) {
        var item_2 = action.payload;
        var index = state.list.findIndex(function (i) { return i.id === item_2; });
        return __assign(__assign({}, state), { list: __spreadArray(__spreadArray([], state.list.slice(0, index), true), state.list.slice(index + 1), true) });
    }
    if (action.type === mailing_1.LOAD_MAILING_ITEM) {
        return __assign(__assign({}, state), { item: action.payload });
    }
    return state;
};
exports.default = MailingReducerMethod;
//# sourceMappingURL=mailing.js.map