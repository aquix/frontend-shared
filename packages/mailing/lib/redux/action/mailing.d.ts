import { IMailing } from "../../types/mailing";
export declare const LOAD_MAILING = "LOAD_MAILING";
export declare const CREATE_MAILING = "CREATE_MAILING";
export declare const SEND_MAILING = "SEND_MAILING";
export declare const REMOVE_MAILING = "REMOVE_MAILING";
export declare const LOAD_MAILING_ITEM = "LOAD_MAILING_ITEM";
export declare const loadMailingAction: (payload: IMailing[]) => {
    payload: IMailing[];
    type: string;
};
export declare const createMailingAction: (payload: IMailing) => {
    payload: IMailing;
    type: string;
};
export declare const sendMailingAction: (payload: IMailing) => {
    payload: IMailing;
    type: string;
};
export declare const removeMailingAction: (payload: number) => {
    payload: number;
    type: string;
};
export declare const loadMailingItemAction: (payload: IMailing) => {
    payload: IMailing;
    type: string;
};
export declare type LoadMailingAction = ReturnType<typeof loadMailingAction>;
export declare type CreateMailingAction = ReturnType<typeof createMailingAction>;
export declare type SendMailingAction = ReturnType<typeof sendMailingAction>;
export declare type RemoveMailingAction = ReturnType<typeof removeMailingAction>;
export declare type LoadMailingItemAction = ReturnType<typeof loadMailingItemAction>;
export declare type MailingAction = LoadMailingAction | CreateMailingAction | SendMailingAction | RemoveMailingAction | LoadMailingItemAction;
