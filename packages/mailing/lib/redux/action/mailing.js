"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadMailingItemAction = exports.removeMailingAction = exports.sendMailingAction = exports.createMailingAction = exports.loadMailingAction = exports.LOAD_MAILING_ITEM = exports.REMOVE_MAILING = exports.SEND_MAILING = exports.CREATE_MAILING = exports.LOAD_MAILING = void 0;
var common_1 = require("@frontend-shared/common");
exports.LOAD_MAILING = 'LOAD_MAILING';
exports.CREATE_MAILING = 'CREATE_MAILING';
exports.SEND_MAILING = 'SEND_MAILING';
exports.REMOVE_MAILING = 'REMOVE_MAILING';
exports.LOAD_MAILING_ITEM = 'LOAD_MAILING_ITEM';
exports.loadMailingAction = (0, common_1.CommonAction)(exports.LOAD_MAILING);
exports.createMailingAction = (0, common_1.CommonAction)(exports.CREATE_MAILING);
exports.sendMailingAction = (0, common_1.CommonAction)(exports.SEND_MAILING);
exports.removeMailingAction = (0, common_1.CommonAction)(exports.REMOVE_MAILING);
exports.loadMailingItemAction = (0, common_1.CommonAction)(exports.LOAD_MAILING_ITEM);
//# sourceMappingURL=mailing.js.map