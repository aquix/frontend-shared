import { IAudience } from "../../types/audience";
export declare const CREATE_AUDIENCE = "CREATE_AUDIENCE";
export declare const LOAD_AUDIENCE = "LOAD_AUDIENCE";
export declare const REMOVE_AUDIENCE = "REMOVE_AUDIENCE";
export declare const LOAD_AUDIENCE_ITEM = "LOAD_AUDIENCE_ITEM";
export declare const createAudienceAction: (payload: IAudience) => {
    payload: IAudience;
    type: string;
};
export declare const loadAudienceAction: (payload: IAudience[]) => {
    payload: IAudience[];
    type: string;
};
export declare const removeAudienceAction: (payload: number) => {
    payload: number;
    type: string;
};
export declare const loadAudienceItemAction: (payload: IAudience) => {
    payload: IAudience;
    type: string;
};
export declare type CreateAudienceAction = ReturnType<typeof createAudienceAction>;
export declare type LoadAudienceAction = ReturnType<typeof loadAudienceAction>;
export declare type RemoveAudienceAction = ReturnType<typeof removeAudienceAction>;
export declare type LoadAudienceItemAction = ReturnType<typeof loadAudienceItemAction>;
export declare type AudienceAction = CreateAudienceAction | CreateAudienceAction | LoadAudienceAction | RemoveAudienceAction | LoadAudienceItemAction;
