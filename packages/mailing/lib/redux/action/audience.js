"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadAudienceItemAction = exports.removeAudienceAction = exports.loadAudienceAction = exports.createAudienceAction = exports.LOAD_AUDIENCE_ITEM = exports.REMOVE_AUDIENCE = exports.LOAD_AUDIENCE = exports.CREATE_AUDIENCE = void 0;
var common_1 = require("@frontend-shared/common");
exports.CREATE_AUDIENCE = 'CREATE_AUDIENCE';
exports.LOAD_AUDIENCE = 'LOAD_AUDIENCE';
exports.REMOVE_AUDIENCE = 'REMOVE_AUDIENCE';
exports.LOAD_AUDIENCE_ITEM = 'LOAD_AUDIENCE_ITEM';
exports.createAudienceAction = (0, common_1.CommonAction)(exports.CREATE_AUDIENCE);
exports.loadAudienceAction = (0, common_1.CommonAction)(exports.LOAD_AUDIENCE);
exports.removeAudienceAction = (0, common_1.CommonAction)(exports.REMOVE_AUDIENCE);
exports.loadAudienceItemAction = (0, common_1.CommonAction)(exports.LOAD_AUDIENCE_ITEM);
//# sourceMappingURL=audience.js.map