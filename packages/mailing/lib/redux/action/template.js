"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTemplateAction = exports.removeTemplateAction = exports.createOrUpdateTemplateAction = exports.loadTemplatesAction = exports.GET_TEMPLATE = exports.REMOVE_TEMPLATE = exports.CREATE_OR_UPDATE_TEMPLATE = exports.LOAD_TEMPLATES = void 0;
var common_1 = require("@frontend-shared/common");
exports.LOAD_TEMPLATES = 'LOAD_TEMPLATES';
exports.CREATE_OR_UPDATE_TEMPLATE = 'CREATE_OR_UPDATE_TEMPLATE';
exports.REMOVE_TEMPLATE = 'REMOVE_TEMPLATE';
exports.GET_TEMPLATE = 'GET_TEMPLATE';
exports.loadTemplatesAction = (0, common_1.CommonAction)(exports.LOAD_TEMPLATES);
exports.createOrUpdateTemplateAction = (0, common_1.CommonAction)(exports.CREATE_OR_UPDATE_TEMPLATE);
exports.removeTemplateAction = (0, common_1.CommonAction)(exports.REMOVE_TEMPLATE);
exports.getTemplateAction = (0, common_1.CommonAction)(exports.GET_TEMPLATE);
//# sourceMappingURL=template.js.map