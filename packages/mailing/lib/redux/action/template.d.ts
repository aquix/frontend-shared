import { IMailTemplate } from "../../types/template";
export declare const LOAD_TEMPLATES = "LOAD_TEMPLATES";
export declare const CREATE_OR_UPDATE_TEMPLATE = "CREATE_OR_UPDATE_TEMPLATE";
export declare const REMOVE_TEMPLATE = "REMOVE_TEMPLATE";
export declare const GET_TEMPLATE = "GET_TEMPLATE";
export declare const loadTemplatesAction: (payload: IMailTemplate[]) => {
    payload: IMailTemplate[];
    type: string;
};
export declare const createOrUpdateTemplateAction: (payload: IMailTemplate) => {
    payload: IMailTemplate;
    type: string;
};
export declare const removeTemplateAction: (payload: number) => {
    payload: number;
    type: string;
};
export declare const getTemplateAction: (payload: IMailTemplate) => {
    payload: IMailTemplate;
    type: string;
};
export declare type LoadTemplatesAction = ReturnType<typeof loadTemplatesAction>;
export declare type CreateOrUpdateTemplateAction = ReturnType<typeof createOrUpdateTemplateAction>;
export declare type RemoveTemplateAction = ReturnType<typeof removeTemplateAction>;
export declare type GetTemplateAction = ReturnType<typeof getTemplateAction>;
export declare type MailTemplateAction = LoadTemplatesAction | CreateOrUpdateTemplateAction | RemoveTemplateAction | GetTemplateAction;
