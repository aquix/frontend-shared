"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var redux_logger_1 = require("redux-logger");
var redux_thunk_1 = __importDefault(require("redux-thunk"));
var MailingReducer_1 = require("./reducer/MailingReducer");
var loggerMiddleware = (0, redux_logger_1.createLogger)();
var initialState = {};
var middleware;
if (process.env.NODE_ENV === 'production') {
    middleware = (0, redux_1.applyMiddleware)(redux_thunk_1.default);
}
else {
    middleware = (0, redux_1.applyMiddleware)(redux_thunk_1.default, loggerMiddleware);
}
exports.default = (0, redux_1.createStore)(MailingReducer_1.MailingReducer, initialState, middleware);
//# sourceMappingURL=store.js.map