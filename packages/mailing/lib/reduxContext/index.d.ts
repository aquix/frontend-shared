import React from 'react';
import { ReactReduxContextValue } from "react-redux";
export declare const createContext: <T extends object>(initialValue?: T | undefined) => React.Context<ReactReduxContextValue<T, import("redux").AnyAction>>;
