import { FunctionComponent } from "react";
import { MailingTableProps } from "./redux";
declare const MailingTable: FunctionComponent<MailingTableProps>;
export default MailingTable;
