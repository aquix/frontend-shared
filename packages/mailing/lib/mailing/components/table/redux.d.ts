import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IMailingReducer } from "../../types/reducer";
import { WithTranslation } from "@frontend-shared/translate";
import { IHandler } from "../../types/handler";
export declare const mapStateToProps: (state: IMailingReducer) => {
    audiences: import("../../types/audience").IAudience[];
    mailing: import("../../types/mailing").IMailing[];
    templates: import("../../types/template").IMailTemplate[];
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => {
    loadAudience: () => Promise<{
        payload: import("../../types/audience").IAudience[];
        type: string;
    }>;
    loadMailing: () => void;
    loadTemplates: () => Promise<{
        payload: import("../../types/template").IMailTemplate[];
        type: string;
    }>;
};
export declare type MailingTableProps = IHandler & WithTranslation & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
