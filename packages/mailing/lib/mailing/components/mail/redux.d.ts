import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IMailing } from "../../types/mailing";
import { IMailingReducer } from "../../types/reducer";
import { WithTranslation } from "@frontend-shared/translate";
import { IHandler } from "../../types/handler";
export declare const mapStateToProps: (state: IMailingReducer) => {
    audiences: import("../../types/audience").IAudience[];
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => {
    sendMailing: (value: IMailing) => Promise<{
        payload: import("@frontend-shared/common/redux/types").INotifMessage;
        type: string;
    }>;
    pauseMailing: (value: IMailing) => Promise<{
        payload: IMailing;
        type: string;
    }>;
    removeMailing: (id: number) => Promise<{
        payload: import("@frontend-shared/common/redux/types").INotifMessage;
        type: string;
    }>;
};
export interface IMailPage {
    tableValues: IMailing[];
}
export declare type MailingPageProps = IHandler & IMailPage & WithTranslation & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
