import { ColumnsType } from "antd/es/table";
import { IMailing } from "../../types/mailing";
import { IAudience } from "../../types/audience";
import { TranslateFunc } from "@frontend-shared/translate/types/translate";
declare const mailColumns: (translate: TranslateFunc, audiences: IAudience[], onSendMailing: (value: IMailing) => void, onPauseSendMailing: (value: IMailing) => void, onRemoveMailing: (id: number) => void, handler: (slug: string, id: number) => void) => ColumnsType<IMailing>;
export default mailColumns;
