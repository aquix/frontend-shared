import { ColumnsType } from "antd/es/table";
import { IAudience } from "../../types/audience";
import { TranslateFunc } from "@frontend-shared/translate/types/translate";
declare const columns: (translate: TranslateFunc, onRemoveAudience: (id: number) => void, handler: (slug: string, id: number) => void) => ColumnsType<IAudience>;
export default columns;
