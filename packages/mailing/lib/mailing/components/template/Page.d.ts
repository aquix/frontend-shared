import { FunctionComponent } from "react";
import { TemplateTableProps } from "./redux";
declare const TemplatePage: FunctionComponent<TemplateTableProps>;
export default TemplatePage;
