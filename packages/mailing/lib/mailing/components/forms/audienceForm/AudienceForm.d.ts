import { FunctionComponent } from "react";
import { AudienceFormProps } from "./redux";
declare const AudienceForm: FunctionComponent<AudienceFormProps>;
export default AudienceForm;
