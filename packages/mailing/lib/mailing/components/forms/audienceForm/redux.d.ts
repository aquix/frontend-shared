import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IAudience } from "../../../types/audience";
import { IMailingReducer } from "../../../types/reducer";
import { IHandler } from "../../../types/handler";
import { WithTranslation } from "@frontend-shared/translate";
export declare const mapStateToProps: (state: IMailingReducer) => {
    audience: IAudience;
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => {
    createAudience: (value: IAudience) => Promise<{
        payload: import("@frontend-shared/common/redux/types").INotifMessage;
        type: string;
    }>;
    getAudienceItem: (id: number) => Promise<{
        payload: IAudience;
        type: string;
    }>;
};
export declare type AudienceFormProps = WithTranslation & IHandler & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
