import { FunctionComponent } from "react";
import { MailingFormProps } from "./redux";
declare const MailingForm: FunctionComponent<MailingFormProps>;
export default MailingForm;
