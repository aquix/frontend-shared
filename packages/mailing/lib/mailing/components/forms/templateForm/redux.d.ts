import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IMailingReducer } from "../../../types/reducer";
import { IMailTemplate } from "../../../types/template";
import { WithTranslation } from "@frontend-shared/translate";
export declare const mapStateToProps: (state: IMailingReducer) => {
    template: IMailTemplate;
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => {
    addOrCreateTemplate: (values: IMailTemplate) => Promise<{
        payload: import("@frontend-shared/common/redux/types").INotifMessage;
        type: string;
    }>;
    getTemplate: (id: number) => Promise<{
        payload: IMailTemplate;
        type: string;
    }>;
};
export declare type TemplateFormProps = WithTranslation & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
