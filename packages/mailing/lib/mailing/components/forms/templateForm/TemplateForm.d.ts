import { FunctionComponent } from "react";
import { TemplateFormProps } from "./redux";
import { IHandler } from "../../../types/handler";
declare const TemplateForm: FunctionComponent<TemplateFormProps & IHandler>;
export default TemplateForm;
