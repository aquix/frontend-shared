import { FunctionComponent } from "react";
import { AudiencePageProps } from "./redux";
declare const AudiencePage: FunctionComponent<AudiencePageProps>;
export default AudiencePage;
