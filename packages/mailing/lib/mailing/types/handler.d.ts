export interface IHandler {
    handler: (handler: string, id?: number) => void;
    id?: number;
}
