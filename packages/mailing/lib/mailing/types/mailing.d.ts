export interface IMailing {
    id: number;
    audienceId: number;
    title: string;
    subject: string;
    template: string;
    status: boolean | null | undefined;
    deleted: boolean;
    total: number;
    current: number;
    date: Date;
}
export interface IMailReducer {
    list: IMailing[];
    item: IMailing;
}
