import { IAudienceReducer } from "../../types/audience";
import { AudienceAction } from "../action/audience";
declare const AudienceReducer: (state: IAudienceReducer | undefined, action: AudienceAction) => IAudienceReducer;
export default AudienceReducer;
