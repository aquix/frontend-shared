import { IMailTemplateReducer } from "../../types/template";
import { MailTemplateAction } from "../action/template";
declare const MailTemplateReducerMethod: (state: IMailTemplateReducer | undefined, action: MailTemplateAction) => IMailTemplateReducer;
export default MailTemplateReducerMethod;
