import { AnyAction } from 'redux';
import { IMailingReducer } from '../types/reducer';
declare const _default: import("redux").Store<IMailingReducer, AnyAction>;
export default _default;
