import { IMailTemplate } from "../types/template";
import { AppDispatch } from "@frontend-shared/common";
export declare const createOrUpdateTemplate: (value: IMailTemplate) => (dispatch: AppDispatch<any>) => Promise<{
    payload: IMailTemplate;
    type: string;
}>;
export declare const removeTemplate: (id: number) => (dispatch: AppDispatch<any>) => Promise<{
    payload: number;
    type: string;
}>;
export declare const loadTemplates: () => (dispatch: AppDispatch<any>) => Promise<{
    payload: IMailTemplate[];
    type: string;
}>;
export declare const getTemplate: (id: number) => (dispatch: AppDispatch<any>) => Promise<{
    payload: IMailTemplate;
    type: string;
}>;
