import { IMailing } from "../types/mailing";
import { AppDispatch } from "@frontend-shared/common";
export declare const createMailing: (value: IMailing) => (dispatch: AppDispatch<IMailing>) => Promise<void>;
export declare const loadMailing: () => (dispatch: AppDispatch<IMailing[]>) => void;
export declare const sendMailing: (value: IMailing) => (dispatch: AppDispatch<IMailing[]>) => Promise<void>;
export declare const pauseMailing: (value: IMailing) => (dispatch: AppDispatch<IMailing>) => Promise<{
    payload: IMailing;
    type: string;
}>;
export declare const removeMailing: (id: number) => (dispatch: AppDispatch<number>) => Promise<{
    payload: number;
    type: string;
}>;
export declare const loadMailingItem: (id: number) => (dispatch: AppDispatch<IMailing>) => Promise<{
    payload: IMailing;
    type: string;
}>;
