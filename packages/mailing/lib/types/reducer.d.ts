import { IAudienceReducer } from "./audience";
import { IMailReducer } from "./mailing";
import { IMailTemplateReducer } from "./template";
export interface IMailingReducer {
    audience: IAudienceReducer;
    mail: IMailReducer;
    template: IMailTemplateReducer;
}
