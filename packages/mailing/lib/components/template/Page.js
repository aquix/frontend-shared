"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var antd_1 = require("antd");
var templateColumns_1 = __importDefault(require("../columns/templateColumns"));
var TemplatePage = function (props) {
    var translate = props.translate, tableValues = props.tableValues, removeTemplate = props.removeTemplate, handler = props.handler;
    var onRemoveTemplate = (0, react_1.useCallback)(function (id) { return removeTemplate(id); }, []);
    var templateColumnsData = (0, templateColumns_1.default)(translate, onRemoveTemplate, handler);
    return ((0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)(antd_1.Table, { dataSource: tableValues, columns: templateColumnsData, pagination: false, rowKey: function (i) { return i.id; } }, void 0) }, void 0));
};
exports.default = TemplatePage;
//# sourceMappingURL=Page.js.map