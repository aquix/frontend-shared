import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IMailTemplate } from "../../types/template";
import { IMailingReducer } from "../../types/reducer";
import { WithTranslation } from "@frontend-shared/translate";
import { IHandler } from "../../types/handler";
export declare const mapStateToProps: (state: IMailingReducer) => {
    templates: IMailTemplate[];
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => {
    removeTemplate: (id: number) => Promise<{
        payload: import("@frontend-shared/common/redux/types").INotifMessage;
        type: string;
    }>;
};
export interface IMailPage {
    tableValues: IMailTemplate[];
}
export declare type TemplateTableProps = WithTranslation & IHandler & IMailPage & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
