"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapDispatchToProps = exports.mapStateToProps = void 0;
var audience_1 = require("../../../commands/audience");
var common_1 = require("@frontend-shared/common");
var mapStateToProps = function (state) { return ({
    audience: state.audience.item
}); };
exports.mapStateToProps = mapStateToProps;
var mapDispatchToProps = function (dispatch) { return ({
    createAudience: function (value) { return dispatch((0, audience_1.createAudience)(value))
        .then(function () { return dispatch((0, common_1.successMessage)({ message: 'common.success' })); }); },
    getAudienceItem: function (id) { return dispatch((0, audience_1.loadAudienceItem)(id)); }
}); };
exports.mapDispatchToProps = mapDispatchToProps;
//# sourceMappingURL=redux.js.map