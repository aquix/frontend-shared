"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var antd_1 = require("antd");
var AudienceForm = function (props) {
    var createAudience = props.createAudience, translate = props.translate, audience = props.audience, getAudienceItem = props.getAudienceItem, handler = props.handler, id = props.id;
    var _a = (0, react_1.useState)(false), inputDisabled = _a[0], setInputDisabled = _a[1];
    var form = antd_1.Form.useForm()[0];
    (0, react_1.useEffect)(function () {
        form.resetFields();
        if (id) {
            setInputDisabled(true);
            getAudienceItem(parseInt(String(id)));
        }
        else {
            setInputDisabled(false);
        }
    }, [id]);
    (0, react_1.useEffect)(function () {
        form.resetFields();
        if (id) {
            form.setFieldsValue(audience);
        }
        else {
            form.setFieldsValue({});
        }
    }, [audience]);
    var onCreateAudience = (0, react_1.useCallback)(function (values) {
        createAudience(values).then(function () { return handler('table'); });
    }, []);
    return ((0, jsx_runtime_1.jsxs)(jsx_runtime_1.Fragment, { children: [(0, jsx_runtime_1.jsx)("style", __assign({ jsx: true, global: true }, { children: "\n\n        .globalFooter {\n          display: none;\n        }\n\n        .ant-layout-content {\n          background-color: #fff;\n        }\n\n      " }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: "panel overflow-hidden m-0", style: { borderBottom: "1px solid rgba(0,0,0,0.1)" } }, { children: [(0, jsx_runtime_1.jsxs)("span", { children: [(0, jsx_runtime_1.jsx)("a", __assign({ onClick: function (e) {
                                    e.preventDefault();
                                    handler('table');
                                } }, { children: translate('mailing.form.bread-mailing') }), void 0), " / ", translate('mailing.audience-form.bread-create-audience')] }, void 0), (0, jsx_runtime_1.jsx)("h1", __assign({ className: "mb-0 mt-16" }, { children: translate('mailing.audience-form.title') }), void 0)] }), void 0), (0, jsx_runtime_1.jsx)("div", __assign({ className: "overflow-hidden m-0", style: { paddingBottom: 0, position: "relative" } }, { children: (0, jsx_runtime_1.jsxs)(antd_1.Form, __assign({ form: form, onFinish: function (values) { return onCreateAudience({ title: values.title, url: values.url }); }, autoComplete: "off" }, { children: [(0, jsx_runtime_1.jsxs)("div", __assign({ style: { display: "flex", flexWrap: "wrap", padding: 24, marginBottom: 64 } }, { children: [(0, jsx_runtime_1.jsxs)("div", __assign({ className: 'mailing-form-block' }, { children: [(0, jsx_runtime_1.jsxs)("div", __assign({ style: { display: "flex", justifyContent: "space-between" } }, { children: [(0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.audience-form.input.title') }, void 0), (0, jsx_runtime_1.jsx)("span", { children: translate('mailing.audience-form.input.title.size') }, void 0)] }), void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, __assign({ name: "title", rules: [
                                                { required: true, message: translate('validation.required') },
                                            ] }, { children: (0, jsx_runtime_1.jsx)(antd_1.Input, { placeholder: translate('mailing.audience-form.input.title.placeholder'), disabled: inputDisabled }, void 0) }), void 0)] }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: 'mailing-form-block' }, { children: [(0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.audience-form.input.url') }, void 0) }, void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, __assign({ name: "url", rules: [
                                                { required: true, message: translate('validation.required') },
                                            ] }, { children: (0, jsx_runtime_1.jsx)(antd_1.Input, { placeholder: translate('mailing.audience-form.input.url.placeholder'), disabled: inputDisabled }, void 0) }), void 0)] }), void 0)] }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: 'bottom-buttons' }, { children: [(0, jsx_runtime_1.jsx)(antd_1.Form.Item, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "default", htmlType: "button", className: 'bottom-button', onClick: function () { return handler('table'); } }, { children: translate('mailing.audience-form.cancel') }), void 0) }, void 0), !inputDisabled && (0, jsx_runtime_1.jsx)(antd_1.Form.Item, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "primary", htmlType: "submit", className: 'bottom-button' }, { children: translate('mailing.audience-form.create') }), void 0) }, void 0)] }), void 0)] }), void 0) }), void 0)] }, void 0));
};
exports.default = AudienceForm;
//# sourceMappingURL=AudienceForm.js.map