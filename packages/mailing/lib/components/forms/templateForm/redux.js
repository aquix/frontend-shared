"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapDispatchToProps = exports.mapStateToProps = void 0;
var template_1 = require("../../../commands/template");
var common_1 = require("@frontend-shared/common");
var mapStateToProps = function (state) { return ({
    template: state.template.item,
}); };
exports.mapStateToProps = mapStateToProps;
var mapDispatchToProps = function (dispatch) { return ({
    addOrCreateTemplate: function (values) { return dispatch((0, template_1.createOrUpdateTemplate)(values))
        .then(function () { return dispatch((0, common_1.successMessage)({ message: 'common.success' })); }); },
    getTemplate: function (id) { return dispatch((0, template_1.getTemplate)(id)); },
}); };
exports.mapDispatchToProps = mapDispatchToProps;
//# sourceMappingURL=redux.js.map