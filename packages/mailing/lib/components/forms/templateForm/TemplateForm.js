"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var antd_1 = require("antd");
var react_email_editor_1 = __importDefault(require("react-email-editor"));
var TemplateForm = function (props) {
    var addOrCreateTemplate = props.addOrCreateTemplate, template = props.template, getTemplate = props.getTemplate, translate = props.translate, handler = props.handler, id = props.id;
    var form = antd_1.Form.useForm()[0];
    var _a = (0, react_1.useState)(false), rerenderIframe = _a[0], setRerenderIframe = _a[1];
    var emailEditorRef = (0, react_1.useRef)({});
    (0, react_1.useEffect)(function () {
        if (id) {
            getTemplate(parseInt(String(id)));
        }
    }, [id]);
    (0, react_1.useEffect)(function () {
        form.resetFields();
        if (id) {
            form.setFieldsValue(template);
        }
        else {
            form.setFieldsValue({});
        }
    }, [template]);
    var onSubmitTemplate = (0, react_1.useCallback)(function (value) {
        addOrCreateTemplate(value).then(function () { return handler('table'); });
    }, []);
    (0, react_1.useEffect)(function () {
        template && template.config && emailEditorRef.current.editor && emailEditorRef.current.loadDesign(template.config);
    }, [rerenderIframe, template, emailEditorRef]);
    var onFinish = function (values) {
        (emailEditorRef.current && emailEditorRef.current.editor).exportHtml(function (data) {
            var design = data.design, html = data.html;
            values.content = html;
            values.config = design;
            onSubmitTemplate(__assign(__assign({}, template), values));
        });
    };
    var onLoad = function () {
        setRerenderIframe(!rerenderIframe);
    };
    return ((0, jsx_runtime_1.jsxs)(jsx_runtime_1.Fragment, { children: [(0, jsx_runtime_1.jsx)("style", __assign({ jsx: true, global: true }, { children: "\n\n        .globalFooter {\n          display: none;\n        }\n\n        .ant-layout-content {\n          background-color: #fff;\n        }\n\n      " }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: "panel overflow-hidden m-0", style: { borderBottom: "1px solid rgba(0,0,0,0.1)" } }, { children: [(0, jsx_runtime_1.jsxs)("span", { children: [(0, jsx_runtime_1.jsx)("a", __assign({ onClick: function (e) {
                                    e.preventDefault();
                                    handler('table');
                                } }, { children: translate('mailing.form.bread-mailing') }), void 0), " / ", translate('mailing.mailing-form.bread-create-template')] }, void 0), (0, jsx_runtime_1.jsx)("h1", __assign({ className: "mb-0 mt-16" }, { children: translate('mailing.mailing-form.title') }), void 0)] }), void 0), (0, jsx_runtime_1.jsx)("div", __assign({ className: "overflow-hidden m-0", style: { paddingBottom: 0, position: "relative" } }, { children: (0, jsx_runtime_1.jsx)(antd_1.Form, __assign({ form: form, onFinish: onFinish }, { children: (0, jsx_runtime_1.jsxs)("div", __assign({ style: { padding: 24, marginBottom: 64 } }, { children: [(0, jsx_runtime_1.jsx)(antd_1.Form.Item, __assign({ label: translate('mailing.template.title'), name: "title", rules: [
                                    {
                                        required: true,
                                        message: translate('template.please-input-title'),
                                    },
                                ] }, { children: (0, jsx_runtime_1.jsx)(antd_1.Input, {}, void 0) }), void 0), react_email_editor_1.default && (0, jsx_runtime_1.jsx)(react_email_editor_1.default, { ref: emailEditorRef, onLoad: onLoad }, void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "primary", htmlType: "submit", className: "float-right mt-10" }, { children: translate('common.save') }), void 0) }, void 0)] }), void 0) }), "templateForm") }), void 0)] }, void 0));
};
exports.default = TemplateForm;
//# sourceMappingURL=TemplateForm.js.map