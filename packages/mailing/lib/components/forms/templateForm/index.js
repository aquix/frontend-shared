"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var TemplateForm_1 = __importDefault(require("./TemplateForm"));
var react_redux_1 = require("react-redux");
var redux_1 = require("./redux");
var translate_1 = require("@frontend-shared/translate");
var mailing_1 = require("../../../context/mailing");
var Connector = (0, react_redux_1.connect)(redux_1.mapStateToProps, redux_1.mapDispatchToProps, undefined, { context: mailing_1.MailingContext })(TemplateForm_1.default);
exports.default = (0, translate_1.withLocale)(Connector);
//# sourceMappingURL=index.js.map