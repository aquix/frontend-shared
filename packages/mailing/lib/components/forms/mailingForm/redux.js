"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapDispatchToProps = exports.mapStateToProps = void 0;
var mailing_1 = require("../../../commands/mailing");
var audience_1 = require("../../../commands/audience");
var template_1 = require("../../../commands/template");
var common_1 = require("@frontend-shared/common");
var mapStateToProps = function (state) { return ({
    audiences: state.audience.list,
    mail: state.mail.item,
    templates: state.template.list,
}); };
exports.mapStateToProps = mapStateToProps;
var mapDispatchToProps = function (dispatch) { return ({
    createMailing: function (value) { return dispatch((0, mailing_1.createMailing)(value))
        .then(function () { return dispatch((0, common_1.successMessage)({ message: 'common.success' })); }); },
    loadAudience: function () { return dispatch((0, audience_1.loadAudience)()); },
    getMailingItem: function (id) { return dispatch((0, mailing_1.loadMailingItem)(id)); },
    loadTemplates: function () { return dispatch((0, template_1.loadTemplates)()); },
}); };
exports.mapDispatchToProps = mapDispatchToProps;
//# sourceMappingURL=redux.js.map