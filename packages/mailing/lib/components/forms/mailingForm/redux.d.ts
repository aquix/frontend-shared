import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IMailing } from "../../../types/mailing";
import { IMailingReducer } from "../../../types/reducer";
import { WithTranslation } from "@frontend-shared/translate";
import { IHandler } from "../../../types/handler";
export declare const mapStateToProps: (state: IMailingReducer) => {
    audiences: import("../../../types/audience").IAudience[];
    mail: IMailing;
    templates: import("../../../types/template").IMailTemplate[];
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => {
    createMailing: (value: IMailing) => Promise<{
        payload: import("@frontend-shared/common/redux/types").INotifMessage;
        type: string;
    }>;
    loadAudience: () => Promise<{
        payload: import("../../../types/audience").IAudience[];
        type: string;
    }>;
    getMailingItem: (id: number) => Promise<{
        payload: IMailing;
        type: string;
    }>;
    loadTemplates: () => Promise<{
        payload: import("../../../types/template").IMailTemplate[];
        type: string;
    }>;
};
export declare type MailingFormProps = WithTranslation & IHandler & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
