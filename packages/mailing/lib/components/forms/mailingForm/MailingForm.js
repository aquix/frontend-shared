"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var antd_1 = require("antd");
var moment_1 = __importDefault(require("moment"));
var react_email_editor_1 = __importDefault(require("react-email-editor"));
var style_form_page_module_css_1 = __importDefault(require("../../../style/style-form-page.module.css"));
var MailingForm = function (props) {
    var audiences = props.audiences, createMailing = props.createMailing, translate = props.translate, getMailingItem = props.getMailingItem, loadAudience = props.loadAudience, loadTemplates = props.loadTemplates, templates = props.templates, mail = props.mail, handler = props.handler, id = props.id;
    var emailEditorRef = (0, react_1.useRef)({});
    var Option = antd_1.Select.Option;
    var form = antd_1.Form.useForm()[0];
    var TextArea = antd_1.Input.TextArea;
    var _a = (0, react_1.useState)(null), status = _a[0], setStatus = _a[1];
    var _b = (0, react_1.useState)(false), inputDisabled = _b[0], setInputDisabled = _b[1];
    (0, react_1.useEffect)(function () {
        if (id) {
            setInputDisabled(true);
            getMailingItem(parseInt(String(id)));
        }
        else {
            setInputDisabled(false);
        }
    }, [id]);
    (0, react_1.useEffect)(function () {
        loadAudience();
        loadTemplates();
        form.resetFields();
        if (id) {
            var date = mail.date, other = __rest(mail, ["date"]);
            form.setFieldsValue({ date: (0, moment_1.default)(mail.date) });
            form.setFieldsValue(__assign({}, other));
        }
        else {
            form.setFieldsValue({});
        }
        var previewItem = document.getElementById('codePreview');
        if (previewItem) {
            document.getElementById('codePreview').innerHTML = id ? mail && mail.template && mail.template || '' : '';
        }
    }, [mail]);
    var onCreateMailing = (0, react_1.useCallback)(function (value) {
        (emailEditorRef.current && emailEditorRef.current.editor).exportHtml(function (data) {
            var html = data.html;
            value.template = html;
            createMailing(__assign(__assign({}, value), { status: status })).then(function () { return handler('table'); });
        });
    }, [status]);
    return ((0, jsx_runtime_1.jsxs)(jsx_runtime_1.Fragment, { children: [(0, jsx_runtime_1.jsx)("style", __assign({ jsx: true, global: true }, { children: "\n\n        .globalFooter {\n          display: none;\n        }\n\n        .ant-layout-content {\n          background-color: #fff;\n        }\n\n      " }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: "panel overflow-hidden m-0", style: { borderBottom: "1px solid rgba(0,0,0,0.1)" } }, { children: [(0, jsx_runtime_1.jsxs)("span", { children: [(0, jsx_runtime_1.jsx)("a", __assign({ onClick: function (e) {
                                    e.preventDefault();
                                    handler('table');
                                } }, { children: translate('mailing.form.bread-mailing') }), void 0), " / ", translate('mailing.mailing-form.bread-create-mailing')] }, void 0), (0, jsx_runtime_1.jsx)("h1", __assign({ className: "mb-0 mt-16" }, { children: translate('mailing.mailing-form.title') }), void 0)] }), void 0), (0, jsx_runtime_1.jsx)("div", __assign({ className: "overflow-hidden m-0", style: { paddingBottom: 0, position: "relative" } }, { children: (0, jsx_runtime_1.jsxs)(antd_1.Form, __assign({ form: form, onFinish: onCreateMailing, autoComplete: "off" }, { children: [(0, jsx_runtime_1.jsxs)("div", __assign({ style: { display: "flex", flexWrap: "wrap", padding: 24, marginBottom: 64 } }, { children: [(0, jsx_runtime_1.jsxs)("div", __assign({ className: style_form_page_module_css_1.default['mailingFormBlock'] }, { children: [(0, jsx_runtime_1.jsxs)("div", __assign({ style: { display: "flex", justifyContent: "space-between" } }, { children: [(0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.mailing-form.input.title') }, void 0), (0, jsx_runtime_1.jsx)("span", { children: translate('mailing.mailing-form.input.title.size') }, void 0)] }), void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, __assign({ name: "title", rules: [
                                                { required: true, message: translate('validation.required') },
                                            ] }, { children: (0, jsx_runtime_1.jsx)(antd_1.Input, { placeholder: translate('mailing.mailing-form.input.title.placeholder'), disabled: inputDisabled }, void 0) }), void 0)] }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: style_form_page_module_css_1.default['mailingFormBlock'] }, { children: [(0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.mailing-form.input.audience') }, void 0) }, void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, __assign({ name: "audienceId", rules: [
                                                { required: true, message: translate('validation.required') },
                                            ] }, { children: (0, jsx_runtime_1.jsx)(antd_1.Select, __assign({ placeholder: translate('mailing.mailing-form.input.audience.placeholder'), disabled: inputDisabled }, { children: audiences.length && audiences.map(function (i, index) {
                                                    return (0, jsx_runtime_1.jsx)(Option, __assign({ value: i.id }, { children: i.title }), i + "_" + index);
                                                }) }), void 0) }), void 0)] }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: style_form_page_module_css_1.default['mailingFormBlock'] }, { children: [(0, jsx_runtime_1.jsx)("div", __assign({ style: { display: "flex", justifyContent: "space-between" } }, { children: (0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.mailing-form.input.template') }, void 0) }), void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, __assign({ name: "subject", rules: [
                                                { required: true, message: translate('validation.required') },
                                            ] }, { children: (0, jsx_runtime_1.jsx)(antd_1.Input, { placeholder: translate('mailing.mailing-form.input.subject.placeholder'), disabled: inputDisabled }, void 0) }), void 0)] }), void 0), !inputDisabled && (0, jsx_runtime_1.jsxs)("div", __assign({ className: style_form_page_module_css_1.default['mailingFormBlock'] }, { children: [(0, jsx_runtime_1.jsxs)("div", __assign({ style: { display: "flex", justifyContent: "space-between" } }, { children: [(0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.mailing-form.input.extra-subject') }, void 0), (0, jsx_runtime_1.jsx)("span", { children: translate('mailing.mailing-form.input.extra-subject.size') }, void 0)] }), void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, { children: (0, jsx_runtime_1.jsx)(antd_1.Select, __assign({ onChange: function (value) {
                                                    var item = templates.find(function (i) { return i.id === value; });
                                                    item && emailEditorRef.current.editor && emailEditorRef.current.loadDesign(item.config);
                                                }, placeholder: translate('mailing.mailing-form.input.template.placeholder'), disabled: inputDisabled }, { children: templates.length && templates.map(function (i, index) {
                                                    return (0, jsx_runtime_1.jsx)(Option, __assign({ value: i.id }, { children: i.title }), i + "_" + index);
                                                }) }), void 0) }, void 0)] }), void 0), inputDisabled ?
                                    (0, jsx_runtime_1.jsxs)("div", __assign({ className: style_form_page_module_css_1.default['mailingTemplateBlock'] }, { children: [(0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.mailing-form.input.preview') }, void 0) }, void 0), (0, jsx_runtime_1.jsx)("div", { style: { border: '1px solid #d9d9d9', borderRadius: 4, padding: '4px 11px', minHeight: 300 }, id: "codePreview" }, void 0)] }), void 0) :
                                    (0, jsx_runtime_1.jsxs)("div", __assign({ className: style_form_page_module_css_1.default['mailingTemplateBlock'] }, { children: [(0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.mailing-form.input.template') }, void 0) }, void 0), react_email_editor_1.default && (0, jsx_runtime_1.jsx)(react_email_editor_1.default, { ref: emailEditorRef }, void 0)] }), void 0)] }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: style_form_page_module_css_1.default['bottomButtons'] }, { children: [(0, jsx_runtime_1.jsx)(antd_1.Form.Item, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "default", htmlType: "button", className: 'bottom-button', onClick: function () { return handler('table'); } }, { children: translate('common.close') }), void 0) }, void 0), !inputDisabled &&
                                    (0, jsx_runtime_1.jsxs)(jsx_runtime_1.Fragment, { children: [(0, jsx_runtime_1.jsx)(antd_1.Form.Item, __assign({ name: "date", initialValue: (0, moment_1.default)() }, { children: (0, jsx_runtime_1.jsx)(antd_1.DatePicker, { showTime: true, defaultValue: (0, moment_1.default)(), className: style_form_page_module_css_1.default['bottomButton'] }, void 0) }), void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "default", htmlType: "submit", className: style_form_page_module_css_1.default['bottomButton'], onClick: function () { return setStatus(null); }, disabled: inputDisabled }, { children: translate('mailing.mailing-form.save-draft') }), void 0) }, void 0), (0, jsx_runtime_1.jsx)(antd_1.Form.Item, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "primary", htmlType: "submit", className: style_form_page_module_css_1.default['bottomButton'], onClick: function () { return setStatus(false); }, disabled: inputDisabled }, { children: translate('mailing.mailing-form.send') }), void 0) }, void 0)] }, void 0)] }), void 0)] }), void 0) }), void 0)] }, void 0));
};
exports.default = MailingForm;
//# sourceMappingURL=MailingForm.js.map