"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var table_1 = __importDefault(require("../table"));
var audienceForm_1 = __importDefault(require("../forms/audienceForm"));
var mailingForm_1 = __importDefault(require("../forms/mailingForm"));
var templateForm_1 = __importDefault(require("../forms/templateForm"));
var store_1 = __importDefault(require("../../redux/store"));
var mailing_1 = require("../../context/mailing");
var react_redux_1 = require("react-redux");
var MailingPage = function () {
    var _a = (0, react_1.useState)((0, jsx_runtime_1.jsx)(jsx_runtime_1.Fragment, {}, void 0)), currentComponent = _a[0], setCurrentComponent = _a[1];
    var changeHandler = function (handler, id) {
        if (handler === void 0) { handler = 'table'; }
        var mainHandler = {
            'table': function () { return (0, jsx_runtime_1.jsx)(table_1.default, { handler: changeHandler }, void 0); },
            'audience': function (id) { return (0, jsx_runtime_1.jsx)(audienceForm_1.default, { id: id, handler: changeHandler }, void 0); },
            'mailing': function (id) { return (0, jsx_runtime_1.jsx)(mailingForm_1.default, { id: id, handler: changeHandler }, void 0); },
            'template': function (id) { return (0, jsx_runtime_1.jsx)(templateForm_1.default, { id: id, handler: changeHandler }, void 0); }
        };
        setCurrentComponent(mainHandler[handler](id && id));
    };
    (0, react_1.useEffect)(function () {
        changeHandler();
    }, []);
    return ((0, jsx_runtime_1.jsx)(react_redux_1.Provider, __assign({ store: store_1.default, context: mailing_1.MailingContext }, { children: (0, jsx_runtime_1.jsx)("div", __assign({ className: 'mailing-layout' }, { children: currentComponent }), void 0) }), void 0));
};
exports.default = MailingPage;
//# sourceMappingURL=Page.js.map