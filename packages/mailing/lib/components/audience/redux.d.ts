import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IAudience } from "../../types/audience";
import { IMailingReducer } from "../../types/reducer";
import { WithTranslation } from "@frontend-shared/translate";
import { IHandler } from "../../types/handler";
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => {
    removeAudience: (id: number) => Promise<{
        payload: import("@frontend-shared/common/redux/types").INotifMessage;
        type: string;
    }>;
};
export interface IMailPage {
    tableValues: IAudience[];
}
export declare type AudiencePageProps = IMailPage & IHandler & WithTranslation & ReturnType<typeof mapDispatchToProps>;
