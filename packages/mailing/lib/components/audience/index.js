"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Page_1 = __importDefault(require("./Page"));
var react_redux_1 = require("react-redux");
var redux_1 = require("./redux");
var translate_1 = require("@frontend-shared/translate");
var mailing_1 = require("../../context/mailing");
var Connector = (0, react_redux_1.connect)(undefined, redux_1.mapDispatchToProps, undefined, { context: mailing_1.MailingContext })(Page_1.default);
exports.default = (0, translate_1.withLocale)(Connector);
//# sourceMappingURL=index.js.map