"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapDispatchToProps = void 0;
var audience_1 = require("../../commands/audience");
var common_1 = require("@frontend-shared/common");
var mapDispatchToProps = function (dispatch) { return ({
    removeAudience: function (id) { return dispatch((0, audience_1.removeAudience)(id))
        .then(function () { return dispatch((0, common_1.successMessage)({ message: 'common.success' })); }); },
}); };
exports.mapDispatchToProps = mapDispatchToProps;
//# sourceMappingURL=redux.js.map