"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var antd_1 = require("antd");
var audienceColumns_1 = __importDefault(require("../columns/audienceColumns"));
var AudiencePage = function (props) {
    var translate = props.translate, tableValues = props.tableValues, removeAudience = props.removeAudience, handler = props.handler;
    var onRemoveAudience = (0, react_1.useCallback)(function (id) { return removeAudience(id); }, []);
    var audienceColumnsData = (0, audienceColumns_1.default)(translate, onRemoveAudience, handler);
    return ((0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)(antd_1.Table, { dataSource: tableValues, columns: audienceColumnsData, pagination: false, rowKey: function (i) { return i.id; } }, void 0) }, void 0));
};
exports.default = AudiencePage;
//# sourceMappingURL=Page.js.map