"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var antd_1 = require("antd");
var mail_1 = __importDefault(require("../mail"));
var template_1 = __importDefault(require("../template"));
var audience_1 = __importDefault(require("../audience"));
var MailingTable = function (props) {
    var translates = props.translates, audiences = props.audiences, loadAudience = props.loadAudience, loadMailing = props.loadMailing, mailing = props.mailing, templates = props.templates, loadTemplates = props.loadTemplates, handler = props.handler, translate = props.translate;
    var _a = (0, react_1.useState)((0, jsx_runtime_1.jsx)(mail_1.default, { handler: function () {
        }, tableValues: mailing }, void 0)), component = _a[0], setComponent = _a[1];
    var _b = (0, react_1.useState)('all'), currentColumn = _b[0], setCurrentColumns = _b[1];
    var tableSetMailPage = function (status) {
        if (mailing) {
            if (status === undefined) {
                setComponent((0, jsx_runtime_1.jsx)(mail_1.default, { handler: handler, tableValues: mailing }, void 0));
                return;
            }
            setComponent((0, jsx_runtime_1.jsx)(mail_1.default, { handler: handler, tableValues: mailing.filter(function (i) { return i.status === status; }) }, void 0));
            return;
        }
        setComponent((0, jsx_runtime_1.jsx)(antd_1.Empty, { description: translate('common.no-data') }, void 0));
    };
    (0, react_1.useEffect)(function () {
        loadAudience();
        loadMailing();
        loadTemplates();
    }, []);
    (0, react_1.useEffect)(function () {
        onChangeSection(currentColumn);
    }, [mailing, audiences, templates]);
    var renderRightSide = (0, react_1.useMemo)(function () { return ((0, jsx_runtime_1.jsxs)("div", __assign({ style: { display: "flex", flexWrap: "wrap", justifyContent: "space-between", marginBottom: 10 } }, { children: [(0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)("h1", { children: translate('mailing.title') }, void 0) }, void 0), (0, jsx_runtime_1.jsxs)("div", { children: [(0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "primary", className: "mr-10", onClick: function () { return handler('audience'); } }, { children: translate('mailing.create-audience') }), void 0), (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "primary", className: "mr-10", onClick: function () { return handler('mailing'); } }, { children: translate('mailing.create-mailing') }), void 0), (0, jsx_runtime_1.jsx)(antd_1.Button, __assign({ type: "primary", className: "mr-10", onClick: function () { return handler('template'); } }, { children: translate('mailing.create-template') }), void 0)] }, void 0)] }), void 0)); }, [translates]);
    var onChangeSection = function (value) {
        setCurrentColumns(value || 'all');
        switch (value) {
            case 'templates':
                setComponent((0, jsx_runtime_1.jsx)(template_1.default, { handler: handler, tableValues: templates }, void 0));
                break;
            case 'audiences':
                setComponent((0, jsx_runtime_1.jsx)(audience_1.default, { handler: handler, tableValues: audiences }, void 0));
                break;
            case "queue":
                tableSetMailPage(false);
                break;
            case "drafts":
                tableSetMailPage(null);
                break;
            default:
                tableSetMailPage(undefined);
                break;
        }
    };
    return ((0, jsx_runtime_1.jsxs)("div", { children: [(0, jsx_runtime_1.jsx)("style", __assign({ jsx: true, global: true }, { children: "\n\n           .ant-radio-button-wrapper {\n             padding: 8px 16px !important;\n             border-radius: 4px !important;\n             margin-right: 10px !important;\n             display: flex !important;\n             justify-content: center;\n             align-items: center;\n             border: 1px solid #d9d9d9 !important;\n           }\n\n           .ant-radio-group {\n             display: flex !important;\n           }\n\n           .ant-radio-button-wrapper::before {\n             display: none !important;\n           }\n\n           .ant-table-cell::before {\n             display: none !important;\n           }\n\n           .mailing-table-actions > button {\n             margin: 0 15px !important;\n           }\n\n           .mailing-table-actions .json {\n             color: #1890ff;\n           }\n         " }), void 0), (0, jsx_runtime_1.jsxs)("div", __assign({ className: "panel overflow-hidden m-0" }, { children: [renderRightSide, (0, jsx_runtime_1.jsxs)(antd_1.Radio.Group, __assign({ onChange: function (e) { return onChangeSection(e.target.value); }, buttonStyle: "solid", defaultValue: currentColumn, className: 'category-buttons', style: { marginBottom: 24 } }, { children: [(0, jsx_runtime_1.jsx)(antd_1.Radio.Button, __assign({ value: "all" }, { children: translate('mailing.all-mailing') }), "all"), (0, jsx_runtime_1.jsx)(antd_1.Radio.Button, __assign({ value: "drafts" }, { children: translate('mailing.drafts') }), "drafts"), (0, jsx_runtime_1.jsx)(antd_1.Radio.Button, __assign({ value: "queue" }, { children: translate('mailing.queue') }), "queue"), (0, jsx_runtime_1.jsx)(antd_1.Radio.Button, __assign({ value: "audiences" }, { children: translate('mailing.audiences') }), "audiences"), (0, jsx_runtime_1.jsx)(antd_1.Radio.Button, __assign({ value: "templates" }, { children: translate('mailing.templates') }), "templates")] }), void 0), component] }), void 0)] }, void 0));
};
exports.default = MailingTable;
//# sourceMappingURL=Table.js.map