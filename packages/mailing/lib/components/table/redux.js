"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapDispatchToProps = exports.mapStateToProps = void 0;
var mailing_1 = require("../../commands/mailing");
var audience_1 = require("../../commands/audience");
var template_1 = require("../../commands/template");
var mapStateToProps = function (state) { return ({
    audiences: state.audience.list,
    mailing: state.mail.list,
    templates: state.template.list,
}); };
exports.mapStateToProps = mapStateToProps;
var mapDispatchToProps = function (dispatch) { return ({
    loadAudience: function () { return dispatch((0, audience_1.loadAudience)()); },
    loadMailing: function () { return dispatch((0, mailing_1.loadMailing)()); },
    loadTemplates: function () { return dispatch((0, template_1.loadTemplates)()); },
}); };
exports.mapDispatchToProps = mapDispatchToProps;
//# sourceMappingURL=redux.js.map