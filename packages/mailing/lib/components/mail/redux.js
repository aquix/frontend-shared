"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapDispatchToProps = exports.mapStateToProps = void 0;
var mailing_1 = require("../../commands/mailing");
var common_1 = require("@frontend-shared/common");
var mapStateToProps = function (state) { return ({
    audiences: state.audience.list,
}); };
exports.mapStateToProps = mapStateToProps;
var mapDispatchToProps = function (dispatch) { return ({
    sendMailing: function (value) { return dispatch((0, mailing_1.sendMailing)(value))
        .then(function () { return dispatch((0, common_1.successMessage)({ message: 'common.success' })); }); },
    pauseMailing: function (value) { return dispatch((0, mailing_1.pauseMailing)(value)); },
    removeMailing: function (id) { return dispatch((0, mailing_1.removeMailing)(id))
        .then(function () { return dispatch((0, common_1.successMessage)({ message: 'common.success' })); }); },
}); };
exports.mapDispatchToProps = mapDispatchToProps;
//# sourceMappingURL=redux.js.map