"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_1 = require("react");
var antd_1 = require("antd");
var mailColumns_1 = __importDefault(require("../columns/mailColumns"));
var MailPage = function (props) {
    var audiences = props.audiences, sendMailing = props.sendMailing, tableValues = props.tableValues, pauseMailing = props.pauseMailing, removeMailing = props.removeMailing, handler = props.handler, translate = props.translate;
    var onSendMailing = (0, react_1.useCallback)(function (value) { return sendMailing(value); }, []);
    var onPauseSendMailing = (0, react_1.useCallback)(function (value) { return pauseMailing(value); }, []);
    var onRemoveMailing = (0, react_1.useCallback)(function (id) { return removeMailing(id); }, []);
    var mailingColumnsData = (0, mailColumns_1.default)(translate, audiences, onSendMailing, onPauseSendMailing, onRemoveMailing, handler);
    return ((0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)(antd_1.Table, { dataSource: tableValues, columns: mailingColumnsData, pagination: false, rowKey: function (i) { return i.id; } }, void 0) }, void 0));
};
exports.default = MailPage;
//# sourceMappingURL=Page.js.map