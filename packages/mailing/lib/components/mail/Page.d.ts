import { FunctionComponent } from "react";
import { MailingPageProps } from "./redux";
declare const MailPage: FunctionComponent<MailingPageProps>;
export default MailPage;
