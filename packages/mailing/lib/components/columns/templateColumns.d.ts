import { ColumnsType } from "antd/es/table";
import { IMailTemplate } from "../../types/template";
import { TranslateFunc } from "@frontend-shared/translate/types/translate";
declare const columns: (translate: TranslateFunc, onRemoveTemplate: (id: number) => void, handler: (slug: string, id: number) => void) => ColumnsType<IMailTemplate>;
export default columns;
