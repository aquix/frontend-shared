"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var antd_1 = require("antd");
var columns = function (translate, onRemoveTemplate, handler) {
    return [
        {
            title: translate('mailing.column.title'),
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: translate('mailing.column.actions'),
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: function (text, record) { return ((0, jsx_runtime_1.jsxs)("div", __assign({ style: { whiteSpace: 'nowrap', display: "flex", justifyContent: "flex-end" }, className: "mailing-table-actions" }, { children: [(0, jsx_runtime_1.jsx)(antd_1.Button, { shape: "circle", icon: (0, jsx_runtime_1.jsx)("span", { className: "json icon-edit" }, void 0), onClick: function () { return handler('template', record.id); } }, void 0), (0, jsx_runtime_1.jsx)(antd_1.Popconfirm, __assign({ title: translate('common.delete'), onConfirm: function () { return onRemoveTemplate(record.id); }, okText: translate('common.yes'), cancelText: translate('common.no') }, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, { type: "link", className: "ghostNoBorderButton", icon: (0, jsx_runtime_1.jsx)("span", { className: "json icon-delete" }, void 0) }, void 0) }), void 0)] }), void 0)); },
        },
    ];
};
exports.default = columns;
//# sourceMappingURL=templateColumns.js.map