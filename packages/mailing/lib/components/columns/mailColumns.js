"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var antd_1 = require("antd");
var moment_1 = __importDefault(require("moment"));
var mailColumns = function (translate, audiences, onSendMailing, onPauseSendMailing, onRemoveMailing, handler) {
    return [
        {
            title: translate('mailing.column.title'),
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: translate('mailing.column.audience'),
            dataIndex: 'audienceId',
            key: 'audienceId',
            render: function (text, record) {
                var index = audiences && audiences.findIndex(function (i) { return i.id === record.audienceId; });
                if (!!(index + 1))
                    return ((0, jsx_runtime_1.jsx)("span", { children: audiences[index].title }, void 0));
            }
        },
        {
            title: translate('mailing.column.send-date'),
            dataIndex: 'date',
            key: 'date',
            render: function (text, record) { return (0, moment_1.default)(record.date).format('DD.MM.YYYY HH:mm'); },
        },
        {
            title: translate('mailing.column.total-count'),
            dataIndex: 'total',
            key: 'total',
            render: function (text, record) { return (0, jsx_runtime_1.jsxs)("span", { children: [record.current ? record.current
                        : record.current === 0 ? 0 : record.total, "/", record.total] }, void 0); },
        },
        {
            title: translate('mailing.column.status'),
            dataIndex: 'status',
            key: 'status',
            render: function (text, record) {
                if (record.status) {
                    return ((0, jsx_runtime_1.jsx)("span", __assign({ style: { color: 'rgb(114,193,64)' } }, { children: translate('mailing.status.send') }), void 0));
                }
                else {
                    return record.status === null ? ((0, jsx_runtime_1.jsx)("span", __assign({ style: { color: 'rgba(0, 0, 0, 0.45)' } }, { children: translate('mailing.status.draft') }), void 0)) : ((0, jsx_runtime_1.jsx)("span", __assign({ style: { color: 'rgb(235,163,62)' } }, { children: translate('mailing.status.queue') }), void 0));
                }
            }
        },
        {
            title: translate('mailing.column.actions'),
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: function (text, record) { return ((0, jsx_runtime_1.jsxs)("div", __assign({ style: { whiteSpace: 'nowrap', display: "flex", justifyContent: "flex-end" }, className: "mailing-table-actions" }, { children: [(0, jsx_runtime_1.jsx)(antd_1.Button, { shape: "circle", icon: (0, jsx_runtime_1.jsx)("span", { className: "json icon-view" }, void 0), onClick: function () { return handler('mailing', record.id); } }, void 0), record.status === null ?
                        (0, jsx_runtime_1.jsx)(antd_1.Button, { shape: "circle", icon: (0, jsx_runtime_1.jsx)("span", { className: "json icon-send" }, void 0), onClick: function () { return onSendMailing(record); } }, void 0) : record.status === false ?
                        (0, jsx_runtime_1.jsx)(antd_1.Button, { shape: "circle", icon: (0, jsx_runtime_1.jsx)("span", { className: "json icon-pause" }, void 0), onClick: function () { return onPauseSendMailing(record); } }, void 0) : record.status === undefined && (0, jsx_runtime_1.jsx)(jsx_runtime_1.Fragment, {}, void 0), (0, jsx_runtime_1.jsx)(antd_1.Popconfirm, __assign({ title: translate('common.delete'), onConfirm: function () { return onRemoveMailing(record.id); }, okText: translate('common.yes'), cancelText: translate('common.no') }, { children: (0, jsx_runtime_1.jsx)(antd_1.Button, { type: "link", className: "ghostNoBorderButton", icon: (0, jsx_runtime_1.jsx)("span", { className: "json icon-delete" }, void 0) }, void 0) }), void 0)] }), void 0)); },
        },
    ];
};
exports.default = mailColumns;
//# sourceMappingURL=mailColumns.js.map