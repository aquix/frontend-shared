import { IAudience } from "../types/audience";
import { AppDispatch } from "@frontend-shared/common";
export declare const createAudience: (value: IAudience) => (dispatch: AppDispatch<IAudience>) => Promise<{
    payload: IAudience;
    type: string;
}>;
export declare const loadAudience: () => (dispatch: AppDispatch<IAudience>) => Promise<{
    payload: IAudience[];
    type: string;
}>;
export declare const removeAudience: (id: number) => (dispatch: AppDispatch<number>) => Promise<{
    payload: number;
    type: string;
}>;
export declare const loadAudienceItem: (id: number) => (dispatch: AppDispatch<IAudience>) => Promise<{
    payload: IAudience;
    type: string;
}>;
