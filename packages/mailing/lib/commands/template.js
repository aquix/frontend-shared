"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTemplate = exports.loadTemplates = exports.removeTemplate = exports.createOrUpdateTemplate = void 0;
var template_1 = require("../redux/action/template");
var common_1 = require("@frontend-shared/common");
var createOrUpdateTemplate = function (value) { return function (dispatch) {
    var options = { method: (value.id) ? 'put' : 'post' };
    return dispatch((0, common_1.apiCall)('/v1/mailing/template', value, options))
        .then(function (_a) {
        var item = _a.item;
        return dispatch((0, template_1.createOrUpdateTemplateAction)(__assign(__assign({}, value), item)));
    });
}; };
exports.createOrUpdateTemplate = createOrUpdateTemplate;
var removeTemplate = function (id) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)('/v1/mailing/template', { id: id }, { method: "delete" }))
        .then(function () { return dispatch((0, template_1.removeTemplateAction)(id)); });
}; };
exports.removeTemplate = removeTemplate;
var loadTemplates = function () { return function (dispatch) {
    return dispatch((0, common_1.apiCall)('/v1/mailing/template', undefined, { method: "get" }))
        .then(function (_a) {
        var list = _a.list;
        return dispatch((0, template_1.loadTemplatesAction)(list));
    });
}; };
exports.loadTemplates = loadTemplates;
var getTemplate = function (id) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)("/v1/mailing/template/item/" + id, undefined, { method: "get" }))
        .then(function (_a) {
        var item = _a.item;
        return dispatch((0, template_1.getTemplateAction)(item));
    });
}; };
exports.getTemplate = getTemplate;
//# sourceMappingURL=template.js.map