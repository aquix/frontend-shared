"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadMailingItem = exports.removeMailing = exports.pauseMailing = exports.sendMailing = exports.loadMailing = exports.createMailing = void 0;
var mailing_1 = require("../redux/action/mailing");
var common_1 = require("@frontend-shared/common");
var createMailing = function (value) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)('/v1/mailing/project', value, { method: 'post', context: 'common' }))
        .then(function (_a) {
        var createdItem = _a.item;
        dispatch((0, mailing_1.createMailingAction)(__assign(__assign(__assign({}, value), createdItem), { status: value.status === false ? false : null })));
        if (value.status === false) {
            dispatch((0, mailing_1.sendMailingAction)({ id: value.id, status: false }));
            dispatch((0, common_1.apiCall)('/v1/mailing/project/send', { projectId: createdItem.id }, { method: 'post' }))
                .then(function (_a) {
                var item = _a.item;
                return dispatch((0, mailing_1.sendMailingAction)(__assign(__assign(__assign({}, value), item), { id: createdItem.id })));
            });
        }
    });
}; };
exports.createMailing = createMailing;
var loadMailing = function () { return function (dispatch) {
    dispatch((0, common_1.apiCall)('/v1/mailing/project', undefined, { method: 'get' }))
        .then(function (_a) {
        var list = _a.list;
        return dispatch((0, mailing_1.loadMailingAction)(list));
    });
}; };
exports.loadMailing = loadMailing;
var sendMailing = function (value) { return function (dispatch) {
    dispatch((0, mailing_1.sendMailingAction)({ id: value.id, status: undefined }));
    return dispatch((0, common_1.apiCall)('/v1/mailing/project', __assign({}, value), { method: 'post', context: 'common' }))
        .then(function (_a) {
        var item = _a.item;
        dispatch((0, mailing_1.sendMailingAction)({ id: value.id, status: false }));
        dispatch((0, common_1.apiCall)('/v1/mailing/project/send', { projectId: value.id }, { method: 'post' }))
            .then(function (_a) {
            var item = _a.item;
            return dispatch((0, mailing_1.sendMailingAction)(__assign(__assign({}, value), item)));
        });
    });
}; };
exports.sendMailing = sendMailing;
var pauseMailing = function (value) { return function (dispatch) {
    dispatch((0, mailing_1.sendMailingAction)(__assign(__assign({}, value), { status: null })));
    return dispatch((0, common_1.apiCall)('/v1/mailing/project/pause', { projectId: value.id }, { method: 'post' }))
        .then(function (_a) {
        var item = _a.item;
        return dispatch((0, mailing_1.sendMailingAction)(__assign(__assign({}, value), { status: null })));
    });
}; };
exports.pauseMailing = pauseMailing;
var removeMailing = function (id) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)('/v1/mailing/project', { id: id }, { method: 'delete' }))
        .then(function (res) { return dispatch((0, mailing_1.removeMailingAction)(id)); });
}; };
exports.removeMailing = removeMailing;
var loadMailingItem = function (id) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)("/v1/mailing/project/item/" + id, undefined, { method: 'get' }))
        .then(function (_a) {
        var item = _a.item;
        return dispatch((0, mailing_1.loadMailingItemAction)(item));
    });
}; };
exports.loadMailingItem = loadMailingItem;
//# sourceMappingURL=mailing.js.map