"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadAudienceItem = exports.removeAudience = exports.loadAudience = exports.createAudience = void 0;
var audience_1 = require("../redux/action/audience");
var common_1 = require("@frontend-shared/common");
var createAudience = function (value) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)('/v1/mailing/audience', value, { method: 'post', context: 'common' }))
        .then(function (_a) {
        var item = _a.item;
        return dispatch((0, audience_1.createAudienceAction)(__assign(__assign({}, value), item)));
    });
}; };
exports.createAudience = createAudience;
var loadAudience = function () { return function (dispatch) {
    return dispatch((0, common_1.apiCall)('/v1/mailing/audience', undefined, { method: 'get' }))
        .then(function (_a) {
        var list = _a.list;
        return dispatch((0, audience_1.loadAudienceAction)(list));
    });
}; };
exports.loadAudience = loadAudience;
var removeAudience = function (id) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)('/v1/mailing/audience', { id: id }, { method: 'delete' }))
        .then(function (res) { return dispatch((0, audience_1.removeAudienceAction)(id)); });
}; };
exports.removeAudience = removeAudience;
var loadAudienceItem = function (id) { return function (dispatch) {
    return dispatch((0, common_1.apiCall)("/v1/mailing/audience/item/" + id, undefined, { method: 'get' }))
        .then(function (_a) {
        var item = _a.item;
        return dispatch((0, audience_1.loadAudienceItemAction)(item));
    });
}; };
exports.loadAudienceItem = loadAudienceItem;
//# sourceMappingURL=audience.js.map