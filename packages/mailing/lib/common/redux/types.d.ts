import { Subject } from 'rxjs';
import { ThunkDispatch } from 'redux-thunk';
import { Action, AnyAction } from 'redux';
export interface INotifMessage {
    message: string;
    title?: string;
    duration?: number;
}
export interface ILoadingEvent {
    kind: string;
    context: string;
    size: number;
}
export interface CommonReducer {
    contexts: ContextType;
    error: Subject<INotifMessage>;
    success: Subject<INotifMessage>;
    loading: Subject<ILoadingEvent>;
}
export declare type ContextType = {
    [key: string]: number;
};
export declare type AppDispatch<R> = ThunkDispatch<R, {}, Action<Promise<R>> | AnyAction>;
