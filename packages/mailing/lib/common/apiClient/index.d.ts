import { Method } from 'axios';
export interface ApiClientOptions {
    method?: Method;
    headers?: object;
    asData?: boolean;
    silent?: boolean;
    context?: string;
    passError?: boolean;
    isFile?: boolean;
    baseURL?: string;
    transformResponse?: ((data: any) => any)[];
    asImpersonate?: boolean;
}
export declare const simpleApiClient: <P extends object | null, T extends object>(url: string, params?: P | undefined, options?: ApiClientOptions | undefined) => Promise<{
    statusCode: number;
} & T>;
export declare const simpleApiServer: <P extends object | null, T extends object>(url: string, params?: P | undefined, options?: ApiClientOptions | undefined) => Promise<{
    statusCode: number;
} & T>;
