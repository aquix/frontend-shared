declare const config: {
    public: {
        redirectPage: string;
        clear: string;
    };
    private: {
        redirectPage: string;
    };
    admin: {
        redirectPage: string;
    };
    notFoundPage: string;
    constructPage: string;
    blockedRedirectPage: string;
};
export default config;
export declare const getPrivatePath: (role: string) => string;
