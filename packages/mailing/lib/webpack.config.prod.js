'use strict';
var webpack = require('webpack');
var path = require('path');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');
var OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
var deps = require('./package.json').dependencies;
module.exports = {
    entry: path.resolve(__dirname, '/index.tsx'),
    output: {
        path: path.join(__dirname, '/lib'),
        filename: 'index.js',
        library: 'mailing-module',
        libraryTarget: 'umd',
        clean: true,
        publicPath: '/',
        globalObject: 'this',
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    target: 'web',
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: [/node_modules/],
                use: ['babel-loader', 'ts-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/i,
                use: [
                    'css-loader',
                ],
            },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'styles.css',
            chunkFilename: '[id].css'
        }),
    ],
    optimization: {
        minimize: false
    },
    externals: {
        react: {
            commonjs: 'React',
            commonjs2: 'react',
            amd: 'react'
        },
        'react-dom': {
            commonjs: 'ReactDOM',
            commonjs2: 'react-dom',
            amd: 'react-dom'
        },
        "react/jsx-runtime": "react/jsx-runtime",
        "react-redux": "react-redux",
        "redux-logger": "redux-logger",
    },
};
//# sourceMappingURL=webpack.config.prod.js.map