export const entry: any;
export namespace output {
    const path: any;
    const filename: string;
    const library: string;
    const libraryTarget: string;
    const clean: boolean;
    const publicPath: string;
    const globalObject: string;
}
export namespace resolve {
    const extensions: string[];
}
export const target: string;
export const mode: string;
export namespace module {
    const rules: ({
        test: RegExp;
        exclude: RegExp[];
        use: string[];
        loader?: undefined;
    } | {
        test: RegExp;
        exclude: RegExp;
        loader: string;
        use?: undefined;
    } | {
        test: RegExp;
        use: string[];
        exclude?: undefined;
        loader?: undefined;
    })[];
}
export const plugins: any[];
export namespace optimization {
    const minimize: boolean;
}
export const externals: {
    react: {
        commonjs: string;
        commonjs2: string;
        amd: string;
    };
    'react-dom': {
        commonjs: string;
        commonjs2: string;
        amd: string;
    };
    "react/jsx-runtime": string;
    "react-redux": string;
    "redux-logger": string;
};
