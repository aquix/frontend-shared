/// <reference types="react" />
import { IMailingReducer } from "../types/reducer";
export declare const MailingContext: import("react").Context<import("react-redux").ReactReduxContextValue<IMailingReducer, import("redux").AnyAction>>;
