import {createContext} from "@frontend-shared/reduxContext";
import {IMailingReducer} from "../types/reducer";

export const MailingContext = createContext<IMailingReducer>();