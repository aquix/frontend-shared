import React from 'react';
import {Button, Popconfirm} from 'antd';
import {ColumnsType} from "antd/es/table";
import {IMailTemplate} from "../../types/template";
import {TranslateFunc} from "@frontend-shared/translate/types/translate";

const columns = (translate: TranslateFunc, onRemoveTemplate: (id: number) => void, handler: (slug: string, id: number) => void): ColumnsType<IMailTemplate> =>
   [
      {
         title: translate('mailing.column.title'),
         dataIndex: 'title',
         key: 'title',
      },
      {
         title: translate('mailing.column.actions'),
         dataIndex: 'action',
         key: 'action',
         align: 'right',
         render: (text, record) => (
            <div style={{whiteSpace: 'nowrap', display: "flex", justifyContent: "flex-end"}}
                 className="mailing-table-actions">
               <Button
                  shape="circle"
                  icon={<span className="json icon-edit"/>}
                  onClick={() => handler('template', record.id)}
               />
               <Popconfirm
                  title={translate('common.delete')}
                  onConfirm={() => onRemoveTemplate(record.id)}
                  okText={translate('common.yes')}
                  cancelText={translate('common.no')}
               >
                  <Button
                     type="link"
                     className="ghostNoBorderButton"
                     icon={<span className="json icon-delete"/>}
                  />
               </Popconfirm>
            </div>
         ),
      },
   ]


export default columns