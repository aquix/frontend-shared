import React from 'react';
import {Button, Popconfirm} from 'antd';
import {ColumnsType} from "antd/es/table";
import moment from "moment";
import {IMailing} from "../../types/mailing";
import {IAudience} from "../../types/audience";
import {TranslateFunc} from "@frontend-shared/translate/types/translate";

const mailColumns = (translate: TranslateFunc,
                     audiences: IAudience[],
                     onSendMailing: (value: IMailing) => void,
                     onPauseSendMailing: (value: IMailing) => void,
                     onRemoveMailing: (id: number) => void,
                     handler: (slug: string, id: number) => void): ColumnsType<IMailing> =>
   [
      {
         title: translate('mailing.column.title'),
         dataIndex: 'title',
         key: 'title',
      },
      {
         title: translate('mailing.column.audience'),
         dataIndex: 'audienceId',
         key: 'audienceId',
         render: (text, record) => {
            const index = audiences && audiences.findIndex(i => i.id === record.audienceId);
            if (!!(index + 1)) return (<span>{audiences[index].title}</span>)
         }
      },
      {
         title: translate('mailing.column.send-date'),
         dataIndex: 'date',
         key: 'date',
         render: (text, record) => moment(record.date).format('DD.MM.YYYY HH:mm'),
      },
      {
         title: translate('mailing.column.total-count'),
         dataIndex: 'total',
         key: 'total',
         render: (text, record) => <span>{record.current ? record.current
            : record.current === 0 ? 0 : record.total}/{record.total}</span>,
      },
      {
         title: translate('mailing.column.status'),
         dataIndex: 'status',
         key: 'status',
         render: (text, record) => {
            if (record.status) {
               return (<span style={{color: 'rgb(114,193,64)'}}>{translate('mailing.status.send')}</span>)
            } else {
               return record.status === null ? (
                  <span style={{color: 'rgba(0, 0, 0, 0.45)'}}>{translate('mailing.status.draft')}</span>) : (
                  <span style={{color: 'rgb(235,163,62)'}}>{translate('mailing.status.queue')}</span>);
            }
         }
      },
      {
         title: translate('mailing.column.actions'),
         dataIndex: 'action',
         key: 'action',
         align: 'right',
         render: (text, record) => (
            <div style={{whiteSpace: 'nowrap', display: "flex", justifyContent: "flex-end"}}
                 className="mailing-table-actions">
               <Button
                  shape="circle"
                  icon={<span className="json icon-view"/>}
                  onClick={() => handler('mailing', record.id)}
               />
               {record.status === null ?
                  <Button
                     shape="circle"
                     icon={<span className={`json icon-send`}/>}
                     onClick={() => onSendMailing(record)}
                  /> : record.status === false ?
                     <Button
                        shape="circle"
                        icon={<span className={`json icon-pause`}/>}
                        onClick={() => onPauseSendMailing(record)}
                     /> : record.status === undefined && <></>
               }
               <Popconfirm
                  title={translate('common.delete')}
                  onConfirm={() => onRemoveMailing(record.id)}
                  okText={translate('common.yes')}
                  cancelText={translate('common.no')}
               >
                  <Button
                     type="link"
                     className="ghostNoBorderButton"
                     icon={<span className="json icon-delete"/>}
                  />
               </Popconfirm>
            </div>
         ),
      },
   ]

export default mailColumns