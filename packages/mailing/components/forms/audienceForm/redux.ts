import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { createAudience, loadAudienceItem } from "../../../commands/audience";
import { IAudience } from "../../../types/audience";
import {IMailingReducer} from "../../../types/reducer";
import {IHandler} from "../../../types/handler";
import {WithTranslation} from "@frontend-shared/translate";
import { successMessage } from "@frontend-shared/common";

export const mapStateToProps = (state: IMailingReducer) => ({
  audience: state.audience.item
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => ({
  createAudience: (value: IAudience) => dispatch(createAudience(value))
    .then(() => dispatch(successMessage({ message: 'common.success' }))),
  getAudienceItem: (id: number) => dispatch(loadAudienceItem(id))
});

export type AudienceFormProps = WithTranslation & IHandler & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
