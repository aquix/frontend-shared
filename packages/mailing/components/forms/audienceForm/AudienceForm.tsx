import React, { FunctionComponent, useCallback, useEffect, useState } from "react";
import { Button, Form, Input } from "antd";
// import maxLengthValidator from "@containers/App/user/settings/forms/validators/maxLengthValidator";
// import AppLayout from "@layouts/AppLayout";
import { IAudience } from "../../../types/audience";
import { AudienceFormProps } from "./redux";

const AudienceForm: FunctionComponent<AudienceFormProps> = (props) => {
  const {
    createAudience,
    translate,
    audience,
    getAudienceItem,
    handler,
    id,
  } = props;

  const [ inputDisabled, setInputDisabled ] = useState(false);

  const [ form ] = Form.useForm();

  useEffect(() => {
    form.resetFields();
    if (id) {
      setInputDisabled(true);
      getAudienceItem(parseInt(String(id)));
    } else {
      setInputDisabled(false);
    }
  }, [ id ]);

  useEffect(() => {
    form.resetFields();
    if (id) {
      form.setFieldsValue(audience);
    } else {
      form.setFieldsValue({} as IAudience)
    }
  }, [ audience ]);

  const onCreateAudience = useCallback((values: IAudience) => {
    createAudience(values).then(() => handler('table'));
  }, []);

  return (
    <>
      <style jsx global>{ `

        .globalFooter {
          display: none;
        }

        .ant-layout-content {
          background-color: #fff;
        }

      ` }</style>
      <div className="panel overflow-hidden m-0" style={ { borderBottom: "1px solid rgba(0,0,0,0.1)" } }>
        <span><a onClick={ (e) => {
          e.preventDefault();
          handler('table')
        } }>{ translate('mailing.form.bread-mailing') }</a> / { translate('mailing.audience-form.bread-create-audience') }</span>
        <h1 className="mb-0 mt-16">{ translate('mailing.audience-form.title') }</h1>
      </div>
      <div className="overflow-hidden m-0" style={ { paddingBottom: 0, position: "relative" } }>
        <Form
          form={ form }
          onFinish={ (values) => onCreateAudience({ title: values.title, url: values.url } as IAudience) }
          autoComplete="off"
        >
          <div style={ { display: "flex", flexWrap: "wrap", padding: 24, marginBottom: 64 } }>
            <div className='mailing-form-block'>
              <div style={ { display: "flex", justifyContent: "space-between" } }>
                <h1>{ translate('mailing.audience-form.input.title') }</h1>
                <span>{ translate('mailing.audience-form.input.title.size') }</span>
              </div>
              <Form.Item
                name="title"
                rules={ [
                  { required: true, message: translate('validation.required') },
                  // { validator: maxLengthValidator(50, translate) }
                ] }
              >
                <Input placeholder={ translate('mailing.audience-form.input.title.placeholder') }
                       disabled={ inputDisabled }/>
              </Form.Item>
            </div>
            <div className='mailing-form-block'>
              <div>
                <h1>{ translate('mailing.audience-form.input.url') }</h1>
              </div>
              <Form.Item
                name="url"
                rules={ [
                  { required: true, message: translate('validation.required') },
                ] }
              >
                <Input placeholder={ translate('mailing.audience-form.input.url.placeholder') }
                       disabled={ inputDisabled }/>
              </Form.Item>
            </div>
          </div>
          <div className='bottom-buttons'>
            <Form.Item>
              <Button
                type="default"
                htmlType="button"
                className='bottom-button'
                onClick={ () => handler('table') }
              >
                { translate('mailing.audience-form.cancel') }
              </Button>
            </Form.Item>
            { !inputDisabled && <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className='bottom-button'
              >
                { translate('mailing.audience-form.create') }
              </Button>
            </Form.Item> }
          </div>
        </Form>
      </div>
    </>

  )
}

export default AudienceForm;