import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { createMailing, loadMailingItem } from "../../../commands/mailing";
import { loadAudience } from "../../../commands/audience";
import { loadTemplates } from "../../../commands/template";
import {IMailing} from "../../../types/mailing";
import {IMailingReducer} from "../../../types/reducer";
import {WithTranslation} from "@frontend-shared/translate";
import { successMessage } from "@frontend-shared/common";
import {IHandler} from "../../../types/handler";

export const mapStateToProps = (state: IMailingReducer) => ({
  audiences: state.audience.list,
  mail: state.mail.item,
  templates: state.template.list,
})

export const mapDispatchToProps = (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => ({
  createMailing: (value: IMailing) => dispatch(createMailing(value))
    .then(() => dispatch(successMessage({ message: 'common.success' }))),
  loadAudience: () => dispatch(loadAudience()),
  getMailingItem: (id: number) => dispatch(loadMailingItem(id)),
  loadTemplates: () => dispatch(loadTemplates()),
});

export type MailingFormProps = WithTranslation & IHandler & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
