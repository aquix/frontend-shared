import React, { FunctionComponent, RefObject, useCallback, useEffect, useRef, useState } from "react";
import { Button, DatePicker, Form, Input, Select } from "antd";
import moment from "moment";
import EmailEditor, { UnlayerOptions } from "react-email-editor";
import { IMailing } from "../../../types/mailing";
import { MailingFormProps } from "./redux";
import styles from "../../../style/style-form-page.module.css"

const MailingForm: FunctionComponent<MailingFormProps> = (props) => {
  const {
    audiences,
    createMailing,
    translate,
    getMailingItem,
    loadAudience,
    loadTemplates,
    templates,
    mail,
    handler,
    id,
  } = props;

  const emailEditorRef = useRef({} as EmailEditor) as RefObject<EmailEditor>;

  const { Option } = Select;
  const [ form ] = Form.useForm();
  const { TextArea } = Input;

  const [ status, setStatus ] = useState(null as boolean | null);
  const [ inputDisabled, setInputDisabled ] = useState(false);

  useEffect(() => {
    if (id) {
      setInputDisabled(true);
      getMailingItem(parseInt(String(id)));
    } else {
      setInputDisabled(false);
    }
  }, [ id ]);

  useEffect(() => {
    loadAudience();
    loadTemplates();
    form.resetFields();
    if (id) {
      const { date, ...other } = mail;
      form.setFieldsValue({ date: moment(mail.date) });
      form.setFieldsValue({ ...other });
    } else {
      form.setFieldsValue({} as IMailing);
    }

    const previewItem = document.getElementById('codePreview');
    if (previewItem) {
      (document.getElementById('codePreview') as HTMLElement).innerHTML = id ? mail && mail.template && mail.template || '' : '';
    }
  }, [ mail ]);

  const onCreateMailing = useCallback((value) => {
    (emailEditorRef.current && (emailEditorRef.current as UnlayerOptions).editor as EmailEditor)!.exportHtml((data) => {
      const { html } = data;
      value.template = html;
      createMailing({ ...value, status }).then(() => handler('table'));
    });
  }, [ status ]);

  return (
    <>
      <style jsx global>{ `

        .globalFooter {
          display: none;
        }

        .ant-layout-content {
          background-color: #fff;
        }

      ` }</style>
      <div className="panel overflow-hidden m-0" style={ { borderBottom: "1px solid rgba(0,0,0,0.1)" } }>
        <span><a onClick={ e => {
          e.preventDefault();
          handler('table')
        } }>{ translate('mailing.form.bread-mailing') }</a> / { translate('mailing.mailing-form.bread-create-mailing') }</span>
        <h1 className="mb-0 mt-16">{ translate('mailing.mailing-form.title') }</h1>
      </div>
      <div className="overflow-hidden m-0" style={ { paddingBottom: 0, position: "relative" } }>
        <Form
          form={ form }
          onFinish={ onCreateMailing }
          autoComplete="off"
        >
          <div style={ { display: "flex", flexWrap: "wrap", padding: 24, marginBottom: 64 } }>
            <div className={styles['mailingFormBlock']}>
              <div style={ { display: "flex", justifyContent: "space-between" } }>
                <h1>{ translate('mailing.mailing-form.input.title') }</h1>
                <span>{ translate('mailing.mailing-form.input.title.size') }</span>
              </div>
              <Form.Item
                name="title"
                rules={ [
                  { required: true, message: translate('validation.required') },
                  // {validator: maxLengthValidator(50, translate)}
                ] }
              >
                <Input placeholder={ translate('mailing.mailing-form.input.title.placeholder') }
                       disabled={ inputDisabled }/>
              </Form.Item>
            </div>
            <div className={styles['mailingFormBlock']}>
              <div>
                <h1>{ translate('mailing.mailing-form.input.audience') }</h1>
              </div>
              <Form.Item
                name="audienceId"
                rules={ [
                  { required: true, message: translate('validation.required') },
                ] }
              >
                <Select placeholder={ translate('mailing.mailing-form.input.audience.placeholder') }
                        disabled={ inputDisabled }>
                  { audiences.length && audiences.map((i, index) => {
                    return <Option key={ `${ i }_${ index }` } value={ i.id }>{ i.title }</Option>
                  }) }
                </Select>
              </Form.Item>
            </div>
            <div className={styles['mailingFormBlock']}>
              <div style={ { display: "flex", justifyContent: "space-between" } }>
                <h1>{ translate('mailing.mailing-form.input.template') }</h1>
              </div>
              <Form.Item
                name="subject"
                rules={ [
                  { required: true, message: translate('validation.required') },
                  // {validator: maxLengthValidator(150, translate)}
                ] }
              >
                <Input placeholder={ translate('mailing.mailing-form.input.subject.placeholder') }
                       disabled={ inputDisabled }/>
              </Form.Item>
            </div>
            { !inputDisabled && <div className={styles['mailingFormBlock']}>
              <div style={ { display: "flex", justifyContent: "space-between" } }>
                <h1>{ translate('mailing.mailing-form.input.extra-subject') }</h1>
                <span>{ translate('mailing.mailing-form.input.extra-subject.size') }</span>
              </div>
              <Form.Item>
                <Select
                  onChange={ (value) => {
                    const item = templates.find(i => i.id === value as number);
                    item && (emailEditorRef.current as UnlayerOptions).editor && emailEditorRef.current!.loadDesign(item.config)
                  } }
                  placeholder={ translate('mailing.mailing-form.input.template.placeholder') }
                  disabled={ inputDisabled }
                >
                  { templates.length && templates.map((i, index) => {
                    return <Option key={ `${ i }_${ index }` } value={ i.id }>{ i.title }</Option>
                  }) }

                </Select>
              </Form.Item>
            </div> }
            { inputDisabled ?
              <div className={styles['mailingTemplateBlock']}>
                <div>
                  <h1>{ translate('mailing.mailing-form.input.preview') }</h1>
                </div>
                <div
                  style={ { border: '1px solid #d9d9d9', borderRadius: 4, padding: '4px 11px', minHeight: 300 } }
                  id="codePreview"
                />
              </div> :
              <div className={styles['mailingTemplateBlock']}>
                <div>
                  <h1>{ translate('mailing.mailing-form.input.template') }</h1>
                </div>
                { EmailEditor && <EmailEditor ref={ emailEditorRef }/> }
              </div>
            }
          </div>
          <div className={styles['bottomButtons']}>
            <Form.Item>
              <Button
                type="default"
                htmlType="button"
                className='bottom-button'
                onClick={ () => handler('table') }
              >
                { translate('common.close') }
              </Button>
            </Form.Item>
            { !inputDisabled &&
            <>
              <Form.Item name="date" initialValue={ moment() }>
                <DatePicker
                  showTime
                  defaultValue={ moment() }
                  className={styles['bottomButton']}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  type="default"
                  htmlType="submit"
                  className={styles['bottomButton']}
                  onClick={ () => setStatus(null) }
                  disabled={ inputDisabled }
                >
                  { translate('mailing.mailing-form.save-draft') }
                </Button>
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className={styles['bottomButton']}
                  onClick={ () => setStatus(false) }
                  disabled={ inputDisabled }
                >
                  { translate('mailing.mailing-form.send') }
                </Button>
              </Form.Item>
            </>
            }
          </div>
        </Form>
      </div>
    </>
  )
}

export default MailingForm;
