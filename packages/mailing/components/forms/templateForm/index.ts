import Page  from './TemplateForm';
import { connect } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from './redux';
import {withLocale} from "@frontend-shared/translate";
import {MailingContext} from "../../../context/mailing";

const Connector = connect(mapStateToProps, mapDispatchToProps, undefined, {context: MailingContext})(Page);
export default withLocale(Connector);
