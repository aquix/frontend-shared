import React, { FunctionComponent, RefObject, useCallback, useEffect, useRef, useState } from "react";
import { Button, Form, Input } from "antd";
import EmailEditor, { UnlayerOptions } from "react-email-editor";
import { IMailTemplate } from "../../../types/template";
import { TemplateFormProps } from "./redux";
import { IHandler } from "../../../types/handler";
const TemplateForm: FunctionComponent<TemplateFormProps & IHandler> = (props) => {
  const {
    addOrCreateTemplate,
    template,
    getTemplate,
    translate,
    handler,
    id,
  } = props;

  const [ form ] = Form.useForm();
  const [ rerenderIframe, setRerenderIframe ] = useState(false);

  const emailEditorRef = useRef({} as EmailEditor) as RefObject<EmailEditor>;

  useEffect(() => {
    if (id) {
      getTemplate(parseInt(String(id)));
    }
  }, [ id ]);

  useEffect(() => {
    form.resetFields();
    if (id) {
      form.setFieldsValue(template)
    } else {
      form.setFieldsValue({} as IMailTemplate)
    }
  }, [ template ]);

  const onSubmitTemplate = useCallback((value) => {
    addOrCreateTemplate(value).then(() => handler('table'));
  }, []);

  useEffect(() => {
    template && template.config && (emailEditorRef.current as UnlayerOptions).editor && emailEditorRef.current!.loadDesign(template.config);
  }, [ rerenderIframe, template, emailEditorRef ]);

  const onFinish = (values: IMailTemplate) => {
    (emailEditorRef.current && (emailEditorRef.current as UnlayerOptions).editor as EmailEditor)!.exportHtml((data) => {
      const { design, html } = data;
      values.content = html;
      values.config = design;
      onSubmitTemplate({ ...template, ...values });
    });
  };

  const onLoad = () => {
    setRerenderIframe(!rerenderIframe);
  }

  return (
    <>
      <style jsx global>{ `

        .globalFooter {
          display: none;
        }

        .ant-layout-content {
          background-color: #fff;
        }

      ` }</style>
      <div className="panel overflow-hidden m-0" style={ { borderBottom: "1px solid rgba(0,0,0,0.1)" } }>
        <span><a onClick={ e => {
          e.preventDefault();
          handler('table');
        } }>{ translate('mailing.form.bread-mailing') }</a> / { translate('mailing.mailing-form.bread-create-template') }</span>
        <h1 className="mb-0 mt-16">{ translate('mailing.mailing-form.title') }</h1>
      </div>
      <div className="overflow-hidden m-0" style={ { paddingBottom: 0, position: "relative" } }>
        <Form
          form={ form }
          key="templateForm"
          onFinish={ onFinish }
        >
          <div style={ { padding: 24, marginBottom: 64 } }>
            <Form.Item
              label={ translate('mailing.template.title') }
              name="title"
              rules={ [
                {
                  required: true,
                  message: translate('template.please-input-title'),
                },
              ] }
            >
              <Input/>
            </Form.Item>
            { EmailEditor && <EmailEditor ref={ emailEditorRef } onLoad={ onLoad }/> }
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="float-right mt-10"
              >
                { translate('common.save') }
              </Button>
            </Form.Item>
          </div>
        </Form>
      </div>
    </>
  )
}

export default TemplateForm;
