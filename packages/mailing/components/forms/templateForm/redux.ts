import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { createOrUpdateTemplate, getTemplate } from "../../../commands/template";
import {IMailingReducer} from "../../../types/reducer";
import {IMailTemplate} from "../../../types/template";
import {WithTranslation} from "@frontend-shared/translate";
import { successMessage } from "@frontend-shared/common";

export const mapStateToProps = (state: IMailingReducer) => ({
  template: state.template.item,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => ({
  addOrCreateTemplate: (values: IMailTemplate) => dispatch(createOrUpdateTemplate(values))
  .then(() => dispatch(successMessage({ message: 'common.success' }))),
  getTemplate: (id: number) => dispatch(getTemplate(id)),
});

export type TemplateFormProps = WithTranslation & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
