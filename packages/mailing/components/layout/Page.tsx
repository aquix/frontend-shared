import React, {FunctionComponent, useState, useEffect} from 'react';
import MailingTable from "../table";
import AudienceForm from "../forms/audienceForm";
import MailingForm from "../forms/mailingForm";
import TemplateForm from "../forms/templateForm";
import store from "../../redux/store";
import {MailingContext} from "../../context/mailing";
import {Provider} from "react-redux";

type HandlerType = { [key: string]: (id?: number) => JSX.Element }

const MailingPage: FunctionComponent = () => {
   const [currentComponent, setCurrentComponent] = useState(<></>);

   const changeHandler = (handler: string = 'table', id?: number) => {
      const mainHandler: HandlerType = {
         'table': () => <MailingTable handler={changeHandler}/>,
         'audience': (id?: number) => <AudienceForm id={id} handler={changeHandler}/>,
         'mailing': (id?: number) => <MailingForm id={id} handler={changeHandler}/>,
         'template': (id?: number) => <TemplateForm id={id} handler={changeHandler}/>
      }

      setCurrentComponent(mainHandler[handler](id && id));
   }

   useEffect(() => {
      changeHandler();
   }, []);

   return (
      <Provider store={store} context={MailingContext}>
         <div className='mailing-layout'>
            {currentComponent}
         </div>
      </Provider>
   );
};
export default MailingPage;
