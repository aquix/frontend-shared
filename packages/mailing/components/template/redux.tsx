import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IMailTemplate } from "../../types/template";
import { removeTemplate } from "../../commands/template";
import {IMailingReducer} from "../../types/reducer";
import {WithTranslation} from "@frontend-shared/translate";
import { successMessage } from "@frontend-shared/common";
import {IHandler} from "../../types/handler";

export const mapStateToProps = (state: IMailingReducer) => ({
  templates: state.template.list,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => ({
  removeTemplate: (id: number) => dispatch(removeTemplate(id))
  .then(() => dispatch(successMessage({ message: 'common.success' }))),
});

export interface IMailPage {
  tableValues: IMailTemplate[],
}

export type TemplateTableProps = WithTranslation & IHandler & IMailPage & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
