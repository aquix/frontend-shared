import React, {FunctionComponent, useCallback} from "react";
import {Table} from "antd";
import templateColumns from "../columns/templateColumns";
import {TemplateTableProps} from "./redux";

const TemplatePage: FunctionComponent<TemplateTableProps> = (props) => {
   const {
      translate,
      tableValues,
      removeTemplate,
      handler,
   } = props;

   const onRemoveTemplate = useCallback((id) => removeTemplate(id), []);

   const templateColumnsData = templateColumns(translate, onRemoveTemplate, handler);

   return (
      <div>
         <Table
            dataSource={tableValues as []}
            columns={templateColumnsData}
            pagination={false}
            rowKey={(i: { id: number }) => i.id}
         />
      </div>
   )
}

export default TemplatePage;
