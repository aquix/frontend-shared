import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import {loadMailing} from "../../commands/mailing";
import {loadAudience} from "../../commands/audience";
import {loadTemplates} from "../../commands/template";
import {IMailingReducer} from "../../types/reducer";
import {WithTranslation} from "@frontend-shared/translate";
import {IHandler} from "../../types/handler";

export const mapStateToProps = (state: IMailingReducer) => ({
  audiences: state.audience.list,
  mailing: state.mail.list,
  templates: state.template.list,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => ({
  loadAudience: () => dispatch(loadAudience()),
  loadMailing: () => dispatch(loadMailing()),
  loadTemplates: () => dispatch(loadTemplates()),
});

export type MailingTableProps = IHandler & WithTranslation & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
