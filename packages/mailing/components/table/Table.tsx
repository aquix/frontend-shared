import React, {FunctionComponent, useEffect, useMemo, useState} from "react";
import {Button, Empty, Radio} from "antd";
import {MailingTableProps} from "./redux";
import MailPage from "../mail";
import TemplatePage from "../template";
import AudiencePage from "../audience";

const MailingTable: FunctionComponent<MailingTableProps> = (props) => {
   const {
      translates,
      audiences,
      loadAudience,
      loadMailing,
      mailing,
      templates,
      loadTemplates,
      handler,
      translate,
   } = props;

   const [component, setComponent] = useState(<MailPage handler={() => {
   }} tableValues={mailing}/>);
   const [currentColumn, setCurrentColumns] = useState('all');

   const tableSetMailPage = (status?: boolean | null) => {
      if (mailing) {
         if (status === undefined) {
            setComponent(<MailPage handler={handler} tableValues={mailing}/>);
            return;
         }
         setComponent(<MailPage handler={handler} tableValues={mailing.filter(i => i.status === status)}/>);
         return;
      }

      setComponent(<Empty description={translate('common.no-data')}/>);
   }

   useEffect(() => {
      loadAudience();
      loadMailing();
      loadTemplates();
   }, []);

   useEffect(() => {
      onChangeSection(currentColumn);
   }, [mailing, audiences, templates]);

   const renderRightSide = useMemo(() => (
      <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "space-between", marginBottom: 10 }}>
         <div>
           <h1>{translate('mailing.title')}</h1>
         </div>
         <div>
            <Button
               type="primary"
               className="mr-10"
               onClick={() => handler('audience')}
            >
               {translate('mailing.create-audience')}
            </Button>
            <Button
               type="primary"
               className="mr-10"
               onClick={() => handler('mailing')}
            >
               {translate('mailing.create-mailing')}
            </Button>
            <Button
               type="primary"
               className="mr-10"
               onClick={() => handler('template')}
            >
               {translate('mailing.create-template')}
            </Button>
         </div>
      </div>
   ), [translates]);

   const onChangeSection = (value: string) => {
      setCurrentColumns(value || 'all');
      switch (value) {
         case 'templates':
            setComponent(<TemplatePage handler={handler} tableValues={templates}/>)
            break;
         case 'audiences':
            setComponent(<AudiencePage handler={handler} tableValues={audiences}/>)
            break;
         case "queue":
            tableSetMailPage(false);
            break;
         case "drafts":
            tableSetMailPage(null);
            break;
         default:
            tableSetMailPage(undefined);
            break;
      }
   }

   return (
      <div>
         <style jsx global>{`

           .ant-radio-button-wrapper {
             padding: 8px 16px !important;
             border-radius: 4px !important;
             margin-right: 10px !important;
             display: flex !important;
             justify-content: center;
             align-items: center;
             border: 1px solid #d9d9d9 !important;
           }

           .ant-radio-group {
             display: flex !important;
           }

           .ant-radio-button-wrapper::before {
             display: none !important;
           }

           .ant-table-cell::before {
             display: none !important;
           }

           .mailing-table-actions > button {
             margin: 0 15px !important;
           }

           .mailing-table-actions .json {
             color: #1890ff;
           }
         `}</style>
         <div className="panel overflow-hidden m-0">
           {renderRightSide}
            <Radio.Group
               onChange={(e) => onChangeSection(e.target.value)}
               buttonStyle="solid"
               defaultValue={currentColumn}
               className='category-buttons'
               style={{marginBottom: 24}}
            >
               <Radio.Button
                  value={"all"}
                  key="all"
               >
                  {translate('mailing.all-mailing')}
               </Radio.Button>
               <Radio.Button
                  value={"drafts"}
                  key="drafts"
               >
                  {translate('mailing.drafts')}
               </Radio.Button>
               <Radio.Button
                  value={"queue"}
                  key="queue"
               >
                  {translate('mailing.queue')}
               </Radio.Button>
               <Radio.Button
                  value="audiences"
                  key="audiences"
               >
                  {translate('mailing.audiences')}
               </Radio.Button>
               <Radio.Button
                  value="templates"
                  key="templates"
               >
                  {translate('mailing.templates')}
               </Radio.Button>
            </Radio.Group>
            {component}
         </div>
      </div>
   )
}

export default MailingTable;
