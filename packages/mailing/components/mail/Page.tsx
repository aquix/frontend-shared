import React, {FunctionComponent, useCallback} from "react";
import {Table} from "antd";
import mailingColumns from "../columns/mailColumns";
import {MailingPageProps} from "./redux";

const MailPage: FunctionComponent<MailingPageProps> = (props) => {
   const {
      audiences,
      sendMailing,
      tableValues,
      pauseMailing,
      removeMailing,
      handler,
      translate,
   } = props;

   const onSendMailing = useCallback((value) => sendMailing(value), []);
   const onPauseSendMailing = useCallback(value => pauseMailing(value), []);

   const onRemoveMailing = useCallback((id) => removeMailing(id), []);

   const mailingColumnsData = mailingColumns(translate, audiences, onSendMailing, onPauseSendMailing, onRemoveMailing, handler);

   return (
      <div>
         <Table
            dataSource={tableValues as []}
            columns={mailingColumnsData}
            pagination={false}
            rowKey={(i: { id: number }) => i.id}
         />
      </div>
   )
}

export default MailPage;
