import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { pauseMailing, removeMailing, sendMailing } from "../../commands/mailing";
import { IMailing } from "../../types/mailing";
import {IMailingReducer} from "../../types/reducer";
import { successMessage } from "@frontend-shared/common";
import { WithTranslation } from "@frontend-shared/translate";
import {IHandler} from "../../types/handler";

export const mapStateToProps = (state: IMailingReducer) => ({
  audiences: state.audience.list,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => ({
  sendMailing: (value: IMailing) => dispatch(sendMailing(value))
  .then(() => dispatch(successMessage({ message: 'common.success' }))),
  pauseMailing: (value: IMailing) => dispatch(pauseMailing(value)),
  removeMailing: (id: number) => dispatch(removeMailing(id))
  .then(() => dispatch(successMessage({ message: 'common.success' }))),
});

export interface IMailPage {
  tableValues: IMailing[],
}

export type MailingPageProps = IHandler & IMailPage & WithTranslation & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
