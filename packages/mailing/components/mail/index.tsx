import Page from './Page';
import {mapDispatchToProps, mapStateToProps} from './redux';
import {withLocale} from "@frontend-shared/translate";
import {MailingContext} from "../../context/mailing";
import { connect } from 'react-redux';

const Connector = connect(mapStateToProps, mapDispatchToProps, undefined, {context: MailingContext})(Page);
export default withLocale(Connector);
