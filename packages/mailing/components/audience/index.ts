import Page  from './Page';
import { connect } from 'react-redux';
import { mapDispatchToProps } from './redux';
import { withLocale } from "@frontend-shared/translate";
import {MailingContext} from "../../context/mailing";

const Connector = connect(undefined, mapDispatchToProps, undefined, {context: MailingContext})(Page);
export default withLocale(Connector);