import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import {IAudience} from "../../types/audience";
import {removeAudience} from "../../commands/audience";
import {IMailingReducer} from "../../types/reducer";
import {WithTranslation} from "@frontend-shared/translate";
import { successMessage } from "@frontend-shared/common";
import {IHandler} from "../../types/handler";

export const mapDispatchToProps = (dispatch: ThunkDispatch<IMailingReducer, void, AnyAction>) => ({
  removeAudience: (id: number) => dispatch(removeAudience(id))
  .then(() => dispatch(successMessage({ message: 'common.success' }))),
});

export interface IMailPage {
  tableValues: IAudience[],
}

export type AudiencePageProps =  IMailPage & IHandler & WithTranslation & ReturnType<typeof mapDispatchToProps>;