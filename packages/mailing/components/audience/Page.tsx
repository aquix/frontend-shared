import React, {FunctionComponent, useCallback} from "react";
import {Table} from "antd";
import {AudiencePageProps} from "./redux";
import audienceColumns from "../columns/audienceColumns";

const AudiencePage: FunctionComponent<AudiencePageProps> = (props) => {
   const {
      translate,
      tableValues,
      removeAudience,
      handler,
   } = props;

   const onRemoveAudience = useCallback((id) => removeAudience(id), []);

   const audienceColumnsData = audienceColumns(translate, onRemoveAudience, handler);

   return (
      <div>
         <Table
            dataSource={tableValues as []}
            columns={audienceColumnsData}
            pagination={false}
            rowKey={(i: { id: number }) => i.id}
         />
      </div>
   )
}

export default AudiencePage;