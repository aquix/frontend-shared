# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.5](https://gitlab.com/aquix/frontend-shared/compare/v1.1.19...v1.2.5) (2022-05-11)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.14-alpha.2](https://gitlab.com/aquix/frontend-shared/compare/v1.1.14-alpha.1...v1.1.14-alpha.2) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.14-alpha.1](https://gitlab.com/aquix/frontend-shared/compare/v1.1.14-alpha.0...v1.1.14-alpha.1) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.14-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.13-alpha.0...v1.1.14-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.13-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.12-alpha.0...v1.1.13-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.12-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.11-alpha.0...v1.1.12-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.11-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.10-alpha.0...v1.1.11-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.10-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.9-alpha.0...v1.1.10-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.9-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.8-alpha.0...v1.1.9-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.8-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.7-alpha.0...v1.1.8-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.7-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.6-alpha.0...v1.1.7-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common





## [1.1.6-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.5-alpha.0...v1.1.6-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/common
