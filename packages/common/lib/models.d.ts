import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
export declare abstract class CommonApp<T> {
    protected readonly dispatch: ThunkDispatch<T, void, AnyAction>;
    constructor(dispatch: ThunkDispatch<T, void, AnyAction>);
    init: () => Promise<void>;
}
