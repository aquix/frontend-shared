import { User } from '../types/user';
declare const useSession: () => {
    user: User;
    userId: string | undefined;
    update: (user: User) => void;
} | {
    user: undefined;
    userId: undefined;
    update: () => void;
};
export default useSession;
