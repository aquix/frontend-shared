'use strict';
const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const {dependencies: deps, peerDependencies} = require('./package.json');
module.exports = {
   entry: path.resolve(__dirname, '/index.tsx'),
   output: {
      path: path.join(__dirname, '/lib'),
      filename: 'index.js',
      library: 'common-module',
      libraryTarget: 'umd',
      clean: true,
      publicPath: '/',
      globalObject: 'this',
   },
   resolve: {
      extensions: ['.ts', '.js', 'tsx']
   },
   optimization: {
      minimize: false
   },
   target: 'web',
   mode: 'production',
   module: {
      rules: [
         {
            test: /\.ts(x?)$/,
            exclude: [/node_modules/],
            use: ['babel-loader', 'ts-loader']
         },
         {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
         },
      ]
   },
   plugins: [
      /**
       * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
       * the following package: sudo apt-get install libpng16-dev
       */
      new ModuleFederationPlugin({
         name: 'common-module',
         filename: 'remoteEntry.js',
         remotes: {},
         exposes: {},
         shared: {
           react: {
             singleton: true,
             requiredVersion: peerDependencies.react,
           },
         },
      }),
   ],
   externals: {
      react: {
         commonjs: 'React',
         commonjs2: 'react',
         amd: 'react'
      },
   },
};
