const config = {
  'public': {
    'redirectPage': '/auth/signin',
    'clear': '/auth/signin',
  },
  'private': {
    'redirectPage': '/private/dashboard',
  },
  'admin': {
    'redirectPage': '/private/team',
  },
  'notFoundPage': '/404',
  'constructPage': '/under-construction',
  'blockedRedirectPage': '/auth/blocked',
}
export default config;

export const getPrivatePath = (role: string) => role === 'admin'
  ? config.admin.redirectPage
  : config.private.redirectPage;
