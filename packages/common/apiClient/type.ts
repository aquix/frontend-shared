export interface BaseCommandProps {
  success: string;
  statusCode?: number;
  message?: string;
}

export interface ResultListRes<P> extends BaseCommandProps {
  list: P
}

export interface ResultItemRes<P> extends BaseCommandProps {
  item: P
}

export interface CommandWithSession extends BaseCommandProps{
  user?: {
    lang?: string,
  },
  isPublic: boolean
}