import { ApiClientOptions } from './apiClient';
import { AppDispatch, INotifMessage } from './redux/types';
import { GrpcError } from './errors/grpcError';
import { BaseCommandProps, ResultItemRes, ResultListRes, CommandWithSession } from './apiClient/type'

export function apiCall<P, T>(url: string, params?: P, options?: ApiClientOptions): (dispatch: AppDispatch<P>) => Promise<T>

export function CommonAction<T>(type: string): (payload: T) => { payload: T, type: string };

export function successMessage(data: INotifMessage): { payload: INotifMessage, type: string };

export function errorHandler(err: GrpcError): void;

export type {
  AppDispatch,
  ApiClientOptions,
  ResultItemRes,
  ResultListRes,
  CommandWithSession,
  BaseCommandProps,
};


export as namespace SharedCommon;