import React from 'react';
import { IAuthContextWithUpdate } from '../types/auth';

export const initialAuthValues = { user: undefined, userId: undefined };
export const AuthContext = React.createContext<IAuthContextWithUpdate>({ ...initialAuthValues, update: () => {} });
