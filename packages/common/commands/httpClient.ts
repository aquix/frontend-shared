import { Action, AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import Router from 'next/router';
import { parseCookies } from 'nookies';
import {BaseCommandProps} from "../apiClient/type";
import {ApiClientOptions, simpleApiClient} from "../apiClient";
import StatusError from "../apiClient/StatusError";
import appConfig from "../configs/app"
import {catchError, clearData, removeLoadingContext, setLoadingContext} from "../redux/actions";
import {AppDispatch, INotifMessage} from "../redux/types";

const httpClient = <P extends object | null, T extends BaseCommandProps>(url: string, params?: P, options?: ApiClientOptions) =>
  (dispatch: ThunkDispatch<P, {}, Action<Promise<P>> | AnyAction>) => {
    const cookies = parseCookies();
    const { context, asImpersonate } = options || { context: '' };
    const { token, originToken, uuid } = cookies;
    const accessToken = asImpersonate ? { accessToken: `Bearer ${originToken}` } : {};
    const headers = {
      ...(options?.headers || {}),
      'Content-Type': 'application/json',
      'authorization': `Bearer ${token}`,
      uuid,
      ...accessToken,
    };

    const clientOptions = { ...(options || {}), headers };

    if (context) {
      dispatch(setLoadingContext(context));
    }

    return simpleApiClient<P, T>(url, params, clientOptions)
      .then((res: T) => {
        if (options?.isFile) {
          return res;
        }

        const { success } = res;

        if (context) {
          dispatch(removeLoadingContext(context));
        }

        if (!success) {
          const { message } = res;
          if (process.env.NODE_ENV === 'production') {
            throw new StatusError(message ? `${message}` : `No success`, res.statusCode || 200);
          } else {
            throw new StatusError(message ? `${message} url: /api${url}` : `No success, url: /api${url}`, res.statusCode || 200);
          }
        }

        return res;
      })
      .catch((e: StatusError) => {
        if (context) {
          dispatch(removeLoadingContext(context));
        }

        const msg: INotifMessage = {
          message: e.message,
        };
        dispatch(catchError(msg));

        if (e.status === 405) {
          Router.push(appConfig.public.redirectPage);
          dispatch(clearData());
        }

        throw e;
      });
  };

export const apiCall = <P extends object | null, T extends BaseCommandProps>(url: string, params?: P, options?: ApiClientOptions) =>
  (dispatch: AppDispatch<P>) =>
    dispatch(httpClient<P, T>(url, params, options))
      .then((res: T | void) => res as T);

export default httpClient;
