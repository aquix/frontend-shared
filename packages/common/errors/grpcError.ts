import StatusError from '../apiClient/StatusError';

export interface GrpcError {
  code: number;
  message: string;
}

export const errorHandler = (err: GrpcError) => {
  if (err) {
    throw new StatusError(err.message ? `${err.message}` : `No success`, err.code || 200);
  }
}