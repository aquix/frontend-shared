import { v4 as uuid } from 'uuid';
import { getCookie, setCookie } from './cookie';

const existAnalyticId = () => {
  const auuidCookie = getCookie('auuid')
  if (!auuidCookie) {
    const auuid = uuid();
    setCookie('auuid', auuid);
    return auuid;
  } else {
    return auuidCookie;
  }
};

export default existAnalyticId