import React from 'react';
import axios from 'axios';
import existAnalyticId from '../tools/ExistAnalitics';

interface IMetrixComponent {
  userId?: number;
  lang?: string;
}

class MetrixComponent extends React.Component<IMetrixComponent> {
  knUrl = process.env.METRIX_KIBANA_URL;
  lang = this.props.lang;
  userId = this.props.userId


  send = (eventName: string, data = {}) => {
    const payload = {
      ...(data || {}),
      event: eventName,
      user_id: this.userId,
      _lid: existAnalyticId(),
      url: window.location.href,
      lang: this.lang,
    };



    axios.request({
      url: this.knUrl,
      method: 'post',
      headers: { 'content-type': 'application/json' },
      data: payload,
    })
      .catch(console.log)
      .then(() => console.log("OK"));
  };


  componentDidMount() {
    //@ts-ignore
    window.metrix = {
      send: this.send,
    };
  }

  render() {
    return null;
  }

}

export default MetrixComponent;
