import React from 'react';
import { Upload, message, Button } from 'antd';
import { RcFile, UploadChangeParam } from 'antd/es/upload/interface';
import { UploadOutlined } from '@ant-design/icons';

interface IFileUpload {
  onChange?: (key: string) => string,
  value?: string,
  text?: string,
  disabled?: boolean,
  translate: (trans: string) => string
}

class FileUpload extends React.Component<IFileUpload> {
  state = {
    loading: false,
    fileList: [],
  };

  beforeUpload = (file: RcFile) => {
    const isLt5M = file.size / 1024 / 1024 < 10;
    if (!isLt5M) {
      message.error('Image must smaller than 10MB!');
    }
    return isLt5M;
  };

  handleChange = (info: UploadChangeParam) => {
    const { onChange } = this.props;
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }

    if (info.file.status === 'done') {
      const { response } = info.file;

      if (!response) {
        return;
      }

      const { key } = response;
      onChange!(key);
      this.setState({ loading: false });
    }
  };

  render() {
    const { translate, value, disabled, text } = this.props;
    const uploadButton = (
      <div
        style={disabled ? { opacity: '0.5' } : { opacity: '1' }}
      >
        {(this.state.loading)
          ? <span>Loading</span>
          : <span>Upload</span>
        }
        <div
          className="ant-upload-text uploadTextImage"
        >
          {translate(`${text}`)}
        </div>
      </div>
    );

    return (
      <>
        <Upload
          name="file"
          multiple={false}
          onChange={this.handleChange}
          action={`${process.env.API_URL_CLIENT}/v1/file/upload`}
        >
          <Button icon={<UploadOutlined />}>{uploadButton}</Button>
        </Upload>
      </>
    );
  }
}

export default FileUpload;
