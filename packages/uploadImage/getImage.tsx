const getImage = (width: number, height: number, key: string, getImageAction: (width: number, height: number, key: string) => string, defaultImg: string = '/news_stub.jpg') => {
  if (!key) {
    return defaultImg
  }
  if (key.charAt(0) === '/') return key;
  return getImageAction(width, height, key);
}

export default getImage
