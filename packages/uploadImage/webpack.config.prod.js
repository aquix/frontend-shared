'use strict';
const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const { dependencies: deps } = require('./package.json');

module.exports = {
  entry: path.resolve(__dirname, '/index.tsx'),
  output: {
    path: path.join(__dirname, '/lib'),
    filename: 'index.js',
    library: 'upload-image',
    globalObject: 'this',
    libraryTarget: 'umd',
    clean: true
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.css']
  },
  target: 'web',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: [/node_modules/],
        use: ['babel-loader', 'ts-loader']
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ]
  },
  plugins: [
    /**
     * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
     * the following package: sudo apt-get install libpng16-dev
     */


    new MiniCssExtractPlugin({
      linkType: "text/css",
    }),
    new ModuleFederationPlugin({
      name: 'upload-image',
      filename: 'remoteEntry.js',
      remotes: {},
      exposes: {},
      shared: {
        // ...deps,
        react: {
          singleton: true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          singleton: true,
          requiredVersion: deps['react-dom'],
        },
      },
    }),
  ],
  externals: {
    react: {
      commonjs: 'React',
      commonjs2: 'react',
      amd: 'react'
    },
    'react-dom': {
      commonjs: 'ReactDOM',
      commonjs2: 'react-dom',
      amd: 'react-dom'
    }
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: false,
        parallel: false
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
};
