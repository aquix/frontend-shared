import React from 'react';
import UploadImage from './UploadImage';
import UploadImageGroup from './UploadImageGroup';
import FileUpload from './FileUploader';
import './main.css';

export { UploadImage, UploadImageGroup, FileUpload };
