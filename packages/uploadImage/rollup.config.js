import compiler from '@ampproject/rollup-plugin-closure-compiler';
import typescript from '@rollup/plugin-typescript';
import styles from "rollup-plugin-styles";

export default {
  input: 'index.tsx',
  external: ['react', 'react-dom'],
  output: {
    file: './lib/index.js',
    format: 'cjs'
  },
  plugins: [
    typescript({
      tsconfig: "./tsconfig.json"
    }),
    compiler(),
    styles()
  ]
};

