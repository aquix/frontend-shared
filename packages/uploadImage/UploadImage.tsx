import React, { FunctionComponent, useEffect, useState } from 'react';
import { Upload, message, Button } from 'antd';
import { RcFile, UploadChangeParam } from 'antd/es/upload/interface';
import SpinComponent from './SpinComponent';
import getImage from './getImage';
import CSS from 'csstype';
import { FormInstance } from 'antd/lib/form/hooks/useForm';

interface IUploadImage {
  onChange?: (key: string, lang?: string) => void,
  value?: string,
  text?: string,
  disabled?: boolean,
  spinImg?: string
  translate: (trans: string) => string
  maxSizeImg?: number
  btn?: boolean
  btnText?: string
  action?: string
  getImageAction?: (width: number, height: number, value: string) => string
  name?: string
  btnPanel?: boolean
  form?: FormInstance
  inputName?: string
}

const UploadImage: FunctionComponent<IUploadImage> = (props) => {
  const {
    translate,
    value,
    disabled,
    text = 'common.photo',
    onChange,
    spinImg,
    maxSizeImg = 10,
    btn = false,
    btnText = text,
    name = 'file',
    btnPanel,
    form,
    inputName,
  } = props;
  const {
    action = `${process.env.API_URL_CLIENT}/v1/image/image-upload`,
    getImageAction = (width, height, key) => `${process.env.API_URL_CLIENT}/v1/image/${width}/${height}/${encodeURIComponent(key)}`,
  } = props;

  const [loading, setLoading] = useState(false);
  const [styleImg, setStyleImg] = useState<CSS.Properties>({ height: '100%', width: 'auto' });
  const [sizeImg, setSizeImg] = useState({ width: 200, height: 200 });

  useEffect(() => {
    if (value) {
      let src;
      isUrl(value) ? src = value : src = getImage(100, 200, value, getImageAction);
      const image = new Image();
      image.onload = () => {
        image.height > image.width ? setStyleImg({ height: '100%', width: 'auto' }) : setStyleImg({
          width: '100%',
          height: 'auto',
        });
      };
      image.src = src;
    }
  }, []);

  const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLimit = file.size / 1024 / 1024 < maxSizeImg;
    if (!isLimit) {
      message.error('Image must smaller than ' + maxSizeImg + 'MB!');
    }

    if (file && isJpgOrPng && isLimit) {
      let url = window.URL || window.webkitURL;
      let img = new Image();
      let objectUrl = url.createObjectURL(file);
      img.onload = () => {
        let ratioWidth = 1;
        let ratioHeight = 1;
        img.width > img.height ? ratioHeight = img.height / img.width : ratioWidth = img.width / img.height;
        setSizeImg({
          width: img.width > img.height ? 200 : Math.floor(200 * ratioWidth),
          height: img.height > img.width ? 200 : Math.floor(200 * ratioHeight) < 75 ? 75 : Math.floor(200 * ratioHeight),
        });
        img.height > img.width
          ? setStyleImg({ height: '100%', width: 'auto' })
          : setStyleImg({
            width: '100%',
            height: 'auto',
          });
        url.revokeObjectURL(objectUrl);
      };
      img.src = objectUrl;
    }

    return isJpgOrPng && isLimit;
  };

  const handleChange = (info: UploadChangeParam) => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      const { response } = info.file;

      if (!response) {
        return;
      }

      const { key } = response;
      if (!!onChange) onChange(key);
      setLoading(false);
    }
  };

  const isUrl = (s: string) => {
    const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(s);
  };

  const uploadButton = (
    <div style={disabled ? { opacity: '0.5' } : { opacity: '1' }}>
      {loading ? <SpinComponent img={spinImg} /> : <span className="json icon-plus uploadTextIcon" />}
      {text && <div className="ant-upload-text uploadTextImage">
        {translate(`${text}`)}
      </div>}
    </div>
  );

  return (
    <>
      <Upload
        name={name}
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action={action}
        beforeUpload={beforeUpload}
        onChange={handleChange}
        disabled={disabled}
        style={{ display: 'flex', justifyContent: 'center' }}
      >
        {!disabled && btn && <Button
          type="primary"
          className="avatar-uploader-button"
          disabled={disabled}
        >
          {translate(`${btnText}`)}
        </Button>}
        {value && !loading ?
          <img src={isUrl(value) ? value : getImage(sizeImg.width, sizeImg.height, value, getImageAction)} alt="avatar"
               style={styleImg} /> : uploadButton}
        {form && inputName && form.getFieldValue(inputName) && btnPanel && <div className="buttons">
          <span className="jsonUI icon-editor" />
          <span className="jsonUI icon-trashcan"
                onClick={e => {
                  e.stopPropagation();
                  form && inputName && form.resetFields([inputName]);
                }}
          />
        </div>}
      </Upload>
    </>
  );
};

export default UploadImage;
