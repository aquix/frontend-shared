import React, { FunctionComponent, useState } from 'react';
import get from 'lodash/get';
import { Upload, message, Button, Modal, Spin } from 'antd';
import { RcFile, UploadChangeParam, UploadFile } from 'antd/es/upload/interface';
import getImage from './getImage';

interface IUploadImageGroupProps {
  disabled: boolean,
  onChange?: (key: string[]) => string,
  value?: string[],
  translate: (trans: string) => string
  action?: string
  getImageAction?: (width: number, height: number, value: string) => string
  name?: string
}

const UploadImageGroup: FunctionComponent<IUploadImageGroupProps> = ( props ) => {
  const { translate, disabled, value, onChange, name = 'file' } = props;
  const url = process.env.API_URL_CLIENT || '';
  const url_client = url.substring(url.length - 3) == 'api' ? url : url + '/api';

  const {
    action = `${url_client}/v1/image/image-upload`,
    getImageAction = (width, height, key) => `${url_client}/v1/image/${width}/${height}/${encodeURIComponent(key)}`
  } = props;

  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [previewImageLoading, setPreviewImageLoading] = useState(true);


  const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';

    if (value && value.length > 4) {
      message.error('Maximum file value exceeded 5!');
      return false;
    }
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
      return false;
    }
    const isLt25M = file.size / 1024 / 1024 < 25;
    if (!isLt25M) {
      message.error('Image must smaller than 25MB!');
      return false;
    }
    return true;
  };

  const onChangeFiles = (list: any[]) => {
    let result = list
      .map(i => {
        let url = get(i, 'url');
        if (!url) {
          url = i.response.key;
        }

        return url;
      })
      .filter(i => !!i);

    onChange!(result);
  };

  const handleChange = (info: UploadChangeParam) => {
    if (info.file.status === 'uploading') {
      return;
    }
    if (info.file.status === 'removed') {
      onChangeFiles(info.fileList);
    }
    if (info.file.status === 'done') {
      onChangeFiles(info.fileList);
    }
  };

  const defaultFileList = (!!value?.length && value || []).map((i, index) => {
    return ({
      uid: String(index),
      name: `file_${index + 1}`,
      status: 'done',
      response: {
        key: i,
      },
      url: getImage(800, 800, i, getImageAction),
      size: 800,
      type: '',
    });
  });

  const handlePreview = (file: any) => {
    setPreviewVisible(true);
    setPreviewImage(getImage(800, 800, file.response.key, getImageAction));
  };

  const handleCancel = () => {
    setPreviewImageLoading(true);
    setPreviewVisible(false);
  }

  return (
    <>
      <Upload
        defaultFileList={defaultFileList as UploadFile[]}
        className="uploadImageGroup"
        name={name}
        action={action}
        beforeUpload={beforeUpload}
        onPreview={handlePreview}
        onChange={handleChange}
        disabled={disabled}
        listType="text"
      >
        <Button
          disabled={disabled}
          type="primary"
        >
          {translate('common.documents')}
        </Button>
      </Upload>
      {previewVisible && <Modal
        visible={previewVisible}
        title={false}
        footer={null}
        onCancel={handleCancel}
        className="modalImageUploadSetting"
      >
        {previewImageLoading && <div className="spin-wrapper"><Spin size="large" /></div>}
        <img alt="imageDocument" style={{ width: '100%', maxHeight: '70vh', objectFit: 'contain' }}
             src={previewImage} onLoad={() => setPreviewImageLoading(false)} />
      </Modal>}
    </>
  );
};

export default UploadImageGroup;
