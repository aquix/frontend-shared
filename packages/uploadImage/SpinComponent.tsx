import React, { FunctionComponent } from 'react';
import { Spin } from 'antd';

interface SpinComponentProps {
  size?: string
  className?: string
  img?: string
}

const SpinComponent: FunctionComponent<SpinComponentProps> = ({ size, className, img }) => {
  const scale = (size === 'small') ? '16px' : (size === 'large') ? '48px' : '32px';
  return (
    <div className="globalSpin">
      {img ? <img
        style={{ width: scale, height: scale }}
        src={img}
        alt="Spinner"
        className={className}
      /> : <Spin size="small" />}
    </div>
  );
};

export default SpinComponent;
