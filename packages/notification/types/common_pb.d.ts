import * as jspb from 'google-protobuf'



export class IdRequest extends jspb.Message {
  getId(): number;
  setId(value: number): IdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): IdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: IdRequest): IdRequest.AsObject;
  static serializeBinaryToWriter(message: IdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): IdRequest;
  static deserializeBinaryFromReader(message: IdRequest, reader: jspb.BinaryReader): IdRequest;
}

export namespace IdRequest {
  export type AsObject = {
    id: number,
  }
}

export class UserIdRequest extends jspb.Message {
  getUserid(): number;
  setUserid(value: number): UserIdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UserIdRequest): UserIdRequest.AsObject;
  static serializeBinaryToWriter(message: UserIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserIdRequest;
  static deserializeBinaryFromReader(message: UserIdRequest, reader: jspb.BinaryReader): UserIdRequest;
}

export namespace UserIdRequest {
  export type AsObject = {
    userid: number,
  }
}

export class ListOptions extends jspb.Message {
  getOffset(): number;
  setOffset(value: number): ListOptions;

  getLimit(): number;
  setLimit(value: number): ListOptions;

  getOrder(): string;
  setOrder(value: string): ListOptions;

  getOrderCase(): ListOptions.OrderCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOptions.AsObject;
  static toObject(includeInstance: boolean, msg: ListOptions): ListOptions.AsObject;
  static serializeBinaryToWriter(message: ListOptions, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOptions;
  static deserializeBinaryFromReader(message: ListOptions, reader: jspb.BinaryReader): ListOptions;
}

export namespace ListOptions {
  export type AsObject = {
    offset: number,
    limit: number,
    order: string,
  }

  export enum OrderCase { 
    _ORDER_NOT_SET = 0,
    ORDER = 3,
  }
}

export class SMTPModel extends jspb.Message {
  getId(): number;
  setId(value: number): SMTPModel;

  getSlug(): string;
  setSlug(value: string): SMTPModel;

  getUrl(): string;
  setUrl(value: string): SMTPModel;

  getPort(): number;
  setPort(value: number): SMTPModel;

  getUsername(): string;
  setUsername(value: string): SMTPModel;

  getPassword(): string;
  setPassword(value: string): SMTPModel;

  getCreatedat(): number;
  setCreatedat(value: number): SMTPModel;

  getUpdatedat(): number;
  setUpdatedat(value: number): SMTPModel;

  getDeletedat(): number;
  setDeletedat(value: number): SMTPModel;

  getDeletedatCase(): SMTPModel.DeletedatCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SMTPModel.AsObject;
  static toObject(includeInstance: boolean, msg: SMTPModel): SMTPModel.AsObject;
  static serializeBinaryToWriter(message: SMTPModel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SMTPModel;
  static deserializeBinaryFromReader(message: SMTPModel, reader: jspb.BinaryReader): SMTPModel;
}

export namespace SMTPModel {
  export type AsObject = {
    id: number,
    slug: string,
    url: string,
    port: number,
    username: string,
    password: string,
    createdat: number,
    updatedat: number,
    deletedat: number,
  }

  export enum DeletedatCase { 
    _DELETEDAT_NOT_SET = 0,
    DELETEDAT = 9,
  }
}

export class NotificationModel extends jspb.Message {
  getId(): number;
  setId(value: number): NotificationModel;

  getUserid(): number;
  setUserid(value: number): NotificationModel;

  getContent(): string;
  setContent(value: string): NotificationModel;

  getRead(): boolean;
  setRead(value: boolean): NotificationModel;

  getCreatedat(): number;
  setCreatedat(value: number): NotificationModel;

  getUpdatedat(): number;
  setUpdatedat(value: number): NotificationModel;

  getDeletedat(): number;
  setDeletedat(value: number): NotificationModel;

  getDeletedatCase(): NotificationModel.DeletedatCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NotificationModel.AsObject;
  static toObject(includeInstance: boolean, msg: NotificationModel): NotificationModel.AsObject;
  static serializeBinaryToWriter(message: NotificationModel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NotificationModel;
  static deserializeBinaryFromReader(message: NotificationModel, reader: jspb.BinaryReader): NotificationModel;
}

export namespace NotificationModel {
  export type AsObject = {
    id: number,
    userid: number,
    content: string,
    read: boolean,
    createdat: number,
    updatedat: number,
    deletedat: number,
  }

  export enum DeletedatCase { 
    _DELETEDAT_NOT_SET = 0,
    DELETEDAT = 7,
  }
}

export class CreateNotificationRequest extends jspb.Message {
  getUserid(): number;
  setUserid(value: number): CreateNotificationRequest;

  getContent(): string;
  setContent(value: string): CreateNotificationRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateNotificationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateNotificationRequest): CreateNotificationRequest.AsObject;
  static serializeBinaryToWriter(message: CreateNotificationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateNotificationRequest;
  static deserializeBinaryFromReader(message: CreateNotificationRequest, reader: jspb.BinaryReader): CreateNotificationRequest;
}

export namespace CreateNotificationRequest {
  export type AsObject = {
    userid: number,
    content: string,
  }
}

export class ListWithLimitModel extends jspb.Message {
  getRawsList(): Array<NotificationModel>;
  setRawsList(value: Array<NotificationModel>): ListWithLimitModel;
  clearRawsList(): ListWithLimitModel;
  addRaws(value?: NotificationModel, index?: number): NotificationModel;

  getCount(): number;
  setCount(value: number): ListWithLimitModel;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListWithLimitModel.AsObject;
  static toObject(includeInstance: boolean, msg: ListWithLimitModel): ListWithLimitModel.AsObject;
  static serializeBinaryToWriter(message: ListWithLimitModel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListWithLimitModel;
  static deserializeBinaryFromReader(message: ListWithLimitModel, reader: jspb.BinaryReader): ListWithLimitModel;
}

export namespace ListWithLimitModel {
  export type AsObject = {
    rawsList: Array<NotificationModel.AsObject>,
    count: number,
  }
}

export class ListWithLimitRequest extends jspb.Message {
  getUserid(): number;
  setUserid(value: number): ListWithLimitRequest;

  getOpts(): ListOptions | undefined;
  setOpts(value?: ListOptions): ListWithLimitRequest;
  hasOpts(): boolean;
  clearOpts(): ListWithLimitRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListWithLimitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListWithLimitRequest): ListWithLimitRequest.AsObject;
  static serializeBinaryToWriter(message: ListWithLimitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListWithLimitRequest;
  static deserializeBinaryFromReader(message: ListWithLimitRequest, reader: jspb.BinaryReader): ListWithLimitRequest;
}

export namespace ListWithLimitRequest {
  export type AsObject = {
    userid: number,
    opts?: ListOptions.AsObject,
  }
}

export class SendRequest extends jspb.Message {
  getSlug(): string;
  setSlug(value: string): SendRequest;

  getData(): string;
  setData(value: string): SendRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendRequest): SendRequest.AsObject;
  static serializeBinaryToWriter(message: SendRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendRequest;
  static deserializeBinaryFromReader(message: SendRequest, reader: jspb.BinaryReader): SendRequest;
}

export namespace SendRequest {
  export type AsObject = {
    slug: string,
    data: string,
  }
}

export class SMTPCreate extends jspb.Message {
  getSlug(): string;
  setSlug(value: string): SMTPCreate;

  getUrl(): string;
  setUrl(value: string): SMTPCreate;

  getPort(): number;
  setPort(value: number): SMTPCreate;

  getUsername(): string;
  setUsername(value: string): SMTPCreate;

  getPassword(): string;
  setPassword(value: string): SMTPCreate;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SMTPCreate.AsObject;
  static toObject(includeInstance: boolean, msg: SMTPCreate): SMTPCreate.AsObject;
  static serializeBinaryToWriter(message: SMTPCreate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SMTPCreate;
  static deserializeBinaryFromReader(message: SMTPCreate, reader: jspb.BinaryReader): SMTPCreate;
}

export namespace SMTPCreate {
  export type AsObject = {
    slug: string,
    url: string,
    port: number,
    username: string,
    password: string,
  }
}

export class TemplateModel extends jspb.Message {
  getId(): number;
  setId(value: number): TemplateModel;

  getTitle(): string;
  setTitle(value: string): TemplateModel;

  getContent(): string;
  setContent(value: string): TemplateModel;

  getSlug(): string;
  setSlug(value: string): TemplateModel;

  getHandler(): string;
  setHandler(value: string): TemplateModel;

  getCreatedat(): number;
  setCreatedat(value: number): TemplateModel;

  getUpdatedat(): number;
  setUpdatedat(value: number): TemplateModel;

  getDeletedat(): number;
  setDeletedat(value: number): TemplateModel;

  getDeletedatCase(): TemplateModel.DeletedatCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateModel.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateModel): TemplateModel.AsObject;
  static serializeBinaryToWriter(message: TemplateModel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateModel;
  static deserializeBinaryFromReader(message: TemplateModel, reader: jspb.BinaryReader): TemplateModel;
}

export namespace TemplateModel {
  export type AsObject = {
    id: number,
    title: string,
    content: string,
    slug: string,
    handler: string,
    createdat: number,
    updatedat: number,
    deletedat: number,
  }

  export enum DeletedatCase { 
    _DELETEDAT_NOT_SET = 0,
    DELETEDAT = 8,
  }
}

export class TemplateCreate extends jspb.Message {
  getTitle(): string;
  setTitle(value: string): TemplateCreate;

  getContent(): string;
  setContent(value: string): TemplateCreate;

  getSlug(): string;
  setSlug(value: string): TemplateCreate;

  getHandler(): string;
  setHandler(value: string): TemplateCreate;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateCreate.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateCreate): TemplateCreate.AsObject;
  static serializeBinaryToWriter(message: TemplateCreate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateCreate;
  static deserializeBinaryFromReader(message: TemplateCreate, reader: jspb.BinaryReader): TemplateCreate;
}

export namespace TemplateCreate {
  export type AsObject = {
    title: string,
    content: string,
    slug: string,
    handler: string,
  }
}

