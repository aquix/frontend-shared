import { INotificationReducer, ISMTPReducer, ITemplateReducer } from './index';
import { CombineTranslateAdmin } from '@frontend-shared/translate';

export interface INotifyReducer {
  smtp: ISMTPReducer,
  template: ITemplateReducer,
  notification: INotificationReducer,
  // translate: CombineTranslateAdmin,
}