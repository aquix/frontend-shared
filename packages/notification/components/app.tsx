import React, { FunctionComponent } from 'react';
import Layout from './layout';
import { Provider } from 'react-redux';
import store from '../redux/store';
import { NotifyContext } from '../context/notify';

const App: FunctionComponent = () => {
  return (
    <Provider store={store} context={NotifyContext}>
      <div className="panel overflow-hidden m-0">
          <Layout />
      </div>
    </Provider>
  )
}

export default App;