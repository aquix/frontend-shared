import { connect } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from './redux';
import { NotifyContext } from '../../../context/notify';
import TemplateTable from './Page';
import { withLocale } from '@frontend-shared/translate';

const Connector = connect(mapStateToProps, mapDispatchToProps, undefined, { context: NotifyContext })(TemplateTable);
export default withLocale(Connector);
