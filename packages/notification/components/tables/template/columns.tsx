import { ColumnsType } from 'antd/es/table';
import { TemplateModel } from '../../../types/common_pb';
import { Button, Divider, Popconfirm } from 'antd';
import React from 'react';
import { TranslateFunc } from '@frontend-shared/translate';

const columns = (translate: TranslateFunc, showForm: (value: TemplateModel.AsObject) => void, remove: (id: number) => void): ColumnsType<TemplateModel.AsObject> => ([
  {
    title: translate('notify.template.column.title'),
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: translate('notify.template.column.content'),
    dataIndex: 'content',
    key: 'content',
  },
  {
    title: translate('notify.template.column.slug'),
    dataIndex: 'slug',
    key: 'slug',
  },
  {
    title: translate('notify.template.column.handler'),
    dataIndex: 'handler',
    key: 'handler',
  },
  {
    title: translate('notify.template.column.actions'),
    dataIndex: 'action',
    key: 'action',
    align: 'right',
    render: (text, record) => (
      <div style={{whiteSpace: 'nowrap', display: "flex", alignItems: "center", justifyContent: "flex-end"}}>
        <Button
          shape="circle"
          icon={<span className="json icon-edit"/>}
          onClick={() => showForm(record)}
        />
        <Divider type="vertical" />
        <Popconfirm
          title={translate('common.delete')}
          onConfirm={() => remove(record.id)}
          okText={translate('common.yes')}
          cancelText={translate('common.no')}
        >
          <Button
            className="ghostNoBorderButton"
            icon={<span className="json icon-delete"/>}
          />
        </Popconfirm>
      </div>
    ),
  },
]);

export default columns