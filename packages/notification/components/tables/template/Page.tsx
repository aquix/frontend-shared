import React, { FunctionComponent, useCallback, useEffect, useMemo, useState } from 'react';
import { ITemplateTableProps } from './redux';
import { Button, Modal, Table } from 'antd';
import templateColumns from './columns';
import { IdRequest, ListOptions, TemplateCreate, TemplateModel } from '../../../types/common_pb';
import TemplateForm from './forms';

const TemplateTable: FunctionComponent<ITemplateTableProps> = (props) => {
  const {
    templates,
    translate,
    loadTemplates,
    createTemplate,
    updateTemplate,
    removeTemplate,
  } = props;

  const [formVisible, setFormVisible] = useState(false);
  const [initialValue, setInitialValue] = useState({} as TemplateModel.AsObject);

  useEffect(() => {
    const value = new ListOptions();
    value.setLimit(10);

    loadTemplates(value)
  }, []);

  const createOrUpdate = useCallback((value: TemplateModel.AsObject) => {
    if (value.id) {
      const item = new TemplateModel();

      item.setTitle(value.title);
      item.setContent(value.content);
      item.setSlug(value.slug);
      item.setHandler(value.handler);
      item.setId(value.id);

      updateTemplate(item);
    } else {
      const item = new TemplateCreate();

      item.setTitle(value.title);
      item.setContent(value.content);
      item.setSlug(value.slug);
      item.setHandler(value.handler);

      createTemplate(item);
    }

    closeForm();
  }, []);

  const closeForm = useCallback(() => {
    setInitialValue({} as TemplateModel.AsObject);
    setFormVisible(false);
  }, []);

  const showForm = useCallback((value: TemplateModel.AsObject) => {
    setInitialValue(value);
    setFormVisible(true);
  }, []);

  const onRemove = useCallback((id: number) => {
    const item = new IdRequest();
    item.setId(id);

    removeTemplate(item);
  }, []);
  
  const columns = useMemo(() => templateColumns(translate, showForm, onRemove), []);

  return (
    <>
      <Button
        type='primary'
        className='mb-10'
        onClick={() => showForm({} as TemplateModel.AsObject)}
      >
        {translate("common.add")}
      </Button>
      <Table dataSource={templates as []} columns={columns} />
      <Modal
        visible={formVisible}
        footer={false}
        centered
        title={translate("notify.form.template.modal")}
        onCancel={() => closeForm()}
      >
        <TemplateForm
          translate={translate}
          initialValue={initialValue}
          createOrUpdate={createOrUpdate}
        />
      </Modal>
    </>
  )
}

export default TemplateTable;