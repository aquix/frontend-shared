import { INotifyReducer } from '../../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { WithTranslation } from '@frontend-shared/translate';
import { IdRequest, ListOptions, TemplateCreate, TemplateModel } from '../../../types/common_pb';
import { createTemplate, loadTemplates, removeTemplate, updateTemplate } from '../../../commands/template';

export const mapStateToProps = (state: INotifyReducer) => ({
  templates: state.template.list,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<INotifyReducer, void, AnyAction>) => ({
  loadTemplates: (value: ListOptions) => dispatch(loadTemplates(value)),
  createTemplate: (value: TemplateCreate) => dispatch(createTemplate(value)),
  updateTemplate: (value: TemplateModel) => dispatch(updateTemplate(value)),
  removeTemplate: (value: IdRequest) => dispatch(removeTemplate(value)),
});

export type ITemplateTableProps = WithTranslation & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;