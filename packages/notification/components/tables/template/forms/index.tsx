import React, { FunctionComponent, useCallback, useEffect } from 'react';
import { Button, Form, Input } from 'antd';
import { TemplateModel } from '../../../../types/common_pb';
import { IForm } from '../../../../types';

const TemplateForm: FunctionComponent<IForm<TemplateModel.AsObject>> = (props) => {
  const {
    translate,
    initialValue,
    createOrUpdate,
  } = props;

  const [ form ] = Form.useForm();

  useEffect(() => {
    form.resetFields();
    if (initialValue.id) {
      form.setFieldsValue(initialValue);
    } else {
      form.setFieldsValue({} as TemplateModel.AsObject)
    }
  }, [ initialValue ]);

  const onChangeSMTP = useCallback((values: TemplateModel.AsObject) => createOrUpdate({ ...initialValue, ...values }), []);

  return (
    <>
      <div className="overflow-hidden m-0" style={ { paddingBottom: 0, position: "relative" } }>
        <Form
          form={ form }
          onFinish={ (values) => onChangeSMTP(values) }
          autoComplete="off"
        >
          <Form.Item
            name="title"
            label={translate('notify.form.template.title')}
            rules={ [
              { required: true, message: translate('validation.required') },
            ] }
          >
            <Input placeholder={ translate('notify.form.template.title-placeholder') } />
          </Form.Item>
          <Form.Item
            name="content"
            label={translate('notify.form.template.content')}
            rules={ [
              { required: true, message: translate('validation.required') },
            ] }
          >
            <Input placeholder={ translate('notify.form.template.content-placeholder') } />
          </Form.Item>
          <Form.Item
            name="slug"
            label={translate('notify.form.template.slug')}
            rules={ [
              { required: true, message: translate('validation.required') },
            ] }
          >
            <Input placeholder={ translate('notify.form.template.slug-placeholder') } />
          </Form.Item>
          <Form.Item
            name="handler"
            label={translate('notify.form.template.handler')}
            rules={ [
              { required: true, message: translate('validation.required') },
            ] }
          >
            <Input placeholder={ translate('notify.form.template.handler-placeholder') } />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="float-right mt-10"
            >
              {translate('common.save')}
            </Button>
          </Form.Item>
        </Form>
      </div>
    </>

  )
}

export default TemplateForm;