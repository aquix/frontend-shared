import { connect } from 'react-redux';
import { NotifyContext } from '../../../context/notify';
import { mapDispatchToProps, mapStateToProps } from './redux';
import NotificationTable from './Page';
import { withLocale } from '@frontend-shared/translate';

const Connector = connect(mapStateToProps, mapDispatchToProps, undefined, { context: NotifyContext })(NotificationTable);
export default withLocale(Connector);