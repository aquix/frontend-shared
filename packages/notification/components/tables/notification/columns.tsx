import { TranslateFunc } from '@frontend-shared/translate';
import { ColumnsType } from 'antd/es/table';
import React from 'react';
import { TemplateModel } from '../../../types/common_pb';

const columns = (translate: TranslateFunc): ColumnsType<TemplateModel.AsObject> => ([
    {
      title: translate('notify.notification.column.userid'),
      dataIndex: 'userid',
      key: 'userid',
    },
    {
      title: translate('notify.notification.column.content'),
      dataIndex: 'content',
      key: 'content',
    },
    {
      title: translate('notify.notification.column.read'),
      dataIndex: 'read',
      key: 'read',
      render: (value) => String(value),
    },
  ]);

export default columns