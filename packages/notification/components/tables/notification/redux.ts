import { INotifyReducer } from '../../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { WithTranslation } from '@frontend-shared/translate';
import { ListWithLimitRequest } from '../../../types/common_pb';
import { loadNotifications } from '../../../commands/notification';

export const mapStateToProps = (state: INotifyReducer) => ({
  notifications: state.notification.list,
  count: state.notification.count,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<INotifyReducer, void, AnyAction>) => ({
  loadNotifications: (value: ListWithLimitRequest) => dispatch(loadNotifications(value)),
});

export type INotificationTableProps = WithTranslation & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;