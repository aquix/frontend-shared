import React, { FunctionComponent, useCallback, useEffect, useMemo, useState } from 'react';
import { INotificationTableProps } from './redux';
import { Input, Pagination, Table } from 'antd';
import notifyColumns from './columns';
import { ListOptions, ListWithLimitRequest } from '../../../types/common_pb';
import { debounce } from 'lodash';

const { Search } = Input;

const NotificationTable: FunctionComponent<INotificationTableProps> = (props) => {
  const {
    translate,
    notifications,
    loadNotifications,
    count,
  } = props;

  const [query, setQuery] = useState("");

  const onSearch = useCallback((id: number, offset: number) => {
    const value = new ListWithLimitRequest();
    const opts = new ListOptions();

    opts.setLimit(10);
    opts.setOffset(offset);

    value.setOpts(opts);
    value.setUserid(id)

    loadNotifications(value);
  }, []);

  useEffect(() => {
    const search = window.location.search;
    const query = new URLSearchParams(search);
    const foo = query.get("id");

    if (foo && foo.length) {
      onSearch(parseInt(String(foo)), 0);
      setQuery(foo);
    }
  }, []);

  const updateSearchValue = useCallback((value: string) => setQuery(value), []);

  const onEnterSearch = useCallback(debounce((query: string) => onSearch(parseInt(query), 0), 2000), []);

  const columns = useMemo(() => notifyColumns(translate), []);

  return (
    <>
      <Search
        className='mb-10'
        style={{ maxWidth: 300 }}
        onChange={({ target: { value } }) => {
          updateSearchValue(value);
          onEnterSearch(value);
        }}
        value={query}
        placeholder={translate('common.search')}
        enterButton
        allowClear
      />
      {!!notifications.length && !!count && !!query.length &&
      <>
        <Table
          pagination={false}
          dataSource={notifications as []}
          columns={columns}
        />
        <Pagination
          className="mt-10"
          style={{ float: 'right' }}
          defaultCurrent={1}
          total={count}
          defaultPageSize={10}
          onChange={(page, pageSize) => onSearch(parseInt(query), --page * (pageSize as number))}
        />
      </>}
    </>
  )
}

export default NotificationTable;