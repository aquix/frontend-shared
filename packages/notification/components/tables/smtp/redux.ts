import { INotifyReducer } from '../../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { WithTranslation } from '@frontend-shared/translate';
import { createSMTP, loadSMTPList, removeSMTP, updateSMTP } from '../../../commands/smtp';
import { IdRequest, SMTPCreate, SMTPModel } from '../../../types/common_pb';

export const mapStateToProps = (state: INotifyReducer) => ({
  smtp: state.smtp.list,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<INotifyReducer, void, AnyAction>) => ({
  loadSMTPList: () => dispatch(loadSMTPList()),
  createSMTP: (value: SMTPCreate) => dispatch(createSMTP(value)),
  updateSMTP: (value: SMTPModel) => dispatch(updateSMTP(value)),
  removeSMTP: (value: IdRequest) => dispatch(removeSMTP(value)),
});

export type ISMTPTableProps = WithTranslation & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;