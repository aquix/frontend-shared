import React, { FunctionComponent, useCallback, useEffect } from 'react';
import { Button, Form, Input } from 'antd';
import { SMTPModel } from '../../../../types/common_pb';
import { IForm } from '../../../../types';

const SMTPForm: FunctionComponent<IForm<SMTPModel.AsObject>> = (props) => {
  const {
    translate,
    initialValue,
    createOrUpdate,
  } = props;

  const [form] = Form.useForm();

  useEffect(() => {
    form.resetFields();
    if (initialValue.id) {
      form.setFieldsValue(initialValue);
    } else {
      form.setFieldsValue({} as SMTPModel.AsObject);
    }
  }, [initialValue]);

  const onChangeSMTP = useCallback((values: SMTPModel.AsObject) => createOrUpdate({ ...initialValue, ...values }), []);

  return (
    <>
      <div className="overflow-hidden m-0" style={{ paddingBottom: 0, position: 'relative' }}>
        <Form
          form={form}
          onFinish={(values) => onChangeSMTP(values)}
          autoComplete="off"
        >
          <Form.Item
            name="slug"
            label={translate('notify.form.smtp.slug')}
            rules={[
              { required: true, message: translate('validation.required') },
            ]}
          >
            <Input placeholder={translate('notify.form.smtp.slug-placeholder')} />
          </Form.Item>
          <Form.Item
            name="url"
            label={translate('notify.form.smtp.url')}
            rules={[
              { required: true, message: translate('validation.required') },
            ]}
          >
            <Input placeholder={translate('notify.form.smtp.url-placeholder')} />
          </Form.Item>
          <Form.Item
            name="port"
            label={translate('notify.form.smtp.port')}
            rules={[
              { required: true, message: translate('validation.required') },
            ]}
          >
            <Input type='number' placeholder={translate('notify.form.smtp.port-placeholder')} />
          </Form.Item>
          <Form.Item
            name="username"
            label={translate('notify.form.smtp.username')}
            rules={[
              { required: true, message: translate('validation.required') },
            ]}
          >
            <Input placeholder={translate('notify.form.smtp.username-placeholder')} />
          </Form.Item>
          <Form.Item
            name="password"
            label={translate('notify.form.smtp.password')}
            rules={[
              { required: true, message: translate('validation.required') },
            ]}
          >
            <Input placeholder={translate('notify.form.smtp.password-placeholder')} />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="float-right mt-10"
            >
              {translate('common.save')}
            </Button>
          </Form.Item>
        </Form>
      </div>
    </>

  );
};

export default SMTPForm;