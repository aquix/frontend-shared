import { connect } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from '../smtp/redux';
import { NotifyContext } from '../../../context/notify';
import SMTPTable from './Page';
import { withLocale } from '@frontend-shared/translate';

const Connector = connect(mapStateToProps, mapDispatchToProps, undefined, { context: NotifyContext })(SMTPTable);
export default withLocale(Connector);