import React, { FunctionComponent, useCallback, useEffect, useMemo, useState } from 'react';
import { ISMTPTableProps } from './redux';
import { Button, Modal, Table } from 'antd';
import smtpColumns from './columns';
import { IdRequest, SMTPCreate, SMTPModel } from '../../../types/common_pb';
import SMTPForm from './forms';

const SMTPTable: FunctionComponent<ISMTPTableProps> = (props) => {
  const {
    translate,
    smtp,
    loadSMTPList,
    createSMTP,
    updateSMTP,
    removeSMTP,
  } = props;

  const [formVisible, setFormVisible] = useState(false);
  const [initialValue, setInitialValue] = useState({} as SMTPModel.AsObject);

  useEffect(() => {
    loadSMTPList();
  }, []);

  const createOrUpdate = useCallback((value: SMTPModel.AsObject) => {
    if (value.id) {
      const item = new SMTPModel();

      item.setSlug(value.slug);
      item.setUrl(value.url);
      item.setPort(value.port);
      item.setUsername(value.username);
      item.setPassword(value.password);
      item.setId(value.id);

      updateSMTP(item);
    } else {
      const item = new SMTPCreate();
      item.setSlug(value.slug);
      item.setUrl(value.url);
      item.setPort(value.port);
      item.setUsername(value.username);
      item.setPassword(value.password);

      createSMTP(item);
    }

    closeForm();
  }, []);

  const closeForm = useCallback(() => {
    setInitialValue({} as SMTPModel.AsObject);
    setFormVisible(false);
  }, []);

  const showForm = useCallback((value: SMTPModel.AsObject) => {
    setInitialValue(value);
    setFormVisible(true);
  }, []);

  const onRemove = useCallback((id: number) => {
    const item = new IdRequest();
    item.setId(id);

    removeSMTP(item);
  }, []);

  const columns = useMemo(() => smtpColumns(translate, showForm, onRemove), []);

  return (
    <>
      <Button
        type='primary'
        className='mb-10'
        onClick={() => showForm({} as SMTPModel.AsObject)}
      >
        {translate("common.add")}
      </Button>
      <Table dataSource={smtp as []} columns={columns} />
      <Modal
        visible={formVisible}
        footer={false}
        centered
        title={translate("notify.form.smtp.modal")}
        onCancel={() => closeForm()}
      >
        <SMTPForm
          translate={translate}
          initialValue={initialValue}
          createOrUpdate={createOrUpdate}
        />
      </Modal>
    </>
  );
};

export default SMTPTable;