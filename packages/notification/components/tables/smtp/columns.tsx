import { TranslateFunc } from '@frontend-shared/translate';
import { ColumnsType } from 'antd/es/table';
import React from 'react';
import { SMTPModel } from '../../../types/common_pb';
import { Button, Divider, Popconfirm } from 'antd';

const columns = (translate: TranslateFunc, showForm: (value: SMTPModel.AsObject) => void, remove: (id: number) => void): ColumnsType<SMTPModel.AsObject> => ([
  {
    title: translate('notify.smtp.column.slug'),
    dataIndex: 'slug',
    key: 'slug',
  },
  {
    title: translate('notify.smtp.column.url'),
    dataIndex: 'url',
    key: 'url',
  },
  {
    title: translate('notify.smtp.column.port'),
    dataIndex: 'port',
    key: 'port',
  },
  {
    title: translate('notify.smtp.column.username'),
    dataIndex: 'username',
    key: 'username',
  },
  {
    title: translate('notify.smtp.column.password'),
    dataIndex: 'password',
    key: 'password',
  },
  {
    title: translate('notify.smtp.column.actions'),
    dataIndex: 'action',
    key: 'action',
    align: 'right',
    render: (text, record) => (
      <div style={{whiteSpace: 'nowrap', display: "flex", alignItems: "center", justifyContent: "flex-end"}}>
        <Button
          shape="circle"
          icon={<span className="json icon-edit"/>}
          onClick={() => showForm(record)}
        />
        <Divider type="vertical" />
        <Popconfirm
          title={translate('common.delete')}
          onConfirm={() => remove(record.id)}
          okText={translate('common.yes')}
          cancelText={translate('common.no')}
        >
          <Button
            className="ghostNoBorderButton"
            icon={<span className="json icon-delete"/>}
          />
        </Popconfirm>
      </div>
    ),
  },
]);

export default columns