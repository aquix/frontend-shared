import Page from './Layout';
import { withLocale } from '@frontend-shared/translate';

export default withLocale(Page)
