import React, { FunctionComponent, useCallback, useState } from 'react';
import { Radio } from 'antd';
import TemplateTable from '../tables/template';
import NotificationTable from '../tables/notification';
import SMTPTable from '../tables/smtp';
import { WithTranslation } from '@frontend-shared/translate';

const Layout: FunctionComponent<WithTranslation> = (props) => {
  const {
    translate
  } = props;

  const [currentRadio, setCurrentRadio] = useState('template');
  const [currentTable, setCurrentTable] = useState(<TemplateTable translate={(value) => value as string} />);

  const changeRadio = useCallback((value: string) => {
    setCurrentRadio(value);
    switch (value) {
      case "notify":
        setCurrentTable(<NotificationTable translate={(value) => value as string} />)
      break;
      case "template":
        setCurrentTable(<TemplateTable translate={(value) => value as string} />)
      break;
      case "smtp":
        setCurrentTable(<SMTPTable translate={(value) => value as string} />)
      break;
    }
  }, []);

  return (
      <>
        <style jsx global>{`
            .ant-radio-button-wrapper {
             padding: 8px 16px !important;
             border-radius: 4px !important;
             margin-right: 10px !important;
             display: flex !important;
             justify-content: center;
             align-items: center;
             border: 1px solid #d9d9d9 !important;
           }

           .ant-radio-group {
             display: flex !important;
           }

           .ant-radio-button-wrapper::before {
             display: none !important;
           }
        `
        }</style>
        <div>
          <Radio.Group
            onChange={(e) => changeRadio(e.target.value)}
            buttonStyle="solid"
            defaultValue={currentRadio}
            className='category-buttons'
            style={{marginBottom: 24}}
          >
            <Radio.Button
              value={"template"}
              key="template"
            >
              {translate('notify.table.template')}
            </Radio.Button>
            <Radio.Button
              value={"smtp"}
              key="smtp"
            >
              {translate('notify.table.smtp')}
            </Radio.Button>
            <Radio.Button
              value={"notify"}
              key="notify"
            >
              {translate('notify.table.notify')}
            </Radio.Button>
          </Radio.Group>
          {currentTable}
        </div>
      </>
  )
}

export default Layout;