'use strict';
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: path.resolve(__dirname, '/index.tsx'),
  output: {
    path: path.join(__dirname, '/lib'),
    filename: 'index.js',
    library: 'node-editor',
    libraryTarget: 'umd',
    clean: true,
    publicPath: '/',
    globalObject: 'this',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.css']
  },
  target: 'web',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules|\.d\.ts$/,
        use: ['ts-loader']
      },
      {
        test: /\.d\.ts$/,
        loader: 'ignore-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ]
  },
  plugins: [
    /**
     * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
     * the following package: sudo apt-get install libpng16-dev
     */
    new MiniCssExtractPlugin({
      linkType: "text/css",
    }),
    // new ModuleFederationPlugin({
    //   name: 'mailing-module',
    //   filename: 'remoteEntry.js',
    //   remotes: {},
    //   exposes: {},
    //   shared: {
    //     // ...deps,
    //     react: {
    //       singleton: true,
    //       requiredVersion: deps.react,
    //     },
    //     'react-dom': {
    //       singleton: true,
    //       requiredVersion: deps['react-dom'],
    //     },
    //   },
    // }),
  ],
  optimization: {
    minimize: false
  },
  externals: {
    react: {
      commonjs: 'React',
      commonjs2: 'react',
      amd: 'react'
    },
    'react-dom': {
      commonjs: 'ReactDOM',
      commonjs2: 'react-dom',
      amd: 'react-dom'
    },
    "react-redux": "react-redux",
    "redux-logger": "redux-logger",
  },
};
