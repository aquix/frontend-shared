/// <reference types="react" />
import { INotifyReducer } from '../types/reducer';
export declare const NotifyContext: import("react").Context<import("react-redux").ReactReduxContextValue<INotifyReducer, import("redux").AnyAction>>;
