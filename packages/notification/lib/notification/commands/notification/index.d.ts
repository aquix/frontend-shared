/// <reference types="grpc-web" />
import { ListWithLimitRequest } from '../../types/common_pb';
import { ThunkDispatch } from 'redux-thunk';
import { ActionModel } from '@frontend-shared/node-editor/types/common_pb';
import { AnyAction } from 'redux';
export declare const loadNotifications: (value: ListWithLimitRequest) => (dispatch: ThunkDispatch<ActionModel[], {}, AnyAction>) => import("grpc-web").ClientReadableStream<import("../../types/common_pb").ListWithLimitModel>;
