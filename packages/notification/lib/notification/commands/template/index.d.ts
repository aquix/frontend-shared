/// <reference types="grpc-web" />
import { ThunkDispatch } from 'redux-thunk';
import { ActionModel } from '@frontend-shared/node-editor/types/common_pb';
import { AnyAction } from 'redux';
import { IdRequest, ListOptions, TemplateCreate, TemplateModel } from '../../types/common_pb';
export declare const loadTemplates: (value: ListOptions) => (dispatch: ThunkDispatch<ActionModel[], {}, AnyAction>) => import("grpc-web").ClientReadableStream<TemplateModel>;
export declare const createTemplate: (value: TemplateCreate) => (dispatch: ThunkDispatch<TemplateCreate, {}, AnyAction>) => import("grpc-web").ClientReadableStream<TemplateModel>;
export declare const updateTemplate: (value: TemplateModel) => (dispatch: ThunkDispatch<TemplateModel, {}, AnyAction>) => import("grpc-web").ClientReadableStream<import("google-protobuf/google/protobuf/empty_pb").Empty>;
export declare const removeTemplate: (value: IdRequest) => (dispatch: ThunkDispatch<TemplateModel, {}, AnyAction>) => import("grpc-web").ClientReadableStream<import("google-protobuf/google/protobuf/empty_pb").Empty>;
