/// <reference types="grpc-web" />
import { IdRequest, SMTPCreate, SMTPModel } from '../../types/common_pb';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { Empty } from 'google-protobuf/google/protobuf/empty_pb';
export declare const loadSMTPList: () => (dispatch: ThunkDispatch<SMTPModel[], {}, AnyAction>) => import("grpc-web").ClientReadableStream<any>;
export declare const createSMTP: (value: SMTPCreate) => (dispatch: ThunkDispatch<SMTPModel, {}, AnyAction>) => import("grpc-web").ClientReadableStream<any>;
export declare const updateSMTP: (value: SMTPModel) => (dispatch: ThunkDispatch<SMTPModel, {}, AnyAction>) => import("grpc-web").ClientReadableStream<Empty>;
export declare const removeSMTP: (value: IdRequest) => (dispatch: ThunkDispatch<SMTPModel, {}, AnyAction>) => import("grpc-web").ClientReadableStream<Empty>;
