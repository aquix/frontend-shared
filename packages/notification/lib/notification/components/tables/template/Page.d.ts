import { FunctionComponent } from 'react';
import { ITemplateTableProps } from './redux';
declare const TemplateTable: FunctionComponent<ITemplateTableProps>;
export default TemplateTable;
