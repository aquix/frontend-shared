import { ColumnsType } from 'antd/es/table';
import { TemplateModel } from '../../../types/common_pb';
import { TranslateFunc } from '@frontend-shared/translate';
declare const columns: (translate: TranslateFunc, showForm: (value: TemplateModel.AsObject) => void, remove: (id: number) => void) => ColumnsType<TemplateModel.AsObject>;
export default columns;
