import { FunctionComponent } from 'react';
import { TemplateModel } from '../../../../types/common_pb';
import { IForm } from '../../../../types';
declare const TemplateForm: FunctionComponent<IForm<TemplateModel.AsObject>>;
export default TemplateForm;
