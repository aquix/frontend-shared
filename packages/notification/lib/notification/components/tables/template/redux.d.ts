/// <reference types="grpc-web" />
import { INotifyReducer } from '../../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { WithTranslation } from '@frontend-shared/translate';
import { IdRequest, ListOptions, TemplateCreate, TemplateModel } from '../../../types/common_pb';
export declare const mapStateToProps: (state: INotifyReducer) => {
    templates: TemplateModel.AsObject[];
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<INotifyReducer, void, AnyAction>) => {
    loadTemplates: (value: ListOptions) => import("grpc-web").ClientReadableStream<TemplateModel>;
    createTemplate: (value: TemplateCreate) => import("grpc-web").ClientReadableStream<TemplateModel>;
    updateTemplate: (value: TemplateModel) => import("grpc-web").ClientReadableStream<import("google-protobuf/google/protobuf/empty_pb").Empty>;
    removeTemplate: (value: IdRequest) => import("grpc-web").ClientReadableStream<import("google-protobuf/google/protobuf/empty_pb").Empty>;
};
export declare type ITemplateTableProps = WithTranslation & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
