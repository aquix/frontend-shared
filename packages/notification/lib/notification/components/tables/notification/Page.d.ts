import { FunctionComponent } from 'react';
import { INotificationTableProps } from './redux';
declare const NotificationTable: FunctionComponent<INotificationTableProps>;
export default NotificationTable;
