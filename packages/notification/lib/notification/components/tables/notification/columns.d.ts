import { TranslateFunc } from '@frontend-shared/translate';
import { ColumnsType } from 'antd/es/table';
import { TemplateModel } from '../../../types/common_pb';
declare const columns: (translate: TranslateFunc) => ColumnsType<TemplateModel.AsObject>;
export default columns;
