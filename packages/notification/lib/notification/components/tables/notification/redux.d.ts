/// <reference types="grpc-web" />
import { INotifyReducer } from '../../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { WithTranslation } from '@frontend-shared/translate';
import { ListWithLimitRequest } from '../../../types/common_pb';
export declare const mapStateToProps: (state: INotifyReducer) => {
    notifications: import("../../../types/common_pb").NotificationModel.AsObject[];
    count: number;
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<INotifyReducer, void, AnyAction>) => {
    loadNotifications: (value: ListWithLimitRequest) => import("grpc-web").ClientReadableStream<import("../../../types/common_pb").ListWithLimitModel>;
};
export declare type INotificationTableProps = WithTranslation & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
