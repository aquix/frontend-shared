/// <reference types="grpc-web" />
import { INotifyReducer } from '../../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { WithTranslation } from '@frontend-shared/translate';
import { IdRequest, SMTPCreate, SMTPModel } from '../../../types/common_pb';
export declare const mapStateToProps: (state: INotifyReducer) => {
    smtp: SMTPModel.AsObject[];
};
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<INotifyReducer, void, AnyAction>) => {
    loadSMTPList: () => import("grpc-web").ClientReadableStream<any>;
    createSMTP: (value: SMTPCreate) => import("grpc-web").ClientReadableStream<any>;
    updateSMTP: (value: SMTPModel) => import("grpc-web").ClientReadableStream<import("google-protobuf/google/protobuf/empty_pb").Empty>;
    removeSMTP: (value: IdRequest) => import("grpc-web").ClientReadableStream<import("google-protobuf/google/protobuf/empty_pb").Empty>;
};
export declare type ISMTPTableProps = WithTranslation & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
