import { FunctionComponent } from 'react';
import { SMTPModel } from '../../../../types/common_pb';
import { IForm } from '../../../../types';
declare const SMTPForm: FunctionComponent<IForm<SMTPModel.AsObject>>;
export default SMTPForm;
