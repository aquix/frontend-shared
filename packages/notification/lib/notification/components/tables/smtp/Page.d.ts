import { FunctionComponent } from 'react';
import { ISMTPTableProps } from './redux';
declare const SMTPTable: FunctionComponent<ISMTPTableProps>;
export default SMTPTable;
