import { TranslateFunc } from '@frontend-shared/translate';
import { ColumnsType } from 'antd/es/table';
import { SMTPModel } from '../../../types/common_pb';
declare const columns: (translate: TranslateFunc, showForm: (value: SMTPModel.AsObject) => void, remove: (id: number) => void) => ColumnsType<SMTPModel.AsObject>;
export default columns;
