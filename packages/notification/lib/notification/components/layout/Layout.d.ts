import { FunctionComponent } from 'react';
import { WithTranslation } from '@frontend-shared/translate';
declare const Layout: FunctionComponent<WithTranslation>;
export default Layout;
