import { NotificationModel, SMTPModel, TemplateModel } from './common_pb';
import { TranslateFunc } from '@frontend-shared/translate';
export interface ISMTPReducer {
    list: SMTPModel.AsObject[];
}
export interface ITemplateReducer {
    list: TemplateModel.AsObject[];
}
export interface INotificationReducer {
    list: NotificationModel.AsObject[];
    count: number;
}
export interface IForm<T> {
    translate: TranslateFunc;
    initialValue: T;
    createOrUpdate: (value: T) => void;
}
