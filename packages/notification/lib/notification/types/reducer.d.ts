import { INotificationReducer, ISMTPReducer, ITemplateReducer } from './index';
export interface INotifyReducer {
    smtp: ISMTPReducer;
    template: ITemplateReducer;
    notification: INotificationReducer;
}
