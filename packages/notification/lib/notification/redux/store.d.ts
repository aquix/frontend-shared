import { AnyAction } from 'redux';
import { INotifyReducer } from '../types/reducer';
declare const _default: import("redux").Store<INotifyReducer, AnyAction>;
export default _default;
