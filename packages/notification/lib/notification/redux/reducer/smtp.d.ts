import { ISMTPReducer } from '../../types';
import { SMTPActions } from '../action/smtp';
declare const SMTPReducer: (state: ISMTPReducer | undefined, action: SMTPActions) => ISMTPReducer;
export default SMTPReducer;
