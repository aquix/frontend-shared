import { INotificationReducer } from '../../types';
import { NotificationActions } from '../action/notification';
declare const NotificationReducer: (state: INotificationReducer | undefined, action: NotificationActions) => INotificationReducer;
export default NotificationReducer;
