import { ITemplateReducer } from '../../types';
import { TemplateActions } from '../action/template';
declare const TemplateReducer: (state: ITemplateReducer | undefined, action: TemplateActions) => ITemplateReducer;
export default TemplateReducer;
