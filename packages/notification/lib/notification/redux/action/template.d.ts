import { TemplateModel } from '../../types/common_pb';
export declare const TEMPLATE_LIST_ACTION = "TEMPLATE_LIST_ACTION";
export declare const CREATE_TEMPLATE_ACTION = "CREATE_TEMPLATE_ACTION";
export declare const UPDATE_TEMPLATE_ACTION = "UPDATE_TEMPLATE_ACTION";
export declare const REMOVE_TEMPLATE_ACTION = "REMOVE_TEMPLATE_ACTION";
export declare const templateListAction: (payload: TemplateModel.AsObject[]) => {
    type: string;
    payload: TemplateModel.AsObject[];
};
export declare const createTemplateAction: (payload: TemplateModel.AsObject) => {
    type: string;
    payload: TemplateModel.AsObject;
};
export declare const updateTemplateAction: (payload: TemplateModel.AsObject) => {
    type: string;
    payload: TemplateModel.AsObject;
};
export declare const removeTemplateAction: (payload: number) => {
    type: string;
    payload: number;
};
export declare type TemplateListAction = ReturnType<typeof templateListAction>;
export declare type CreateTemplateAction = ReturnType<typeof createTemplateAction>;
export declare type UpdateTemplateAction = ReturnType<typeof updateTemplateAction>;
export declare type RemoveTemplateAction = ReturnType<typeof removeTemplateAction>;
export declare type TemplateActions = TemplateListAction | CreateTemplateAction | UpdateTemplateAction | RemoveTemplateAction;
