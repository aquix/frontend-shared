import { SMTPModel } from '../../types/common_pb';
export declare const SMTP_LIST_ACTION = "SMTP_LIST_ACTION";
export declare const CREATE_SMTP_ACTION = "CREATE_SMTP_ACTION";
export declare const UPDATE_SMTP_ACTION = "UPDATE_SMTP_ACTION";
export declare const REMOVE_SMTP_ACTION = "REMOVE_SMTP_ACTION";
export declare const smtpListAction: (payload: SMTPModel.AsObject[]) => {
    type: string;
    payload: SMTPModel.AsObject[];
};
export declare const createSMTPAction: (payload: SMTPModel.AsObject) => {
    type: string;
    payload: SMTPModel.AsObject;
};
export declare const updateSMTPAction: (payload: SMTPModel.AsObject) => {
    type: string;
    payload: SMTPModel.AsObject;
};
export declare const removeSMTPAction: (payload: number) => {
    type: string;
    payload: number;
};
export declare type SmtpListAction = ReturnType<typeof smtpListAction>;
export declare type CreateSMTPAction = ReturnType<typeof createSMTPAction>;
export declare type UpdateSMTPAction = ReturnType<typeof updateSMTPAction>;
export declare type RemoveSMTPAction = ReturnType<typeof removeSMTPAction>;
export declare type SMTPActions = SmtpListAction | CreateSMTPAction | UpdateSMTPAction | RemoveSMTPAction;
