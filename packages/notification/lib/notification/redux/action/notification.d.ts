import { NotificationModel } from '../../types/common_pb';
export declare const NOTIFICATION_LIST_ACTION = "NOTIFICATION_LIST_ACTION";
export declare const NOTIFICATION_COUNT_ACTION = "NOTIFICATION_COUNT_ACTION";
export declare const notificationListAction: (payload: NotificationModel.AsObject[]) => {
    type: string;
    payload: NotificationModel.AsObject[];
};
export declare const notificationCountAction: (payload: number) => {
    type: string;
    payload: number;
};
export declare type NotificationListAction = ReturnType<typeof notificationListAction>;
export declare type NotificationCountAction = ReturnType<typeof notificationCountAction>;
export declare type NotificationActions = NotificationListAction | NotificationCountAction;
