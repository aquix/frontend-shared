declare class StatusError extends Error {
    status: number;
    constructor(message: string, status: number);
}
export default StatusError;
