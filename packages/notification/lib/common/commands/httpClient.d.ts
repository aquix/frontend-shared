import { Action, AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BaseCommandProps } from "../apiClient/type";
import { ApiClientOptions } from "../apiClient";
import { AppDispatch } from "../redux/types";
declare const httpClient: <P extends object | null, T extends BaseCommandProps>(url: string, params?: P | undefined, options?: ApiClientOptions | undefined) => (dispatch: ThunkDispatch<P, {}, AnyAction | Action<Promise<P>>>) => Promise<T>;
export declare const apiCall: <P extends object | null, T extends BaseCommandProps>(url: string, params?: P | undefined, options?: ApiClientOptions | undefined) => (dispatch: AppDispatch<P>) => Promise<T>;
export default httpClient;
