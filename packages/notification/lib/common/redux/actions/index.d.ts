import { INotifMessage } from '../types';
export declare const CATCH_ERROR = "CATCH_ERROR";
export declare const CLEAR_ALL = "CLEAR_ALL";
export declare const SUCCESS_MESSAGE = "SUCCESS_MESSAGE";
export declare const SET_LOADING_CONTEXT = "SET_LOADING_CONTEXT";
export declare const REMOVE_LOADING_CONTEXT = "REMOVE_LOADING_CONTEXT";
export declare const CommonAction: <T>(type: string) => (payload: T) => {
    type: string;
    payload: T;
};
export declare const catchError: (data: INotifMessage) => {
    type: string;
    payload: INotifMessage;
};
export declare const successMessage: (data: INotifMessage) => {
    type: string;
    payload: INotifMessage;
};
export declare const setLoadingContext: (context?: string) => {
    type: string;
    payload: string;
};
export declare const removeLoadingContext: (context?: string) => {
    type: string;
    payload: string;
};
export declare const clearData: () => {
    type: string;
};
interface NotifAction {
    type: string;
    payload: INotifMessage | string | void;
}
export declare type CommonAction = NotifAction;
export {};
