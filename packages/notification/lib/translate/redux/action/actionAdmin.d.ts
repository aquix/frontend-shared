import { ILang } from "../../types/lang";
import { ITranslateListAdmin } from "../../types/translateListAdmin";
export declare const SET_LANGUAGE_LIST_ADMIN = "SET_LANGUAGE_LIST_ADMIN";
export declare const SET_TRANSLATE_LIST_ADMIN = "SET_TRANSLATE_LIST_ADMIN";
export declare const CREATE_LANG_ITEM = "CREATE_LANG_ITEM";
export declare const CREATE_TRANSLATE_LIST_ITEM_ADMIN = "CREATE_TRANSLATE_LIST_ITEM_ADMIN";
export declare const EXPORT_TRANSLATE_ITEM_ADMIN = "EXPORT_TRANSLATE_ITEM_ADMIN";
export declare const REMOVE_EXPORT_TRANSLATE_ITEM_ADMIN = "REMOVE_EXPORT_TRANSLATE_ITEM_ADMIN";
export declare const UPDATE_LANG_ITEM = "UPDATE_LANG_ITEM";
export declare const UPDATE_TRANSLATE_LIST_ITEM_ADMIN = "UPDATE_TRANSLATE_LIST_ITEM_ADMIN";
export declare const SET_BASE_URL = "SET_BASE_URL";
export declare const REMOVE_TRANSLATE = "REMOVE_TRANSLATE";
export declare const SET_TRANSLATE_COUNT = "SET_TRANSLATE_COUNT";
export interface SetLanguageListAdminAction {
    type: string;
    payload: {
        langs: ILang[];
    };
}
export interface SetTranslateListAdminAction {
    type: string;
    payload: {
        list: ITranslateListAdmin[];
    };
}
export interface UpdateTranslateListItemAdminAction {
    type: string;
    payload: {
        item: ITranslateListAdmin;
    };
}
export interface CreateTranslateListItemAdminAction {
    type: string;
    payload: {
        item: ITranslateListAdmin;
    };
}
export interface ExportTranslateListAdminAction {
    type: string;
    payload: {
        item: number;
    };
}
export interface RemoveExportTranslateListAdminAction {
    type: string;
    payload: {
        item: number;
    };
}
export interface CreateLangItemAction {
    type: string;
    payload: {
        item: ILang;
    };
}
export interface UpdateLangItemAction {
    type: string;
    payload: {
        item: ILang;
    };
}
export interface SetBaseUrlAction {
    type: string;
    payload: {
        url: string;
    };
}
export declare type TranslateActionAdmin = SetLanguageListAdminAction | SetTranslateListAdminAction | SetBaseUrlAction | UpdateTranslateListItemAdminAction | CreateTranslateListItemAdminAction | ExportTranslateListAdminAction | CreateLangItemAction | RemoveTranslateItem | setTranslateCountAction;
export declare const setLanguageListAdmin: (langs: ILang[]) => {
    type: string;
    payload: {
        langs: ILang[];
    };
};
export declare const setTranslateListAdmin: (list: ITranslateListAdmin[]) => {
    type: string;
    payload: {
        list: ITranslateListAdmin[];
    };
};
export declare const exportTranslateItem: (item: number) => {
    type: string;
    payload: {
        item: number;
    };
};
export declare const removeExportTranslateItem: (item: number) => {
    type: string;
    payload: {
        item: number;
    };
};
export declare const createLangItem: (item: ILang) => {
    type: string;
    payload: {
        item: ILang;
    };
};
export declare const updateLangItem: (item: ILang) => {
    type: string;
    payload: {
        item: ILang;
    };
};
export declare const setBaseUrl: (url: string) => {
    type: string;
    payload: {
        url: string;
    };
};
export declare const updateTranslateListItemAdmin: (item: ITranslateListAdmin) => {
    type: string;
    payload: {
        item: ITranslateListAdmin;
    };
};
export declare const createTranslateListItemAdmin: (item: ITranslateListAdmin) => {
    type: string;
    payload: {
        item: ITranslateListAdmin;
    };
};
export declare const removeTranslateItem: (payload: number) => {
    type: string;
    payload: number;
};
export declare const setTranslateCount: (payload: number) => {
    type: string;
    payload: number;
};
export declare type setTranslateCountAction = ReturnType<typeof setTranslateCount>;
export declare type RemoveTranslateItem = ReturnType<typeof removeTranslateItem>;
