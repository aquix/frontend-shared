import { ITranslate } from "../../types/translate";
import { ILang } from "../../types/lang";
export declare const SET_LANGUAGE = "SET_LANGUAGE";
export declare const SET_LANGUAGE_LIST = "SET_LANGUAGE_LIST";
export declare const SET_TRANSLATE_LIST = "SET_TRANSLATE_LIST";
export declare const SET_CACHE = "SET_CACHE";
export interface SetLanguageAction {
    type: string;
    payload: {
        code: string;
    };
}
export interface SetLanguageListAction {
    type: string;
    payload: {
        langs: ILang[];
    };
}
export interface SetTranslateListAction {
    type: string;
    payload: {
        translates: ITranslate;
    };
}
export interface SetCachetAction {
    type: string;
    payload: {
        cache: number;
    };
}
export declare type TranslateAction = SetLanguageAction | SetLanguageListAction | SetTranslateListAction | SetCachetAction;
export declare const setCache: (cache: number) => {
    type: string;
    payload: {
        cache: number;
    };
};
export declare const setLanguage: (code: string) => {
    type: string;
    payload: {
        code: string;
    };
};
export declare const setLanguageList: (langs: ILang[]) => {
    type: string;
    payload: {
        langs: ILang[];
    };
};
export declare const setTranslateList: (translates: ITranslate) => {
    type: string;
    payload: {
        translates: ITranslate;
    };
};
