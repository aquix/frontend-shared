import { ILang } from "./lang";
import { ITranslateListAdmin } from "./translateListAdmin";
import { ISource } from "./translate";
import { TranslateReducer } from "./transReducer";
export interface TranslateReducerAdmin {
    langs: ILang[];
    list: ITranslateListAdmin[];
    source: ISource;
    export: number[];
    count: number;
}
export interface CombineTranslateAdmin {
    admin: TranslateReducerAdmin;
    user: TranslateReducer;
}
