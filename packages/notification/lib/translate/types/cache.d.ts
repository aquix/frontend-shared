import { BaseCommandProps } from "@frontend-shared/common/apiClient/type";
export interface CacheObjectRes extends BaseCommandProps {
    cache: number;
}
