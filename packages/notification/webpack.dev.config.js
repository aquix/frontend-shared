'use strict';
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { HotModuleReplacementPlugin } = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: path.resolve(__dirname, 'public/index.tsx'),
  output: {
    path: path.join(__dirname, 'lib'),
    filename: 'index.js',
    library: 'smtp',
    libraryTarget: 'umd',
    clean: true,
    publicPath: '/',
    globalObject: 'this',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.css'],
    alias: {
      process: "process/browser"
    },
  },
  target: 'web',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules|\.d\.ts$/,
        use: ['ts-loader']
      },
      {
        test: /\.d\.ts$/,
        loader: 'ignore-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ]
  },
  plugins: [
    /**
     * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
     * the following package: sudo apt-get install libpng16-dev
     */
    new webpack.DefinePlugin({
      'process.env':{
        'ENVOY_PROXY': JSON.stringify('http://localhost:8081'),
      }
    }),
    new MiniCssExtractPlugin({
      linkType: "text/css",
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'lib/index.html'),
      filename: 'index.html',
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    new HotModuleReplacementPlugin(),
    // new ModuleFederationPlugin({
    //   name: 'mailing-module',
    //   filename: 'remoteEntry.js',
    //   remotes: {},
    //   exposes: {},
    //   shared: {
    //     // ...deps,
    //     react: {
    //       singleton: true,
    //       requiredVersion: deps.react,
    //     },
    //     'react-dom': {
    //       singleton: true,
    //       requiredVersion: deps['react-dom'],
    //     },
    //   },
    // }),
  ],
  optimization: {
    minimize: false
  },
  devServer: {
    static: path.join(__dirname, 'lib'),
    historyApiFallback: true,
    port: 4000,
    open: true,
    hot: true,
  },
};
