import {createContext} from "@frontend-shared/reduxContext";
import { INotifyReducer } from '../types/reducer';

export const NotifyContext = createContext<INotifyReducer>();