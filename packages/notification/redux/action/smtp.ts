import { CommonAction } from "@frontend-shared/common";
import { SMTPModel } from '../../types/common_pb';

export const SMTP_LIST_ACTION = 'SMTP_LIST_ACTION';
export const CREATE_SMTP_ACTION = 'CREATE_SMTP_ACTION';
export const UPDATE_SMTP_ACTION = 'UPDATE_SMTP_ACTION';
export const REMOVE_SMTP_ACTION = 'REMOVE_SMTP_ACTION';

export const smtpListAction = CommonAction<SMTPModel.AsObject[]>(SMTP_LIST_ACTION);
export const createSMTPAction = CommonAction<SMTPModel.AsObject>(CREATE_SMTP_ACTION);
export const updateSMTPAction = CommonAction<SMTPModel.AsObject>(UPDATE_SMTP_ACTION);
export const removeSMTPAction = CommonAction<number>(REMOVE_SMTP_ACTION);

export type SmtpListAction = ReturnType<typeof smtpListAction>;
export type CreateSMTPAction = ReturnType<typeof createSMTPAction>;
export type UpdateSMTPAction = ReturnType<typeof updateSMTPAction>;
export type RemoveSMTPAction = ReturnType<typeof removeSMTPAction>;

export type SMTPActions = SmtpListAction | CreateSMTPAction | UpdateSMTPAction | RemoveSMTPAction;