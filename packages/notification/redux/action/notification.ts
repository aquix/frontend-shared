import { CommonAction } from '@frontend-shared/common/lib';
import { NotificationModel } from '../../types/common_pb';

export const NOTIFICATION_LIST_ACTION = 'NOTIFICATION_LIST_ACTION';
export const NOTIFICATION_COUNT_ACTION = 'NOTIFICATION_COUNT_ACTION';

export const notificationListAction = CommonAction<NotificationModel.AsObject[]>(NOTIFICATION_LIST_ACTION);
export const notificationCountAction = CommonAction<number>(NOTIFICATION_COUNT_ACTION);

export type NotificationListAction = ReturnType<typeof notificationListAction>;
export type NotificationCountAction = ReturnType<typeof notificationCountAction>;

export type NotificationActions = NotificationListAction | NotificationCountAction;