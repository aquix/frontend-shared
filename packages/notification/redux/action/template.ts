import { CommonAction } from '@frontend-shared/common';
import { TemplateModel } from '../../types/common_pb';

export const TEMPLATE_LIST_ACTION = 'TEMPLATE_LIST_ACTION';
export const CREATE_TEMPLATE_ACTION = 'CREATE_TEMPLATE_ACTION';
export const UPDATE_TEMPLATE_ACTION = 'UPDATE_TEMPLATE_ACTION';
export const REMOVE_TEMPLATE_ACTION = 'REMOVE_TEMPLATE_ACTION';

export const templateListAction = CommonAction<TemplateModel.AsObject[]>(TEMPLATE_LIST_ACTION);
export const createTemplateAction = CommonAction<TemplateModel.AsObject>(CREATE_TEMPLATE_ACTION);
export const updateTemplateAction = CommonAction<TemplateModel.AsObject>(UPDATE_TEMPLATE_ACTION);
export const removeTemplateAction = CommonAction<number>(REMOVE_TEMPLATE_ACTION);

export type TemplateListAction = ReturnType<typeof templateListAction>;
export type CreateTemplateAction = ReturnType<typeof createTemplateAction>;
export type UpdateTemplateAction = ReturnType<typeof updateTemplateAction>;
export type RemoveTemplateAction = ReturnType<typeof removeTemplateAction>;

export type TemplateActions = TemplateListAction |
  CreateTemplateAction |
  UpdateTemplateAction |
  RemoveTemplateAction;