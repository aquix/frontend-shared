import { ISMTPReducer } from '../../types';
import {
  CREATE_SMTP_ACTION,
  CreateSMTPAction, REMOVE_SMTP_ACTION, RemoveSMTPAction,
  SMTP_LIST_ACTION,
  SMTPActions,
  SmtpListAction,
  UPDATE_SMTP_ACTION, UpdateSMTPAction,
} from '../action/smtp';

const defaultState: ISMTPReducer = {
  list: [],
}

const SMTPReducer = (
  state: ISMTPReducer = defaultState,
  action: SMTPActions,
): ISMTPReducer => {
  if (action.type === SMTP_LIST_ACTION) {
    const list = (action as SmtpListAction).payload;
    return {
      ...state,
      list
    }
  }

  if (action.type === CREATE_SMTP_ACTION) {
    const item = (action as CreateSMTPAction).payload;

    return {
      ...state,
      list: [
        item,
        ...state.list,
      ]
    }
  }

  if (action.type === UPDATE_SMTP_ACTION) {
    const item = (action as UpdateSMTPAction).payload;
    const index = state.list.findIndex(i => i.id === item.id);

    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        item,
        ...state.list.slice(index + 1),
      ]
    }
  }

  if (action.type === REMOVE_SMTP_ACTION) {
    const id = (action as RemoveSMTPAction).payload;
    const index = state.list.findIndex(i => i.id === id);

    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        ...state.list.slice(index + 1),
      ]
    }
  }

  return state;
}

export default SMTPReducer;