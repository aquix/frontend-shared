import { INotificationReducer } from '../../types';
import {
  NOTIFICATION_COUNT_ACTION,
  NOTIFICATION_LIST_ACTION,
  NotificationActions, NotificationCountAction,
  NotificationListAction,
} from '../action/notification';

const defaultState: INotificationReducer = {
  list: [],
  count: 0,
}

const NotificationReducer = (
  state: INotificationReducer = defaultState,
  action: NotificationActions,
): INotificationReducer => {
  if (action.type === NOTIFICATION_LIST_ACTION) {
    const list = (action as NotificationListAction).payload;
    return {
      ...state,
      list
    }
  }

  if (action.type === NOTIFICATION_COUNT_ACTION) {
    const count = (action as NotificationCountAction).payload;
    return {
      ...state,
      count
    }
  }

  return state;
}

export default NotificationReducer;