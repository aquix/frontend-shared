import { combineReducers } from 'redux';
import SMTPReducer from './smtp';
import { INotifyReducer } from '../../types/reducer';
import TemplateReducer from './template';
import NotificationReducer from './notification';

export const NotifyReducer = combineReducers<INotifyReducer>({
  smtp: SMTPReducer,
  template: TemplateReducer,
  notification: NotificationReducer,
})