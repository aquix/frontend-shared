import { ITemplateReducer } from '../../types';
import {
  CREATE_TEMPLATE_ACTION, CreateTemplateAction,
  REMOVE_TEMPLATE_ACTION, RemoveTemplateAction,
  TEMPLATE_LIST_ACTION,
  TemplateActions,
  TemplateListAction,
  UPDATE_TEMPLATE_ACTION, UpdateTemplateAction,
} from '../action/template';

const defaultState: ITemplateReducer = {
  list: [],
}

const TemplateReducer = (
  state: ITemplateReducer = defaultState,
  action: TemplateActions,
): ITemplateReducer => {
  if (action.type === TEMPLATE_LIST_ACTION) {
    const list = (action as TemplateListAction).payload;
    return {
      ...state,
      list
    }
  }

  if (action.type === CREATE_TEMPLATE_ACTION) {
    const item = (action as CreateTemplateAction).payload;

    return {
      ...state,
      list: [
        item,
        ...state.list,
      ]
    }
  }

  if (action.type === UPDATE_TEMPLATE_ACTION) {
    const item = (action as UpdateTemplateAction).payload;
    const index = state.list.findIndex(i => i.id === item.id);

    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        item,
        ...state.list.slice(index + 1),
      ]
    }
  }

  if (action.type === REMOVE_TEMPLATE_ACTION) {
    const id = (action as RemoveTemplateAction).payload;
    const index = state.list.findIndex(i => i.id === id);

    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        ...state.list.slice(index + 1),
      ]
    }
  }

  return state;
}

export default TemplateReducer;