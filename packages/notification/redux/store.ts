import { createStore, applyMiddleware, AnyAction } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { INotifyReducer } from '../types/reducer';
import { NotifyReducer } from './reducer/notifyReducer';

const loggerMiddleware = createLogger();
const initialState = {} as INotifyReducer;

let middleware;

if (process.env.NODE_ENV === 'production') {
  middleware = applyMiddleware(thunkMiddleware, loggerMiddleware);
} else {
  middleware = applyMiddleware(thunkMiddleware, loggerMiddleware);
}

export default createStore<INotifyReducer, AnyAction, {}, {}>(NotifyReducer, initialState, middleware);