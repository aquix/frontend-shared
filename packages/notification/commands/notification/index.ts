import { ListWithLimitRequest } from '../../types/common_pb';
import { ThunkDispatch } from 'redux-thunk';
import { ActionModel } from '@frontend-shared/node-editor/types/common_pb';
import { AnyAction } from 'redux';
import { NotificationClient } from './notification_grpc_web_pb';
import { notificationCountAction, notificationListAction } from '../../redux/action/notification';

const notificationClient = new NotificationClient(String(process.env.ENVOY_PROXY), null, null);

export const loadNotifications = (value: ListWithLimitRequest) => (dispatch: ThunkDispatch<ActionModel[], {}, AnyAction>) => {
  return notificationClient.listWithLimit(value, {}, (err, response) => {
    dispatch(notificationListAction(response.getRawsList().map(i => i.toObject())));
    dispatch(notificationCountAction(response.getCount()));
  });
}