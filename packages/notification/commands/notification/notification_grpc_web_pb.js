/**
 * @fileoverview gRPC-Web generated client stub for notificationMs
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var common_pb = require('../../types/common_pb.js')

var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = {};
proto.notificationMs = require('./notification_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.notificationMs.NotificationClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.notificationMs.NotificationPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.CreateNotificationRequest,
 *   !proto.notificationMs.NotificationModel>}
 */
const methodDescriptor_Notification_Create = new grpc.web.MethodDescriptor(
  '/notificationMs.Notification/Create',
  grpc.web.MethodType.UNARY,
  common_pb.CreateNotificationRequest,
  common_pb.NotificationModel,
  /**
   * @param {!proto.notificationMs.CreateNotificationRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  common_pb.NotificationModel.deserializeBinary
);

/**
 * @param {!proto.notificationMs.CreateNotificationRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.notificationMs.NotificationModel)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.NotificationModel>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.NotificationClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Notification/Create',
      request,
      metadata || {},
      methodDescriptor_Notification_Create,
      callback);
};


/**
 * @param {!proto.notificationMs.CreateNotificationRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.notificationMs.NotificationModel>}
 *     Promise that resolves to the response
 */
proto.notificationMs.NotificationPromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Notification/Create',
      request,
      metadata || {},
      methodDescriptor_Notification_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.NotificationModel,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Notification_Update = new grpc.web.MethodDescriptor(
  '/notificationMs.Notification/Update',
  grpc.web.MethodType.UNARY,
  common_pb.NotificationModel,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.NotificationModel} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.NotificationModel} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.NotificationClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Notification/Update',
      request,
      metadata || {},
      methodDescriptor_Notification_Update,
      callback);
};


/**
 * @param {!proto.notificationMs.NotificationModel} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.NotificationPromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Notification/Update',
      request,
      metadata || {},
      methodDescriptor_Notification_Update);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Notification_DeleteExpired = new grpc.web.MethodDescriptor(
  '/notificationMs.Notification/DeleteExpired',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.NotificationClient.prototype.deleteExpired =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Notification/DeleteExpired',
      request,
      metadata || {},
      methodDescriptor_Notification_DeleteExpired,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.NotificationPromiseClient.prototype.deleteExpired =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Notification/DeleteExpired',
      request,
      metadata || {},
      methodDescriptor_Notification_DeleteExpired);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.IdRequest,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Notification_ReadOne = new grpc.web.MethodDescriptor(
  '/notificationMs.Notification/ReadOne',
  grpc.web.MethodType.UNARY,
  common_pb.IdRequest,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.IdRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.IdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.NotificationClient.prototype.readOne =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Notification/ReadOne',
      request,
      metadata || {},
      methodDescriptor_Notification_ReadOne,
      callback);
};


/**
 * @param {!proto.notificationMs.IdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.NotificationPromiseClient.prototype.readOne =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Notification/ReadOne',
      request,
      metadata || {},
      methodDescriptor_Notification_ReadOne);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.UserIdRequest,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Notification_ReadAll = new grpc.web.MethodDescriptor(
  '/notificationMs.Notification/ReadAll',
  grpc.web.MethodType.UNARY,
  common_pb.UserIdRequest,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.UserIdRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.UserIdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.NotificationClient.prototype.readAll =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Notification/ReadAll',
      request,
      metadata || {},
      methodDescriptor_Notification_ReadAll,
      callback);
};


/**
 * @param {!proto.notificationMs.UserIdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.NotificationPromiseClient.prototype.readAll =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Notification/ReadAll',
      request,
      metadata || {},
      methodDescriptor_Notification_ReadAll);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.ListWithLimitRequest,
 *   !proto.notificationMs.ListWithLimitModel>}
 */
const methodDescriptor_Notification_ListWithLimit = new grpc.web.MethodDescriptor(
  '/notificationMs.Notification/ListWithLimit',
  grpc.web.MethodType.UNARY,
  common_pb.ListWithLimitRequest,
  common_pb.ListWithLimitModel,
  /**
   * @param {!proto.notificationMs.ListWithLimitRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  common_pb.ListWithLimitModel.deserializeBinary
);

/**
 * @param {!proto.notificationMs.ListWithLimitRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.notificationMs.ListWithLimitModel)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.ListWithLimitModel>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.NotificationClient.prototype.listWithLimit =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Notification/ListWithLimit',
      request,
      metadata || {},
      methodDescriptor_Notification_ListWithLimit,
      callback);
};


/**
 * @param {!proto.notificationMs.ListWithLimitRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.notificationMs.ListWithLimitModel>}
 *     Promise that resolves to the response
 */
proto.notificationMs.NotificationPromiseClient.prototype.listWithLimit =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Notification/ListWithLimit',
      request,
      metadata || {},
      methodDescriptor_Notification_ListWithLimit);
};


module.exports = proto.notificationMs;

