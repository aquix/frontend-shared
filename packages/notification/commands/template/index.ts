import { ThunkDispatch } from 'redux-thunk';
import { ActionModel } from '@frontend-shared/node-editor/types/common_pb';
import { AnyAction } from 'redux';
import { TemplateClient } from './template_grpc_web_pb';
import { IdRequest, ListOptions, TemplateCreate, TemplateModel } from '../../types/common_pb';
import { createTemplateAction, removeTemplateAction, templateListAction } from '../../redux/action/template';
import { successMessage } from '@frontend-shared/common';
import { errorHandler } from '@frontend-shared/common';

const templateClient = new TemplateClient(String(process.env.ENVOY_PROXY), null, null);

export const loadTemplates = (value: ListOptions) => (dispatch: ThunkDispatch<ActionModel[], {}, AnyAction>) => {
  const list: TemplateModel.AsObject[] = [];
  return templateClient.list(value).on('data', (e) => {
    list.push(e.toObject());
  })
    .on('error', (err) => {
      errorHandler(err);
    })
    .on('end', () => {
      dispatch(templateListAction(list));
    });
}

export const createTemplate = (value: TemplateCreate) => (dispatch: ThunkDispatch<TemplateCreate, {}, AnyAction>) => {
  return templateClient.create(value, { }, (err, response) => {
    errorHandler(err);
    dispatch(createTemplateAction(response.toObject()));
    dispatch(successMessage({ message: 'common.success' }));
  });
}

export const updateTemplate = (value: TemplateModel) => (dispatch: ThunkDispatch<TemplateModel, {}, AnyAction>) => {
  return templateClient.update(value, { }, (err, response) => {
    errorHandler(err);
    dispatch(createTemplateAction(value.toObject()));
    dispatch(successMessage({ message: 'common.success' }));
  });
}

export const removeTemplate = (value: IdRequest) => (dispatch: ThunkDispatch<TemplateModel, {}, AnyAction>) => {
  return templateClient.delete(value, { }, (err, response) => {
    errorHandler(err);
    dispatch(removeTemplateAction(value.getId()));
    dispatch(successMessage({ message: 'common.success' }));
  });
}