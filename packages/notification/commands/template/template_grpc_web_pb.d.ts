import * as grpcWeb from 'grpc-web';

import * as google_protobuf_empty_pb from 'google-protobuf/google/protobuf/empty_pb';
import * as common_pb from '../../types/common_pb';


export class TemplateClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  send(
    request: common_pb.SendRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: google_protobuf_empty_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<google_protobuf_empty_pb.Empty>;

  create(
    request: common_pb.TemplateCreate,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: common_pb.TemplateModel) => void
  ): grpcWeb.ClientReadableStream<common_pb.TemplateModel>;

  update(
    request: common_pb.TemplateModel,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: google_protobuf_empty_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<google_protobuf_empty_pb.Empty>;

  delete(
    request: common_pb.IdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: google_protobuf_empty_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<google_protobuf_empty_pb.Empty>;

  list(
    request: common_pb.ListOptions,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<common_pb.TemplateModel>;

}

export class TemplatePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  send(
    request: common_pb.SendRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<google_protobuf_empty_pb.Empty>;

  create(
    request: common_pb.TemplateCreate,
    metadata?: grpcWeb.Metadata
  ): Promise<common_pb.TemplateModel>;

  update(
    request: common_pb.TemplateModel,
    metadata?: grpcWeb.Metadata
  ): Promise<google_protobuf_empty_pb.Empty>;

  delete(
    request: common_pb.IdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<google_protobuf_empty_pb.Empty>;

  list(
    request: common_pb.ListOptions,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<common_pb.TemplateModel>;

}

