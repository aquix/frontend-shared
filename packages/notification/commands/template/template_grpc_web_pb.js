/**
 * @fileoverview gRPC-Web generated client stub for notificationMs
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var common_pb = require('../../types/common_pb.js')

var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = {};
proto.notificationMs = require('./template_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.notificationMs.TemplateClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.notificationMs.TemplatePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.SendRequest,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Template_Send = new grpc.web.MethodDescriptor(
  '/notificationMs.Template/Send',
  grpc.web.MethodType.UNARY,
  common_pb.SendRequest,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.SendRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.SendRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.TemplateClient.prototype.send =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Template/Send',
      request,
      metadata || {},
      methodDescriptor_Template_Send,
      callback);
};


/**
 * @param {!proto.notificationMs.SendRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.TemplatePromiseClient.prototype.send =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Template/Send',
      request,
      metadata || {},
      methodDescriptor_Template_Send);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.TemplateCreate,
 *   !proto.notificationMs.TemplateModel>}
 */
const methodDescriptor_Template_Create = new grpc.web.MethodDescriptor(
  '/notificationMs.Template/Create',
  grpc.web.MethodType.UNARY,
  common_pb.TemplateCreate,
  common_pb.TemplateModel,
  /**
   * @param {!proto.notificationMs.TemplateCreate} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  common_pb.TemplateModel.deserializeBinary
);

/**
 * @param {!proto.notificationMs.TemplateCreate} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.notificationMs.TemplateModel)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.TemplateModel>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.TemplateClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Template/Create',
      request,
      metadata || {},
      methodDescriptor_Template_Create,
      callback);
};


/**
 * @param {!proto.notificationMs.TemplateCreate} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.notificationMs.TemplateModel>}
 *     Promise that resolves to the response
 */
proto.notificationMs.TemplatePromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Template/Create',
      request,
      metadata || {},
      methodDescriptor_Template_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.TemplateModel,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Template_Update = new grpc.web.MethodDescriptor(
  '/notificationMs.Template/Update',
  grpc.web.MethodType.UNARY,
  common_pb.TemplateModel,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.TemplateModel} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.TemplateModel} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.TemplateClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Template/Update',
      request,
      metadata || {},
      methodDescriptor_Template_Update,
      callback);
};


/**
 * @param {!proto.notificationMs.TemplateModel} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.TemplatePromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Template/Update',
      request,
      metadata || {},
      methodDescriptor_Template_Update);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.IdRequest,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Template_Delete = new grpc.web.MethodDescriptor(
  '/notificationMs.Template/Delete',
  grpc.web.MethodType.UNARY,
  common_pb.IdRequest,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.IdRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.IdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.TemplateClient.prototype.delete =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.Template/Delete',
      request,
      metadata || {},
      methodDescriptor_Template_Delete,
      callback);
};


/**
 * @param {!proto.notificationMs.IdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.TemplatePromiseClient.prototype.delete =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.Template/Delete',
      request,
      metadata || {},
      methodDescriptor_Template_Delete);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.ListOptions,
 *   !proto.notificationMs.TemplateModel>}
 */
const methodDescriptor_Template_List = new grpc.web.MethodDescriptor(
  '/notificationMs.Template/List',
  grpc.web.MethodType.SERVER_STREAMING,
  common_pb.ListOptions,
  common_pb.TemplateModel,
  /**
   * @param {!proto.notificationMs.ListOptions} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  common_pb.TemplateModel.deserializeBinary
);

/**
 * @param {!proto.notificationMs.ListOptions} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.TemplateModel>}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.TemplateClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/notificationMs.Template/List',
      request,
      metadata || {},
      methodDescriptor_Template_List);
};


/**
 * @param {!proto.notificationMs.ListOptions} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.TemplateModel>}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.TemplatePromiseClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/notificationMs.Template/List',
      request,
      metadata || {},
      methodDescriptor_Template_List);
};


module.exports = proto.notificationMs;

