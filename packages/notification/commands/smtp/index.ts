import { IdRequest, SMTPCreate, SMTPModel } from '../../types/common_pb';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { errorHandler } from '@frontend-shared/common';
import { successMessage } from '@frontend-shared/common';
import { SMTPClient } from './smtp_grpc_web_pb';
import { createSMTPAction, removeSMTPAction, smtpListAction, updateSMTPAction } from '../../redux/action/smtp';
import { Empty } from 'google-protobuf/google/protobuf/empty_pb';

const smtpClient = new SMTPClient(String(process.env.ENVOY_PROXY), null, null);

export const loadSMTPList = () => (dispatch: ThunkDispatch<SMTPModel[], {}, AnyAction>) => {
  const list: SMTPModel.AsObject[] = [];
  return smtpClient.list(new Empty).on('data', (e) => {
    list.push(e.toObject());
  })
    .on('error', (err) => {
      errorHandler(err);
    })
    .on('end', () => {
      dispatch(smtpListAction(list));
    });
}

export const createSMTP = (value: SMTPCreate) => (dispatch: ThunkDispatch<SMTPModel, {}, AnyAction>) => {
  return smtpClient.create(value, { }, (err, response) => {
    errorHandler(err);
    dispatch(createSMTPAction(response.toObject()));
    dispatch(successMessage({ message: 'common.success' }));
  });
}

export const updateSMTP = (value: SMTPModel) => (dispatch: ThunkDispatch<SMTPModel, {}, AnyAction>) => {
  return smtpClient.update(value, { }, (err, response) => {
    errorHandler(err);
    dispatch(updateSMTPAction(value.toObject()));
    dispatch(successMessage({ message: 'common.success' }));
  });
}

export const removeSMTP = (value: IdRequest) => (dispatch: ThunkDispatch<SMTPModel, {}, AnyAction>) => {
  return smtpClient.delete(value, { }, (err, response) => {
    errorHandler(err);
    dispatch(removeSMTPAction(value.getId()));
    dispatch(successMessage({ message: 'common.success' }));
  });
}