/**
 * @fileoverview gRPC-Web generated client stub for notificationMs
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var common_pb = require('../../types/common_pb.js')

var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = {};
proto.notificationMs = require('./smtp_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.notificationMs.SMTPClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.notificationMs.SMTPPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.SMTPCreate,
 *   !proto.notificationMs.SMTPModel>}
 */
const methodDescriptor_SMTP_Create = new grpc.web.MethodDescriptor(
  '/notificationMs.SMTP/Create',
  grpc.web.MethodType.UNARY,
  common_pb.SMTPCreate,
  common_pb.SMTPModel,
  /**
   * @param {!proto.notificationMs.SMTPCreate} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  common_pb.SMTPModel.deserializeBinary
);

/**
 * @param {!proto.notificationMs.SMTPCreate} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.notificationMs.SMTPModel)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.SMTPModel>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.SMTPClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.SMTP/Create',
      request,
      metadata || {},
      methodDescriptor_SMTP_Create,
      callback);
};


/**
 * @param {!proto.notificationMs.SMTPCreate} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.notificationMs.SMTPModel>}
 *     Promise that resolves to the response
 */
proto.notificationMs.SMTPPromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.SMTP/Create',
      request,
      metadata || {},
      methodDescriptor_SMTP_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.SMTPModel,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_SMTP_Update = new grpc.web.MethodDescriptor(
  '/notificationMs.SMTP/Update',
  grpc.web.MethodType.UNARY,
  common_pb.SMTPModel,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.SMTPModel} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.SMTPModel} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.SMTPClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.SMTP/Update',
      request,
      metadata || {},
      methodDescriptor_SMTP_Update,
      callback);
};


/**
 * @param {!proto.notificationMs.SMTPModel} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.SMTPPromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.SMTP/Update',
      request,
      metadata || {},
      methodDescriptor_SMTP_Update);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.notificationMs.IdRequest,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_SMTP_Delete = new grpc.web.MethodDescriptor(
  '/notificationMs.SMTP/Delete',
  grpc.web.MethodType.UNARY,
  common_pb.IdRequest,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.notificationMs.IdRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);

/**
 * @param {!proto.notificationMs.IdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.SMTPClient.prototype.delete =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/notificationMs.SMTP/Delete',
      request,
      metadata || {},
      methodDescriptor_SMTP_Delete,
      callback);
};


/**
 * @param {!proto.notificationMs.IdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.notificationMs.SMTPPromiseClient.prototype.delete =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/notificationMs.SMTP/Delete',
      request,
      metadata || {},
      methodDescriptor_SMTP_Delete);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.notificationMs.SMTPModel>}
 */
const methodDescriptor_SMTP_List = new grpc.web.MethodDescriptor(
  '/notificationMs.SMTP/List',
  grpc.web.MethodType.SERVER_STREAMING,
  google_protobuf_empty_pb.Empty,
  common_pb.SMTPModel,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  common_pb.SMTPModel.deserializeBinary
);

/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.SMTPModel>}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.SMTPClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/notificationMs.SMTP/List',
      request,
      metadata || {},
      methodDescriptor_SMTP_List);
};


/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.notificationMs.SMTPModel>}
 *     The XHR Node Readable Stream
 */
proto.notificationMs.SMTPPromiseClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/notificationMs.SMTP/List',
      request,
      metadata || {},
      methodDescriptor_SMTP_List);
};


module.exports = proto.notificationMs;

