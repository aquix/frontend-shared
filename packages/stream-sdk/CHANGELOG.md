# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.5](https://gitlab.com/aquix/frontend-shared/compare/v1.1.19...v1.2.5) (2022-05-11)

**Note:** Version bump only for package @frontend-shared/stream-sdk





## [1.0.6](https://gitlab.com/4sdev/langco/langco-react/compare/@stream/stream-sdk@1.0.5...@stream/stream-sdk@1.0.6) (2021-11-13)

**Note:** Version bump only for package @stream/stream-sdk





## 1.0.5 (2021-11-13)

**Note:** Version bump only for package @stream/stream-sdk





## 1.0.3 (2021-11-13)

**Note:** Version bump only for package stream-sdk





## 1.0.2 (2021-11-13)

**Note:** Version bump only for package stream-sdk





## 1.0.1 (2021-11-13)

**Note:** Version bump only for package stream-sdk
