export interface IGeneralDocumentsProps {
  lang?: string
  domain?: string
  actualPage?: string
  site?: string
}
