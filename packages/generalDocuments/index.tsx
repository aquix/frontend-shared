import React from 'react';
import PrivacyPolicy from './pages/privacy-policy';
import UserAgreement from './pages/user-agreement';
import RiskDisclosure from './pages/risk-disclosure';
import Aml from './pages/aml';
import "./main.css";

export { PrivacyPolicy, UserAgreement, RiskDisclosure, Aml };
