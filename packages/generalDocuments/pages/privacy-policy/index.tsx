import React, { FunctionComponent } from 'react';
import PageRu from './PageRu';
import PageEn from './PageEn';
import { IGeneralDocumentsProps } from '../../types';

const PrivacyPolicy: FunctionComponent<IGeneralDocumentsProps> = (props) => {
  const { lang, domain, actualPage } = props;
  if (lang === 'ru') {
    return (
      <PageRu domain={domain} actualPage={actualPage} />
    );
  } else {
    return (<PageEn domain={domain} actualPage={actualPage} />);
  }
};

export default PrivacyPolicy;
