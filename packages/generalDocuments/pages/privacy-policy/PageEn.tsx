import React, { FunctionComponent } from 'react';
import { IGeneralDocumentsProps } from '../../types';

const PageEn: FunctionComponent<IGeneralDocumentsProps> = (props) => {
  const { domain, actualPage } = props;
  return (
    <div className="content-container__content">
      <div className="numeric-list">
        <h1>Personal Data Processing Policy</h1>
        <ol>
          <li>General terms<br />This Personal Data Processing Policy is made in accordance with the requirements of
            the
            legislation of Georgia.
            <ul>
              <li>When processing personal data, the operator sets the observance of human and civil rights and
                freedoms, including the protection of the rights to privacy, personal and family secrets as its most
                important goal and condition for the implementation of activities.
              </li>
              <li>This Operator's policy regarding the processing of personal data (hereinafter – the Policy)
                applies
                to
                all information that the Operator may receive about visitors to the website {domain}.
              </li>
            </ul>
          </li>
          <li>Basic concepts used in the Policy
            <ul>
              <li>Automated processing of personal data – processing of personal data by means of computer
                technology;
              </li>
              <li>Blocking of personal data – temporary termination of personal data processing (except for cases
                when
                processing is necessary to clarify personal data);
              </li>
              <li>Website – a set of graphics and information materials, as well as computer programs and databases
                that
                ensure their availability on the Internet at a network address {domain};
              </li>
              <li>Information system of personal data — a set of personal data contained in databases and providing
                their processing of information technologies and technical means;
              </li>
              <li>Depersonalization of personal data — actions as a result of which it is impossible to determine
                the
                identity of personal data to a particular User or other subject of personal data without the use of
                additional information;
              </li>
              <li>Personal data processing – any action (operation) or a set of actions (operations) performed with
                the
                use of automation tools or without the use of such tools with personal data, including the
                collection,
                recording, systematization, accumulation, storage, clarification (update, change), extraction, use,
                transfer (distribution, provision, access), depersonalization, blocking, deletion, destruction of
                personal data;
              </li>
              <li>Operator – a state body, municipal body, legal entity or individual, independently or jointly with
                other persons organizing and (or) carrying out the processing of personal data, as well as
                determining
                the purpose of processing personal data, the composition of personal data to be processed, actions
                (operations) performed with personal data;
              </li>
              <li>Personal data – any information relating directly or indirectly to a particular or identifiable
                User
                of the website {domain};
              </li>
              <li>User – any visitor to the website {domain};</li>
              <li>Provision of personal data – actions aimed at disclosure of personal data to a certain person or a
                certain circle of persons;
              </li>
              <li>Distribution of personal data – any actions aimed at disclosure of personal data to an indefinite
                circle of persons (transfer of personal data) or familiarization with personal data of an unlimited
                circle of persons, including disclosure of personal data in the media, placement in information and
                telecommunication networks or providing access to personal data in any other way;
              </li>
              <li>Cross-border transfer of personal data – transfer of personal data to the territory of a foreign
                state
                to the authority of a foreign state, a foreign individual or a foreign legal entity;
              </li>
              <li>Destruction of personal data – any action in which personal data is destroyed irrevocably
                impossible
                to restore the contents of personal data in the information system of personal data and (or) which
                are
                destroyed material carriers of personal data.
              </li>
            </ul>
          </li>
          <li>The operator may process the following personal data of the User
            <ul>
              <li>Surname, name, middle name (patronymic);</li>
              <li>Phone number;</li>
              <li>Email address;</li>
              <li>The details of the document proving the identity, and their scanned image.</li>
              <li>Also on the website there is the collection and processing of anonymous data about visitors
                (including
                "cookies") through the services of Internet statistics ("Yandex Metric, Google Analytics" and
                others).
              </li>
              <li>The aforesaid data hereinafter in the text of the Policy are united by the general concept of
                Personal
                data.
              </li>
            </ul>
          </li>
          <li>Purposes of personal data processing
            <ul>
              <li>The purpose of processing the User's personal data is making, execution and termination of civil
                contracts; providing the User with access to the services, information and/or materials contained on
                the
                website {domain}; clarification of order details.
              </li>
              <li>The Operator also has the right to send the User notifications about new products and services,
                special offers and various events, as well as notifications about promotions.
              </li>
              <li>The user can always refuse to receive information messages by sending an email to the Operator
                policy@aquix.pro marked "Opt-out of notifications of new products, services and special offers".
              </li>
              <li>Depersonalized User data collected through Internet statistics services are used to collect
                information about Users' actions on the site, improve the quality of the website and its content.
              </li>
            </ul>
          </li>
          <li>Legal grounds for processing personal data
            <ul>
              <li>The operator processes the User's personal data only if they are filled in and/or sent by the User
                himself/herself through special forms located on the website {domain}. By filling in the
                appropriate forms and/or sending his/her personal data to the Operator, the User agrees to this
                Policy.
              </li>
              <li>The operator processes impersonal data about the User in case it is allowed in the settings of the
                User's browser (the saving of cookies and the use of JavaScript technology is enabled).
              </li>
            </ul>
          </li>
          <li>Procedure for collection, storage, transfer and other types of personal data processing
            <ul>
              <li>The security of personal data processed by the Operator is ensured through the implementation of
                legal, organizational and technical measures necessary to fully comply with the requirements of the
                current legislation in the field of personal data protection.
              </li>
              <li>The operator ensures the safety of personal data and takes all possible measures to exclude access
                to
                personal data of unauthorized persons.
              </li>
              <li>Personal data of the User will never, under any circumstances, be transferred to third parties,
                except
                in cases related to the implementation of the current legislation.
              </li>
              <li>In case of inaccuracies in personal data, the User can update them independently by sending a
                notification to the Operator's e-mail Address policy@aquix.pro marked "Updating of personal data".
              </li>
              <li>The operator can carry out any actions on the disposal of personal data of the user, to which the
                latter agrees in advance.
              </li>
              <li>In more detail, the procedure for the use and disposal of personal data, as well as their transfer
                is
                reflected in the user agreement of the site.
              </li>
            </ul>
          </li>
          <li>Cross-border transfer of personal data
            <ul>
              <li>The operator is obliged to ensure that the rights of personal data subjects are reliably protected
                by
                the foreign state to whose territory the transfer of personal data is to be carried out before the
                start
                of the cross-border transfer of personal data.
              </li>
              <li>Cross-border transfer of personal data on the territory of foreign states that do not meet the
                above
                requirements may be carried out without the written consent of the personal data subject to
                cross-border
                transfer of his personal data and/or execution of the contract to which the personal data subject is
                a
                party.
              </li>
            </ul>
          </li>
          <li>Final provision
            <ul>
              <li>The user can receive any clarifications on issues of interest relating to the processing of his
                personal data by contacting the Operator via e-mail policy@aquix.pro.
              </li>
              <li>This document will reflect any changes to the Operator's personal data processing policy. The
                policy
                is valid indefinitely until replaced by a new version.
              </li>
              <li>The current version of the Policy is freely available on the Internet at <a
                href={`${domain}${actualPage}`}>{domain}{actualPage}</a>.
              </li>
            </ul>
          </li>
        </ol>
      </div>
    </div>
  );
};

export default PageEn;
