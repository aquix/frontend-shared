import React, { FunctionComponent } from 'react';
import PageRu from './PageRu';
import PageEn from './PageEn';
import { IGeneralDocumentsProps } from '../../types';


const UserAgreement: FunctionComponent<IGeneralDocumentsProps> = (props) => {
  const { lang, domain, actualPage } = props;
  const re = new RegExp(`https?:\\/\\/(?:www\\.|)([\\w.-]+).*`);
  const res =  domain && domain.match(re);
  const site = !!res ? res[1] : domain

  if (lang === 'ru' || lang === 'uk') {
    return (
      <PageRu domain={domain} actualPage={actualPage} site={site} />
    );
  } else {
    return (
      <PageEn domain={domain} actualPage={actualPage} site={site} />
    );
  }
};

export default UserAgreement;
