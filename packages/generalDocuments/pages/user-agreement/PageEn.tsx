import React, { FunctionComponent } from 'react';
import { IGeneralDocumentsProps } from '../../types';

const PageEn: FunctionComponent<IGeneralDocumentsProps> = (props) => {
  const { domain, actualPage, site } = props;
  return (
    <div className="content-container__content">
      <div className="numeric-list">
        <h1>User agreement</h1>
        <h2>Terms and definitions.</h2>
        <p><b>Agreement</b> is this user agreement defining the rights and obligations of the copyright Holder, Users
          and third parties.</p>
        <p><b>Platform</b> is the website located at: {domain}.</p>
        <p><b>Right Holder</b> is the owner of the Platform.</p>
        <p><b>Business User</b> is a capable individual, individual entrepreneur or legal entity who has visited and /
          or uses the Platform to implement their business processes, as well as promote business projects, the whose
          status depends on the Registration, and whose opportunities depend on the type of the Service used.</p>
        <p>
          <b>User</b> is a legally capable individual, individual entrepreneur or legal entity who has visited and / or
          uses the Platform for purposes not related to the implementation of business processes, whose status depends
          on the Registration, and whose opportunities depend on the type of the Service used.
        </p>
        <p>
          <b>Registration</b> is the established procedure of actions after which the User is granted access to the
          Account and Services.
        </p>
        <p>
          <b>Account</b> is a unique account that allows the Administrator to identify the User by login and password
          and provides the User with access to the Personal backoffice.
        </p>
        <p>
          <b>Service</b> is part of the platform / website, endowed with certain functionality to provide services to
          the Users.
        </p>
        <p>
          <b>Content</b> is any objects placed on the platform / website including texts, comments, design elements,
          graphics, illustrations, video and photo materials, etc., which belong to the Right Holder, Users, third
          parties.
        </p>
        <p>
          <b>Applications</b> are rules governing the provision of Services, which are an integral part of the Agreement
          and subject to application in the presence of contradictions with the text of the Agreement.
        </p>
        <p>
          <b>Product</b> are services provided by the Company for the development of the computer programs, grouped on
          the website depending on the nature, conditions and other rules.
        </p>
        <p>
          <b>Consent</b> is the document "Consent to personal data processing" which determines the procedure for
          processing personal data and is an integral part of the Agreement.
        </p>
        <p>
          <b>Means of payment</b> are the financial instruments developed and implemented on the Platform that are owned
          and fully controlled by the Business User and used by them to promote their business projects. The Platform
          has no legal relation to the means of payment offered by the Business User.
        </p>
        <p>
          <b>Place of Conclusion and Execution</b> of this Agreement is Tbilisi, Georgia.
        </p>

        <ol>
          <li><b>General terms.</b><br /><br />
            <ol>
              <li>
                Joint Stock Company "AQUIX", located at: Uznadze str., 111, b. 11, Building 2, Didube district, Tbilisi,
                Georgia, hereinafter the Company), acting legally in the person of the Director Artem Balyakno and is
                the licensee of the website located on the Internet at: {domain} (hereinafter – {site}), (developer and
                copyright holder {site} is Joint Stock Company "AQUIX" (hereinafter the Right Holder) offers the
                Internet user (capable physical identity using {site} in its own interests or acting on behalf of and in
                the interests of the legal entity represented by it) (hereinafter the User) to use {site} and the
                services placed on it (hereinafter the Services) on the terms set forth in this user agreement
                (hereinafter – the Agreement). The Agreement shall enter into force from the moment the User agrees to
                its terms in the manner provided for in clause 1.4 of the Agreement.
                <br /><br />
                <ol>
                  <li>
                    Artem Balyakno is a Business User of the Platform for the implementation of his venture project
                    "AQUIX", in accordance with which he has the ability to use the Platform and its functionality to
                    promote his business project and implement payment methods in accordance with the provisions of
                    4.5.2 and 4.5.3. of this Agreement.
                  </li>
                </ol>
              </li>
              <li>
                This Agreement governs only the terms and conditions of {site} using. The provisions of this Agreement
                are mandatory for all the Users who have registered through {site}, or started using any of the
                Services.
                <br /><br />
                The Company offers the User on the terms of the Agreement to use the available on {site} opportunities
                and the Services.
              </li>
              <li>
                The use of the Services is also regulated by the Company's agreements placed in each section of the
                respective Service on {site}. This Agreement may be amended by the Company without any special
                notifications to the User, to which the User agrees in advance. The new version of the Agreement shall
                enter into force upon its posting on the Internet at the address specified in this paragraph, unless
                otherwise provided by the new version of the Agreement. The current version of the Agreement is on the
                webpage {site}.
              </li>
              <li>
                From the moment of opening any page of any of sections on {site} by the User from any technical device
                in the Internet browser, including any mobile application {site} as well as at the beginning of the use
                of any Service, the User agrees to the terms of the Agreement in full, without any reservations and
                exceptions.
                <br /><br />
                Accessing services of {site} the User acknowledges that he/she has concluded and approved this Agreement
                and contracts accompanying each of the types of services and products, and also agrees to perform
                actions on the part of the Company to execute the concluded agreements.
                <br /><br />
                At the same time, obtaining access, as well as any other type of use by the User of Products and
                Services (purchase of the Product, provision of property and any other actions arising from the
                execution of contracts or related to them) offered by the Company are qualified as conclusive actions
                and entail the conclusion of the contract.
                <br /><br />
                If the User disagrees with any of the provisions of the Agreement, the User may not use {site} and must
                refrain from using it. If the Company has made any changes to the Agreement in the manner provided for
                in paragraph 1.3. of this Agreement with which the User does not agree, he/she is obliged to stop
                using {site}.
                <br /><br />
                The user's disagreement with the rules of rendering services, Products and the Services of the Company
                can be expressed only in the form of presentation to the Company of the message with the developed
                motivation (claim) and actual refusal or immediate termination of use of any products and services of
                the Company. Notification by the User of the Company of disagreement with certain terms of interaction,
                without actual termination of the use of the Company's services, does not indicate disagreement of the
                User. Continued use of the Services and the Products of the Company, even after expressed disagreement,
                qualifies as approval of the terms by the User, expressed in a passive form.
              </li>
            </ol>
          </li>
          <li><b>User’s personal account.</b><br /><br />
            <ol>
              <li>
                By posting credentials and other data and information and joining the Agreement, the User expresses
                his/her consent to the Company to process credentials and other data, to reflect them in the User’s
                profile. The User agrees to transfer accounting and other data to third parties, including for the
                purposes of their processing, to ensure the functioning of {site} services, implementation of
                partnership and other programs and the Services of the Company.
              </li>
              <li>
                The purpose of processing the User’s credentials is to provide the latter with the possibility of using
                the Services and granting the rights to use additional functional software capabilities of {site},
                carrying out advertising campaigns, conducting statistical research and analysis of the statistical data
                obtained, the implementation of other actions described in the Agreement. Processing of the User’s
                credentials is carried out within the period from the moment of registration of the User and until the
                moment of deletion of his/her account.
              </li>
              <li>
                The User agrees that the Company in the process of working with the accounting data has the right to
                perform the following actions with the accounting data: collection, systematization, accumulation,
                storage, use, destruction and other necessary for the purpose of execution of the Agreement. After
                registering an account on {site} the Company has the right to fill the User's account with content,
                including adding information about the User's orders, placing advertising and other information. The
                User undertakes not to post on {site} Services e-mail addresses and other personal information of the
                other Users or any third parties without their personal consent to such actions.
              </li>
              <li>
                The User is personally responsible for the safety and security of his/her password and information data.
              </li>
              <li>
                The Company is not responsible for and does not guarantee the security of the User’s data in the
                following cases: transmission of the User’s password by the User to third parties (intentionally or
                inadvertently); third party access to the electronic mailbox of the User with the use of software
                allowing selection and/or password decoding; access of third parties to the electronic mailbox of the
                User by simply guessing the password; the User’s non-fulfillment of the recommendations specified in
                this Agreement or the Service interface.
              </li>
              <li>
                The User is solely responsible for the security (resistance to guessing) of the means chosen by him/her
                to access the account, and also independently ensures their confidentiality.
                <br /><br />
                All actions within or using the Services under the User’s account are considered to be performed by the
                User himself/herself, except for the cases when the User, in the manner provided for in clause 2.9.1. of
                this Agreement, notified the Company of unauthorized access to the Services using the User’s account
                and/or any violation (suspected violation) of the confidentiality of their means of access to the
                account (password).
              </li>
              <li>
                The User undertakes to comply with and implement additional recommendations of the Company in addition
                to those provided in this Agreement and related to improving the security of his account.
              </li>
              <li>The user undertakes:<br /><br />
                <ol>
                  <li>
                    Immediately notify the Company of any unauthorized use of the User's password or account, or any
                    other breach of security.
                  </li>
                  <li>
                    Log out of his/her account (end each session by clicking the "log out" button) when finishing
                    working with the personal account. The company shall not be liable for any loss or damage of data
                    that may occur due to non-compliance of the recommendations set out in section 2 of this Agreement.
                  </li>
                </ol>
              </li>
              <li>
                The User’s password cannot be recovered. Customer support of {site} will not be able to recover the
                password guaranteedly when you contact. The company recommends to protect your password and fill out the
                registration form of the Services accurately, correctly and fully.
              </li>
              <li>
                The User has the right to delete his/her account from {site} at any time.
              </li>
              <li>
                By entering into this Agreement, the User confirms that he/she does not belong to persons engaged in
                financing terrorist activities, carrying out legalization of funds obtained by criminal means, etc. The
                funds provided by the User for the purchase of products and services of the Company are legal, acquired
                legally and belong to the User on the basis of ownership. In case of non-fulfillment of these
                guarantees, the potential user shall refuse to enter into this Agreement.
              </li>
              <li>
                In the case that the User is legally married, he/she ensures that products and services of the Company
                acquired by him/her at the expense of property belonging exclusively to the User or to property acquired
                during the marriage, however, the other spouse informed about these intentions to purchase services of
                the Company and expressed consent.
              </li>
            </ol>
          </li>
          <li><b>General terms on the use of the Services.</b><br /><br />
            <ol>
              <li>
                {site} established for the purpose of carrying out activities with the assistance of the Users in their
                activities for the purpose of generating income. {site} provides access to the Services that are
                available through a personal computer and mobile devices, both currently existing and those that will be
                developed in the future.
              </li>
              <li>
                The Company does not assume any responsibility for the User's settings, and is not responsible for the
                compliance of the Service with the User's goals. All issues of granting access rights to the Internet,
                as well as setting up for this relevant equipment and software products are solved by the User
                independently and do not fall under the scope of this Agreement.
              </li>
              <li>
                The User agrees that the Company may collect anonymized statistical data on Users of the Services.
              </li>
              <li>
                The Company has the right to send its Users information messages concerning the Services.
              </li>
              <li>
                The Company has the right to establish any additional requirements and restrictions in relation to
                registration on {site} and use of the Services.
              </li>
              <li>
                In order to use {site} as a set of computer programs, the Company grants the User (licensee) on the
                terms of a simple (non-exclusive) royalty-free license the right to use free functional (software)
                features of {site}.
              </li>
            </ol>
          </li>
          <li><b>Terms of use of the Services</b><br /><br />
            <ol>
              <li>
                The User is solely responsible to third parties for their actions related to the use of {site},
                including if such actions will lead to violation of the rights and legitimate interests of third
                parties, as well as for compliance with the law when using the Services.
              </li>
              <li>When using the Services the User may not:<br /><br />
                <ol>
                  <li>
                    Impersonate another person or representative of the organization and/or the Company, including the
                    staff of {site}, without having any right to this, as well as use any other forms and methods of
                    illegal representation of other persons in the network, as well as mislead the Company about the
                    properties and characteristics of any subjects or objects.
                  </li>
                  <li>
                    Send, transmit or otherwise post and/or distribute content of {site} in the absence of rights to
                    such actions, according to the legislation.
                  </li>
                  <li>
                    Disrupt the normal operation of {site} and the Services.
                  </li>
                  <li>
                    Promote actions aimed at violating the restrictions and prohibitions imposed by the Agreement.
                  </li>
                  <li>
                    Otherwise violate the norms of the legislation, including the norms of international law.
                  </li>
                </ol>
              </li>
              <li>
                The User, using the functionality of the Services, agrees that the User's information can be transferred
                and transmitted as required by the Services ordered by the User, and only to the extent necessary for
                the proper provision of these Services.
              </li>
              <li>The Company may:<br /><br />
                <ol>
                  <li>
                    Add, modify, delete Content at its discretion.
                  </li>
                  <li>
                    Delete information posted on the website and in social networks by the User, when such information
                    violates the terms of this Agreement or the rights of third parties.
                  </li>
                  <li>
                    Discontinue the operation of the Platform, restrict or terminate access to it for the time necessary
                    for maintenance and/or modernization of the website.
                  </li>
                </ol>
              </li>
              <li>Settlement system.<br /><br />
                <ol>
                  <li>
                    The Company, being the owner of the Platform, does not make any payments, does not acquire any
                    rights and obligations from the relations on transfer of payment means, and is not a recipient of
                    funds and property transferred by the Platform users. It is not a payment system, but it acts as a
                    subject providing information services about the facts of settlements between third parties, based
                    on information provided by the Users of the Platform.
                  </li>
                  <li>
                    In this case, the platform accounting tools, for the execution of which the executive mechanism is
                    used between users, is considered completed from the moment the accounting tools are credited in the
                    User's personal account. termination or refusal from the agreement for the acquisition of platform
                    accounting tools at the will of the User is not acceptable. To inform about upcoming transactions
                    between the Users of the Platform, the Company grants the Users the right to use the Platform's
                    information services about settlements between them.
                  </li>
                  <li>The Parties can use the information Services “Master account" and "Bonus account”:
                    <ul>
                      <li>
                        "Master account" is a resource for informing the Users of the Platform about the Commission
                        and/or possibility of making payments between the Users of the Platform in quantitative and/or
                        aggregate terms. The use of this information resource by the Users of the Platform assumes that
                        payments between the Users of the Platform are made in full and without using the platform. At
                        the same time, the completed payments represent the transfer of a security payment aimed at
                        guaranteeing the fulfillment of the obligations of towards each other (conclusion and execution
                        of the Agreement) and are not subject to refund.
                      </li>
                      <li>
                        "Bonus account” is intended to inform the Users of the Platform about the occurrence, change, or
                        termination of mutual claims for the performance by the Users of the Platform of the obligation
                        to settle accounts with each other.
                      </li>
                    </ul>
                  </li>
                </ol>
              </li>
            </ol>
          </li>
          <li><b>Termination of the User's account.</b><br /><br />
            <ol>
              <li>
                The User agrees that the Company reserves the right to terminate the User's account on {site} at any
                time without prior notice to the User.
              </li>
              <li>
                Account termination may occur for the following reasons:
                <ul>
                  <li>
                    violation of the provisions of this Agreement, as well as amendments to it, which are an integral
                    part of it;
                  </li>
                  <li>
                    at the appropriate request of the authorities, according to the legislation;
                  </li>
                  <li>
                    in case of dissemination by the User of unconfirmed negative information about the Company;
                  </li>
                  <li>
                    due to unforeseen technical problems or safety-related circumstances, etc.
                  </li>
                </ul>
              </li>
              <li>
                The Company has the right to delete the User's account on {site} and/or suspend, restrict or terminate
                access to any of the Services, if the Company finds in the actions of the User signs of violation of the
                terms of this Agreement, without explaining the reasons for such actions.
              </li>
            </ol>
          </li>
          <li><b>Exclusive rights to the content of the Services and the Content.</b><br /><br />
            <ol>
              <li>
                The User undertakes not to reproduce, copy, repeat, sell or resell, as well as not to use for any
                commercial purposes and not to make available to the public, not to distribute the program of {site} in
                whole or any part of the Services of {site} (including the Content available to the User through the
                Services), or access to them, except where the User has previously received such permission from the
                Company.
              </li>
              <li>
                All objects accessible through the Services and {site}, including design elements, text, graphics,
                illustrations, videos, computer programs, databases, music, sounds and other objects (hereinafter the
                Content of the Services), as well as any content posted on the Services and {site}, are objects of
                exclusive rights of the Right Holder and other rights holders.
              </li>
              <li>
                Use of the Content, as well as any other elements of the Services and {site} is possible only within the
                functionality offered by a particular Service. No elements of the Content of the Services and {site}, as
                well as any content posted on the Services and {site}, may not be used in any other way without the
                prior permission of the copyright holder.
                <br /><br />
                Use includes but is not limited to: reproduction, copying, processing, distribution on any basis,
                display in a frame, etc. Exception are the cases expressly provided by law.
                <br /><br />
                The User's use of elements of the Services' content, as well as any content for personal non-commercial
                use is permitted provided you retain all the copyright, related rights, trademarks, other notices of
                authorship, save the name (or alias) of the author/right holder's name unchanged, maintaining
                appropriate object unchanged. Exceptions are cases expressly provided for by law.
              </li>
            </ol>
          </li>
          <li><b>Breaks in {site} work.</b><br /><br />
            <ol>
              <li>
                The Company has the right to perform preventive works in the Services with temporary suspension of the
                Services.
              </li>
              <li>
                In the case of force-majeure circumstances, and also accidents or failures in hardware-software
                complexes of third parties cooperating with the Company, or actions of third parties aimed at suspension
                or termination of functioning of all or part of the Services, suspension of the Services without notice
                and without liability to the Users is possible.
              </li>
            </ol>
          </li>
          <li><b>Feedback and complaints procedure.</b><br /><br />
            <ol>
              <li>
                The User who believes that his/her rights and interests are violated due to the actions of the Company,
                can send an appropriate appeal. The Customer Support Service deals with the consideration of requests in
                accordance with the General procedure for consideration of incoming requests. All requests, including
                regarding the operation of the Services, the User can send via the chat on any page of the
                website {site} or email ceo@aquix.pro. The User and the Company agree that all possible disputes
                regarding the Agreement will be resolved in accordance with the provisions of the current legislation of
                Georgia.
              </li>
              <li>
                All disputes between the parties under this Agreement shall be settled by correspondence and
                negotiations using the mandatory pre-trial (claim) procedure. In case of failure to reach agreement
                between the parties through negotiations within sixty (60) calendar days of receipt by the other party a
                written claim, the dispute shall be submitted by any interested party to the court at the location of
                the company (excluding the jurisdiction of any other courts).
              </li>
              <li>
                The court's recognition of any provision of the Agreement as invalid or not enforceable shall not entail
                the invalidity or unenforceability of other provisions of the Agreement.
              </li>
            </ol>
          </li>
          <li><b>Changes and additions to the Agreement.</b><br /><br />
            <ol>
              <li>
                This Agreement may be amended by the Company without any prior notice. Any changes to the Agreement made
                unilaterally by the Company shall enter into force on the day following the date of publication of such
                changes on the {site} on the pages of the corresponding Service. The User undertakes to independently
                check the Agreement for changes. Failure by the User to perform actions on familiarization may not serve
                as a basis for failure by the User to fulfill its obligations and non-compliance by the User with the
                restrictions established by this Agreement.
              </li>
              <li>
                In case of adoption by authorities of the regulatory legal acts affecting entirely or in part
                functioning of {site}, the Company also reserves the right to make any changes to the functioning of the
                Services and {site}, aimed at bringing the Company's activities in line with the new standards.
              </li>
              <li>
                The User has the right to refuse to accept changes and additions to the Agreement and/or individual
                Services, which means the User's refusal to use {site} and/or individual Services and all previously
                granted rights.
              </li>
            </ol>
          </li>
          <li><b>Responsibility.</b><br /><br />
            The user understands and agrees that:
            <br /><br />
            <ol>
              <li>
                The Company does not warrant that the Services will meet your requirements; will be uninterrupted,
                timely, secure or error-free; the results that may be obtained will be accurate or reliable; quality of
                any product and other information obtained using the Services will meet the User's expectations, and
                that all program errors will be corrected. The software of the Services, computer programs with respect
                to any Services that ensure the functioning of the Services, is licensed to Users on the principle of
                providing a non-exclusive license, without warranty of performance and technical support.
              </li>
              <li>
                The Company shall not be liable for any direct or indirect losses resulting from the use or inability to
                use the Services and {site}; due to unauthorized access to User's communications; due to fraudulent
                activities of third parties.
              </li>
              <li>
                The Company is not responsible for the deposit and withdrawal of funds from the User's account when
                using third parties and third-party organizations.
              </li>
            </ol>
          </li>
          <li><b>Privacy.</b><br /><br />
            <ol>
              <li>
                The Company may use some technologies to identify the Users, including the use of cookies. The Company
                uses cookies for marketing purposes to study the preferences of Users, in order to register visits by
                the Users of {site}, remembering pages and links that the Users clicked. The Company uses this
                information to {site} and displayed on {site} advertising as accurately as possible corresponded to the
                interests of Users. For the same purposes, the Company may also transfer this information to third
                parties. At the same time, such identification is non-personalized and generalized, the Company monitors
                the actions of individual Users.
                <br /><br />
                The specified data can be provided to authorized state bodies according to the current legislation.
              </li>
              <li>
                Within the functioning of the Services and {site} the secrecy of messages and the confidentiality of
                information about Users shall be ensured, except for the cases stipulated by the legislation of Georgia
                and clause 11.1 of this Agreement.
              </li>
            </ol>
          </li>
          <li><b>Other provisions.</b><br /><br />
            <ol>
              <li>
                This Agreement is a contract between the User and the Company regarding the use of the Services
                of {site} and supersedes all previous agreements between the User and the Company on the above issues.
              </li>
              <li>
                This Agreement shall be governed by and interpreted in accordance with the laws of Georgia. Issues that
                are not regulated by this Agreement shall be resolved in accordance with the legislation of Georgia.
                <br /><br />
                All possible disputes arising out of the relations regulated by this Agreement shall be settled in
                accordance with the procedure established by the current legislation of Georgia, according to the norms
                of the Georgian law. Throughout the text of this Agreement, unless expressly stated otherwise, the term
                "legislation" shall mean the legislation of Georgia.
              </li>
              <li>
                Due to the gratuitousness of the Services provided under this Agreement, the rules on consumer
                protection provided by the legislation may not be applicable to the relations between the User and the
                Company.
              </li>
              <li>
                Nothing in the Agreement can be understood as the establishment between the User and the Company of
                partnership relations, joint activity relations, personal employment relations, or any other relations
                not expressly provided for in the Agreement.
              </li>
              <li>
                If, for any reason, one or more provisions of this Agreement are found to be invalid or unenforceable,
                this shall not affect the validity or applicability of the remaining provisions of the Agreement.
              </li>
              <li>
                Inaction on the part of the Company in case of violation by the User or other users of the provisions of
                the Agreements does not deprive the Company of the right to take appropriate actions to protect its
                interests later, and also does not mean the Company's waiver of its rights in case of subsequent similar
                violations.
              </li>
              <li>
                This Agreement is made in Russian language.
              </li>
            </ol>
          </li>
        </ol>
        <p>
          The current version of the user agreement is always available on the website <a
          href={`${domain}${actualPage}`}>{domain}{actualPage}.</a>
          <br /><br />
          Contacts:<br /><br />
          Artem Balyakno <br />
          <a href="ceo@aquix.pro">ceo@aquix.pro</a><br />
          TG: @aquix_ceo
        </p>
      </div>
    </div>
  );
};

export default PageEn;
