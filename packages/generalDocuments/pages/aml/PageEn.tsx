import React, { FunctionComponent } from 'react';

const PageEn: FunctionComponent = () => {
  return (
    <div className="content-container__content">
      <div className="numeric-list">
        <h1>AML Policy JSC “AQUIX” (AQUIX) anti-money laundering Policy.</h1>
        <p>AQUIX participates in the fight against the legalization of illegal income. As part of our regulatory
          commitment and our efforts to combat the legalization of proceeds of crime, we have appointed an employee
          responsible for enforcing this policy.</p>
        <p>AQUIX requires that all personnel be trained in the area of anti-money laundering and our internal
          compliance procedures.</p>
        <p>Financial monitoring officer ("MLRO"). The functions of the financial monitoring officer are to receive
          all
          reports of suspicious activity by the staff.</p>
        <p>This employee is responsible for implementing the procedures set out in this anti-money laundering
          Policy,
          determining the need for and extent of reporting suspicious activity to the national Bank of the Republic
          of
          Georgia ("NB"), responding to NB requests, and resolving any issues or controversies arising from this
          Policy.</p>
        <p>AQUIX has developed four main policies to help identify, prevent, and report illegal income laundering
          activities:
          <ol>
            <li>Сustomer awareness Policy (“KYC”)</li>
            <li>Policy identification of the beneficial owner (the “BOV”)</li>
            <li>a Policy of monitoring suspicious activity</li>
            <li>Policy for developing systems, management tools, and training</li>
          </ol>
        </p>
        <p>The legislation of Georgia on combating the legalization of illegal income on the territory of Georgia,
          as
          well as throughout the world, recognizes the use of professional services for the legalization of such
          income.</p>
        <p>Thus, laws on countering the legalization of illicit income and the financing of terrorism in these
          jurisdictions are extended beyond the traditional regulated institutions, including independent lawyers
          and
          proxies, service providers.</p>
        <p>Legislation on combating the legalization of illegal income in the territory of Georgia is mainly
          reflected
          in Law No. 2391, dated 06/06/2003 <b>“On assistance in preventing the legalization of illegal income”</b>.
        </p>
        <p>The provisions of the Law create mandatory requirements for: identifying the name and identity of your
          client or investor and maintaining proper records of such verification; maintaining proper records of
          transactions with investors or clients for a minimum period of at least 5 years; internal control and
          reporting procedures, including training employees to prevent the legalization of illegal income, as well
          as
          the appointment of a financial monitoring officer. Failure to comply with the Law is a serious
          offense.</p>
        <p>Brief description of the main principles of the company AQUIX on fight against legalization of illegal
          income: AQUIX protection from money laundering and financing of terrorism.</p>
        <p>Maintaining policies and procedures to combat the legalization of illegal income and the financing of
          terrorism, internal control systems to ensure compliance with current legislation in this area by the
          designated person (s) and taking appropriate measures when suspicious activity is detected by submitting
          data on such transactions in accordance with the guidelines established by the National Bank of
          Georgia.</p>
        <p>Compliance with existing laws and regulations on combating the legalization of illegal income and the
          financing of terrorism established by Georgian legislation.</p>
        <p>AQUIX's anti-money laundering policy applies to all business entities. Notification of all detected
          suspicious activities to the extent required by all applicable foreign and domestic laws.</p>
        <p>Storage of all client documents for the period specified by the legislation of Georgia. AQUIX does not
          offer services for opening anonymous accounts.</p>
        <p>Fully cooperate with law enforcement and regulatory authorities to the extent permitted by all applicable
          laws. Training of employees in the "know your customer" and "fight against the legalization of illegal
          income" methods and new laws and regulations on countering the legalization of illegal income.</p>
        <p>For more information about our anti-money laundering Policy, please contact us by
          email: <b>ceo@aquix.ru</b></p>
      </div>
    </div>
  );
};

export default PageEn;
