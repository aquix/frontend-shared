import React, { FunctionComponent } from 'react';
import PageRu from './PageRu';
import PageEn from './PageEn';
import { IGeneralDocumentsProps } from '../../types';

const Aml: FunctionComponent<IGeneralDocumentsProps> = (props) => {
  const { lang } = props;
  if (lang === 'ru') {
    return (
      <PageRu />
    );
  } else {
    return (
      <PageEn />
    );
  }
};

export default Aml;
