import React, { FunctionComponent } from 'react';
import PageRu from './PageRu';
import PageEn from './PageEn';
import { IGeneralDocumentsProps } from '../../types';


const RiskDisclosure: FunctionComponent<IGeneralDocumentsProps> = (props) => {
  const { lang } = props;
  if (lang === 'ru' || lang === 'uk') {
    return (
      <PageRu />
    );
  } else {
    return (
      <PageEn />
    );
  }
};

export default RiskDisclosure;
