import React, { FunctionComponent } from 'react';

const PageEn: FunctionComponent = () => {
  return (
    <div className="content-container__content">
      <div className="numeric-list">
        <h1>Risk Disclosure <small>№ 0.0.2 от 03.11.2019</small></h1>
        <p><b>Joint-stock company «AQUIX»</b> (thereinafter the Company, (Extract from Registry of
          Entrepreneurs
          and Non-Entrepreneurial (Non-Commercial) Legal Entities, serial number of the application — B18209595,
          date
          of
          preparation — 21/12/2018), having an address at Georgia, Tbilisi, Didube district, Uznadze str., N111, b.
          N11,
          Building N2, represented by its CEO Artem Balyakno, in accordance with current legislation, inform all
          acquirers or potential acquirers of shares of the following risks.</p>
        <p>The need for diversification when investing. Diversification involves distributing your investments among
          different types of investments with different risks to reduce overall risk. However, this will not reduce
          all
          types of risk. Diversification is an integral part of investing. Investors should invest only a part of
          their
          available investment funds and balance it with investments in safer, more liquid assets.</p>
        <p>JSC "AQUIX" is a venture enterprise, which is based on the combination of skills, abilities, knowledge
          and
          property of individuals in order to create a single functioning enterprise, thus, the Company is inherent
          in
          the risks that usually accompany venture projects. Investing in the capital of Joint-stock company
          "AQUIX" does not involve regular income from investments, unlike mini-bonds, which offer regularly paid
          interest. Please take into account the following certain risks when purchasing shares in JSC "AQUIX".</p>
        <p>1) Loss of investment. Most businesses initially fail or do not expand according to plan due to the
          inflow
          of
          the missing number of investments or to the lack of them, so investing in this business can be associated
          with
          significant risk. It is likely that you may lose all or part of your investment. You should invest only
          the
          amount you are ready to lose, and build a diversified portfolio to spread the risks and increase the
          likelihood of total return on investment capital. If the business you are investing in fails, the company
          will
          not return your investment.</p>
        <p>2) Rare dividend payout. Dividends are payments made by the business from the profits of the company to
          its
          founders. This means that you are unlikely to feel the return on your investment until you can sell your
          shares. Profits are typically reinvested in the business to drive growth and raise the value of shares.
          Thus,
          the main source of profitability of the company's participants is the total capitalization of the company,
          which in turn leads to an increase in the value of the company’s shares.</p>
        <p>In addition to the risk disclosure, the company hereby declares the limitation of liability in terms of
          the
          possible expectations of the shares’ acquirers in respect of the following circumstances and events. Due
          to
          the fact that the company "AQUIX" is a venture enterprise, the activities of which are accompanied by
          risks,
          the Company limits liability in the following part.</p>
        <p>The information contained in advertisements and other brochures, acts, programs, leaflets and other media
          relative to the beginning and capitalization of the Company, increasing the value of its shares, size and
          timing of dividend payments, is approximate, provided solely for educational purposes, it is not
          advertising,
          it does not act as any elements of a public offer or a public offer as a whole.</p>
        <p>The proposals contained in the sources are for informational purposes only and do not entail mutual
          rights
          and obligations. The information contained in this Disclosure and other media is current as of the date of
          creation of the document or medium and may and will be updated, supplemented, corrected, confirmed or
          changed
          without notification. This does not exclude significant changes in the information contained in this
          Disclosure.</p>
        <p>The information provided by the Company is not an incentive to acquire property rights, does not form it
          and
          should not be considered as it.</p>
        <p>Advertisements, their parts or the fact of their distribution are not the basis for any proposal,
          contract,
          obligation or investment decision, and they can not be relied upon in connection with them, as well as
          they
          are not a recommendation for the emergence of legal relations with the Company.</p>
        <p>Any person intending to participate in the Company or its activities in making any decisions on investing
          should rely only on the information set out in the contracts, any attachments or modifications and other
          documents developed by the company for interaction with third parties. Other documents intended for third
          parties may contain information other than that contained in advertisements. No representations or
          warranties
          are made as to the accuracy, credibility, validity or completeness of the information contained in the
          advertisements and no person may rely on the content of the advertisements.</p>
        <p>The information presented was not subject to independent verification. The information provided by the
          Company contains forward-looking statements. Such forward-looking statements include statements regarding
          plans, objectives, goals, strategy, future events or results, and any other statements that are not
          statements
          of fact. Forward-looking statements include statements regarding strategy, forecasts, growth prospects;
          future
          plans and potential for future growth; liquidity, capital resources and capital expenditures; and growth
          in
          demand for products; economic forecasts and industry trends; market development; impact of legislative
          initiatives and competitive advantages.</p>
        <p>Forward-looking statements are inherently risky and uncertain since they relate to events and
          circumstances
          that may or may not occur in the future. The forward-looking statements contained in the information
          sources
          are based on various assumptions and logical conclusions. Thus, no such assumptions are characterized by a
          high degree of uncertainty and conditionality, which are difficult or impossible to predict and which are
          beyond control of the company, whereby the company may be unable to fulfill expectations, hopes or
          predictions. The company warns you that forward-looking statements are not guarantees of future results
          and
          that the results of its activities, financial condition, liquidity, as well as the development of the
          industry
          in which the Company operates, may differ materially from the forecasts or assumptions made in the
          statements
          contained in the forecasts.</p>
        <p>This notice may be supplemented by the Company unilaterally, to which the participant of the Company (the
          acquirer and/or the owner of the shares) gives full and unconditional consent in advance.</p>
        <p>The use of this material from the website of the Company in the Internet space is allowed only with the
          obligatory link to the source of publication: the link to the website with the location of notification: a
          link to the place on the website.</p>
        <p>The fact of acquisition of the Company’s shares is an indisputable proof of familiarization with this
          risk
          disclosure and full and unconditional acceptance of each of its terms and conditions separately and the
          entire
          disclosure as a whole.</p>
        <p>This disclosure is a modification of the terms of previous disclosures and applies to all purchasers and
          holders of shares of the Company.</p>
      </div>
    </div>
  );
};

export default PageEn;
