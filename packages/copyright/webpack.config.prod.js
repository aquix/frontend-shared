'use strict';
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const { dependencies: deps } = require('./package.json');

module.exports = {
  entry: path.resolve(__dirname, '/index.tsx'),
  output: {
    path: path.join(__dirname, '/lib'),
    filename: 'index.js',
    library: 'copyright',
    globalObject: 'this',
    libraryTarget: 'umd',
    clean: true
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.css']
  },
  target: 'web',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: [/node_modules/],
        use: ['babel-loader', 'ts-loader']
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'source-map-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
    ]
  },
  plugins: [
    /**
     * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
     * the following package: sudo apt-get install libpng16-dev
     */

    new ModuleFederationPlugin({
      name: 'copyright',
      filename: 'remoteEntry.js',
      remotes: {},
      exposes: {},
      shared: {
        react: {
          singleton: true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          singleton: true,
          requiredVersion: deps['react-dom'],
        },
      },
    }),
  ],
  externals: {
    react: {
      commonjs: 'React',
      commonjs2: 'react',
      amd: 'react'
    },
    'react-dom': {
      commonjs: 'ReactDOM',
      commonjs2: 'react-dom',
      amd: 'react-dom'
    },
    'react-markdown': {
      commonjs: 'ReactMarkdown',
      commonjs2: 'react-markdown',
      amd: 'react-markdown'
    }
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: false,
        parallel: false
      }),
    ]
  },
};
