import React from 'react';
import ReactMarkdown from 'react-markdown';

interface ICopyrightComponent {
  translateKey: string;
  keyPrefix?: string;
  params?: { [key: string]: string };
  getContent: (key: string, params: { [key: string]: string }) => string;
}

const CopyrightComponent = (props: ICopyrightComponent) => {
  const { translateKey, params, getContent, keyPrefix } = props;
  const key = keyPrefix ? translateKey + `-${keyPrefix}` : translateKey;

  return (
    <ReactMarkdown>
      {getContent(
        key,
        {
          date: `${new Date().getFullYear()}`,
          ...params,
        },
      )}
    </ReactMarkdown>
  );
};

export default CopyrightComponent;
