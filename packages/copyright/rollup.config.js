import typescript from '@rollup/plugin-typescript';

export default {
  input: 'index.tsx',
  external: ['react', 'react-dom', 'react-markdown'],
  output: {
    file: './lib/index.js',
    format: 'cjs'
  },
  plugins: [
    typescript({
      tsconfig: "./tsconfig.json"
    })
  ]
};

// import dts from 'rollup-plugin-dts'
// import esbuild from 'rollup-plugin-esbuild'

// export default [
//   {
//     input: `index.tsx`,
//     plugins: [resolve(), babel({ babelHelpers: 'bundled' })]
//     output: [
//       {
//         file: `dist/bundle.js`,
//         format: 'cjs',
//         sourcemap: true,
//         exports: 'default',
//       },
//     ]
//   },
//   {
//     input: `index.tsx`,
//     plugins: [dts()],
//     output: {
//       file: `dist/bundle.d.ts`,
//       format: 'es',
//     },
//   }
// ]
