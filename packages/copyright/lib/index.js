'use strict';

var jsxRuntime = require('react/jsx-runtime');
var ReactMarkdown = require('react-markdown');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var ReactMarkdown__default = /*#__PURE__*/_interopDefaultLegacy(ReactMarkdown);

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

var CopyrightComponent = function (props) {
    var translateKey = props.translateKey, params = props.params, getContent = props.getContent, keyPrefix = props.keyPrefix;
    var key = keyPrefix ? translateKey + "-".concat(keyPrefix) : translateKey;
    return (jsxRuntime.jsx(ReactMarkdown__default["default"], { children: getContent(key, __assign({ date: "".concat(new Date().getFullYear()) }, params)) }));
};

module.exports = CopyrightComponent;
