# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.14-alpha.4](https://gitlab.com/aquix/frontend-shared/compare/v1.1.14-alpha.3...v1.1.14-alpha.4) (2021-09-29)

**Note:** Version bump only for package @frontend-shared/support-messager





## [1.1.7-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.6-alpha.0...v1.1.7-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/support-messager





## [1.1.6-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.5-alpha.0...v1.1.6-alpha.0) (2021-09-28)

**Note:** Version bump only for package @frontend-shared/support-messager





## [1.1.2-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.1-alpha.0...v1.1.2-alpha.0) (2021-09-21)

**Note:** Version bump only for package @frontend-shared/support-messager





## [1.1.1-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.0...v1.1.1-alpha.0) (2021-09-13)

**Note:** Version bump only for package @frontend-shared/support-messager
