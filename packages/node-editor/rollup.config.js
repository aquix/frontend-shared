import typescript from '@rollup/plugin-typescript';
import { uglify } from 'rollup-plugin-uglify';
import css from 'rollup-plugin-import-css';
import dts from 'rollup-plugin-dts';
import { typescriptPaths } from 'rollup-plugin-typescript-paths';
import { visualizer } from "rollup-plugin-visualizer";
import postcss from 'rollup-plugin-postcss';

const bundle = config => ({
  ...config,
  input: './index.tsx',
  external: ['react', 'react-dom', 'antd', '@ant-design/icons'],
});

export default [
  bundle({
    plugins: [
      dts(),
      typescriptPaths({ preserveExtensions: true }),
    ],
    output: {
      file: `./lib/index.d.ts`,
      format: 'cjs',
      interop: 'compat',
    },
  }),
  bundle({
    output: {
      file: './lib/index.js',
      format: 'cjs',
      interop: 'compat',
    },
    plugins: [
      typescript(),
      css(),
      uglify(),
      typescriptPaths(),
      visualizer(),
    ]
  }),
  {
    input: 'src/public/styles/main.css',
    output: [
      { file: "lib/index.css", format: 'es' }
    ],
    plugins: [
      postcss({
        extract: "index.css",
        minimize: true,
      }),
    ],
  },
  {
    input: 'src/public/styles/resizable.css',
    output: [
      { file: "lib/resizable.css", format: 'es' }
    ],
    plugins: [
      postcss({
        extract: "resizable.css",
        minimize: true,
      }),
    ],
  }
];