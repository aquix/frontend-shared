import 'reflect-metadata';
import React from "react";
import ReactDOM from "react-dom/client";
import 'antd/dist/antd.css';
import './styles/main.css';
import './styles/resizable.css';
import BotLayout from '../components/layout/bot';
import BotUserLayout from '../components/layout/botUser';
import EventLayout from '../components/layout/event';
import ScenariosLayout from '../components/layout/scenarios';
import StateMachineLayout from '../components/layout/stateMachine';

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(<BotLayout />);