import { interfaces } from 'inversify';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { ActionModule } from '@local/modules/action/module';
import { BotModule } from '@local/modules/bot/module';
import { BotUserModule } from '@local/modules/botUser/module';
import { EventModule } from '@local/modules/event/module';
import { ScenarioModule } from '@local/modules/scenario/module';
import { ScenarioActionModule } from '@local/modules/scenarioAction/module';
import { ConnectorModule } from '@local/modules/connector/module';
import { ScenarioItemModule } from '@local/modules/scenarioItem/module';
import { StateMachineServiceModule } from '@local/modules/stateMachineService/module';
import { stateServiceModule } from '@local/modules/stateService/module';
import { transitionModule } from '@local/modules/transition/module';
import { ApiClient } from '@modules/grpcClient';
import { ToastMessageModule } from '@modules/toaster/module';

const initBaseContainers = (container: interfaces.Container) => {
  if (process.env.LOCAL_DEV === 'TRUE') {
    container.bind<ApiClient>(ApiClient.diKey).to(ApiClient).inSingletonScope();
  }
  container.bind<ScenarioEngineMV>(ScenarioEngineMV.diKey).to(ScenarioEngineMV).inSingletonScope();
  container.bind<StateMachineEngineMV>(StateMachineEngineMV.diKey).to(StateMachineEngineMV).inSingletonScope();

  container.load(
    ActionModule,
    BotModule,
    BotUserModule,
    EventModule,
    ScenarioModule,
    ScenarioActionModule,
    ConnectorModule,
    ScenarioItemModule,
    StateMachineServiceModule,
    stateServiceModule,
    transitionModule,
    ToastMessageModule,
  );
};

export default initBaseContainers;