import serverContainer from "./server";
import clientContainer from "./client";

const container = (typeof window === 'undefined') ? serverContainer : clientContainer;

export default container;