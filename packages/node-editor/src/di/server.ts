import { Container, interfaces } from 'inversify';
import initBaseContainers from './container';
import { ServerApiConfigImpl } from '@configs/api';
import { ApiConfigDiKey } from '@sharedModules/grpcClient/types';
import { IApiConfig } from '@sharedModules/client/types';

const container: interfaces.Container = new Container();
container.bind<IApiConfig>(ApiConfigDiKey).to(ServerApiConfigImpl);
initBaseContainers(container);

export default container;