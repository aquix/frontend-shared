import { Container, interfaces } from 'inversify';
import initBaseContainers from './container';
import { ApiConfigDiKey } from '@sharedModules/grpcClient/types';
import { IApiConfig } from '@sharedModules/client/types';
import { ClientApiConfigImpl } from '@configs/api';

const container: interfaces.Container = new Container();
container.bind<IApiConfig>(ApiConfigDiKey).to(ClientApiConfigImpl);
initBaseContainers(container);

export default container;