import { BaseViewModel } from '@sharedModules/Stream/model/BaseVM';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { merge, mergeMap, toArray } from 'rxjs';

class ScenarioEditorVM extends BaseViewModel {
  private engineMV: ScenarioEngineMV;

  constructor(
    engineMV: ScenarioEngineMV,
  ) {
    super();
    this.engineMV = engineMV;
  }

  load = () => {
    const sub = merge(
      this.engineMV.scenarioItemsModule.load(),
      this.engineMV.actionsModule.load(),
    ).pipe(
      toArray(),
      mergeMap(() => {
        return this.engineMV.scenarioActionsModule.load();
      }),
      toArray(),
      mergeMap(() => {
        return this.engineMV.scenarioActionsModule.connectorsModule.load();
      }),
    ).subscribe();

    this.addSubscription(sub);
  };
}

export default ScenarioEditorVM;