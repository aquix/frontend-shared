import { CanvasWidget } from '@projectstorm/react-canvas-core';
import ContextMenuComponent from '@components/scenarioEditor/contextMenu';
import React, { FC } from 'react';
import { DiagramEngine } from '@projectstorm/react-diagrams';

interface IScenarioEditorView {
  engine: DiagramEngine,
}

const ScenarioEditorView: FC<IScenarioEditorView> = (props) => {
  const { engine } = props;

  return (
    <ContextMenuComponent>
      <CanvasWidget
        className="diagram-container"
        engine={engine}
      />
    </ContextMenuComponent>
  );
};

export default ScenarioEditorView;