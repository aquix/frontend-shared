import { FC, PropsWithChildren, useCallback, useState } from 'react';
import { useInjection } from 'inversify-react';
import React from 'react';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import ContextMenuComponentView from '@components/scenarioEditor/contextMenu/Component';
import ScenarioContextMenuVM from '@components/scenarioEditor/contextMenu/VM';
import useLoadableVM from '@sharedModules/Stream/hooks';
import { Point } from '@projectstorm/geometry';

const ContextMenuComponent: FC<PropsWithChildren<{}>> = (props) => {
  const {
    children,
  } = props;

  const engineMV = useInjection<ScenarioEngineMV>(ScenarioEngineMV.diKey);

  const [vm] = useState(new ScenarioContextMenuVM(engineMV));
  useLoadableVM(vm);

  const addAction = useCallback((id: number, position: Point) => {
    vm.addAction(id, position);
  }, []);

  const addTool = useCallback((type: string, position: Point) => {
    vm.addTool(type, position);
  }, []);

  return <ContextMenuComponentView
    add={addAction}
    addTool={addTool}
    actions={engineMV.actionsModule.list.items}
    children={children}
  />;
};

export default ContextMenuComponent;