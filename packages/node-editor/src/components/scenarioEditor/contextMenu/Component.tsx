import React, { PropsWithChildren } from 'react';
import { FC, useState } from 'react';
import { Dropdown, Input, Menu } from 'antd';
import ActionNodeModel from '@local/modules/action/node';
import { Point } from '@projectstorm/geometry';
import { observer } from 'mobx-react-lite';

type ToolType = 'commentBox';

interface IContextMenuComponentView {
  actions: ActionNodeModel[],
  add: (id: number, position: Point) => void,
  addTool: (tool: ToolType, position: Point) => void,
}

const { Search } = Input;

const ContextMenuComponentView: FC<PropsWithChildren<IContextMenuComponentView>> = (props) => {
  const {
    actions,
    add,
    addTool,
    children,
  } = props;

  const [searchQuery, setSearchQuery] = useState('');

  return (
    <Dropdown overlay={<Menu
      style={{ height: 300, overflow: 'scroll' }}
      items={[
        { key: 'search', label: <Search placeholder="Search..." onKeyDown={(e) => e.stopPropagation()} onChange={(e) => setSearchQuery(e.target.value)} />, type: 'group'},
        { key: 'tools', label: 'Tools', children: [
            { key: 'comment-box', label: 'Comment Box', onClick: (info) => {
                const event = info.domEvent as React.MouseEvent<HTMLElement>;
                addTool('commentBox', new Point(event.clientX, event.clientY));
              }
            }
          ],
        },
        ...actions.filter(i => i.model.name.search(new RegExp(searchQuery, 'i')) !== -1).map((i, index) => ({
          key: `${i.model.id}_action_${index}_index`,
          onClick: (info) => {
            const event = info.domEvent as React.MouseEvent<HTMLElement>;
            add(i.model.id, new Point(event.clientX, event.clientY));
          },
          label: i.model.name,
        })),
      ]}
    />} trigger={['contextMenu']}>
      <div className="diagram-container">
        {children}
      </div>
    </Dropdown>
  );
};

export default observer(ContextMenuComponentView);