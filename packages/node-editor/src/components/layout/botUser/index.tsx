import 'reflect-metadata';
import React from 'react';
import { FunctionComponent } from 'react';
import BotUserTable from '../../tables/botUser';
import container from '../../../di';
import DiProvider from '@modules/tools/di-provider';

const BotUserLayout: FunctionComponent = () => (
  <DiProvider child={container}>
    <BotUserTable />
  </DiProvider>
);

export default BotUserLayout;