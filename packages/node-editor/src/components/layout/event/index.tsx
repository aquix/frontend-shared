import React from 'react';
import { FunctionComponent } from 'react';
import EventTable from '../../tables/event';
import container from '../../../di';
import DiProvider from '@modules/tools/di-provider';

const EventLayout: FunctionComponent = () => (
  <DiProvider child={container}>
    <EventTable />
  </DiProvider>
);

export default EventLayout;