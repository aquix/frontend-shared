import 'reflect-metadata';
import React from 'react';
import { FunctionComponent } from 'react';
import ScenarioTable, { IScenarioTableProps } from '../../tables/scenarios';
import container from '../../../di';
import DiProvider from '@modules/tools/di-provider';

const ScenarioLayout: FunctionComponent<IScenarioTableProps> = (props) => {
  const {
    beforeOpenEditor,
    scenarioId,
  } = props;

  return (
    <DiProvider child={container}>
      <ScenarioTable
        beforeOpenEditor={beforeOpenEditor}
        scenarioId={scenarioId}
      />
    </DiProvider>
  );
}

export default ScenarioLayout;