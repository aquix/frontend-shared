import 'reflect-metadata';
import React, { FC } from 'react';
import BotTable from '../../tables/bot';
import container from '../../../di';
import DiProvider from '@modules/tools/di-provider';

const BotLayout: FC = () => (
  <DiProvider child={container}>
      <BotTable />
  </DiProvider>
);

export default BotLayout;