import 'reflect-metadata';
import React from 'react';
import { FunctionComponent } from 'react';
import EditorPage from '../../scenarioEditor';
import container from '../../../di';
import DiProvider from '@modules/tools/di-provider';

const EditorLayout: FunctionComponent = () => (
  <DiProvider child={container}>
    <EditorPage />
  </DiProvider>
);

export default EditorLayout;