import 'reflect-metadata';
import React from 'react';
import { FunctionComponent } from 'react';
import StateMachineServiceTable, { IStateMachineTableProps } from '../../tables/stateMachineService';
import container from '../../../di';
import DiProvider from '@modules/tools/di-provider';

const StateMachineLayout: FunctionComponent<IStateMachineTableProps> = (props) => {
  const {
    beforeOpenEditor,
  } = props;

  return (
    <DiProvider child={container}>
      <StateMachineServiceTable
        beforeOpenEditor={beforeOpenEditor}
      />
    </DiProvider>
  );
}

export default StateMachineLayout;