import 'reflect-metadata';
import React from 'react';
import { FunctionComponent } from 'react';
import StateMachineEditor from '../../stateMachineEditor';
import container from '../../../di';
import DiProvider from '@modules/tools/di-provider';

const MachineEditorLayout: FunctionComponent = () => (
  <DiProvider child={container}>
    <StateMachineEditor />
  </DiProvider>
);

export default MachineEditorLayout;