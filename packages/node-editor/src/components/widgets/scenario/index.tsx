import * as React from 'react';
import { FC, useState } from 'react';
import NodeWidgetView from '@components/widgets/scenario/NodeComponent';
import NodeWidgetVM from '@components/widgets/scenario/ViewModel';
import useLoadableVM from '@sharedModules/Stream/hooks';
import ScenarioActionNode from '@local/modules/scenarioAction/node';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';

interface INodeWidget {
  node: ScenarioActionNode;
  engineMV: ScenarioEngineMV;
}

const NodeWidget: FC<INodeWidget> = (props) => {
  const {
    node,
    engineMV,
  } = props;

  const [vm] = useState(new NodeWidgetVM(engineMV, node));
  useLoadableVM(vm);

  return <NodeWidgetView
    setInitial={vm.setInitial}
    node={node}
    engineMV={engineMV}
  />
};

export default NodeWidget;
