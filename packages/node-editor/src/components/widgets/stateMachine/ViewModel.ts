import { BaseViewModel } from '@sharedModules/Stream/model/BaseVM';
import StateMachineServiceUsecase from '@local/modules/stateMachineService/usecase';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import StateServiceNodeModel from '@local/modules/stateService/node';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import ScenarioUsecase from '@local/modules/scenario/usecase';
import { IdRequest } from '@sharedModules/shared/commonTypes';

class StateMachineNodeWidgetVM extends BaseViewModel {
  private stateMachineUC: StateMachineServiceUsecase;
  private engineMV: StateMachineEngineMV;
  private scenarioEngineMV: ScenarioEngineMV;
  private node: StateServiceNodeModel;
  private scenarioUC: ScenarioUsecase;

  constructor(stateMachineUC: StateMachineServiceUsecase, engineMV: StateMachineEngineMV, scenarioUC: ScenarioUsecase, scenarioEngineMV: ScenarioEngineMV, node: StateServiceNodeModel) {
    super();
    this.stateMachineUC = stateMachineUC;
    this.engineMV = engineMV;
    this.node = node;
    this.scenarioEngineMV = scenarioEngineMV;
    this.scenarioUC = scenarioUC;
  }

  setInitial = () => {
    this.engineMV.layerModel.getItem()!.startStateId = this.node.model().id;
    this.addSubscription(this.stateMachineUC.update(this.engineMV.layerModel.getItem()!).subscribe(() => {
      this.engineMV.setInitialState(this.node.model().id);
    }));
  }

  getInScenario = () => {
    this.addSubscription(this.scenarioUC.getById(IdRequest.fromPartial({id: this.node.model().scenarioId })).subscribe(res => {
      this.scenarioEngineMV.loadLayerModel(res);
    }));
  }
}

export default StateMachineNodeWidgetVM;