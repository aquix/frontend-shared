import * as React from 'react';
import { FC, useState } from 'react';
import StateServiceNodeModel from '@local/modules/stateService/node';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import StateMachineNodeWidgetView from '@components/widgets/stateMachine/NodeComponent';
import StateMachineNodeWidgetVM from '@components/widgets/stateMachine/ViewModel';
import useLoadableVM from '@sharedModules/Stream/hooks';
import { useInjection } from 'inversify-react';
import StateMachineServiceUsecase from '@local/modules/stateMachineService/usecase';
import ScenarioUsecase from '@local/modules/scenario/usecase';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';

interface IStateMachineWidget {
  node: StateServiceNodeModel,
  engineMV: StateMachineEngineMV,
  scenarioEngineVM: ScenarioEngineMV,
}

const StateMachineNodeWidget: FC<IStateMachineWidget> = (props) => {
  const {
    node,
    engineMV,
    scenarioEngineVM,
  } = props;

  const stateMachineUC = useInjection<StateMachineServiceUsecase>(StateMachineServiceUsecase.diKey);
  const scenarioUC = useInjection<ScenarioUsecase>(ScenarioUsecase.diKey);


  const [vm] = useState(new StateMachineNodeWidgetVM(stateMachineUC, engineMV, scenarioUC, scenarioEngineVM, node));
  useLoadableVM(vm);

  return <StateMachineNodeWidgetView setInitial={vm.setInitial} getIn={vm.getInScenario} node={node} engineMV={engineMV} />
};

export default StateMachineNodeWidget;