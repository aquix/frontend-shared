import { BaseViewModel } from '@sharedModules/Stream/model/BaseVM';
import { CommentBoxNodeModel } from '@local/modules/scenarioItem/nodes/commentBox/node';
import EngineMV from '@local/modules/engine/model';
import { ObjectWithId } from '@sharedModules/types';

class CommentBoxNodeWidgetVM<ENGINE_MODULE extends ObjectWithId> extends BaseViewModel {
  private node: CommentBoxNodeModel<ENGINE_MODULE>;
  private engineMV: EngineMV<ENGINE_MODULE>;

  constructor(engineMV: EngineMV<ENGINE_MODULE>, node: CommentBoxNodeModel<ENGINE_MODULE>) {
    super();
    this.engineMV = engineMV;
    this.node = node;
  }

  onLockNode() {
    this.node.lockNode();
  }

  onUnlockNode() {
    this.node.unlockNode();
  }

  changeBackgroundColor(color: string) {
    this.node.changeBackgroundColor(color);
    this.node.setLocked(this.node.extraModel().meta.isLocked)
  }

  onUpdateResize(width: number, height: number) {
    this.node.updateSize(width, height);
  }

  onBlur = (value: string) => {
    this.node.extraModel().meta.description = value;
    this.engineMV.scenarioItemsModule.updateItem(this.node.model())
  };
}

export default CommentBoxNodeWidgetVM;