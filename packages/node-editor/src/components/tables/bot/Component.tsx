import React, { FC, useCallback } from 'react';
import { BotModel } from '@local/modules/proto/botCommon';
import { Button } from 'antd';
import TableStream from '@local/TableStream/TableStream';
import BotUsecase from '@local/modules/bot/usecase';
import BotForm from './forms';
import { IBaseFormProps, IBaseTableViewProps } from '../types';

const BotTableView: FC<IBaseTableViewProps<BotModel>> = (props) => {
  const {
    add,
    columns,
    formItem,
    closeForm,
    formVisible,
  } = props;

  const Form: FC<IBaseFormProps<BotModel>> = useCallback((props) => {
    return <BotForm {...props} values={formItem} />;
  }, [formItem]);

  return (
    <div>
      <div>
        <Button onClick={() => add()} style={{ marginBottom: 10 }}>
          Add
        </Button>
      </div>
      <TableStream<BotModel>
        columns={columns}
        idKey="BotList"
        diKey={BotUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle="Bot edit"
        formVisible={formVisible}
        closeForm={closeForm}
      />
    </div>
  );
};

export default BotTableView;