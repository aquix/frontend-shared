import React, { FC, useCallback, useEffect, useState } from 'react';
import columns from './columns';
import { useInjection } from 'inversify-react';
import { observer } from 'mobx-react-lite';
import EventTableView from './Component';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { BotModel, EventModel, ScenarioListReq, ScenarioModel } from '@local/modules/proto/botCommon';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { ListOptions } from '@sharedModules/shared/paginatior';
import ScenarioUsecase, { IScenarioUsecase } from '@local/modules/scenario/usecase';
import BotUsecase, { IBotUsecase } from '@local/modules/bot/usecase';
import EventUsecase, { IEventUsecase } from '@local/modules/event/usecase';

const EventTable: FC = observer(() => {
  const eventUsecase = useInjection<IEventUsecase>(EventUsecase.diKey);
  const botUsecase = useInjection<IBotUsecase>(BotUsecase.diKey);

  const scenarioUsecase = useInjection<IScenarioUsecase>(ScenarioUsecase.diKey);

  const [formItem, setFormItem] = useState<EventModel>({} as EventModel);
  const [scenarioList, setScenarioList] = useState<ScenarioModel[]>([]);
  const [botList, setBotList] = useState<BotModel[]>([]);
  const [formVisible, setFormVisible] = useState(false);

  const onEdit = useCallback((values: EventModel) => {
    setFormItem(values);
    setFormVisible(true);
  }, []);

  const onRemoveEvent = useCallback((id: number) => {
    eventUsecase.remove(IdRequest.fromPartial({ id })).subscribe();
  }, []);

  useEffect(() => {
    const sub = botUsecase.load(ListOptions.fromPartial({
      limit: 1000,
      offset: 0,
      order: 'id asc'
    })).subscribe(res => setBotList(res.items));

    const sub2 = scenarioUsecase.list(ScenarioListReq.fromPartial({})).subscribe(res => {
      setScenarioList(res.items);
    });

    return () => {
      sub.unsubscribe();
      sub2.unsubscribe();
    }
  }, []);

  const columnsData = useCallback((methods: ITableMethods<EventModel>) => columns(onRemoveEvent, onEdit, botList, scenarioList, methods), [botList, scenarioList]);

  return <EventTableView
    add={() => onEdit({} as EventModel)}
    columns={columnsData}
    closeForm={() => setFormVisible(false)}
    formVisible={formVisible}
    formItem={formItem}
    scenarioList={scenarioList}
    botList={botList}
  />;
});

export default EventTable;