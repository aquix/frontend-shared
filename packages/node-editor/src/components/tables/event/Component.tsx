import { IBaseFormProps, IBaseTableViewProps } from '../types';
import { BotModel, EventModel, ScenarioModel } from '@local/modules/proto/botCommon';
import React, { FC } from 'react';
import { Button } from 'antd';
import TableStream from '@local/TableStream/TableStream';
import EventUsecase from '@local/modules/event/usecase';
import EventForm from './forms';

interface IEventTableView extends IBaseTableViewProps<EventModel> {
  scenarioList: ScenarioModel[],
  botList: BotModel[],
}

const EventTableView: FC<IEventTableView> = (props) => {
  const {
    columns,
    add,
    closeForm,
    formVisible,
    formItem,
    scenarioList,
    botList,
  } = props;

  const Form: FC<IBaseFormProps<EventModel>> = (props) => {
    return <EventForm {...props} botList={botList} scenarioList={scenarioList} values={formItem} />;
  }

  return (
    <div>
      <Button onClick={() => add()} style={{ marginBottom: 10 }}>
        Add
      </Button>
      <TableStream
        idKey='EventList'
        columns={columns}
        diKey={EventUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle='Event edit'
        formVisible={formVisible}
        closeForm={closeForm}
      />
    </div>
  )
}

export default EventTableView;