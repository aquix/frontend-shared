import { ITableMethods } from '@sharedModules/Stream/components/types';
import { ColumnsType } from 'antd/es/table';
import { FormInstance } from 'antd';

export interface IBaseTableViewProps<T extends object> {
  columns: (methods: ITableMethods<T>) => ColumnsType<T>,
  add: () => void,
  formItem: T,
  formVisible?: boolean,
  closeForm?: () => void,
}

export interface IBaseFormProps<T extends object> {
  values: T,
  onCloseForm: () => void,
  add: (item: T, toTop?: boolean) => void,
  update: (item: T) => void,
}

export interface IBaseFormViewProps<T extends object> {
  form: FormInstance<T>,
  onSubmit: (values: T) => void,
}