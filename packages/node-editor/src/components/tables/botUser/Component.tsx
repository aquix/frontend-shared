import React, { FC, useState } from 'react';
import { Button, Select } from 'antd';
import { BotModel, BotUserModel } from '@local/modules/proto/botCommon';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import TableStream from '@local/TableStream/TableStream';
import BotUserUsecase from '@local/modules/botUser/usecase';
import BotUserForm from './forms';

interface IBotUserTableView extends IBaseTableViewProps<BotUserModel> {
  botsList: BotModel[],
}

const { Option } = Select;

const BotUserTableView: FC<IBotUserTableView> = (props) => {
  const {
    columns,
    add,
    closeForm,
    formVisible,
    formItem,
    botsList,
  } = props;

  const Form: FC<IBaseFormProps<BotUserModel>> = (props) => {
    return <BotUserForm {...props} botsList={botsList} values={formItem} />;
  };

  if (!botsList.length) {
    return <div>Bots list is empty</div>;
  }

  const [handler, setHandler] = useState(botsList[0].handler);

  return (
    <div>
      <Button onClick={() => add()} style={{ marginBottom: 10 }}>
        Add
      </Button>
      <div>
        <Select
          placeholder="Select bot"
          onChange={setHandler}
          defaultValue={botsList[0].handler}
        >
          {botsList.map(i => (<Option key={i.id} value={i.handler}>{i.slug}</Option>))}
        </Select>
      </div>
      <TableStream
        idKey="BotUserList"
        columns={columns}
        diKey={BotUserUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle="Bot user edit"
        formVisible={formVisible}
        closeForm={closeForm}
        query={{ handler }}
      />
    </div>
  );
};

export default BotUserTableView;