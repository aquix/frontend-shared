import React, { FC, useCallback, useEffect, useState } from 'react';
import columns from './columns';
import { useInjection } from 'inversify-react';
import { observer } from 'mobx-react-lite';
import { StateMachine } from '@local/modules/proto/stateMachine';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import StateMachineTableView from './Component';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { BotModel, ScenarioModel } from '@local/modules/proto/botCommon';
import BotUsecase, { IBotUsecase } from '@local/modules/bot/usecase';
import StateMachineServiceUsecase, { IStateMachineServiceUsecase } from '@local/modules/stateMachineService/usecase';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';

export interface IStateMachineTableProps {
  beforeOpenEditor?: (stateMachine: StateMachine) => void,
}

const StateMachineServiceTable: FC<IStateMachineTableProps> = observer((props) => {
  const {
    beforeOpenEditor,
  } = props;

  const engine = useInjection<StateMachineEngineMV>(StateMachineEngineMV.diKey);
  const scenarioEngine = useInjection<ScenarioEngineMV>(ScenarioEngineMV.diKey);

  const stateMachineServiceUsecase = useInjection<IStateMachineServiceUsecase>(StateMachineServiceUsecase.diKey);

  const botsUsecase = useInjection<IBotUsecase>(BotUsecase.diKey);

  const [botsList, setBotsList] = useState<BotModel[]>([]);
  const [formItem, setFormItem] = useState<StateMachine>({} as StateMachine);
  const [formVisible, setFormVisible] = useState(false);

  // const [engineVisible, setEngineVisible] = useState(false);

  const onEdit = useCallback((values: StateMachine) => {
    setFormItem(values);
    setFormVisible(true);
  }, []);

  const onSetLayoutVisible = useCallback((values: StateMachine) => {
    scenarioEngine.loadLayerModel({} as ScenarioModel);
    engine.loadLayerModel(values);
    beforeOpenEditor && beforeOpenEditor(values);
    // setEngineVisible(true);
  }, []);

  const onRemove = useCallback((id: number) => {
    stateMachineServiceUsecase.delete(IdRequest.fromPartial({ id }));
  }, []);

  useEffect(() => {
    const listReq = ListOptions.fromPartial({
      limit: 100,
      offset: 0,
      order: 'id asc',
    });

    const sub = botsUsecase.load(listReq).subscribe(res => {
      setBotsList(res.items);
    });

    return () => sub.unsubscribe();
  }, []);

  useEffect(() => {
    scenarioEngine.clear();
    engine.clear();
  }, [])

  const columnsData = useCallback((methods: ITableMethods<StateMachine>) => columns(onRemove, onEdit, onSetLayoutVisible, botsList, methods), [botsList]);

  return <StateMachineTableView
    add={() => onEdit({} as StateMachine)}
    columns={columnsData}
    closeForm={() => setFormVisible(false)}
    formVisible={formVisible}
    formItem={formItem}
    botsList={botsList}
  />;

  // return (
  //   engineVisible ? <StateMachineEditor /> : <StateMachineTableView
  //     add={() => onEdit({} as StateMachine)}
  //     columns={columnsData}
  //     closeForm={() => setFormVisible(false)}
  //     formVisible={formVisible}
  //     formItem={formItem}
  //     botsList={botsList}
  //     />
  // )
});

export default StateMachineServiceTable;