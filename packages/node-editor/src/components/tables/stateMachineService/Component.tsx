import React, { FC } from 'react';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import { StateMachine } from '@local/modules/proto/stateMachine';
import { Button } from 'antd';
import TableStream from '@local/TableStream/TableStream';
import StateMachineServiceUsecase from '@local/modules/stateMachineService/usecase';
import ScenarioMachineForm from './forms';
import { BotModel } from '@local/modules/proto/botCommon';

interface IStateMachineTableView extends IBaseTableViewProps<StateMachine> {
  botsList: BotModel[],
}

const StateMachineTableView: FC<IStateMachineTableView> = (props) => {
  const {
    add,
    columns,
    formVisible,
    closeForm,
    botsList,
    formItem,
  } = props;

  const Form: FC<IBaseFormProps<StateMachine>> = (props) => {
    return <ScenarioMachineForm {...props} botsList={botsList} values={formItem} />
  }

  return (
    <div>
      <Button onClick={() => add()} style={{ marginBottom: 10 }}>
        Add
      </Button>
      <TableStream
        idKey='StateMachineList'
        columns={columns}
        diKey={StateMachineServiceUsecase.diKey}
        form={Form}
        modalTitle='State machine edit'
        formVisible={formVisible}
        closeForm={closeForm}
      />
    </div>
  )
}

export default StateMachineTableView;