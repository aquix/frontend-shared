import React from 'react';
import { StateMachine } from '@local/modules/proto/stateMachine';
import { ColumnsType } from 'antd/es/table';
import { BotModel } from '@local/modules/proto/botCommon';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { Button, Divider } from 'antd';
import { DeleteOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';

export default (onRemove: (id: number) => void, onEdit: (obj: StateMachine) => void, onView: (obj: StateMachine) => void, bots: BotModel[], methods: ITableMethods<StateMachine>): ColumnsType<StateMachine> => ([
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Bot',
    dataIndex: 'bot',
    key: 'bot',
    render: (value, record) => {
      const bot = bots.find(bot => bot.id === record.botId);
      if (bot) {
        return <span>{bot.slug}</span>
      } else {
        return <span/>;
      }
    },
  },
  {
    title: 'Meta',
    dataIndex: 'meta',
    key: 'meta',
  },
  {
    title: 'Actions',
    dataIndex: 'actions',
    key: 'actions',
    render: (value, record) => (
      <>
        <Button onClick={() => onView(record)}>
          <EyeOutlined />
        </Button>
        <Divider type="vertical" />
        <Button onClick={() => onEdit(record)}>
          <EditOutlined />
        </Button>
        <Divider type="vertical" />
        <Button onClick={() => {
          onRemove(record.id);
          methods.removeById(record.id);
        }} >
          <DeleteOutlined />
        </Button>
      </>
    ),
  },
]);