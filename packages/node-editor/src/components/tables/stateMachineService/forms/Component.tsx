import React, { FC } from 'react';
import { IBaseFormViewProps } from '../../types';
import { StateMachine } from '@local/modules/proto/stateMachine';
import { Button, Form, Input, Select } from 'antd';
import { BotModel } from '@local/modules/proto/botCommon';

interface IStateMachineFormView extends IBaseFormViewProps<StateMachine> {
  botsList: BotModel[],
}

const StateMachineFormView: FC<IStateMachineFormView> = (props) => {
  const {
    form,
    onSubmit,
    botsList,
  } = props;

  return (
    <Form<StateMachine>
      form={form}
      onFinish={onSubmit}
    >
      <Form.Item
        label="Bot"
        name="botId"
        initialValue={!!botsList.length && botsList[0].id}
      >
        <Select>
          {botsList.map(i => (<Select.Option key={i.id} value={i.id}>{i.slug}</Select.Option>))}
        </Select>
      </Form.Item>
      <Form.Item
        name="name"
        label="Name"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">Save</Button>
      </Form.Item>
    </Form>
  )
}

export default StateMachineFormView;