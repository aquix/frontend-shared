import React, { FC } from 'react';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { Button } from 'antd';
import TableStream from '@local/TableStream/TableStream';
import ScenarioUsecase from '@local/modules/scenario/usecase';
import ScenarioForm from './forms';

const ScenariosTableView: FC<IBaseTableViewProps<ScenarioModel>> = (props) => {
  const {
    add,
    columns,
    formVisible,
    closeForm,
    formItem,
  } = props;

  const Form: FC<IBaseFormProps<ScenarioModel>> = (props) => {
    return <ScenarioForm {...props} values={formItem} />;
  }

  return (
    <div>
      <Button onClick={() => add()} style={{ marginBottom: 10 }}>
        Add
      </Button>
      <TableStream
        idKey='ScenariosList'
        columns={columns}
        diKey={ScenarioUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle='Scenario edit'
        formVisible={formVisible}
        closeForm={closeForm}
      />
    </div>
  )
}

export default ScenariosTableView;