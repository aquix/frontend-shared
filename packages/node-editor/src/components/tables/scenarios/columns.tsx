import React from 'react';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { ColumnsType } from 'antd/es/table';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { Button, Divider } from 'antd';
import { DeleteOutlined, EditOutlined, EyeOutlined, LinkOutlined } from '@ant-design/icons';
import CopyToClipboard from '@modules/copyToClipboard/CopyToClipboard';
import ToasterMessageHandler from '@modules/toaster/handler';

export default (onRemove: (id: number) => void, onEdit: (obj: ScenarioModel) => void, onView: (obj: ScenarioModel) => void, methods: ITableMethods<ScenarioModel>): ColumnsType<ScenarioModel> => ([
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Title',
    dataIndex: 'title',
    key: 'title',
    render: (value, record) => (
      <>
        {record.title}
        <LinkOutlined onClick={() => CopyToClipboard((`${window.location.href}?scenario=${record.id}`), new ToasterMessageHandler())} />
      </>
    )
  },
  {
    title: 'Category',
    dataIndex: 'category',
    key: 'category',
  },
  {
    title: 'Actions',
    dataIndex: 'actions',
    key: 'actions',
    render: (value, record) => (
      <>
        <Button onClick={() => onView(record)}>
          <EyeOutlined />
        </Button>
        <Divider type="vertical" />
        <Button onClick={() => onEdit(record)}>
          <EditOutlined />
        </Button>
        <Divider type="vertical" />
        <Button onClick={() => {
          onRemove(record.id);
          methods.removeById(record.id);
        }} >
          <DeleteOutlined />
        </Button>
      </>
    ),
  },
]);