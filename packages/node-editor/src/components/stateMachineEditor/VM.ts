import { BaseViewModel } from '@sharedModules/Stream/model/BaseVM';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { merge, mergeMap, toArray } from 'rxjs';

class StateMachineEditorVM extends BaseViewModel {
  private engineVM: StateMachineEngineMV;

  constructor(
    engineVM: StateMachineEngineMV,
  ) {
    super();
    this.engineVM = engineVM;
  }

  load = () => {
    const sub = merge(
      this.engineVM.scenarioModule.load(),
      this.engineVM.stateServiceModule.load(),
    ).pipe(
      toArray(),
      mergeMap(() => {
        return this.engineVM.stateServiceModule.transitionsModule.load();
      })
    ).subscribe();

    this.addSubscription(sub);
  }
}

export default StateMachineEditorVM;