import { BaseViewModel } from '@sharedModules/Stream/model/BaseVM';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { Point } from '@projectstorm/geometry';
import { State } from '@local/modules/proto/stateMachine';

class StateMachineContextMenuVM extends BaseViewModel {
  private engineVM: StateMachineEngineMV;

  constructor(engineVM: StateMachineEngineMV) {
    super();

    this.engineVM = engineVM;
  }

  private getMouseLocation = (position: Point) => this.engineVM.engine.getRelativeMousePoint({ clientX: position.x, clientY: position.y }).clone();

  addState = (value: ScenarioModel, position: Point) => {
    this.engineVM.stateServiceModule.add(State.fromPartial({
      stateMachineId: this.engineVM.layerModel.getItem()!.id,
      scenarioId: value.id,
      meta: JSON.stringify({ position: this.getMouseLocation(position) }),
      name: value.title,
    }))
  }
}

export default StateMachineContextMenuVM;