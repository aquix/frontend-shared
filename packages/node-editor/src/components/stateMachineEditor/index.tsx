import React, { FC, useEffect, useState } from 'react';
import { useInjection } from 'inversify-react';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import StateMachineEditorVM from '@components/stateMachineEditor/VM';
import StateMachineEditorView from '@components/stateMachineEditor/Component';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { observer } from 'mobx-react-lite';

const StateMachineEditor: FC = () => {
  const engineMV = useInjection<StateMachineEngineMV>(StateMachineEngineMV.diKey);
  const scenarioEngineVM = useInjection<ScenarioEngineMV>(ScenarioEngineMV.diKey);

  const [vm] = useState(new StateMachineEditorVM(engineMV));

  useEffect(() => {
    if (!!engineMV.layerModel.getItem()?.id) {
      vm.load();
      return () => vm.unload();
    }
  }, [engineMV.layerModel.getItem()?.id]);

  return <StateMachineEditorView
    engine={engineMV.engine}
    scenario={scenarioEngineVM.layerModel.getItem()!}
    backToSM={() => scenarioEngineVM.clear()}
  />;
};

export default observer(StateMachineEditor);