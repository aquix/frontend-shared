import React, { FC } from 'react';
import { DiagramEngine } from '@projectstorm/react-diagrams';
import { CanvasWidget } from '@projectstorm/react-canvas-core';
import ScenarioEditor from '@components/scenarioEditor';
import ContextMenuComponent from '@components/stateMachineEditor/contextMenu';
import { Button } from 'antd';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { observer } from 'mobx-react-lite';

interface IStateMachineEditorView {
  engine: DiagramEngine,
  scenario: ScenarioModel,
  backToSM: () => void,
}

const StateMachineEditorView: FC<IStateMachineEditorView> = (props) => {
  const {
    engine,
    scenario,
    backToSM,
  } = props;

  return (
    <div style={{ position: 'relative', height: '100%' }}>
      {!scenario.id ? <ContextMenuComponent>
        <CanvasWidget
          className="diagram-container"
          engine={engine}
        />
      </ContextMenuComponent> : <>
        <Button style={{ position: 'absolute', top: 20, left: 20, zIndex: 1 }} onClick={() => backToSM()}>
          Back to SM
        </Button>
        <ScenarioEditor />
      </>}
    </div>
  )
}

export default StateMachineEditorView;