import React from 'react';
import { Action, InputType } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams';
import ScenarioActionNode from '@local/modules/scenarioAction/node';
import { Point } from '@projectstorm/geometry';
import ScenarioActionsModulesVM from '@local/modules/engine/modelViews/scenarioActions';
import { ScenarioActionModel } from '@local/modules/proto/botCommon';

class CopyItemAction extends Action {
  private prevKey: string = '';
  private copiedNode?: ScenarioActionNode;

  constructor(engine: DiagramEngine, module: ScenarioActionsModulesVM) {
    super({
      type: InputType.KEY_DOWN,
      fire: event => {
        const pressedKey = (event.event as React.KeyboardEvent).key;

        const selectedEntities = engine.getModel().getSelectedEntities();

        if (selectedEntities.length > 0) {
          if ((this.prevKey === 'Control' || this.prevKey === 'Meta') && pressedKey === 'c') {
            for (const node of selectedEntities) {
              if (node instanceof ScenarioActionNode) {
                const clonedElement = node.clone() as ScenarioActionNode;
                clonedElement.setId();
                clonedElement.setPosition(new Point(clonedElement.getPosition().x + 20, clonedElement.getPosition().y + 20));
                module.add(ScenarioActionModel.fromPartial({
                  ...clonedElement.model(),
                  meta: JSON.stringify(clonedElement.model().meta),
                  id: 0,
                }));
              }
            }
          } else {
            this.prevKey = pressedKey;
          }
        }

        // if ((this.prevKey === "Control" || this.prevKey === "Meta") && pressedKey === "v") {
        //   if (this.copiedNode) {
        //     const clonedElement = this.copiedNode.clone() as ScenarioActionNode;
        //     clonedElement.setId();
        //     clonedElement.setPosition(new Point(clonedElement.getPosition().x + 20, clonedElement.getPosition().y + 20))
        //     module.add(ScenarioActionModel.fromPartial({ ...clonedElement.model(), meta: JSON.stringify(clonedElement.model().meta), id: 0 }));
        //   }
        //
        //   return;
        // }
        //
        // if (selectedEntities.length === 1) {
        //   if ((this.prevKey === "Control" || this.prevKey === "Meta") && pressedKey === "c") {
        //     const node = selectedEntities[0];
        //     if (node instanceof ScenarioActionNode) {
        //       this.copiedNode = node;
        //     }
        //   } else {
        //     this.prevKey = pressedKey;
        //   }
        // }
      },
    });
  }
}

export default CopyItemAction;