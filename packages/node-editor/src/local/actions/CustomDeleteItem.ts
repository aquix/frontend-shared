import React from 'react';
import { Action, InputType } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams';

class CustomDeleteItemsAction extends Action {
  constructor(engine: DiagramEngine) {
    const options = {
      keyCodes: ['Backspace'],
      engine,
    };
    super({
      type: InputType.KEY_DOWN,
      fire: (event) => {
        if (options.keyCodes && options.keyCodes.indexOf((event.event as React.KeyboardEvent).key) !== -1) {
          const selectedEntities = options.engine.getModel().getSelectedEntities();
          if (selectedEntities.length > 0) {
            const confirm = window.confirm('Are you sure you want to delete?');

            if (confirm) {
              selectedEntities.forEach((model) => {
                model.fireEvent(model, 'remove');
              });
            }
          }
        }
      }
    });
  }
}

export default CustomDeleteItemsAction;