import useStream from '@modules/Stream/components/useStream';
import { ObjectWithId } from '@sharedModules/types';
import { observer } from 'mobx-react-lite';
import React, { FC, useMemo } from 'react';
import { IBaseTableContentProps, ITableMethods } from '@sharedModules/Stream/components/types';
import { Modal, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import PaginatorComponent from '@local/TableStream/Pagination';
import { IBaseFormProps } from '@components/tables/types';

interface ITableStream<T extends ObjectWithId, QueryType extends object = object> extends IBaseTableContentProps<T, QueryType> {
  idKey: string
  classNamePagination?: string,
  activeClassName?: string,
  tableCassName?: string,
  hidePaginator?: boolean,
  showSizeChanger?: boolean,
  columns: (methods: ITableMethods<T>) => ColumnsType<T>,
  form?: FC<IBaseFormProps<T>>,
  modalTitle?: string,
  formVisible?: boolean,
  closeForm?: () => void,
}

const scroll = { x: 1300 };

const TableStream = <T extends ObjectWithId, QueryType extends object = object>(props: ITableStream<T, QueryType>) => {
  const {
    form,
    formVisible,
    closeForm,
  } = props;

  const [items, pagination, onPageChanged, methods] = useStream<T>(props.diKey, props.query, props.method, props.countPerPage, props.order, props.currentPage);

  const Form: FC<IBaseFormProps<T>> = useMemo(() => form || (() => <></>), [form]);

  const columns = useMemo(() => props.columns(methods), [props.columns, methods]);

  return (
    <>
      <Table
        scroll={scroll}
        className={props.tableCassName}
        dataSource={items}
        columns={columns}
        rowKey={row => `TableItem-${props.idKey}-${row.id}`}
        pagination={false}
      />
      {!props.hidePaginator && (
        <div className="mt-16">
          <PaginatorComponent
            showSizeChanger={props.showSizeChanger}
            classNamePagination={props.classNamePagination}
            activeClassName={props.activeClassName}
            paginator={pagination}
            onPageChanged={onPageChanged}
            onChangeSize={onPageChanged}
          />
        </div>
      )}
      {props.form &&
        <Modal title={props.modalTitle} open={formVisible} footer={null} onCancel={() => closeForm && closeForm()}>
          <Form
            {...props.form.defaultProps as IBaseFormProps<T>}
            onCloseForm={() => closeForm && closeForm()}
            add={methods.add}
            update={methods.update}
          />
        </Modal>}
    </>
  );
};

export default observer(TableStream);