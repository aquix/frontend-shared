import ScenarioActionNode from '../scenarioAction/node';
import SocketNode from '../socket/node';

export interface ConnectorExtraModel {
  id: number,
  fromScenarioAction: ScenarioActionNode,
  fromSocket: SocketNode,
  toScenarioAction: ScenarioActionNode,
  toSocket: SocketNode,
  scenarioId: number,
}