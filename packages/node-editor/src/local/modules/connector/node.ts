import { DefaultLinkModel } from '@projectstorm/react-diagrams-defaults';
import { IdRequest } from '../proto/commonTypes';
import SocketNode from '../socket/node';
import ScenarioActionNode from '../scenarioAction/node';
import { ConnectorExtraModel } from './models';
import { action, makeObservable, observable } from 'mobx';
import { DefaultLinkModelGenerics } from '@projectstorm/react-diagrams';
import { ListenerHandle } from '@projectstorm/react-canvas-core';
import { ObjectWithId } from '@sharedModules/types';
import ConnectorsModulesVM from '@local/modules/engine/modelViews/connectors';
import { ConnectorModel } from '@local/modules/proto/botCommon';

export class ConnectorLink extends DefaultLinkModel implements ObjectWithId {
  @observable
  public id;
  @observable
  private readonly connector: ConnectorExtraModel = {} as ConnectorExtraModel;

  private module: ConnectorsModulesVM;

  constructor(module: ConnectorsModulesVM, props?: ConnectorExtraModel) {
    super();

    this.module = module;

    if (props) {
      this.id = props.id;
      this.options.id = `connector_${props.id}`;
      this.connector = props;
      this.setSourcePort(props.fromSocket);
      this.setTargetPort(props.toSocket);
    }

    this.setLocked(true);
    makeObservable(this);
  }

  @action
  setId(id: number) {
    this.id = id;
  }

  model() {
    return this.connector;
  }

  @action
  registerListener(listener: DefaultLinkModelGenerics['LISTENER']): ListenerHandle {
    const _listener = {
      'remove': () => {
        if (this.getTargetPort() as SocketNode) {
          this.module.remove(IdRequest.fromPartial({ id: this.connector.id }));
        } else {
          this.remove();
        }
      },
      entityRemoved: (e) => e.entity.getParent().getParent().removeLink(this),
      targetPortChanged: ({ entity }: any) => {
        const source = entity.sourcePort;
        const target = entity.targetPort;

        const targetParent = target.getParent() as ScenarioActionNode;
        const sourceParent = source.getParent() as ScenarioActionNode;

        let data = JSON.parse(targetParent.model().data as string || '{}');
        if (JSON.parse(sourceParent.model().data)[source.model.slug]) {
          data = {
            ...data,
            [`${target.model.slug}`]: JSON.parse((sourceParent).model().data)[source.model.slug],
          };
        } else {
          delete data[target.model.slug];
        }

        targetParent.model().data = JSON.stringify(data);

        targetParent.update();

        this.module.add(ConnectorModel.fromPartial({
          ...this.model(),
          fromScenarioAction: this.model().fromScenarioAction.model().id,
          fromSocket: this.model().fromSocket.model.id,
          toScenarioAction: this.model().toScenarioAction.model().id,
          toSocket: this.model().toSocket.model.id,
          scenarioId: this.model().scenarioId,
        }), this);
      }
    }

    const listenerId = `connector_${this.model().id}_listener`;

    this.listeners[listenerId] = _listener;
    return {
      id: listenerId,
      listener: _listener,
      deregister: () => {
        delete this.listeners[listenerId];
      }
    };
  }

  setSourcePort(port: SocketNode | null) {
    if (port !== null) {
      port.addLink(this);
      this.model().fromSocket = port;
      this.model().fromScenarioAction = (port.getParent() as ScenarioActionNode);
    }

    if (this.sourcePort) {
      this.sourcePort.removeLink(this);
    }

    this.sourcePort = port;
    this.fireEvent({ port }, 'sourcePortChanged');
    if (port?.reportedPosition) {
      this.getPointForPort(port).setPosition(port.getCenter());
    }
  }

  setTargetPort(port: SocketNode | null) {
    let source = this.getSourcePort() as SocketNode;

    if (port) {
      if ((port.model.type !== source.model.type) || (port.model.id === source.model.id)) {
        this.remove();
        return;
      }

      if (source.model.type === port.model.type) {
        let newTargetPort: SocketNode;
        let newSourcePort: SocketNode;

        if (!source.model.direction) {
          newTargetPort = source;
          newSourcePort = port;
          this.model().fromScenarioAction = (newSourcePort.getParent() as ScenarioActionNode);
        } else {
          newTargetPort = port;
          newSourcePort = source;
        }

        const targetParent = newTargetPort.getParent() as ScenarioActionNode;
        const sourceParent = newSourcePort.getParent() as ScenarioActionNode;

        this.model().fromScenarioAction = sourceParent;
        this.model().fromSocket = newSourcePort;

        this.model().toScenarioAction = targetParent;
        this.model().toSocket = newTargetPort;
        this.model().scenarioId = targetParent.model().scenarioId;

        if (!source.model.direction) {
          super.setSourcePort(newSourcePort);
        }

        newTargetPort.setIsEditable(false);

        super.setTargetPort(newTargetPort);
      }
    }
  }

  // performanceTune() {
  //   return true;
  // }
}

export default ConnectorLink;