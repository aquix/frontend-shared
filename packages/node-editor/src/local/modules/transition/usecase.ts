import { Transition } from '../proto/stateMachine';
import { inject, injectable } from 'inversify';
import TransitionService, { ITransitionService } from './service';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface ITransitionUsecase {
  create(request: Transition): Observable<IdRequest>,
  update(request: Transition): Observable<void>,
  delete(request: IdRequest): Observable<void>,
  list(request: IdRequest): Observable<Transition>,
}

@injectable()
class TransitionUsecase implements ITransitionUsecase {
  public static diKey = Symbol.for('TransitionUsecaseDiKey');

  private transitionService: ITransitionService;

  constructor(
    @inject(TransitionService.diKey) transitionService: ITransitionService,
  ) {
    this.transitionService = transitionService;
  }

  create(request: Transition): Observable<IdRequest> {
    return this.transitionService.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.transitionService.delete(request);
  }

  list(request: IdRequest): Observable<Transition> {
    return this.transitionService.list(request);
  }

  update(request: Transition): Observable<void> {
    return this.transitionService.update(request);
  }
}

export default TransitionUsecase;