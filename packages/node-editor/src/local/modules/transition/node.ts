import { DefaultLinkModelGenerics } from '@projectstorm/react-diagrams';
import { DefaultLinkModel, DefaultPortModel } from '@projectstorm/react-diagrams-defaults';
import StateServiceNodeModel from '../stateService/node';
import { TransitionPortNode } from '../statePort/node';
import { action, makeObservable, observable } from 'mobx';
import { TransitionExtraModel } from './models';
import { ListenerHandle } from '@projectstorm/react-canvas-core';
import { Transition } from '../proto/stateMachine';
import { ObjectWithId } from '@sharedModules/types';
import TransitionModuleVM from '@local/modules/engine/modelViews/transition';

class TransitionNode extends DefaultLinkModel implements ObjectWithId {
  @observable
  public id;
  @observable
  private readonly transition: TransitionExtraModel = {} as TransitionExtraModel;

  private module: TransitionModuleVM;

  constructor(module: TransitionModuleVM, props?: TransitionExtraModel) {
    super();

    this.module = module;

    if (props) {
      this.id = props.id;
      this.transition = props;
    }

    this.setLocked(true);
    makeObservable(this);
  }

  @action
  setId(id: number) {
    this.id = id;
  }

  @action
  registerListener(listener: DefaultLinkModelGenerics['LISTENER']): ListenerHandle {
    const _listener = {
      'remove': (e) => {
        this.module.update(Transition.fromPartial({
          id: this.model().id,
          stateId: this.model().stateId,
          trigger: this.model().trigger,
          meta: JSON.stringify(this.model().meta),
        }));
        this.remove();
      },
      entityRemoved: (e) => e.entity.getParent().getParent().removeLink(this),
      targetPortChanged: (e) => {
        this.module.update(Transition.fromPartial({
          id: this.model().id,
          stateId: this.model().stateId,
          trigger: this.model().trigger,
          toStateId: this.model().toState!.model().id,
          meta: JSON.stringify(this.model().meta),
        }));
      },
    };

    const listenerId = `transition_${this.model().id}_listener`;

    this.listeners[listenerId] = _listener;
    return {
      id: listenerId,
      listener: _listener,
      deregister: () => {
        delete this.listeners[listenerId];
      },
    };
  }

  model() {
    return this.transition;
  }

  setSourcePort(port: DefaultPortModel | null) {
    if (port !== null) {
      port.addLink(this);
    }

    this.sourcePort = port;
    this.fireEvent({ port }, 'sourcePortChanged');
    if (port?.reportedPosition) {
      this.getPointForPort(port).setPosition(port.getCenter());
    }
  }

  setTargetPort(port: DefaultPortModel | null) {
    const source = this.getSourcePort() as TransitionPortNode;

    if (port) {
      let newTargetPort;
      let newSourcePort;

      if (source.getOptions().in) {
        newTargetPort = source;
        newSourcePort = port;
      } else {
        newTargetPort = port;
        newSourcePort = source;
      }

      const targetParent = newTargetPort.getParent() as StateServiceNodeModel;

      if (newSourcePort.transition.model().stateId === targetParent.model().id) {
        super.setTargetPort(newTargetPort);
        return;
      }

      this.transition.toState = targetParent;

      super.setTargetPort(newTargetPort);
    } else {
      this.targetPort = null;
    }
  }
}

export default TransitionNode;