import StateServiceNodeModel from '../stateService/node';
import { PointModel } from '@projectstorm/react-diagrams';

interface Meta {
  points: PointModel[],
}

export interface TransitionExtraModel {
  id: number;
  stateId: number;
  trigger: string;
  toState?: StateServiceNodeModel;
  meta: Meta;
}