import { inject, injectable } from 'inversify';
import { Transition } from '../proto/stateMachine';
import {
  TransitionServiceCreateDesc,
  TransitionServiceDeleteDesc,
  TransitionServiceListDesc, TransitionServiceUpdateDesc,
} from '../proto/stateMachineServices';
import { ApiClient } from '@sharedModules/grpcClient';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface ITransitionService {
  create(request: Transition): Observable<IdRequest>,
  update(request: Transition): Observable<void>,
  delete(request: IdRequest): Observable<void>,
  list(request: IdRequest): Observable<Transition>,
}

@injectable()
class TransitionService implements ITransitionService {
  public static diKey = Symbol.for('TransitionService');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    this.api = api;
  }

  create(request: Transition): Observable<IdRequest> {
    return this.api.unary$(
      TransitionServiceCreateDesc,
      {
        obj: request,
        coder: Transition,
      }
    );
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      TransitionServiceDeleteDesc,
      {
        obj: request,
        coder: IdRequest
      }
    );
  }

  list(request: IdRequest): Observable<Transition> {
    return this.api.stream$(
      TransitionServiceListDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    );
  }

  update(request: Transition): Observable<void> {
    return this.api.unary$(
      TransitionServiceUpdateDesc,
      {
        obj: request,
        coder: Transition,
      }
    );
  }
}

export default TransitionService;