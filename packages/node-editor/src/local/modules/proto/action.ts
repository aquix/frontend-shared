/* eslint-disable */
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { ActionModel } from "./botCommon";
import { IdRequest, BoolValue, UInt64Value } from "./commonTypes";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "botMs";

export interface Action {
  Create(
    request: DeepPartial<ActionModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest>;
  Update(
    request: DeepPartial<ActionModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ActionModel>;
  List(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Observable<ActionModel>;
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<UInt64Value>;
}

export class ActionClientImpl implements Action {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.Delete = this.Delete.bind(this);
    this.Get = this.Get.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
  }

  Create(
    request: DeepPartial<ActionModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest> {
    return this.rpc.unary(
      ActionCreateDesc,
      ActionModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<ActionModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ActionUpdateDesc,
      ActionModel.fromPartial(request),
      metadata
    );
  }

  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      ActionDeleteDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ActionModel> {
    return this.rpc.unary(
      ActionGetDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Observable<ActionModel> {
    return this.rpc.invoke(
      ActionListDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<UInt64Value> {
    return this.rpc.unary(
      ActionCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }
}

export const ActionDesc = {
  serviceName: "botMs.Action",
};

export const ActionCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: ActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ActionModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...IdRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ActionUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: ActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ActionModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ActionDeleteDesc: UnaryMethodDefinitionish = {
  methodName: "Delete",
  service: ActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ActionGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: ActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ActionModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ActionListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: ActionDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ActionModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ActionCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: ActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...UInt64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
