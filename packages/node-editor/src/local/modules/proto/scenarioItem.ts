/* eslint-disable */
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { ScenarioItemModel, ScenarioItemBatch } from "./botCommon";
import { IdRequest, BoolValue } from "./commonTypes";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "botMs";

export interface ScenarioItem {
  Create(
    request: DeepPartial<ScenarioItemModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest>;
  Update(
    request: DeepPartial<ScenarioItemModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  BatchUpdate(
    request: DeepPartial<ScenarioItemBatch>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ScenarioItemModel>;
  List(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Observable<ScenarioItemModel>;
}

export class ScenarioItemClientImpl implements ScenarioItem {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.BatchUpdate = this.BatchUpdate.bind(this);
    this.Delete = this.Delete.bind(this);
    this.Get = this.Get.bind(this);
    this.List = this.List.bind(this);
  }

  Create(
    request: DeepPartial<ScenarioItemModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest> {
    return this.rpc.unary(
      ScenarioItemCreateDesc,
      ScenarioItemModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<ScenarioItemModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ScenarioItemUpdateDesc,
      ScenarioItemModel.fromPartial(request),
      metadata
    );
  }

  BatchUpdate(
    request: DeepPartial<ScenarioItemBatch>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ScenarioItemBatchUpdateDesc,
      ScenarioItemBatch.fromPartial(request),
      metadata
    );
  }

  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      ScenarioItemDeleteDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ScenarioItemModel> {
    return this.rpc.unary(
      ScenarioItemGetDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Observable<ScenarioItemModel> {
    return this.rpc.invoke(
      ScenarioItemListDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }
}

export const ScenarioItemDesc = {
  serviceName: "botMs.ScenarioItem",
};

export const ScenarioItemCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: ScenarioItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ScenarioItemModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...IdRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioItemUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: ScenarioItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ScenarioItemModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioItemBatchUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "BatchUpdate",
  service: ScenarioItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ScenarioItemBatch.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioItemDeleteDesc: UnaryMethodDefinitionish = {
  methodName: "Delete",
  service: ScenarioItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioItemGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: ScenarioItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ScenarioItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioItemListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: ScenarioItemDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ScenarioItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
