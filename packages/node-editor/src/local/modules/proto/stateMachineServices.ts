/* eslint-disable */
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import {
  StateMachine,
  StateMachineResp,
  State,
  StateBatch,
  Transition,
} from "./stateMachine";
import { IdRequest, BoolValue, UInt64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { Observable } from "rxjs";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "botMs";

export interface StateMachineService {
  Create(
    request: DeepPartial<StateMachine>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest>;
  Update(
    request: DeepPartial<StateMachine>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<StateMachine>;
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Promise<StateMachineResp>;
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<UInt64Value>;
}

export class StateMachineServiceClientImpl implements StateMachineService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.Delete = this.Delete.bind(this);
    this.Get = this.Get.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
  }

  Create(
    request: DeepPartial<StateMachine>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest> {
    return this.rpc.unary(
      StateMachineServiceCreateDesc,
      StateMachine.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<StateMachine>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      StateMachineServiceUpdateDesc,
      StateMachine.fromPartial(request),
      metadata
    );
  }

  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      StateMachineServiceDeleteDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<StateMachine> {
    return this.rpc.unary(
      StateMachineServiceGetDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Promise<StateMachineResp> {
    return this.rpc.unary(
      StateMachineServiceListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<UInt64Value> {
    return this.rpc.unary(
      StateMachineServiceCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }
}

export const StateMachineServiceDesc = {
  serviceName: "botMs.StateMachineService",
};

export const StateMachineServiceCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: StateMachineServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return StateMachine.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...IdRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateMachineServiceUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: StateMachineServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return StateMachine.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateMachineServiceDeleteDesc: UnaryMethodDefinitionish = {
  methodName: "Delete",
  service: StateMachineServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateMachineServiceGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: StateMachineServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...StateMachine.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateMachineServiceListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: StateMachineServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...StateMachineResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateMachineServiceCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: StateMachineServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...UInt64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export interface StateService {
  Create(
    request: DeepPartial<State>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest>;
  Update(request: DeepPartial<State>, metadata?: grpc.Metadata): Promise<Empty>;
  BatchUpdate(
    request: DeepPartial<StateBatch>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<State>;
  List(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Observable<State>;
}

export class StateServiceClientImpl implements StateService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.BatchUpdate = this.BatchUpdate.bind(this);
    this.Delete = this.Delete.bind(this);
    this.Get = this.Get.bind(this);
    this.List = this.List.bind(this);
  }

  Create(
    request: DeepPartial<State>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest> {
    return this.rpc.unary(
      StateServiceCreateDesc,
      State.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<State>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      StateServiceUpdateDesc,
      State.fromPartial(request),
      metadata
    );
  }

  BatchUpdate(
    request: DeepPartial<StateBatch>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      StateServiceBatchUpdateDesc,
      StateBatch.fromPartial(request),
      metadata
    );
  }

  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      StateServiceDeleteDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<State> {
    return this.rpc.unary(
      StateServiceGetDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Observable<State> {
    return this.rpc.invoke(
      StateServiceListDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }
}

export const StateServiceDesc = {
  serviceName: "botMs.StateService",
};

export const StateServiceCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: StateServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return State.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...IdRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateServiceUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: StateServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return State.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateServiceBatchUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "BatchUpdate",
  service: StateServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return StateBatch.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateServiceDeleteDesc: UnaryMethodDefinitionish = {
  methodName: "Delete",
  service: StateServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateServiceGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: StateServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...State.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StateServiceListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: StateServiceDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...State.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export interface TransitionService {
  Create(
    request: DeepPartial<Transition>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest>;
  Update(
    request: DeepPartial<Transition>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<Transition>;
  List(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Observable<Transition>;
}

export class TransitionServiceClientImpl implements TransitionService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.Delete = this.Delete.bind(this);
    this.Get = this.Get.bind(this);
    this.List = this.List.bind(this);
  }

  Create(
    request: DeepPartial<Transition>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest> {
    return this.rpc.unary(
      TransitionServiceCreateDesc,
      Transition.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<Transition>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      TransitionServiceUpdateDesc,
      Transition.fromPartial(request),
      metadata
    );
  }

  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      TransitionServiceDeleteDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<Transition> {
    return this.rpc.unary(
      TransitionServiceGetDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Observable<Transition> {
    return this.rpc.invoke(
      TransitionServiceListDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }
}

export const TransitionServiceDesc = {
  serviceName: "botMs.TransitionService",
};

export const TransitionServiceCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: TransitionServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Transition.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...IdRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const TransitionServiceUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: TransitionServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Transition.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const TransitionServiceDeleteDesc: UnaryMethodDefinitionish = {
  methodName: "Delete",
  service: TransitionServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const TransitionServiceGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: TransitionServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Transition.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const TransitionServiceListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: TransitionServiceDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Transition.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
