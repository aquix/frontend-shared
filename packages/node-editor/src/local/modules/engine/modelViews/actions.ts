import BaseEngineModuleVM, { IBaseEngineModuleVM } from '@local/modules/engine/modelViews/base';
import { ActionModel, ScenarioModel } from '@local/modules/proto/botCommon';
import ActionNodeModel from '@local/modules/action/node';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { action } from 'mobx';
import ActionUsecase from '@local/modules/action/usecase';
import { EMPTY, map, Observable } from 'rxjs';

class ActionsModuleVM extends BaseEngineModuleVM<ActionModel, ActionNodeModel, ScenarioModel, ScenarioEngineMV> implements IBaseEngineModuleVM<ActionModel> {
  private actionsUC: ActionUsecase;

  constructor(engine: ScenarioEngineMV, actionsUC: ActionUsecase) {
    super(engine);
    this.actionsUC = actionsUC;
  }

  @action
  load(): Observable<ActionModel> {
    if (!!this.list.items.length) {
      return EMPTY;
    }

    return this.actionsUC.load()
      .pipe(
        map(i => {
          this.list.add(new ActionNodeModel(i));

          return i;
        }),
      );
  }
}

export default ActionsModuleVM;