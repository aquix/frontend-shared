import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import ScenarioUsecase from '@local/modules/scenario/usecase';
import { action } from 'mobx';
import { ScenarioListReq, ScenarioModel, ScenarioModelListResp } from '@local/modules/proto/botCommon';
import { map, Observable } from 'rxjs';
import BaseEngineModuleVM from '@local/modules/engine/modelViews/base';
import { StateMachine } from '@local/modules/proto/stateMachine';

class ScenarioModuleVM extends BaseEngineModuleVM<ScenarioModel, ScenarioModel, StateMachine, StateMachineEngineMV> {
  private scenarioUC: ScenarioUsecase;

  constructor(engine: StateMachineEngineMV, scenarioUC: ScenarioUsecase) {
    super(engine);
    this.scenarioUC = scenarioUC;
  }

  @action
  load(): Observable<ScenarioModelListResp> {
    return this.scenarioUC.list(ScenarioListReq.fromPartial({})).pipe(map(res => {
      this.list.items = res.items;
      return res;
    }));
  }
}

export default ScenarioModuleVM;