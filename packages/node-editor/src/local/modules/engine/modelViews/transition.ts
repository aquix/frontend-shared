import BaseEngineModuleVM, { IBaseEngineModuleVM } from '@local/modules/engine/modelViews/base';
import { StateMachine, Transition } from '@local/modules/proto/stateMachine';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import TransitionNode from '@local/modules/transition/node';
import TransitionUsecase from '@local/modules/transition/usecase';
import { action } from 'mobx';
import { IdRequest } from '@local/modules/proto/commonTypes';
import { map, Observable } from 'rxjs';
import { StatePortNode, TransitionPortNode } from '@local/modules/statePort/node';
import { Id64Request } from '@sharedModules/shared/commonTypes';

class TransitionModuleVM extends BaseEngineModuleVM<Transition, TransitionNode, StateMachine, StateMachineEngineMV> implements IBaseEngineModuleVM<Transition> {
  private readonly transitionUC: TransitionUsecase;

  constructor(engine: StateMachineEngineMV, transitionUC: TransitionUsecase) {
    super(engine);
    this.transitionUC = transitionUC;
  }

  @action
  load(): Observable<Transition> {
    this.list.clear();
    return this.transitionUC.list(IdRequest.fromPartial({ id: this.engineVM.layerModel.getItem()!.id })).pipe(map(res => {
        const fromState = this.engineVM.stateServiceModule.list.items.find(i => i.model().id === res.stateId);
        const toState = this.engineVM.stateServiceModule.list.items.find(i => i.model().id === res.toStateId);

        this.engineVM.stateServiceModule.list.items.find(node => {
          node.model().transitions.map(transition => {
            if (transition.model().id === res.id) {
              if (toState && fromState) {
                transition.setSourcePort(fromState.getPort(`transition_${res.id}_out`) as TransitionPortNode);
                transition.setTargetPort(toState.getPort(`state_${toState.model().id}_in`) as StatePortNode);
                this.engineVM.engine.getModel().addLink(transition);
                this.list.add(transition);
              }
            }
          });
        });

      this.engineVM.engine.repaintCanvas();
      return res;
    }));
  }

  @action
  add(value: Transition): void {
    this.transitionUC.create(value)
      .pipe(map(res => {
        value.id = res.id;
        const stateList = this.engineVM.stateServiceModule.list.items;

        const state = stateList.find(i => i.model().id === value.stateId);

        if (state) {
          const transitionModule = new TransitionNode(this, {
            ...value,
            meta: JSON.parse(value.meta),
          });
          state.addPort(new TransitionPortNode(transitionModule));
          transitionModule.setId(res.id);
          this.list.add(transitionModule);
        }

        this.engineVM.engine.repaintCanvas();
        return res;
      })).subscribe();
  }

  @action
  update(value: Transition) {
    this.transitionUC.update(value).subscribe(() => {
      const list = this.list.items;

      const transitionItem = list.find(i => i.model().id === value.id);

      if (transitionItem) {
        transitionItem.model().stateId = value.stateId;
        transitionItem.model().trigger = value.trigger;
        transitionItem.model().meta = JSON.parse(value.meta);

        if (!value.toStateId) {
          this.engineVM.engine.getModel().removeLink(transitionItem);
        }

        this.list.update(transitionItem);
      }

      this.engineVM.engine.repaintCanvas();
    });
  }

  @action
  remove(value: Id64Request) {
    return this.transitionUC.delete(value).subscribe(() => {
      const list = this.list.items;
      const index = list.findIndex(i => i.model().id === value.id);

      if (!!(index + 1)) {
        const item = list[index];
        if (item.getSourcePort()) {
          item.getSourcePort().removeLink(item);
        }

        if (item.getTargetPort()) {
          item.getTargetPort().removeLink(item);
        }

        const stateList = this.engineVM.stateServiceModule.list.items;
        const stateIndex = stateList.findIndex(i => i.model().id === item.model().stateId);

        if (!!~index) {
          const stateTransitions = stateList[stateIndex].model().transitions;
          const stateTransitionIndex = stateTransitions.findIndex(i => i.id === value.id);

          if (!!~stateTransitionIndex) {
            stateTransitions.splice(stateTransitionIndex, 1);
          }
        }

        this.engineVM.engine.getModel().removeLink(item);
        this.list.removeById(value.id);
      }
      this.engineVM.engine.repaintCanvas();
    });
  }
}

export default TransitionModuleVM;