import { injectable } from 'inversify';
import createEngine, { DefaultDiagramState, DiagramEngine } from '@projectstorm/react-diagrams';
import { action, observable } from 'mobx';
import CustomDiagramModel from './diagramModel';
import { InputType } from '@projectstorm/react-canvas-core';
import ScenarioItemsModuleVM from '@local/modules/engine/modelViews/scenarioItems';
import ScenarioItemUsecase from '@local/modules/scenarioItem/usecase';
import { ObjectWithId } from '@sharedModules/types';
import ObservableVM from '@sharedModules/Stream/model/BaseVM';
import CustomDeleteItemsAction from '@local/actions/CustomDeleteItem';

@injectable()
class EngineMV<ENGINE_MODEL extends ObjectWithId = ObjectWithId> {
  @observable
  engine: DiagramEngine = createEngine();
  @observable
  layerModel: ObservableVM<ENGINE_MODEL> = new ObservableVM({} as ENGINE_MODEL);

  scenarioItemsModule: ScenarioItemsModuleVM<ENGINE_MODEL>;

  constructor(scenarioItemUC: ScenarioItemUsecase) {
    this.scenarioItemsModule = new ScenarioItemsModuleVM<ENGINE_MODEL>(this, scenarioItemUC);

    const actions = this.engine.getActionEventBus().getActionsForType(InputType.KEY_DOWN);
    this.engine.getActionEventBus().deregisterAction(actions[0]);
    this.engine.getActionEventBus().registerAction(new CustomDeleteItemsAction(this.engine));

    const state = this.engine.getStateMachine().getCurrentState();

    if (state instanceof DefaultDiagramState) {
      state.dragNewLink.config.allowLooseLinks = false;
    }
  }

  getModel(): CustomDiagramModel {
    // @ts-ignore
    return this.engine.getModel();
  }

  @action
  clear() {
    const model: any = new CustomDiagramModel();
    this.engine.setModel(model);
    this.layerModel.setItem({} as ENGINE_MODEL);
  }

  @action
  loadLayerModel(model: ENGINE_MODEL) {
    this.clear();
    this.layerModel.setItem(model);
  }
}

export default EngineMV;