import { DefaultNodeModel } from '@projectstorm/react-diagrams';
import { ActionModel } from '../proto/botCommon';
import { computed } from 'mobx';
import { injectable } from 'inversify';

@injectable()
class ActionNodeModel extends DefaultNodeModel {
  public id: number;
  private readonly actionModel: ActionModel;

  constructor(props: ActionModel) {
    super();

    this.id = props.id;
    this.actionModel = props;
    this.addOutPort('Scenario Action');
    this.options.color = 'rgb(192,255,0)';
    this.options.name = props.name;
  }

  @computed
  get model() {
    return this.actionModel;
  }
}

export default ActionNodeModel;