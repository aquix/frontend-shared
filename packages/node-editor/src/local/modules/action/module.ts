import { ContainerModule, interfaces } from 'inversify';
import ActionService, { IActionService } from './service';
import ActionUsecase, { IActionUsecase } from './usecase';

export const ActionModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IActionService>(ActionService.diKey).to(ActionService);

  bind<IActionUsecase>(ActionUsecase.diKey).to(ActionUsecase).inSingletonScope();
});