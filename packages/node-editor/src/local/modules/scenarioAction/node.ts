import { DefaultNodeModel, DefaultNodeModelGenerics } from '@projectstorm/react-diagrams';
import { ScenarioActionModel } from '../proto/botCommon';
import { ScenarioActionExtraModel } from './models';
import SocketNode from '../socket/node';
import { ListenerHandle } from '@projectstorm/react-canvas-core';
import { debounce } from 'lodash';
import { action, makeObservable, observable } from 'mobx';
import { DefaultNodeModelOptions } from '@projectstorm/react-diagrams-defaults';
import { ObjectWithId } from '@sharedModules/types';
import ScenarioActionsModulesVM from '@local/modules/engine/modelViews/scenarioActions';
import { IdRequest } from '@sharedModules/shared/commonTypes';

class ScenarioActionNode extends DefaultNodeModel implements ObjectWithId {
  @observable
  public id;

  private module: ScenarioActionsModulesVM;

  @observable
  public options: DefaultNodeModelOptions = {
    type: 'node_factory'
  };

  @observable
  ports: { [s: string]: SocketNode } = {};

  @observable
  scenarioAction: ScenarioActionExtraModel = {} as ScenarioActionExtraModel;

  constructor(props: ScenarioActionExtraModel, module: ScenarioActionsModulesVM) {
    super();

    this.module = module;

    this.id = props.id;
    this.options.id = `scenario_action_${props.id}`;
    this.options.name = props.name;

    this.scenarioAction.id = props.id;
    this.scenarioAction.actionId = props.actionId;
    this.scenarioAction.data = props.data;
    this.scenarioAction.scenarioId = props.scenarioId;
    this.scenarioAction.initial = props.initial;
    this.scenarioAction.sockets = props.sockets;

    this.scenarioAction.meta = { ...this.scenarioAction.meta, ...props.meta };

    if (this.scenarioAction.meta.position) {
      this.setPosition(this.scenarioAction.meta.position.x, this.scenarioAction.meta.position.y);
    }

    const sortedArray = [
      ...props.sockets.filter(({ type }) => type === 4),
      ...props.sockets.filter(({ type }) => type !== 4),
    ];

    sortedArray.forEach(i => {
      this.addPort(new SocketNode(i, module.connectorsModule));
    });
    makeObservable(this);
  }

  @action
  setId(id?: number) {
    this.id = id;
  }

  @action
  setSelected(selected?: boolean) {
    super.setSelected(selected);
  }

  @action
  registerListener(listener: DefaultNodeModelGenerics['LISTENER']): ListenerHandle {
    const _listener = {
      positionChanged: debounce(() => this.module.updatePosition(ScenarioActionModel.fromPartial({ ...this.model(), meta: JSON.stringify(this.model().meta) })), 200),
      'remove': () => {
        this.module.remove(IdRequest.fromPartial({ id: this.model().id }));
      },
    }

    const listenerId = `node_${this.model().id}_listener`;

    this.listeners[listenerId] = _listener;
    return {
      id: listenerId,
      listener: _listener,
      deregister: () => {
        delete this.listeners[listenerId];
      }
    };
  }

  model() {
    this.scenarioAction.meta.position = this.getPosition();
    return this.scenarioAction;
  }

  @action
  setModel(model: ScenarioActionExtraModel) {
    this.scenarioAction = model;
  }

  @action
  update() {
    this.module.update(ScenarioActionModel.fromPartial({ ...this.model(), meta: JSON.stringify(this.model().meta) }));
  }
}

export default ScenarioActionNode;