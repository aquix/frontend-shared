import { Point } from '@projectstorm/geometry';
import { SocketModel } from '../proto/botCommon';

interface Meta {
  position?: Point,
}

export interface ScenarioActionExtraModel {
  id: number;
  name: string,
  actionId: number;
  data: string;
  meta: Meta;
  scenarioId: number;
  initial: boolean;
  sockets: SocketModel[],
}