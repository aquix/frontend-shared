import { ContainerModule, interfaces } from 'inversify';
import ScenarioActionService, { IScenarioActionService } from './service';
import ScenarioActionUsecase, { IScenarioActionUsecase } from './usecase';

export const ScenarioActionModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IScenarioActionService>(ScenarioActionService.diKey).to(ScenarioActionService);

  bind<IScenarioActionUsecase>(ScenarioActionUsecase.diKey).to(ScenarioActionUsecase).inSingletonScope();
});