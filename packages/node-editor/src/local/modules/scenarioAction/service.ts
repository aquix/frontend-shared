import { ScenarioActionBatch, ScenarioActionModel, ScenarioActionReq } from '../proto/botCommon';
import { inject, injectable } from 'inversify';
import {
  ScenarioActionBatchUpdateDesc,
  ScenarioActionCreateDesc,
  ScenarioActionDeleteDesc,
  ScenarioActionListDesc, ScenarioActionUpdateDesc,
} from '../proto/scenarioAction';
import { ApiClient } from '@sharedModules/grpcClient';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface IScenarioActionService {
  create(request: ScenarioActionModel): Observable<IdRequest>,
  delete(request: IdRequest): Observable<void>,
  list(request: ScenarioActionReq): Observable<ScenarioActionModel>,
  batchUpdate(request: ScenarioActionBatch): Observable<void>,
  update(request: ScenarioActionModel): Observable<void>,
}

@injectable()
class ScenarioActionService implements IScenarioActionService {
  public static diKey = Symbol.for('ScenarioActionService');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    this.api = api;
  }

  create(request: ScenarioActionModel): Observable<IdRequest> {
    return this.api.unary$(
      ScenarioActionCreateDesc,
      {
        obj: request,
        coder: ScenarioActionModel,
      }
    );
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      ScenarioActionDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    );
  }

  list(request: ScenarioActionReq): Observable<ScenarioActionModel> {
    return this.api.stream$(
      ScenarioActionListDesc,
      {
        obj: request,
        coder: ScenarioActionReq,
      }
    );
  }

  batchUpdate(request: ScenarioActionBatch): Observable<void> {
    return this.api.unary$(
      ScenarioActionBatchUpdateDesc,
      {
        obj: request,
        coder: ScenarioActionBatch,
      }
    )
  }

  update(request: ScenarioActionModel): Observable<void> {
    return this.api.unary$(
      ScenarioActionUpdateDesc,
      {
        obj: request,
        coder: ScenarioActionModel,
      }
    )
  }
}

export default ScenarioActionService;