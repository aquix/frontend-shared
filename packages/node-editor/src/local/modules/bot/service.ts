import { inject, injectable } from 'inversify';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { ApiClient } from '@sharedModules/grpcClient';
import StreamHandler from '@sharedModules/Stream/service/StreamHandler';
import { map, mergeMap, Observable } from 'rxjs';
import { BotListResp, BotModel } from '@local/modules/proto/botCommon';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { BotCountDesc, BotCreateDesc, BotDeleteDesc, BotListDesc, BotUpdateDesc } from '@local/modules/proto/bot';
import { Empty } from '@sharedModules/shared/empty';
import { UInt64Value } from '@local/modules/proto/commonTypes';

export interface IBotService {
  create: (req: BotModel) => Observable<BotModel>,
  update: (req: BotModel) => Observable<void>,
  remove: (req: IdRequest) => Observable<void>,
  load: (req: ListOptions) => Observable<BotListResp>,
}

@injectable()
class BotService extends StreamHandler<BotModel> implements IBotService {
  public static diKey = Symbol.for('BotServiceKey');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    super();
    this.api = api;
    this.add('default', this.streamDefault)
  }

  private streamDefault = {
    stream: (opts: ListOptions): Observable<BotModel> => {
      return this.load(opts).pipe(mergeMap(e => e.items));
    },
    loadCount: (): Observable<number> => {
      return this.count().pipe(map(res => res.value));
    },
  }

  create(req: BotModel): Observable<BotModel> {
    return this.api.unary$(
      BotCreateDesc,
      {
        obj: req,
        coder: BotModel,
      }
    );
  }

  load(req: ListOptions): Observable<BotListResp> {
    return this.api.unary$(
      BotListDesc,
      {
        obj: req,
        coder: ListOptions,
      }
    );
  }

  remove(req: IdRequest): Observable<void> {
    return this.api.unary$(
      BotDeleteDesc,
      {
        obj: req,
        coder: IdRequest,
      }
    );
  }

  update(req: BotModel): Observable<void> {
    return this.api.unary$(
      BotUpdateDesc,
      {
        obj: req,
        coder: BotModel,
      }
    );
  }

  count(): Observable<UInt64Value> {
    return this.api.unary$(
      BotCountDesc,
      {
        obj: {},
        coder: Empty,
      }
    )
  }
}

export default BotService;