import { ContainerModule, interfaces } from 'inversify';
import BotUserService, { IBotUserService } from './service';
import BotUserUsecase, { IBotUserUsecase } from './usecase';

export const BotUserModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IBotUserService>(BotUserService.diKey).to(BotUserService);

  bind<IBotUserUsecase>(BotUserUsecase.diKey).to(BotUserUsecase).inSingletonScope();
});