import { inject, injectable } from 'inversify';
import { BotUserList, BotUserListResp, BotUserModel } from '@local/modules/proto/botCommon';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { map, mergeMap, Observable } from 'rxjs';
import { ApiClient } from '@sharedModules/grpcClient';
import StreamHandler from '@sharedModules/Stream/service/StreamHandler';
import { ListOptions } from '@sharedModules/shared/paginatior';
import {
  BotUserCountDesc,
  BotUserCreateDesc,
  BotUserDeleteDesc,
  BotUserListDesc,
  BotUserUpdateDesc,
} from '@local/modules/proto/botUser';
import { Empty } from '@sharedModules/shared/empty';
import { UInt64Value } from '@local/modules/proto/commonTypes';

export interface IBotUserService {
  create: (req: BotUserModel) => Observable<BotUserModel>,
  update: (req: BotUserModel) => Observable<void>,
  remove: (req: IdRequest) => Observable<void>,
  load: (req: BotUserList) => Observable<BotUserListResp>,
  count: () => Observable<UInt64Value>,
}

@injectable()
class BotUserService extends StreamHandler<BotUserModel> implements IBotUserService {
  public static diKey = Symbol.for('BotUserServiceKey');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    super();
    this.api = api;
    this.add('default', this.streamDefault);
  }

  private streamDefault = {
    stream: (opts: ListOptions, method: string, query: { handler: string }): Observable<BotUserModel> => {
      return this.load(BotUserList.fromPartial({ opts, handler: query.handler })).pipe(mergeMap(e => e.items));
    },
    loadCount: (): Observable<number> => {
      return this.count().pipe(map(res => res.value));
    }
  }

  create(req: BotUserModel): Observable<BotUserModel> {
    return this.api.unary$(
      BotUserCreateDesc,
      {
        obj: req,
        coder: BotUserModel,
      }
    );
  }

  load(req: BotUserList): Observable<BotUserListResp> {
    return this.api.unary$(
      BotUserListDesc,
      {
        obj: req,
        coder: BotUserList,
      }
    );
  }

  remove(req: IdRequest): Observable<void> {
    return this.api.unary$(
      BotUserDeleteDesc,
      {
        obj: req,
        coder: IdRequest,
      }
    );
  }

  update(req: BotUserModel): Observable<void> {
    return this.api.unary$(
      BotUserUpdateDesc,
      {
        obj: req,
        coder: BotUserModel,
      }
    );
  }

  count(): Observable<UInt64Value> {
    return this.api.unary$(
      BotUserCountDesc,
      {
        obj: {},
        coder: Empty,
      }
    );
  }
}

export default BotUserService;