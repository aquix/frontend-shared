import { inject, injectable } from 'inversify';
import BotUserService from './service';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { BotUserList, BotUserListResp, BotUserModel } from '@local/modules/proto/botCommon';
import { Observable } from 'rxjs';
import { IBaseListUsecase, IPageService } from '@sharedModules/Stream/types';
import { PageService } from '@sharedModules/Stream/service/ListService';
import ListMV from '@sharedModules/Stream/model/ListMV';
import { ListOptions } from '@sharedModules/shared/paginatior';

export interface IBotUserUsecase {
  create: (req: BotUserModel) => Observable<BotUserModel>,
  update: (req: BotUserModel) => Observable<void>,
  remove: (req: IdRequest) => Observable<void>,
  load: (req: BotUserList) => Observable<BotUserListResp>,
}

@injectable()
class BotUserUsecase implements IBotUserUsecase, IBaseListUsecase<BotUserModel> {
  public static diKey = Symbol.for('BotUserUsecaseDiKey');

  private botUserService: BotUserService;
  private pageService: IPageService<BotUserModel>;

  constructor(
    @inject(BotUserService.diKey) botUserService: BotUserService,
  ) {
    this.botUserService = botUserService;
    this.pageService = new PageService(this.botUserService);
  }

  create(req: BotUserModel): Observable<BotUserModel> {
    return this.botUserService.create(req);
  }

  load(req: BotUserList): Observable<BotUserListResp> {
    return this.botUserService.load(req);
  }

  remove(req: IdRequest): Observable<void> {
    return this.botUserService.remove(req);
  }

  update(req: BotUserModel): Observable<void> {
    return this.botUserService.update(req);
  }

  loadItems(model: ListMV<BotUserModel>, opts: ListOptions, method?: string, query?: object): Observable<BotUserModel> {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<BotUserModel>, method?: string, query?: object): Observable<number> {
    return this.pageService.loadTotalCount(model, method, query);
  }
}

export default BotUserUsecase;