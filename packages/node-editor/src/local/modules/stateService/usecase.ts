import { State, StateBatch } from '../proto/stateMachine';
import { inject, injectable } from 'inversify';
import StateService, { IStateService } from './service';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface IStateUsecase {
  create(request: State): Observable<IdRequest>,
  delete(request: IdRequest): Observable<void>,
  list(request: IdRequest): Observable<State>,
  batchUpdate(request: StateBatch): Observable<void>,
  update(request: State): Observable<void>
}

@injectable()
class StateUsecase implements IStateUsecase {
  public static diKey = Symbol.for('StateUsecaseDiKey');

  private stateService: IStateService;

  constructor(
    @inject(StateService.diKey) stateService: IStateService,
  ) {
    this.stateService = stateService;
  }

  create(request: State): Observable<IdRequest> {
    return this.stateService.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.stateService.delete(request);
  }

  list(request: IdRequest): Observable<State> {
    return this.stateService.list(request);
  }

  batchUpdate(request: StateBatch): Observable<void> {
    return this.stateService.batchUpdate(request);
  }

  update(request: State): Observable<void> {
    return this.stateService.update(request);
  }
}

export default StateUsecase;