import { DefaultNodeModel, DefaultNodeModelGenerics } from '@projectstorm/react-diagrams';
import { State } from '../proto/stateMachine';
import { StateServiceExtraModel } from './models';
import { IdRequest } from '../proto/commonTypes';
import { StatePortNode, TransitionPortNode } from '../statePort/node';
import { action, makeObservable, observable } from 'mobx';
import { ListenerHandle } from '@projectstorm/react-canvas-core';
import { debounce } from 'lodash';
import { DefaultNodeModelOptions } from '@projectstorm/react-diagrams-defaults';
import { ObjectWithId } from '@sharedModules/types';
import StateServiceModuleVM from '@local/modules/engine/modelViews/stateService';

class StateServiceNodeModel extends DefaultNodeModel implements ObjectWithId {
  @observable
  public id;
  @observable
  private readonly stateModel: StateServiceExtraModel = {} as StateServiceExtraModel;

  private module: StateServiceModuleVM;

  @observable
  public options: DefaultNodeModelOptions = {
    type: 'node_factory'
  };

  @observable
  ports: { [s: string]: TransitionPortNode } = {};
  @observable
  portsOut = [];
  @observable
  portsIn = [];

  constructor(state: StateServiceExtraModel, module: StateServiceModuleVM) {
    super({ type: 'node_factory' });

    this.module = module;

    this.id = state.id;
    this.options.id = `state_${state.id}`;
    this.stateModel.id = state.id;
    this.stateModel.name = state.name;
    this.options.name = state.name;
    this.stateModel.scenarioId = state.scenarioId;
    this.stateModel.stateMachineId = state.stateMachineId;
    this.stateModel.meta = state.meta;
    this.stateModel.initial = state.initial;
    this.stateModel.transitions = state.transitions;

    if (state.meta.position) {
      this.setPosition(state.meta.position.x, state.meta.position.y);
    } else {
      this.stateModel.meta.position = this.getPosition();
    }

    this.addPort(new StatePortNode(this.stateModel.id));

    for (const transition of state.transitions) {
      const newTransitionPort = new TransitionPortNode(transition);
      this.addPort(newTransitionPort);
    }

    makeObservable(this);
  }

  @action
  registerListener(listener: DefaultNodeModelGenerics['LISTENER']): ListenerHandle {
    const _listener = {
      positionChanged: debounce(() => this.module.updatePosition(State.fromPartial({
        ...this.model(),
        transitions: this.model().transitions.map(i => ({
          ...i.model(),
          meta: JSON.stringify(i.model().meta),
        })),
        meta: JSON.stringify(this.model().meta),
      })), 200),
      'remove': () => this.module.remove(IdRequest.fromPartial({ id: this.model().id })),
    };

    const listenerId = `node_${this.model().id}_listener`;

    this.listeners[listenerId] = _listener;
    return {
      id: listenerId,
      listener: _listener,
      deregister: () => {
        delete this.listeners[listenerId];
      },
    };
  }

  model() {
    this.stateModel.meta.position = this.getPosition();
    return this.stateModel;
  }

  @action
  setId(id: number) {
    this.id = id;
  }
}

export default StateServiceNodeModel;