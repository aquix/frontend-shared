import { inject, injectable } from 'inversify';
import { ScenarioItemBatch, ScenarioItemModel } from '../proto/botCommon';
import ScenarioItemService, { IScenarioItemService } from './service';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface IScenarioItemUsecase {
  create(request: ScenarioItemModel): Observable<IdRequest>,
  delete(request: IdRequest): Observable<void>
  list(request: IdRequest): Observable<ScenarioItemModel>,
  batchUpdate(request: ScenarioItemBatch): Observable<void>,
  update(request: ScenarioItemModel): Observable<void>,
}

@injectable()
class ScenarioItemUsecase implements IScenarioItemUsecase {
  public static diKey = Symbol.for('ScenarioItemUsecaseDiKey');

  private scenarioItemService: IScenarioItemService;

  constructor(
    @inject(ScenarioItemService.diKey) scenarioItemService: IScenarioItemService,
  ) {
    this.scenarioItemService = scenarioItemService;
  }

  create(request: ScenarioItemModel): Observable<IdRequest> {
    return this.scenarioItemService.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.scenarioItemService.delete(request);
  }

  list(request: IdRequest): Observable<ScenarioItemModel> {
    return this.scenarioItemService.list(request);
  }

  batchUpdate(request: ScenarioItemBatch): Observable<void> {
    return this.scenarioItemService.batchUpdate(request);
  }

  update(request: ScenarioItemModel): Observable<void> {
    return this.scenarioItemService.update(request);
  }
}

export default ScenarioItemUsecase;