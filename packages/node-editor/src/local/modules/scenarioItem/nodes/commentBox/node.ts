import { ScenarioItemModel } from '../../../proto/botCommon';
import { DefaultNodeModel, DefaultNodeModelGenerics } from '@projectstorm/react-diagrams';
import { ListenerHandle } from '@projectstorm/react-canvas-core';
import { debounce } from 'lodash';
import { action, makeObservable, observable } from 'mobx';
import { DefaultNodeModelOptions } from '@projectstorm/react-diagrams-defaults';
import ScenarioItemsModuleVM from '@local/modules/engine/modelViews/scenarioItems';
import { ScenarioItemExtraModel } from '@local/modules/scenarioItem/types';
import { ObjectWithId } from '@sharedModules/types';

export class CommentBoxNodeModel<ENGINE_MODEL extends ObjectWithId> extends DefaultNodeModel implements ObjectWithId {
  @observable
  id: number;

  public static type: string = 'commentBox';
  @observable
  private readonly scenarioItem: ScenarioItemExtraModel = {} as ScenarioItemExtraModel;

  @observable
  public options: DefaultNodeModelOptions = {
    type: 'comment_box_factory',
  };

  private module: ScenarioItemsModuleVM<ENGINE_MODEL>;

  constructor(scenarioItem: ScenarioItemModel, module: ScenarioItemsModuleVM<ENGINE_MODEL>) {
    super();

    this.module = module;

    this.id = scenarioItem.id;
    this.options.id = `scenario_item_${scenarioItem.id}`;
    this.scenarioItem.id = scenarioItem.id;
    this.scenarioItem.type = scenarioItem.type;
    this.scenarioItem.scenarioId = scenarioItem.scenarioId;

    const newMeta = { ...this.scenarioItem.meta, ...JSON.parse(scenarioItem.meta) };
    this.scenarioItem.meta = newMeta;

    if (newMeta.position) {
      this.setPosition(newMeta.position.x, newMeta.position.y);
    }

    if (newMeta.isLocked) {
      this.setLocked(newMeta.isLocked);
    }

    if (newMeta.width) {
      this.width = newMeta.width;
    } else {
      this.width = 300;
    }

    if (newMeta.height) {
      this.height = newMeta.height;
    } else {
      this.height = 300;
    }

    makeObservable(this);
  }

  extraModel() {
    return this.scenarioItem;
  }

  model() {
    return ScenarioItemModel.fromPartial({ ...this.scenarioItem, meta: JSON.stringify(this.scenarioItem.meta) });
  }

  registerListener(listener: DefaultNodeModelGenerics['LISTENER']): ListenerHandle {
    const _listener = {
      positionChanged: debounce(() => this.updatePosition(), 200),
      'remove': () => this.module.removeItem(this.model().type, this.model().id),
    }

    const listenerId = `node_${this.model().id}_listener`;

    this.listeners[listenerId] = _listener;
    return {
      id: listenerId,
      listener: _listener,
      deregister: () => {
        delete this.listeners[listenerId];
      }
    };
  }

  @action
  private update() {
    this.module.updateItem(ScenarioItemModel.fromPartial(this.model()));
  }

  @action
  private updatePosition() {
    this.scenarioItem.meta.position = this.getPosition();
    this.module.updatePosition(ScenarioItemModel.fromPartial(this.model()));
  }

  @action
  changeBackgroundColor(color: string) {
    this.scenarioItem.meta.color = color;
    return this.update();
  }

  @action
  lockNode() {
    this.scenarioItem.meta.isLocked = true;
    this.setLocked(true);
    return this.update();
  }

  @action
  unlockNode() {
    this.scenarioItem.meta.isLocked = false;
    this.setLocked(false);
    return this.update();
  }

  @action
  updateSize(width: number, height: number) {
    this.scenarioItem.meta.width = width;
    this.scenarioItem.meta.height = height;

    return this.update();
  }

  @action
  setSelected(selected: boolean) {
    if (!this.isLocked()) {
      return super.setSelected(selected);
    }
    super.setSelected(false);
  }

  @action
  setId(id: number) {
    this.scenarioItem.id = id;
  }
}