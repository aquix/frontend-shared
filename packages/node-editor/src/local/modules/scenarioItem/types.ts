import { DefaultNodeModel } from '@projectstorm/react-diagrams';
import { ScenarioItemModel } from '../proto/botCommon';
import { Point } from '@projectstorm/geometry';

export interface IGetScenarioItemModel<T = ScenarioItemModel> extends DefaultNodeModel {
  model: () => T,
}

export interface ISetScenarioItemId extends DefaultNodeModel, IGetScenarioItemModel {
  setId: (id: number) => void,
}

interface Meta {
  position: Point,
  color: string,
  description: string,
  isLocked: boolean,
  width: number,
  height: number,
}

export interface ScenarioItemExtraModel {
  id: number,
  $type: "botMs.ScenarioItemModel",
  type: string,
  meta: Meta,
  scenarioId: number,
}