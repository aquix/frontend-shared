import { ContainerModule, interfaces } from 'inversify';
import StateMachineService, { IStateMachineService } from './service';
import StateMachineServiceUsecase, { IStateMachineServiceUsecase } from './usecase';

export const StateMachineServiceModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IStateMachineService>(StateMachineService.diKey).to(StateMachineService);

  bind<IStateMachineServiceUsecase>(StateMachineServiceUsecase.diKey).to(StateMachineServiceUsecase).inSingletonScope();
});