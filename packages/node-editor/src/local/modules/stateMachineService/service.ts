import { StateMachine, StateMachineResp } from '../proto/stateMachine';
import { inject, injectable } from 'inversify';
import {
  StateMachineServiceCountDesc,
  StateMachineServiceCreateDesc,
  StateMachineServiceDeleteDesc,
  StateMachineServiceListDesc, StateMachineServiceUpdateDesc,
} from '../proto/stateMachineServices';
import { ApiClient } from '@sharedModules/grpcClient';
import StreamHandler from '@sharedModules/Stream/service/StreamHandler';
import { map, mergeMap, Observable } from 'rxjs';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { Empty } from '@sharedModules/shared/empty';
import { UInt64Value } from '@local/modules/proto/commonTypes';

export interface IStateMachineService {
  create(request: StateMachine): Observable<IdRequest>,
  update(request: StateMachine): Observable<void>,
  delete(request: IdRequest): Observable<void>,
  list(request: ListOptions): Observable<StateMachineResp>,
  count(): Observable<UInt64Value>,
}

@injectable()
class StateMachineService extends StreamHandler<StateMachine> implements IStateMachineService {
  public static diKey = Symbol.for('StateMachineService');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    super();
    this.api = api;
    this.add('default', this.streamDefault);
  }

  private streamDefault = {
    stream: (opts: ListOptions): Observable<StateMachine> => {
      return this.list(opts).pipe(mergeMap(e => e.items));
    },
    loadCount: (): Observable<number> => {
      return this.count().pipe(map(res => res.value));
    }
  }

  create(request: StateMachine): Observable<IdRequest> {
    return this.api.unary$(
      StateMachineServiceCreateDesc,
      {
        obj: request,
        coder: StateMachine,
      }
    );
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      StateMachineServiceDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    );
  }

  list(request: ListOptions): Observable<StateMachineResp> {
    return this.api.stream$(
      StateMachineServiceListDesc,
      {
        obj: request,
        coder: ListOptions,
      }
    );
  }

  update(request: StateMachine): Observable<void> {
    return this.api.unary$(
      StateMachineServiceUpdateDesc,
      {
        obj: request,
        coder: StateMachine,
      }
    );
  }

  count(): Observable<UInt64Value> {
    return this.api.unary$(
      StateMachineServiceCountDesc,
      {
        obj: {},
        coder: Empty,
      }
    )
  }
}

export default StateMachineService;