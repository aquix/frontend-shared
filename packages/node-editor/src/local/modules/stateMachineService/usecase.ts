import { StateMachine, StateMachineResp } from '../proto/stateMachine';
import { inject, injectable } from 'inversify';
import StateMachineService from './service';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { IBaseListUsecase, IPageService } from '@sharedModules/Stream/types';
import { PageService } from '@sharedModules/Stream/service/ListService';
import ListMV from '@sharedModules/Stream/model/ListMV';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { Observable } from 'rxjs';

export interface IStateMachineServiceUsecase {
  create(request: StateMachine): Observable<IdRequest>,

  update(request: StateMachine): Observable<void>,

  delete(request: IdRequest): Observable<void>,

  list(request: ListOptions): Observable<StateMachineResp>,
}

@injectable()
class StateMachineServiceUsecase implements IStateMachineServiceUsecase, IBaseListUsecase<StateMachine> {
  public static diKey = Symbol.for('StateMachineServiceUsecaseDiKey');

  private service: StateMachineService;
  private pageService: IPageService<StateMachine>;
  
  constructor(
    @inject(StateMachineService.diKey) service: StateMachineService,
  ) {
    this.service = service;
    this.pageService = new PageService(this.service);
  }

  create(request: StateMachine): Observable<IdRequest> {
    return this.service.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.service.delete(request);
  }

  list(request: ListOptions): Observable<StateMachineResp> {
    return this.service.list(request);
  }

  update(request: StateMachine): Observable<void> {
    return this.service.update(request);
  }

  loadItems(model: ListMV<StateMachine>, opts: ListOptions, method?: string, query?: object): Observable<StateMachine> {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<StateMachine>, method?: string, query?: object): Observable<number> {
    return this.pageService.loadTotalCount(model, method, query);
  }
}

export default StateMachineServiceUsecase;