import { EvenListResp, EventModel } from '../proto/botCommon';
import { inject, injectable } from 'inversify';
import { EventCountDesc, EventCreateDesc, EventDeleteDesc, EventListDesc, EventUpdateDesc } from '../proto/event';
import { ApiClient } from '@sharedModules/grpcClient';
import StreamHandler from '@sharedModules/Stream/service/StreamHandler';
import { map, mergeMap, Observable } from 'rxjs';
import { Empty } from '@sharedModules/shared/empty';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { UInt64Value } from '@local/modules/proto/commonTypes';

export interface IEventService {
  create: (req: EventModel) => Observable<EventModel>,
  update: (req: EventModel) => Observable<void>,
  remove: (req: IdRequest) => Observable<void>,
  load: (req: ListOptions) => Observable<EvenListResp>,
  count: () => Observable<UInt64Value>,
}

@injectable()
class EventService extends StreamHandler<EventModel> implements IEventService {
  public static diKey = Symbol.for('EventServiceKey');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    super();
    this.api = api;
    this.add('default', this.streamDefault);
  }

  private streamDefault = {
    stream: (opts: ListOptions): Observable<EventModel> => {
      return this.load(opts).pipe(mergeMap(e => e.items));
    },
    loadCount: (): Observable<number> => {
      return this.count().pipe(map(res => {
        return res.value
      }));
    }
  }

  create(req: EventModel): Observable<EventModel> {
    return this.api.unary$(
      EventCreateDesc,
      {
        obj: req,
        coder: EventModel,
      }
    );
  }

  load(req: ListOptions): Observable<EvenListResp> {
    return this.api.unary$(
      EventListDesc,
      {
        obj: req,
        coder: ListOptions,
      }
    );
  }

  remove(req: IdRequest): Observable<void> {
    return this.api.unary$(
      EventDeleteDesc,
      {
        obj: req,
        coder: IdRequest,
      }
    );
  }

  update(req: EventModel): Observable<void> {
    return this.api.unary$(
      EventUpdateDesc,
      {
        obj: req,
        coder: EventModel,
      }
    );
  }

  count(): Observable<UInt64Value> {
    return this.api.unary$(
      EventCountDesc,
      {
        obj: {},
        coder: Empty,
      }
    );
  }
}

export default EventService;