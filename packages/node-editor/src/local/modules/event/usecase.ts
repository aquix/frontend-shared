import { EvenListResp, EventModel } from '../proto/botCommon';
import { inject, injectable } from 'inversify';
import EventService from './service';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { Observable } from 'rxjs';
import { IBaseListUsecase, IPageService } from '@sharedModules/Stream/types';
import { PageService } from '@sharedModules/Stream/service/ListService';
import ListMV from '@sharedModules/Stream/model/ListMV';

export interface IEventUsecase {
  create: (req: EventModel) => Observable<EventModel>,
  update: (req: EventModel) => Observable<void>,
  remove: (req: IdRequest) => Observable<void>,
  load: (req: ListOptions) => Observable<EvenListResp>,
}

@injectable()
class EventUsecase implements IEventUsecase, IBaseListUsecase<EventModel> {
  public static diKey = Symbol.for('EventUsecaseDiKey');

  private eventService: EventService;
  private pageService: IPageService<EventModel>;

  constructor(
    @inject(EventService.diKey) eventService: EventService,
  ) {
    this.eventService = eventService;
    this.pageService = new PageService(this.eventService);
  }

  create(req: EventModel): Observable<EventModel> {
    return this.eventService.create(req);
  }

  load(req: ListOptions): Observable<EvenListResp> {
    return this.eventService.load(req)
  }

  remove(req: IdRequest): Observable<void> {
    return this.eventService.remove(req);
  }

  update(req: EventModel): Observable<void> {
    return this.eventService.update(req);
  }

  loadItems(model: ListMV<EventModel>, opts: ListOptions, method?: string, query?: object): Observable<EventModel> {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<EventModel>, method?: string, query?: object): Observable<number> {
    return this.pageService.loadTotalCount(model, method, query);
  }
}

export default EventUsecase;