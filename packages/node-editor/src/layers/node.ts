import { NodeLayerModelGenerics } from '@projectstorm/react-diagrams-core';
import { NodeLayerModel } from '@projectstorm/react-diagrams-core';

export class CustomNodeLayerModel<G extends NodeLayerModelGenerics = NodeLayerModelGenerics> extends NodeLayerModel {
  addModel(model: NodeLayerModelGenerics['CHILDREN']) {
    super.addModel(model);
    super.allowRepaint(true);
  }
}

export default CustomNodeLayerModel;