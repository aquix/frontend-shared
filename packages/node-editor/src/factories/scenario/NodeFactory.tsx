import { DefaultNodeFactory } from '@projectstorm/react-diagrams-defaults';
import * as React from 'react';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import NodeWidget from '@components/widgets/scenario';

export class NodeFactory extends DefaultNodeFactory {
  private readonly scenarioEngineVM: ScenarioEngineMV;

  constructor(scenarioEngineVM: ScenarioEngineMV) {
    super();
    this.type = 'node_factory';
    this.scenarioEngineVM = scenarioEngineVM;
  }

  generateReactWidget(event): JSX.Element {
    return <NodeWidget engineMV={this.scenarioEngineVM} node={event.model} />
  }
}