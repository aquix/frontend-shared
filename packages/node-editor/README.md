#BUILD GRPC WEB REACT
docker build -t grpc-web-react .
#RUN GRPC PROXY SERVER 
docker run -d --name grpc-web-react -p 8081:8081 -p 9901:9901 grpc-web-react
#RUN BOT SERVICE
docker run -d -p 3001:3001 -e DB_HOST=host.docker.internal -e DB_USERNAME=aquix -e DB_DATABASE=aquix -e DB_PASSWORD=123456 -e DB_PORT=5432 -e GRPC_SERVER=0.0.0.0:3001 -e REDIS_SERVER=localhost:6379 -e TAG=all registry.gitlab.com/nikita.morozov/bots-ms:v2.7.5