'use strict';
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { HotModuleReplacementPlugin } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, 'src/public/index.tsx'),
  devtool: "source-map",
  output: {
    path: path.join(__dirname, 'lib'),
    filename: 'index.js',
    library: 'node-editor',
    libraryTarget: 'umd',
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.css'],
    alias: {
      "@sharedModules": path.resolve(__dirname, "src/sharedModules/"),
      "@local": path.resolve(__dirname, "src/local/"),
      "@configs": path.resolve(__dirname, "src/configs/"),
      "@services": path.resolve(__dirname, "src/services/"),
      "@factories": path.resolve(__dirname, "src/factories/"),
      "@layers": path.resolve(__dirname, "src/layers/"),
      "@widgets": path.resolve(__dirname, "src/widgets/"),
      "@components": path.resolve(__dirname, "src/components/"),
      "@modules": path.resolve(__dirname, "src/sharedModules/"),
    }
  },
  target: 'web',
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(tsx|ts)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ]
  },
  plugins: [
    /**
     * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
     * the following package: sudo apt-get install libpng16-dev
     */
    new webpack.DefinePlugin({
      'process.env': {
        'PUBLIC_API_DOMAIN': JSON.stringify('http://localhost:8081'),
        'LOCAL_DEV': JSON.stringify('TRUE'),
      }
    }),
    new MiniCssExtractPlugin({
      linkType: 'text/css',
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'lib/index.html'),
      filename: 'index.html',
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    new HotModuleReplacementPlugin(),
    new TsconfigPathsPlugin(),
  ],
  optimization: {
    minimize: false
  },
  devServer: {
    static: path.join(__dirname, 'lib'),
    historyApiFallback: true,
    port: 4000,
    open: true,
    hot: true,
  },
};
