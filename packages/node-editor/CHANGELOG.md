# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.0.13](https://gitlab.com/aquix/frontend-shared/compare/v3.0.12...v3.0.13) (2023-05-08)

**Note:** Version bump only for package @frontend-shared/node-editor





## [3.0.12](https://gitlab.com/aquix/frontend-shared/compare/v3.0.11...v3.0.12) (2023-05-08)

**Note:** Version bump only for package @frontend-shared/node-editor





## [3.0.11](https://gitlab.com/aquix/frontend-shared/compare/v0.0.7...v3.0.11) (2023-05-08)

**Note:** Version bump only for package @frontend-shared/node-editor





## [1.2.5](https://gitlab.com/aquix/frontend-shared/compare/v1.1.19...v1.2.5) (2022-05-11)

**Note:** Version bump only for package @frontend-shared/node-editor
