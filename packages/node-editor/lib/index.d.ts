import { FC, FunctionComponent } from 'react';
import _m0 from 'protobufjs/minimal';

declare const BotLayout: FC;

declare const BotUserLayout: FunctionComponent;

declare const EventLayout: FunctionComponent;

interface ScenarioActionModel {
    $type: "botMs.ScenarioActionModel";
    id: number;
    meta: string;
    scenarioId: number;
    actionId: number;
    data: string;
}
declare const ScenarioActionModel: {
    $type: "botMs.ScenarioActionModel";
    encode(message: ScenarioActionModel, writer?: _m0.Writer): _m0.Writer;
    decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioActionModel;
    fromJSON(object: any): ScenarioActionModel;
    toJSON(message: ScenarioActionModel): unknown;
    fromPartial(object: DeepPartial$1<ScenarioActionModel>): ScenarioActionModel;
};
interface ScenarioModel {
    $type: "botMs.ScenarioModel";
    id: number;
    createdAt: number;
    updatedAt: number;
    initial?: number | undefined;
    title: string;
    category: string;
    actions: ScenarioActionModel[];
    deletedAt?: number | undefined;
}
declare const ScenarioModel: {
    $type: "botMs.ScenarioModel";
    encode(message: ScenarioModel, writer?: _m0.Writer): _m0.Writer;
    decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioModel;
    fromJSON(object: any): ScenarioModel;
    toJSON(message: ScenarioModel): unknown;
    fromPartial(object: DeepPartial$1<ScenarioModel>): ScenarioModel;
};
type Builtin$1 = Date | Function | Uint8Array | string | number | boolean | undefined;
type DeepPartial$1<T> = T extends Builtin$1 ? T : T extends Array<infer U> ? Array<DeepPartial$1<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial$1<U>> : T extends {} ? {
    [K in Exclude<keyof T, "$type">]?: DeepPartial$1<T[K]>;
} : Partial<T>;

interface IScenarioTableProps {
    beforeOpenEditor?: (scenario: ScenarioModel) => void;
    scenarioId?: number;
}

declare const ScenarioLayout: FunctionComponent<IScenarioTableProps>;

declare const EditorLayout: FunctionComponent;

interface StateMachine {
    $type: "botMs.StateMachine";
    id: number;
    name: string;
    botId: number;
    startStateId: number;
    meta: string;
}
declare const StateMachine: {
    $type: "botMs.StateMachine";
    encode(message: StateMachine, writer?: _m0.Writer): _m0.Writer;
    decode(input: _m0.Reader | Uint8Array, length?: number): StateMachine;
    fromJSON(object: any): StateMachine;
    toJSON(message: StateMachine): unknown;
    fromPartial(object: DeepPartial<StateMachine>): StateMachine;
};
type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;
type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]>;
} : Partial<T>;

interface IStateMachineTableProps {
    beforeOpenEditor?: (stateMachine: StateMachine) => void;
}

declare const StateMachineLayout: FunctionComponent<IStateMachineTableProps>;

declare const MachineEditorLayout: FunctionComponent;

export { BotLayout, BotUserLayout, EditorLayout, EventLayout, MachineEditorLayout, ScenarioLayout, StateMachineLayout };
