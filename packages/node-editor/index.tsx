import BotLayout from './src/components/layout/bot';
import BotUserLayout from './src/components/layout/botUser';
import EventLayout from './src/components/layout/event';
import ScenarioLayout from './src/components/layout/scenarios';
import EditorLayout from './src/components/layout/editor';
import StateMachineLayout from './src/components/layout/stateMachine';
import MachineEditorLayout from './src/components/layout/stateMachineEditor';

export {
  BotLayout,
  BotUserLayout,
  EventLayout,
  ScenarioLayout,
  EditorLayout,
  StateMachineLayout,
  MachineEditorLayout,
}