import React, { FunctionComponent } from 'react';
import { Dispatch } from 'redux';
import { Diff } from 'utility-types';
import { connect } from 'react-redux';
import { TranslateChanger, WithTranslation } from './types/withTranslation';
import { ITranslate, ITranslateContext } from './types/translate';
import { TranslateFunction } from './components/TranslateFunction';
import { TranslateContext } from './context/translate';
import { ITranslateReducer } from './types/reducer';
import { setLanguage } from './redux/action';

const withLocale = <P extends WithTranslation>(WrappedComponent: React.ComponentType<P>, translates?: ITranslate) => {
  const mapStateToProps = (state: ITranslateReducer) => {
    const translate = TranslateFunction(!!translates && translates || state.translate.user.translates);
    return ({
      currentLanguage: state.translate.user.currentLanguage,
      translates: state.translate.user.translates,
      langs: state.translate.user.langs,
      translate,
    });
  };

  const mapDispatchToProps = (dispatch: Dispatch): TranslateChanger => ({
    setLanguage: (code: string) => dispatch(setLanguage(code)),
  });

  type HocProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

  const WithLocal: FunctionComponent<HocProps> = (props) => (
    <TranslateContext.Consumer>
      {(contexts: ITranslateContext) => {
        const { translatesServer, currentLanguageServer } = contexts;
        const { translates, currentLanguage, ...fields } = props;
        const __translates = translatesServer || translates;
        const lang = currentLanguageServer || currentLanguage;

        const translate = TranslateFunction(__translates);
        const newProps = {
          ...fields,
          translates: __translates,
          currentLanguage: lang,
          translate,
        };
        return <WrappedComponent {...newProps as P} />;
      }}
    </TranslateContext.Consumer>
  );

  return connect<ReturnType<typeof mapStateToProps>,
    TranslateChanger,
    Diff<P, WithTranslation>,
    ITranslateReducer>(mapStateToProps, mapDispatchToProps)(WithLocal);
};

export default withLocale;
