const saveFile = (res: any, fileName: string, type: string) => {
  const blob = new Blob([res], {type});
  // if (typeof window.navigator.msSaveBlob !== 'undefined') {
  //   window.navigator.msSaveBlob(blob, fileName);
  //   return;
  // }
  const blobURL = window.URL.createObjectURL(blob);
  const tempLink = document.createElement('a');
  tempLink.style.display = 'none';
  tempLink.href = blobURL;
  tempLink.setAttribute('download', fileName);
  if (typeof tempLink.download === 'undefined') {
    tempLink.setAttribute('target', '_blank');
  }
  document.body.appendChild(tempLink);
  tempLink.click();
  document.body.removeChild(tempLink);
  setTimeout(() => {
    window.URL.revokeObjectURL(blobURL);
  }, 100);
}

export default saveFile;
