import { ILang } from '../types/lang';
import { IncomingHttpHeaders } from 'http';

const getUserLocal = (headers: IncomingHttpHeaders[], langs: ILang[], _lang: string) => {
  let loc: string = '';
  if (!_lang) {
    const cfLang = headers['cf-ipcountry'];
    loc = cfLang && cfLang.toLowerCase();
    const langExist = langs.find(i => i.code == loc);
    loc = langExist?.code || 'en';
  }

  return loc;
};

export default getUserLocal;
