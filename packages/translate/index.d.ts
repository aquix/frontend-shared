import React, { FunctionComponent } from 'react';
import { ComponentProps, mapDispatchToProps, mapStateToProps } from './components/provider/redux';
import { WithTranslation } from './types/withTranslation';
import { IServer, ITranslate, TranslateFunc, TranslateObjectReq, TransProps } from './types/translate';
import { ConnectedComponent, InferableComponentEnhancerWithProps } from 'react-redux';
import { AppDispatch } from '@frontend-shared/common/redux/types';
import { CacheObjectRes } from './types/cache';
import { ITranslateReducer } from './types/reducer';
import { ExportTranslateRes, ILang, LangObjectEditRes, LangObjectRes } from './types/lang';
import {
  ITranslateListAdmin,
  TranslateListAdminObjectRes,
  TranslateListItemAdminObjectRes,
} from './types/translateListAdmin';
import { BaseCommandProps } from '@frontend-shared/common/apiClient/type';
import { TransObject } from './types/transObject';
import { CombineTranslateAdmin } from './types/transReducerAdmin';
import { AnyAction, CombinedState, Reducer, ReducersMapObject } from 'redux';
import { TranslateFuncType } from './components/TranslateFunction';

export const TranslateProvider: FunctionComponent<IServer & {headers: any}>;
export const TranslateAdminReducer: Reducer<CombinedState<CombineTranslateAdmin>, AnyAction>;

export function withLocale<T extends WithTranslation>(WrappedComponent: React.ComponentType<T>, translates?: ITranslate): ConnectedComponent<FunctionComponent<any>, any>;

export function loadCache(): (dispatch: AppDispatch<CacheObjectRes>) => Promise<{ payload: { cache: number }, type: string }>;

export function loadTranslateAdmin(currentPage: number, pageSize: number): (dispatch: AppDispatch<ITranslateReducer>, getState: () => ITranslateReducer) => void;

export function loadLangsListWithSource(): (dispatch: AppDispatch<LangObjectRes>, getState: () => ITranslateReducer) => Promise<void>;

export function createOrUpdateTranslate(value: TranslateObjectReq): (dispatch: AppDispatch<TranslateListItemAdminObjectRes>, getState: () => ITranslateReducer) => (Promise<{ payload: { item: ITranslateListAdmin }, type: string }>);

export function createOrUpdateLang(value: ILang): (dispatch: AppDispatch<LangObjectEditRes>, getState: () => ITranslateReducer) => (Promise<{ payload: { item: ILang }, type: string }>);

export function importTranslate(value: object): (dispatch: AppDispatch<BaseCommandProps>, getState: () => ITranslateReducer) => void;

export function exportTranslate(): (dispatch: AppDispatch<ExportTranslateRes>, getState: () => ITranslateReducer) => void;

export function searchTranslate(value: string, currentPage: number, pageSize: number): (dispatch: AppDispatch<TranslateListAdminObjectRes>, getState: () => ITranslateReducer) => void;

export function removeTranslate(value: number): (dispatch: AppDispatch<BaseCommandProps>, getState: () => ITranslateReducer) => Promise<{ payload: number, type: string }>;

export function saveLanguageUser(code: string): (dispatch: AppDispatch<BaseCommandProps>) => Promise<BaseCommandProps>;

export function loadTransInputValue(key: string): (dispatch: AppDispatch<object>) => Promise<TransObject<string>>;

export function exportTranslateAll(): (dispatch: AppDispatch<object>, getState: () => ITranslateReducer) => void;

export function loadTranslates(lang: string | undefined, cache: number): (dispatch: AppDispatch<BaseCommandProps>) => Promise<{ payload: { translates: ITranslate }, type: string }>;

export function useLocale(): [TranslateFuncType, string, ITranslate, ILang[], ((code: string) => void), ILang[]];

export type {
  WithTranslation,
  ILang,
  IServer,
  CombineTranslateAdmin,
  TransProps,
  TranslateFunc,
  TranslateFuncType,
};

export as namespace SharedTranslate;