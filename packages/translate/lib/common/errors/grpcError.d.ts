export interface GrpcError {
    code: number;
    message: string;
}
export declare const errorHandler: (err: GrpcError) => void;
