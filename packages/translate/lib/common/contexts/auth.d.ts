import React from 'react';
import { IAuthContextWithUpdate } from '../types/auth';
export declare const initialAuthValues: {
    user: undefined;
    userId: undefined;
};
export declare const AuthContext: React.Context<IAuthContextWithUpdate>;
