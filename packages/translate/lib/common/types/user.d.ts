export interface IProfile {
    alias?: string;
    parent_id?: number;
    email: string;
    verified: boolean;
    firstName?: string;
    middleName?: string;
    surname?: string;
    phone?: string;
    lang: string;
    telegram?: string;
    weight?: number;
    createdAt?: string;
    deleted: boolean;
    blocked: boolean;
    path?: number[];
}
export default interface IUser extends IProfile {
    id: number;
    role: string;
    deleted: boolean;
    docVerify: boolean;
}
export declare class User implements IUser {
    readonly id: number;
    email: string;
    lang: string;
    readonly role: string;
    readonly deleted: boolean;
    readonly blocked: boolean;
    readonly verified: boolean;
    createdAt?: string;
    docVerify: boolean;
    alias?: string;
    readonly parent_id?: number;
    firstName?: string;
    middleName?: string;
    surname?: string;
    phone?: string;
    telegram?: string;
    readonly weight?: number;
    path?: number[];
    constructor(obj: IUser);
    get shortName(): string;
    get isAdmin(): boolean;
    get isStudent(): boolean;
    get isTeacher(): boolean;
    get profileName(): string;
    get fullName(): string;
    get refId(): string;
    setValues(obj: User): this;
}
