import { User } from './user';
export interface IAuthContext {
    user?: User;
    userId?: string;
}
export interface IAuthContextWithUpdate extends IAuthContext {
    update: (user: User) => void;
}
