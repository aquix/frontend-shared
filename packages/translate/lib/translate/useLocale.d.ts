import { ITranslate } from './types/translate';
import { ILang } from './types/lang';
import { TranslateFuncType } from './components/TranslateFunction';
declare const useLocale: () => [TranslateFuncType, string, ITranslate, ILang[], (code: string) => void, ILang[]];
export default useLocale;
