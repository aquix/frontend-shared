import React, { FunctionComponent } from 'react';
import { ComponentProps } from './redux';
declare const TranslateProvider: FunctionComponent<React.PropsWithChildren<ComponentProps>>;
export default TranslateProvider;
