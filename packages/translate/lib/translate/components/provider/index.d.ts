/// <reference types="react" />
declare const _default: import("react-redux").ConnectedComponent<import("react").FunctionComponent<import("react").PropsWithChildren<import("./redux").ComponentProps>>, Pick<import("react").PropsWithChildren<import("./redux").ComponentProps>, "isServer" | "headers" | "translatesServer" | "currentLanguageServer" | "langsServer" | "children">>;
export default _default;
