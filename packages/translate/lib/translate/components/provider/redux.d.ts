/// <reference types="node" />
import { ITranslateReducer } from '../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IServer, ITranslateContext } from '../../types/translate';
import { IncomingHttpHeaders } from 'http';
export declare const mapStateToProps: (state: ITranslateReducer) => {
    currentLanguage: string;
    translates: import("../../types/translate").ITranslate;
    langs: import("../../types/lang").ILang[];
};
interface ITransProviderProps {
    headers: IncomingHttpHeaders[];
}
export declare const mapDispatchToProps: (dispatch: ThunkDispatch<ITranslateReducer, void, AnyAction>) => {
    setLanguage: (code: string) => {
        type: string;
        payload: {
            code: string;
        };
    };
    loadLangs: () => Promise<void>;
    loadCache: () => Promise<{
        type: string;
        payload: {
            cache: number;
        };
    }>;
    loadTranslates: (lang: string | undefined, cache: number) => Promise<{
        type: string;
        payload: {
            translates: import("../../types/translate").ITranslate;
        };
    }>;
};
export declare type ComponentProps = IServer & ITransProviderProps & ITranslateContext & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
export {};
