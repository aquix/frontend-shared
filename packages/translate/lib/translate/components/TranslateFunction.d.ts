/// <reference types="react" />
import { ITranslate, TransModifier, TransProps } from '../types/translate';
export declare type TranslateFuncType = (trans: string | TransProps, modifier?: TransModifier) => string | React.ReactElement;
export declare const TranslateFunction: (state: ITranslate, edit?: boolean, editKey?: ((key: string) => void) | undefined) => TranslateFuncType;
