import { ILang } from '../types/lang';
import { IncomingHttpHeaders } from 'http';
declare const getUserLocal: (headers: IncomingHttpHeaders[], langs: ILang[], _lang: string) => string;
export default getUserLocal;
