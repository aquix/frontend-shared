import React from 'react';
import { TranslateChanger, WithTranslation } from './types/withTranslation';
import { ITranslate } from './types/translate';
declare const withLocale: <P extends WithTranslation>(WrappedComponent: React.ComponentType<P>, translates?: ITranslate | undefined) => import("react-redux").ConnectedComponent<React.FunctionComponent<{
    currentLanguage: string;
    translates: ITranslate;
    langs: import("./types/lang").ILang[];
    translate: import("./components/TranslateFunction").TranslateFuncType;
} & TranslateChanger>, Pick<{
    currentLanguage: string;
    translates: ITranslate;
    langs: import("./types/lang").ILang[];
    translate: import("./components/TranslateFunction").TranslateFuncType;
} & TranslateChanger, never> & Pick<P, import("utility-types").SetDifference<keyof P, "translate" | "currentLanguage" | "translates" | "langs" | "setLanguage">>>;
export default withLocale;
