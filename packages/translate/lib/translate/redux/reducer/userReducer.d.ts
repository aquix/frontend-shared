import { TranslateReducer } from "../../types/transReducer";
import { TranslateAction } from "../action";
declare const UserReducer: (state: TranslateReducer | undefined, action: TranslateAction) => TranslateReducer;
export default UserReducer;
