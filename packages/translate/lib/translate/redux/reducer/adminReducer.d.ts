import { TranslateReducerAdmin } from "../../types/transReducerAdmin";
import { TranslateActionAdmin } from '../action/actionAdmin';
declare const AdminReducer: (state: TranslateReducerAdmin | undefined, action: TranslateActionAdmin) => TranslateReducerAdmin;
export default AdminReducer;
