import React from 'react';
import { ITransFieldData, TransFieldProps } from "./types/withTranslation";
declare const withLocaleField: <P extends TransFieldProps>(WrappedComponent: React.ComponentType<P & ITransFieldData>) => {
    new (props: P & ITransFieldData, context: any): {
        getKey: () => string | P["value"];
        onChangeText: (value: string, lang: keyof ITransFieldData) => void;
        componentDidMount(): void;
        render(): JSX.Element;
        context: any;
        setState<K extends "type" | "en" | "key" | "ru" | "loading">(state: (ITransFieldData & {
            loading: boolean;
        }) | ((prevState: Readonly<ITransFieldData & {
            loading: boolean;
        }>, props: Readonly<P>) => (ITransFieldData & {
            loading: boolean;
        }) | Pick<ITransFieldData & {
            loading: boolean;
        }, K> | null) | Pick<ITransFieldData & {
            loading: boolean;
        }, K> | null, callback?: (() => void) | undefined): void;
        forceUpdate(callback?: (() => void) | undefined): void;
        readonly props: Readonly<P> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<ITransFieldData & {
            loading: boolean;
        }>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        shouldComponentUpdate?(nextProps: Readonly<P>, nextState: Readonly<ITransFieldData & {
            loading: boolean;
        }>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<P>, prevState: Readonly<ITransFieldData & {
            loading: boolean;
        }>): any;
        componentDidUpdate?(prevProps: Readonly<P>, prevState: Readonly<ITransFieldData & {
            loading: boolean;
        }>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<P>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<P>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<P>, nextState: Readonly<ITransFieldData & {
            loading: boolean;
        }>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<P>, nextState: Readonly<ITransFieldData & {
            loading: boolean;
        }>, nextContext: any): void;
    };
    contextType?: React.Context<any> | undefined;
};
export default withLocaleField;
