import React from 'react';
import { ITranslateContext } from "../types/translate";
export declare const initialTranslatesValue: {
    translatesServer: undefined;
    currentLanguageServer: string;
};
export declare const TranslateContext: React.Context<ITranslateContext>;
