import { ILang } from "./lang";
import { ITranslateListAdmin } from "./translateListAdmin";
import { ISource } from "./translate";
import { TranslateReducer } from "./transReducer";
export interface TranslateReducerAdmin {
    langs: ILang[];
    langsAdmin: ILang[];
    list: ITranslateListAdmin[];
    source: ISource;
    export: number[];
    count: number;
    editTranslateKey?: string;
    editTranslate: boolean;
}
export interface CombineTranslateAdmin {
    admin: TranslateReducerAdmin;
    user: TranslateReducer;
}
