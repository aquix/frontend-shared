import { ILang } from "./lang";
import { ITranslate } from "./translate";
export interface TranslateReducer {
    currentLanguage: string;
    langs: ILang[];
    translates: ITranslate;
    cache: number;
}
