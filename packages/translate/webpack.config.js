'use strict';
const webpack = require('webpack');
const path = require('path');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const { dependencies: deps, peerDependencies: peerDeps } = require('./package.json');
module.exports = {
  entry: path.resolve(__dirname, '/index.tsx'),
  output: {
    path: path.join(__dirname, '/lib'),
    filename: 'index.js',
    library: 'translate-module',
    libraryTarget: 'umd',
    globalObject: 'this',
    clean: true
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  target: 'web',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: [/node_modules/],
        use: ['babel-loader', 'ts-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
    ]
  },
  plugins: [
    /**
     * Known issue for the CSS Extract Plugin in Ubuntu 16.04: You'll need to install
     * the following package: sudo apt-get install libpng16-dev
     */
    new ModuleFederationPlugin({
      name: 'translate-module',
      filename: 'remoteEntry.js',
      remotes: {},
      exposes: {},
      shared: {
        ...deps,
        react: {
          singleton: true,
          requiredVersion: peerDeps.react,
        },
        moment: {
          singleton: true,
          requiredVersion: peerDeps.moment,
        },
      },
    }),
  ],
  externals: {
    react: {
      commonjs: 'React',
      commonjs2: 'react',
      amd: 'react'
    },
    'moment': 'moment',
    'react/jsx-runtime': 'react/jsx-runtime',
    'react-redux': 'react-redux',
    'antd': 'antd'
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: false,
        parallel: false
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
};
