import React from 'react';
import {ITranslateContext} from "../types/translate";

export const initialTranslatesValue = { translatesServer: undefined, currentLanguageServer: 'en' };
export const TranslateContext = React.createContext<ITranslateContext>({ ...initialTranslatesValue });