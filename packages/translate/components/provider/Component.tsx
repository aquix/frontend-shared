import React, { FunctionComponent, useEffect } from 'react';
import { parseCookies } from 'nookies';
import getUserLoc from '../../tools/getUserLoc';
import moment from 'moment';
import { TranslateContext } from '../../context/translate';
import { ComponentProps } from './redux';

const TranslateProvider: FunctionComponent<React.PropsWithChildren<ComponentProps>> = (props) => {
  const {
    children,
    setLanguage,
    isServer,
    loadLangs,
    currentLanguage,
    langs,
    headers,
    translates,
    loadTranslates,
    loadCache,
  } = props;

  useEffect(() => {
    let cache: number = 0;
    let lang: string | undefined = undefined;
    let loc: string = '';
    const { token, lang: _lang } = parseCookies();

    loadLangs();

    if (!_lang) {
      loc = getUserLoc(headers, langs, currentLanguage);
    }

    if (isServer) {
      lang = _lang || loc;
      loadCache().then(res => {
        cache = res.payload.cache;
        loadTranslates(lang, cache);
      });
    }

    if (lang) {
      setLanguage(lang);
      moment.locale(lang);
    }
  }, []);

  return (
    <TranslateContext.Provider value={{ currentLanguageServer: currentLanguage, translatesServer: translates }}>
      {children}
    </TranslateContext.Provider>
  );
};

export default TranslateProvider;
