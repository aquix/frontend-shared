import { ITranslateReducer } from '../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { setLanguage } from '../../redux/action';
import { IServer, ITranslateContext } from '../../types/translate';
import { loadCache, loadLangsListWithSource, loadTranslates } from '../../commands/translate';
import { IncomingHttpHeaders } from 'http';

export const mapStateToProps = (state: ITranslateReducer) => ({
  currentLanguage: state.translate.user.currentLanguage,
  translates: state.translate.user.translates,
  langs: state.translate.user.langs,
});

interface ITransProviderProps {
  headers: IncomingHttpHeaders[]
}

export const mapDispatchToProps = (dispatch: ThunkDispatch<ITranslateReducer, void, AnyAction>) => ({
  setLanguage: (code: string) => dispatch(setLanguage(code)),
  loadLangs: () => dispatch(loadLangsListWithSource()),
  loadCache: () => dispatch(loadCache()),
  loadTranslates: (lang: string | undefined, cache: number) => dispatch(loadTranslates(lang, cache)),
});

export type ComponentProps =
  IServer
  & ITransProviderProps
  & ITranslateContext
  & ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>;
