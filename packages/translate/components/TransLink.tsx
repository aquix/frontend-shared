import React, { FunctionComponent } from 'react';
import {WithTranslation} from "../types/withTranslation";
import withLocale from "../withLocale";

interface TransLinkProps {
  href: string,
  className?: string,
  defaultHref: string,
  transKey: string
}

const TransLink: FunctionComponent<TransLinkProps & WithTranslation> = (props) => {
  const { currentLanguage, translate, href, className, defaultHref, transKey } = props;
  return (
    <a
      className={className}
      href={href + currentLanguage || defaultHref}
      target="_blank"
    >
      {translate(transKey)}
    </a>
  );
}

export default withLocale(TransLink);
