import { ITranslate, TransModifier, TransProps } from '../types/translate';
import isObject from 'lodash/isObject';

export type TranslateFuncType = (trans: string | TransProps, modifier?: TransModifier) => string | React.ReactElement;

export const TranslateFunction = (state: ITranslate, edit: boolean = false, editKey?: (key: string) => void): TranslateFuncType => (trans: string | TransProps, modifier?: TransModifier) => {
  const buttonEdit = (value: string) => (
    <>
      <span
        onClick={(e) => {
          e.preventDefault();
          editKey && editKey(value);
        }}
        className="json icon-edit button-translate-edit"
      />
    </>
  );
  if (typeof trans === 'string') {
    if (edit) {
      return (
        <span>
          {state[trans] || trans}
          {buttonEdit(trans)}
        </span>
      );
    }

    return state[trans] || trans;
  }

  let tValue = state[trans.key];

  if (!tValue) {
    if (edit) {
      return (
        <span>
          {trans.key}
          {buttonEdit(trans.key)}
        </span>
      );
    }
    return trans.key;
  }

  if (trans.params) {
    Object.keys(trans.params).forEach((pKey: string) => {
      const value = trans.params[pKey];
      const _val = isObject(value) ? value : value.toString();
      const val = TranslateFunction(state)(_val);

      // @ts-ignore
      const mVal = modifier && modifier[pKey] ? modifier[pKey](val) : val;
      // @ts-ignore
      tValue = ('' + tValue).split('{$' + pKey + '}').join(mVal);
    });
  }

  if (edit) {
    return (
      <span>
        {tValue}
        {buttonEdit(trans.key)}
      </span>
    );

  }
  return tValue;
};
