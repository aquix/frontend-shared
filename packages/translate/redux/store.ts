import { createStore, applyMiddleware, AnyAction } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import {CombineTranslateAdmin} from "../types/transReducerAdmin";
import TranslateAdminReducer from "./reducer";

const loggerMiddleware = createLogger();
const initialState = {};

let middleware;

if (process.env.NODE_ENV === 'production') {
   middleware = applyMiddleware(thunkMiddleware);
} else {
   middleware = applyMiddleware(thunkMiddleware, loggerMiddleware);
}

export default createStore<CombineTranslateAdmin, AnyAction, {}, {}>(TranslateAdminReducer, initialState as CombineTranslateAdmin, middleware);
