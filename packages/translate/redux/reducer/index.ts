import { combineReducers } from 'redux';
import userReducer from './userReducer'
import adminReducer from './adminReducer'
import {CombineTranslateAdmin} from "../../types/transReducerAdmin";

export const TranslateAdminReducer = combineReducers<CombineTranslateAdmin>({
  admin: adminReducer,
  user: userReducer
});
