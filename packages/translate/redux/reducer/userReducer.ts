import {TranslateReducer} from "../../types/transReducer";
import {
  SET_CACHE,
  SET_LANGUAGE,
  SET_LANGUAGE_LIST,
  SET_TRANSLATE_LIST,
  SetCachetAction,
  SetLanguageAction,
  SetLanguageListAction,
  SetTranslateListAction,
  TranslateAction
} from "../action";

const defaultState: TranslateReducer = {
  currentLanguage: 'en',
  langs: [],
  translates: {},
  cache: 0,
}

const UserReducer = (
  state: TranslateReducer = defaultState,
  action: TranslateAction,
): TranslateReducer => {
  if (action.type === SET_LANGUAGE) {
    return {
      ...state,
      currentLanguage: (action as SetLanguageAction).payload.code,
    }
  }

  if (action.type === SET_LANGUAGE_LIST) {
    return {
      ...state,
      langs: (action as SetLanguageListAction).payload.langs,
    }
  }

  if (action.type === SET_TRANSLATE_LIST) {
    return {
      ...state,
      translates: (action as SetTranslateListAction).payload.translates,
    }
  }

  if (action.type === SET_CACHE) {
    return {
      ...state,
      cache: (action as SetCachetAction).payload.cache,
    }
  }

  return state;
};

export default UserReducer;
