import React, { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ITranslate, TransModifier, TransProps } from './types/translate';
import { ILang } from './types/lang';
import { setLanguage } from './redux/action';
import { TranslateFunction, TranslateFuncType } from './components/TranslateFunction';
import { setEditTranslateKeyAction } from './redux/action/actionAdmin';
import { CombineTranslateAdmin } from './types/transReducerAdmin';
import useSession from '@frontend-shared/common/hooks/useSessions';

interface UseLocalProps {
  currentLanguage: string,
  translates: ITranslate,
  langs: ILang[]
  langsAdmin: ILang[]
  translate: (trans: string | TransProps, modifier?: TransModifier) => string
}

const useLocale = (): [TranslateFuncType, string, ITranslate, ILang[], (code: string) => void, ILang[]] => {
  const dispatch = useDispatch();
  const { user } = useSession();

  const _setLanguage: (code: string) => void = useCallback((code: string) => dispatch(setLanguage(code)), []);
  const setEditKey: (key: string) => void = useCallback((key: string) => dispatch(setEditTranslateKeyAction(key)), []);

  const methods = useSelector<CombineTranslateAdmin, UseLocalProps>(state => {
    const edit = user?.isAdmin && state.admin.editTranslate;
    const translate = useMemo(() => TranslateFunction(state.user.translates, edit, setEditKey), [edit, state.user.translates]);

    return ({
      currentLanguage: state.user.currentLanguage,
      translates: state.user.translates,
      langs: state.user.langs,
      langsAdmin: state.admin.langsAdmin,
      translate,
    } as UseLocalProps);
  });

  return [methods.translate, methods.currentLanguage, methods.translates, methods.langs, _setLanguage, methods.langsAdmin];
};


export default useLocale;
