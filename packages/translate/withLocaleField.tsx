import React from 'react';
import { Spin } from 'antd';
import { v4 as uuid } from 'uuid';
import isObject from 'lodash/isObject';
import {ITransFieldData, TransFieldProps} from "./types/withTranslation";

const withLocaleField = <P extends TransFieldProps>(WrappedComponent: React.ComponentType<P & ITransFieldData>) => {
  return class extends React.Component<P, ITransFieldData & { loading: boolean }> {
    getKey = () => {
      const { value, locKey } = this.props;

      if (isObject(value)) {
        return value.key || `${locKey}.${uuid()}`;
      }

      return value || `${locKey}.${uuid()}`;
    };

    onChangeText = (value: string, lang: keyof ITransFieldData) => {
      const data = { ...this.state, [lang]: value };
      this.setState(data);

      const { onChange } = this.props;
      onChange && onChange(data);
    }

    constructor(props: P & ITransFieldData, context: any) {
      super(props, context);

      this.state = {
        key: this.getKey() as string,
        type: 'trans-field',
        ru: '',
        en: '',
        loading: false,
      }
    }

    componentDidMount() {
      const { load } = this.props;
      const { key } = this.state;
      this.setState({ loading: true });

      load && load(key)
        .then((res) => {
          this.setState({
            key,
            loading: false,
            type: 'trans-field',
            ...res,
          });
        });
    }

    render() {
      const { loading } = this.state;

      if (loading) {
        return (<Spin />);
      }

      return (
        <WrappedComponent
          {...this.state}
          {...this.props as P}
          onChangeText={this.onChangeText}
        />
      );
    }
  }
}

export default withLocaleField;
