import { ITranslateReducer } from '../types/reducer';
import { setCache, setLanguageList, setTranslateList } from '../redux/action';
import { TranslateListAdminObjectRes, TranslateListItemAdminObjectRes } from '../types/translateListAdmin';
import {
  createLangItem,
  createTranslateListItemAdmin, removeTranslateItem,
  setLanguageListAdmin,
  setTranslateCount,
  setTranslateListAdmin, updateLangItem,
  updateTranslateListItemAdmin,
} from '../redux/action/actionAdmin';
import { ExportTranslateRes, ILang, LangObjectEditRes, LangObjectRes } from '../types/lang';
import { ITranslate, TranslateObjectReq, TransObjectRes } from '../types/translate';
import saveFile from '../tools/saveFile';
import { CacheObjectRes } from '../types/cache';
import { apiCall } from '@frontend-shared/common';
import { AppDispatch } from '@frontend-shared/common/redux/types';
import { BaseCommandProps, ResultListRes } from '@frontend-shared/common/apiClient/type';

export const loadCache = () => (dispatch: AppDispatch<CacheObjectRes>) => {
  return dispatch(apiCall<object, CacheObjectRes>('/v1/translate/cache', undefined, {
    method: 'get',
    context: 'translate',
  }))
    .then((res) => dispatch(setCache(res?.cache)));
};

export const loadTranslateAdmin = (currentPage: number, pageSize: number) => (dispatch: AppDispatch<ITranslateReducer>, getState: () => ITranslateReducer) => {
  dispatch(loadLangsListWithSource()).then(() => {
    const state: ITranslateReducer = getState();
    const { source } = state.translate.admin;
    dispatch(apiCall<object, TranslateListAdminObjectRes>('/v1/translate/translate-admin', { currentPage, pageSize }, {
      method: 'post',
      baseURL: source.current,
    }))
      .then(({ list, count }) => {
        dispatch(setTranslateListAdmin(list));
        dispatch(setTranslateCount(count));
      });
  });
};

export const loadLangsListWithSource = () => (dispatch: AppDispatch<LangObjectRes>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { source } = state.translate.admin;
  return dispatch(apiCall<object, LangObjectRes>('/v1/translate/lang', undefined, {
    method: 'get',
    baseURL: source.current,
  }))
    .then((res) => {
      dispatch(setLanguageListAdmin(res?.list));
      dispatch(setLanguageList(res?.list));
    });
};

export const createOrUpdateTranslate = (value: TranslateObjectReq) => (dispatch: AppDispatch<TranslateListItemAdminObjectRes>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { source } = state.translate.admin;

  if (value.id) {
    return dispatch(apiCall<TranslateObjectReq, TranslateListItemAdminObjectRes>('/v1/translate/translate-update', value, {
      method: 'put',
      baseURL: source.current,
    }))
      .then((res) => dispatch(updateTranslateListItemAdmin(value)));
  }
  return dispatch(apiCall<TranslateObjectReq, TranslateListItemAdminObjectRes>('/v1/translate/translate', value, {
    method: 'post',
    baseURL: source.current,
  }))
    .then((res) => dispatch(createTranslateListItemAdmin(res.item)));
};

export const createOrUpdateLang = (value: ILang) => (dispatch: AppDispatch<LangObjectEditRes>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { source } = state.translate.admin;

  if (value.id) {
    return dispatch(apiCall<ILang, LangObjectEditRes>('/v1/translate/lang', value, {
      method: 'put',
      baseURL: source.current,
    }))
      .then(() => dispatch(updateLangItem(value)));
  }
  return dispatch(apiCall<ILang, LangObjectEditRes>('/v1/translate/lang', value, {
    method: 'post',
    baseURL: source.current,
  }))
    .then((res: LangObjectEditRes) => dispatch(createLangItem(res.lang)));
};

export const importTranslate = (value: object) => (dispatch: AppDispatch<BaseCommandProps>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { source } = state.translate.admin;
  dispatch(apiCall<object, BaseCommandProps>('/v1/translate/translate-import', value, {
    method: 'post',
    baseURL: source.current,
  }))
    .then((res) => console.log(res));
};

export const exportTranslate = () => (dispatch: AppDispatch<ExportTranslateRes>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { admin } = state.translate;
  dispatch(apiCall<object, ExportTranslateRes>('/v1/translate/translate-export', admin.export, {
    method: 'post',
    isFile: true,
    baseURL: admin.source.current,
  }))
    .then(({ list }) => {
      const data = JSON.stringify(list);
      saveFile(data, 'i18n.json', 'application/json');
    });
};


export const searchTranslate = (value: string, currentPage: number, pageSize: number) => (dispatch: AppDispatch<TranslateListAdminObjectRes>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { source } = state.translate.admin;

  dispatch(apiCall<object, TranslateListAdminObjectRes>('/v1/translate/translate-search', {
    query: value,
    currentPage,
    pageSize,
  }, {
    method: 'post',
    baseURL: source.current,
  }))
    .then(({ list, count }) => {
      dispatch(setTranslateListAdmin(list));
      dispatch(setTranslateCount(count));
    });
};

export const removeTranslate = (value: number) => (dispatch: AppDispatch<BaseCommandProps>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { source } = state.translate.admin;

  return dispatch(apiCall<object, BaseCommandProps>('/v1/translate/translate-remove', { id: value }, {
    method: 'post',
    baseURL: source.current,
  }))
    .then((res) => dispatch(removeTranslateItem(value)));
};

export const saveLanguageUser = (code: string) => (dispatch: AppDispatch<BaseCommandProps>) => {
  return dispatch(apiCall<object, BaseCommandProps>('/v1/user/save-language-user', { code }, { method: 'post' }));
};

export const loadTransInputValue = (key: string) => (dispatch: AppDispatch<object>) =>
  dispatch(apiCall<object, TransObjectRes>(`/v1/translate/input/${key}`, undefined, { method: 'get' }))
    .then((res: TransObjectRes) => res.values);

export const exportTranslateAll = () => (dispatch: AppDispatch<object>, getState: () => ITranslateReducer) => {
  const state: ITranslateReducer = getState();
  const { source } = state.translate.admin;

  dispatch(apiCall<object, any>('/v1/translate/translateExportAll', undefined, {
    method: 'get',
    isFile: true,
    baseURL: source.current,
  }))
    .then((res) => {
      const data = JSON.stringify(res);
      saveFile(data, 'i18n.json', 'application/json');
    });
};

export const loadTranslates = (lang: string | undefined, cache: number) => (dispatch: AppDispatch<BaseCommandProps>) => {
  return dispatch(apiCall<object, ResultListRes<ITranslate>>(`/v1/translate/list/${process.env.DEFAULT_HANDLER}/${lang}?ts=${cache}`, undefined, { method: 'get' }))
    .then(({ list }) => dispatch(setTranslateList(list)));
};
