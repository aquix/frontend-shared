import { BaseCommandProps } from "@frontend-shared/common/apiClient/type";

export interface ILang {
  id: number,
  code: string,
  sort: number,
  isoCode: string
}

export type ILangObject = { [key: string]: string }
export type ILangObjectFormTranslate = { [key: string]: { value: string } }

export interface LangObjectRes extends BaseCommandProps {
  list: ILang[]
}

export interface LangObjectEditRes extends BaseCommandProps {
  lang: ILang
}

export interface UserLangRes extends BaseCommandProps {
  code: string
}

export interface ExportTranslateRes extends BaseCommandProps {
  list: string
}
