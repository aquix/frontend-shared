import { CSSProperties } from 'react';
import {ITranslate, TranslateFunc} from "./translate";
import {ILang} from "./lang";
import {TransObject} from "./transObject";

export interface WithTranslation extends TranslateChanger {
  currentLanguage: string,
  translates: ITranslate,
  translate: TranslateFunc,
  langs: ILang[]
}

export interface TranslateChanger {
  setLanguage: (lang: string) => void,
}

export interface PubTransFieldProps {
  locKey?: string
  value?: string | ITransFieldData
  style?: CSSProperties
}

export interface TransFieldProps extends PubTransFieldProps {
  load?: (key: string) => Promise<TransObject<string>>
  onChange?: (val: ITransFieldData) => void
}

export interface ITransFieldData extends TransObject<string> {
  key: string
  type: string
}
