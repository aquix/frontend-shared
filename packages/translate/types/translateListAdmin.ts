import { BaseCommandProps } from "@frontend-shared/common/apiClient/type";

export interface TranslateListAdminObjectRes extends BaseCommandProps {
  list: ITranslateListAdmin[]
  count: number,
}

export interface TranslateListItemAdminObjectRes extends BaseCommandProps {
  item: ITranslateListAdmin
}

export interface ITranslateListAdmin {
  id: number,
  key: string,
  domain: string,
  values: {
    [code: string]: {
      value: string
      autoValue?: string
      conflict?: boolean
    }
  }
}
