import {ILang, ILangObjectFormTranslate} from "./lang";
import {TransObject} from "./transObject";
import { BaseCommandProps } from "@frontend-shared/common/apiClient/type";

export interface ITranslate {
  [key :string]: string
}

export interface TransProps {
  key: string
  params: { [key: string]: string | TransProps }
}

export type TransModifierRes = (value: string) => string
export type TransModifier = { [key: string]: TransModifierRes }

export type TranslateFunc = (trans: string | TransProps, modifier?: TransModifier) => string;

export interface ISource {
  current: string,
  items: ISourceItems[]
}
export interface ISourceItems {
  name: string,
  url: string
}

export interface TranslateObjectRes extends BaseCommandProps {
  list: ITranslate
}

export interface TranslateObjectReq {
  id: number
  key: string
  domain: string
  values: ILangObjectFormTranslate
}

export interface TransObjectRes extends BaseCommandProps {
  values: TransObject<string>
}

export interface ITranslateContext {
  translatesServer?: ITranslate,
  currentLanguageServer?: string,
  langsServer?: ILang[],
}

export interface IServer {
  isServer: boolean,
}
