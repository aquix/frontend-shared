import { createStore, applyMiddleware, AnyAction } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { SupportMessengerAdminReducer } from './reducer/SupportMessagerReducer';
import { ISupportMessengerAdminReducer } from '../types/reducer';

const loggerMiddleware = createLogger();
const initialState = {};

let middleware;

if (process.env.NODE_ENV === 'production') {
  middleware = applyMiddleware(thunkMiddleware);
} else {
  middleware = applyMiddleware(thunkMiddleware, loggerMiddleware);
}

export default createStore<ISupportMessengerAdminReducer, AnyAction, {}, {}>(SupportMessengerAdminReducer, initialState as ISupportMessengerAdminReducer, middleware);
