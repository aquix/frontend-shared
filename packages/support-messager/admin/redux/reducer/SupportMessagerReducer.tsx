import { combineReducers } from 'redux';
import commonReducer from '../../module/common/reducers';
import { ISupportMessengerAdminReducer } from '../../types/reducer';
import ChatAdminReducer from './chatReducer';

export const SupportMessengerAdminReducer = combineReducers<ISupportMessengerAdminReducer>({
  chatAdmin: ChatAdminReducer,
  common: commonReducer,
});


