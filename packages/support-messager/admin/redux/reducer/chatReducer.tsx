import { IChatReducerAdmin } from '../../types/chat';
import {
  ChatAction,
  CREATE_OR_UPDATE_CHAT_ITEM_ADMIN,
  createOrUpdateType, LOAD_COUNT_CHAT_ADMIN,
  LOAD_LIST_CHAT_ADMIN, loadCountType,
  loadListType, REMOVE_CHAT_ITEM_ADMIN, removeAction, removeType,
} from '../action/agent';

const defaultState: IChatReducerAdmin = {
  list: [],
  count: 0,
};

const ChatAdminReducer = (
  state: IChatReducerAdmin = defaultState,
  action: ChatAction,
): IChatReducerAdmin => {
  if (action.type === LOAD_LIST_CHAT_ADMIN) {
    return {
      ...state,
      list: (action as loadListType).payload || [],
    };
  }

  if (action.type === LOAD_COUNT_CHAT_ADMIN) {
    return {
      ...state,
      count: (action as loadCountType).payload || 0,
    };
  }

  if (action.type === CREATE_OR_UPDATE_CHAT_ITEM_ADMIN) {
    const item = (action as createOrUpdateType).payload;
    const index = state.list.findIndex(i => i.id === item.id);

    if (!!~index) {
      return {
        ...state,
        list: [
          ...state.list.slice(0, index),
          item,
          ...state.list.slice(index + 1),
        ],
      };
    }

    return {
      ...state,
      list: [
        item,
        ...state.list,
      ],
    };

  }

  if (action.type === REMOVE_CHAT_ITEM_ADMIN) {
    const id = (action as removeType).payload;
    const index = state.list.findIndex(i => i.id === id);

    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        ...state.list.slice(index + 1),
      ],
    };
  }

  return state;
};

export default ChatAdminReducer;
