import { CommonAction } from '../../module/common/actions';
import { IChat } from '../../types/chat';

export const LOAD_LIST_CHAT_ADMIN = 'LOAD_LIST_CHAT_ADMIN';
export const LOAD_COUNT_CHAT_ADMIN = 'LOAD_COUNT_CHAT_ADMIN';
export const CREATE_OR_UPDATE_CHAT_ITEM_ADMIN = 'CREATE_OR_UPDATE_CHAT_ITEM_ADMIN';
export const REMOVE_CHAT_ITEM_ADMIN = 'REMOVE_CHAT_ITEM_ADMIN';

export const loadListAction = CommonAction<IChat[]>(LOAD_LIST_CHAT_ADMIN);
export const createOrUpdateAction = CommonAction<IChat>(CREATE_OR_UPDATE_CHAT_ITEM_ADMIN);
export const removeAction = CommonAction<number>(REMOVE_CHAT_ITEM_ADMIN);
export const loadCountAction = CommonAction<number>(LOAD_COUNT_CHAT_ADMIN);

export type loadListType = ReturnType<typeof loadListAction>;
export type createOrUpdateType = ReturnType<typeof createOrUpdateAction>;
export type removeType = ReturnType<typeof removeAction>;
export type loadCountType = ReturnType<typeof loadCountAction>;

export type ChatAction = loadListType | createOrUpdateType | removeType | loadCountType
