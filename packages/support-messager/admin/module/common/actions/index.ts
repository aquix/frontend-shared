import { INotifMessage } from '../types';

export const CATCH_ERROR = 'CATCH_ERROR';
export const SUCCESS_MESSAGE = 'SUCCESS_MESSAGE';

export const SET_LOADING_CONTEXT = 'SET_LOADING_CONTEXT';
export const REMOVE_LOADING_CONTEXT = 'REMOVE_LOADING_CONTEXT';


export const CommonAction = <T>(type: string) => (payload: T) => ({
  type,
  payload,
});

export const catchError = (data: INotifMessage) => ({
  type: CATCH_ERROR,
  payload: data,
});

export const successMessage = (data: INotifMessage) => ({
  type: SUCCESS_MESSAGE,
  payload: data,
});

export const setLoadingContext = (context: string = 'common') => ({
  type: SET_LOADING_CONTEXT,
  payload: context,
});

export const removeLoadingContext = (context = 'common') => ({
  type: REMOVE_LOADING_CONTEXT,
  payload: context,
});

interface NotifAction {
  type: string;
  payload: INotifMessage | string | void;
}

export type CommonAction = NotifAction;
