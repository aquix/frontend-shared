import { Subject } from 'rxjs';

export interface INotifMessage {
  message: string;
  title?: string;
  duration?: number;
}

export interface ILoadingEvent {
  kind: string;
  context: string;
  size: number;
}

export interface CommonReducer {
  contexts: ContextType;
  error: Subject<INotifMessage>;
  success: Subject<INotifMessage>;
  loading: Subject<ILoadingEvent>;
}

export type ContextType = { [key: string]: number }
