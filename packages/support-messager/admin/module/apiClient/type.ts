import { BaseCommandProps } from '../../types/reducer';

export interface ResultListRes<P> extends BaseCommandProps {
  list: P
}

export interface ResultItemRes<P> extends BaseCommandProps {
  item: P
}
