import { apiCall } from './httpClient';
import { AppDispatch, BaseCommandProps } from '../types/reducer';
import { ResultItemRes, ResultListRes } from '../module/apiClient/type';
import { IChat } from '../types/chat';
import { ApiClientOptions } from '../module/apiClient';
import { createOrUpdateAction, loadCountAction, loadListAction, removeAction } from '../redux/action/agent';

export const load = (currentPage, limit: number) => (dispatch: AppDispatch<ResultListRes<IChat[]>>) => {
  const value = {
    opts: {
      limit,
      offset: currentPage,
    },
  };
  return dispatch(apiCall<object, ResultListRes<IChat[]>>('/messenger/chat/admin/list', value, { method: 'post' }))
    .then(({ list }) => dispatch(loadListAction(list)));
};

export const count = () => (dispatch: AppDispatch<ResultListRes<IChat[]>>) => {
  return dispatch(apiCall<object, { count: number } & BaseCommandProps>('/messenger/chat/admin/count', undefined, { method: 'get' }))
    .then(({ count }) => dispatch(loadCountAction(count)));
};

export const createOrUpdate = (value: IChat) => (dispatch: AppDispatch<IChat>) => {
  const options: ApiClientOptions = { method: value.id && 'put' || 'post' };
  return dispatch(apiCall<object, ResultItemRes<IChat>>('/messenger/chat/admin', value, options))
    .then(({ item }) => dispatch(createOrUpdateAction({ ...value, ...item })));
};


export const remove = (id: number) => (dispatch: AppDispatch<IChat>) => {
  return dispatch(apiCall<object, ResultItemRes<IChat>>('/messenger/chat/admin', { id }, { method: 'delete' })).then(() => dispatch(removeAction(id)));
};
