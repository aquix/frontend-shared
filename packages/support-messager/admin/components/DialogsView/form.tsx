import React, { FunctionComponent, useEffect } from 'react';
import { Button, Input, Form } from 'antd';
import { IChat } from '../../types/chat';

interface IPrivacyForm {
  onSubmitForm(values: IChat): void,

  initialValues: IChat
}

const ChatForm: FunctionComponent<IPrivacyForm> = (props: IPrivacyForm) => {
  const { initialValues } = props;
  const [form] = Form.useForm();

  const submitHandler = (values: IChat) => {
    const { onSubmitForm } = props;
    onSubmitForm(values);
  };
  useEffect(() => {
    form.setFieldsValue(initialValues);
  }, [initialValues]);

  return (
    <Form
      form={form}
      initialValues={initialValues}
      onFinish={(values: IChat) => submitHandler({ ...initialValues, ...values } as IChat)}
    >
      <Form.Item
        label="title"
        name="title"
        key="title"
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          className="float-right"
        >
          Save
        </Button>
      </Form.Item>
    </Form>
  );
};

export default ChatForm;
