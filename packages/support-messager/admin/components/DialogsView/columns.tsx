import React from 'react';
import { ColumnsType } from 'antd/es/table';
import { IChat } from '../../types/chat';
import { Button, Divider, Popconfirm } from 'antd';

const columns = (edit: (item: IChat) => void, remove: (id: number) => void): ColumnsType<IChat> => ([
  {
    title: 'chat-title',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: 'privateKey',
    dataIndex: 'privateKey',
    key: 'privateKey',
  },
  {
    title: 'Action',
    key: 'action',
    align: 'right',
    render: (text: string, record: IChat) => (
      <span>
        <Button shape="circle" icon={<span className="json icon-edit" />} onClick={() => edit(record)} />
        <Divider type="vertical" />
        <Button shape="circle" icon={<span className="json icon-delete" />} onClick={() => remove(record.id)} />
      </span>
    ),
  },
]);

export default columns;
