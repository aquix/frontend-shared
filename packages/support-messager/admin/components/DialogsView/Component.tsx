import React, { FunctionComponent, useEffect, useMemo, useState } from 'react';
import { ChatAdminProps } from './redux';
import { Button, Modal, Pagination, Table } from 'antd';
import columns from './columns';
import { IChat } from '../../types/chat';
import ChatForm from './form';
import { remove } from '../../commands/chat';

const ChatTable: FunctionComponent<ChatAdminProps> = (props) => {
  const { load, countLoad, count, list, createOrUpdate, remove } = props;
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [item, setItem] = useState<IChat>({} as IChat);
  const [visible, setVisible] = useState<boolean>(false);

  useEffect(() => {
    countLoad();
  }, []);

  const handleSubmit = (value: IChat) => createOrUpdate(value).then(() => {
    setVisible(false);
    setItem({} as IChat);
  });

  const edit = (value: IChat) => {
    setVisible(true);
    setItem(value);
  };

  useEffect(() => {
    load(currentPage, pageSize);
  }, [pageSize, currentPage]);

  const dataSource = useMemo(() => list, [list]);
  const tableColumns = useMemo(() => columns(edit, remove), [list]);

  return (
    <div>
      <Button
        type="primary"
        onClick={() => setVisible(true)}
      >
        Создать чат
      </Button>
      <Table
        scroll={{ x: 1000 }}
        size={'small'}
        columns={tableColumns}
        dataSource={dataSource}
        rowKey={record => record.id}
        locale={{ emptyText: 'Нет данных' }}
        className="tableNoMarginPanel"
        pagination={false}
      />
      <Pagination
        className="mt-10"
        current={currentPage}
        pageSize={pageSize}
        onChange={(page, pageSize) => {
          setCurrentPage(page);
          setPageSize(pageSize || 10);
        }}
        total={count}
      />
      {visible && (
        <Modal
          visible={true}
          footer={false}
          title='Чаты'
          onCancel={() => {
            setItem({} as IChat)
            setVisible(false)
          }}
        >
          <ChatForm
            onSubmitForm={handleSubmit}
            initialValues={item}
          />
        </Modal>
      )}
    </div>
  );
};

export default ChatTable;
