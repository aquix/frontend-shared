import { ISupportMessengerAdminReducer } from '../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { count, createOrUpdate, load, remove } from '../../commands/chat';
import { IChat } from '../../types/chat';

export const mapStateToProps = (state: ISupportMessengerAdminReducer) => {
  return ({
    list: state.chatAdmin.list,
    count: state.chatAdmin.count,
  });
}


export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessengerAdminReducer, void, AnyAction>) => ({
  load: (currentPage, limit: number) => dispatch(load(currentPage, limit)),
  countLoad: () => dispatch(count()),
  createOrUpdate: (value: IChat) => dispatch(createOrUpdate(value)),
  remove: (id: number) => dispatch(remove(id)),
});

export type ChatAdminProps =
  & ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>;
