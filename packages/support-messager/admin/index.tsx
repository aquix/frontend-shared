import { Provider } from 'react-redux';
import React, { FunctionComponent } from 'react';
import App from './components/DialogsView';
import store from './redux/store';

const AdminChat: FunctionComponent<{}> = (props) => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );

};


export default AdminChat;

