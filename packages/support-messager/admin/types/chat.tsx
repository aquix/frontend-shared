
export interface IChat {
  id: number
  title: string,
  privateKey: string,
}

export interface IChatReducerAdmin {
  list: IChat[],
  count: number,
}
