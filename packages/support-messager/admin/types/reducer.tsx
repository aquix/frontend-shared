import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { CommonReducer } from '../module/common/types';
import { Action, AnyAction } from 'redux';
import { IChatReducerAdmin } from './chat';

export interface ISupportMessengerAdminReducer {
  chatAdmin: IChatReducerAdmin
  common: CommonReducer;
}

export type ThunkResult<R> = ThunkAction<R, ISupportMessengerAdminReducer, null, Action<Promise<R>>>;
export type AppDispatch<R> = ThunkDispatch<ISupportMessengerAdminReducer, {}, Action<Promise<R>> | AnyAction>

export interface BaseCommandProps {
  success: string;
  statusCode?: number;
  message?: string;
}
