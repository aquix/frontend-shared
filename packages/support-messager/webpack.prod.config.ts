// @ts-ignore
import path from 'path';
import webpack, { Configuration, HotModuleReplacementPlugin } from 'webpack';
// @ts-ignore
import HtmlWebpackPlugin from 'html-webpack-plugin';

const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const deps = require('./package.json').dependencies;

const config: Configuration = {
  mode: 'production',
  output: {
    path: path.join(__dirname + '/build'),
    publicPath: '/',
  },
  entry: './src/index.tsx',
  module: {
    rules: [
      {
        test: /\.(ts)x?$/i,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react',
              '@babel/preset-typescript',
            ],
          },
        },
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      // {
      //   test: /\.(woff|woff2|ttf)$/,
      //   loader: 'url-loader',
      // },
      // {
      //   test: /\.(woff(2)?|ttf)(\?v=\d+\.\d+\.\d+)?$/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         name: 'aquix-messager.[ext]',
      //         outputPath: '/fonts',
      //       },
      //     },
      //   ],
      // },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
    new MiniCssExtractPlugin({
      filename: 'main.css',
      chunkFilename: '[id].css',
    }),
    new HotModuleReplacementPlugin(),

    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
    new ModuleFederationPlugin({
      name: 'chatClient',
      filename: 'remoteEntry.js',
      remotes: {},
      exposes: {},
      shared: {
        // ...deps,
        react: {
          singleton: true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          singleton: true,
          requiredVersion: deps['react-dom'],
        },
      },
    }),
  ],
  // externals: {
  //   // 'redux-thunk': 'redux-thunk',
  //   // 'redux': 'redux',
  //   // 'react-redux': 'react-redux',
  // 'react': 'react',
  // 'react-dom': 'react-dom',
  // },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: false,
        parallel: false,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
};

export default config;
