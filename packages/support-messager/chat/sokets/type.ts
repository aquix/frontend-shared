export interface ISocketResult {
  event: string,
  item: any
}
