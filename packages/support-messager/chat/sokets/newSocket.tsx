import { FunctionComponent, useEffect } from 'react';
import { connect } from 'react-redux';
import { ISupportMessagerReducer } from '../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { onConnectSocketAction } from '../redux/action/socket';
import socketController from './handlers';
import { AnyAction } from 'redux';


export interface Message {
  action: string;
  topic?: string;
  message?: string;
}

const WebSocketComponent: FunctionComponent<WebSocketProps> = (props) => {
  const { OnConnect } = props;

  const ping = (message: Message, wss: WebSocket) => {
    setTimeout(() => wss.send(JSON.stringify({ action: 'ping' })), 3000);
  };

  const connect = () => {
    const wss = new WebSocket(process.env.SOCKET_CHAT || '');
    wss.onopen = (e) => {
      OnConnect(wss);
      setTimeout(() => wss.send(JSON.stringify({ action: 'ping' })), 3000);
    };

    wss.onmessage = (e) => {
      const data: Message = typeof e.data != 'object' && JSON.parse(e.data) || e.data;
      if (data.action == 'pong') {
        ping(data, wss);
      } else {
        socketController(data);
      }
    };

    wss.onclose = (e) => {
      console.log('Socket is closed. Reconnect will be attempted in 1 second.');
      OnConnect(undefined);
      wss.close();
      setTimeout(function () {
        connect();
      }, 1000);
    };

    wss.onerror = (err) => {
      console.error('Socket encountered error: ', 'Closing socket');
      wss.close();
    };
  };
  useEffect(() => {
    connect();
  }, []);

  return null;
};

const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => ({
  OnConnect: (ws: WebSocket | undefined) => dispatch(onConnectSocketAction(ws)),
});

export type WebSocketProps = ReturnType<typeof mapDispatchToProps>


export default connect(null, mapDispatchToProps)(WebSocketComponent);
