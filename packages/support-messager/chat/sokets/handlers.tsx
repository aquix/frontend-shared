import store from '../redux/store';
import { Message } from '../types/message';
import { setNewMessageAction } from '../redux/action/message';
import {
  closeAppealAction,
  loadRoomsAction,
  moveRoomArchiveToMyAction, removeRoomAgentsAction, setEmptyRoomAction,
  setNewRoomAction, setTypingAction,
  takeOverAction, takeOverEmptyAction,
  updateLastMessageInRoomAction, updateRoomNotifyAction,
} from '../redux/action/room';
import {
  refillNotifyInCategoryAction,
  setCurrentCategoryAction,
  withdrawalNotifyInCategoryAction,
} from '../redux/action/category';
import { Room } from '../types/room';
import { setOnlineUserAction } from '../redux/action/online';

const socketController = (result: any) => {
  const { event, item, room } = result;
  const agentIdIsSocket = room && room.agentId;
  const state = store.getState();
  const agentId = state.agent.agent?.user?.id;
  switch (event) {
    case 'createAppeal':
      const newRoom = new Room(item);
      store.dispatch(setNewRoomAction(newRoom));
      store.dispatch(updateRoomNotifyAction({ slug: 'new', count: 1, roomId: newRoom.id, method: 'refill' }));
      break;
    case 'takeOver':
      const roomTakeOver = new Room(item);
      if (agentId != roomTakeOver.agentId) {
        store.dispatch(takeOverAction({ room: roomTakeOver, agentId }));
        store.dispatch(withdrawalNotifyInCategoryAction({ slug: 'new', count: roomTakeOver.countNotify }));
        store.dispatch(updateRoomNotifyAction({
          slug: 'new',
          count: roomTakeOver.countNotify,
          roomId: roomTakeOver.id,
          method: 'withdrawal',
        }));
      }
      break;
    case 'takeOverEmpty':
      const roomTakeOverEmpty = new Room(item);
      if (agentId != roomTakeOverEmpty.agentId) {
        store.dispatch(takeOverEmptyAction({ room: roomTakeOverEmpty, agentId }));
      }
      break;
    case 'newMessage':
      const message = new Message(item);
      if (room.open && !agentIdIsSocket) {
        if (!message.agentId) {
          store.dispatch(refillNotifyInCategoryAction({ slug: 'new', count: 1 }));
          store.dispatch(updateRoomNotifyAction({ slug: 'new', count: 1, roomId: room.id, method: 'refill' }));
        }
        store.dispatch(setNewMessageAction(message));
        store.dispatch(updateLastMessageInRoomAction(message));
        break;
      }

      const newMessageRoom = new Room(room);
      store.dispatch(moveRoomArchiveToMyAction({ room: newMessageRoom, agentId }));


      if (agentIdIsSocket && (agentIdIsSocket == agentId)) {
        if (!message.agentId) {
          store.dispatch(refillNotifyInCategoryAction({ slug: 'my', count: 1 }));
          store.dispatch(updateRoomNotifyAction({ slug: 'my', count: 1, roomId: room.id, method: 'refill' }));
        }
        store.dispatch(setNewMessageAction(message));
        store.dispatch(updateLastMessageInRoomAction(message));
      }
      break;
    case 'closeAppeal':
      const closeAppealRoom = new Room(item);
      store.dispatch(closeAppealAction({ room: closeAppealRoom, agentId }));
      if (agentId != closeAppealRoom.agentId) {
        store.dispatch(refillNotifyInCategoryAction({ slug: 'archive', count: closeAppealRoom.countNotify }));
        store.dispatch(updateRoomNotifyAction({
          slug: 'archive',
          count: 1,
          roomId: closeAppealRoom.id,
          method: 'refill',
        }));
      }
      break;
    case 'typing':
      store.dispatch(setTypingAction(item));
      break;
    case 'stopTyping':
      store.dispatch(setTypingAction(item));
      break;
    case 'changeOnline' : {
      const { accessory } = item;
      if (!accessory) {
        break;
      }
      store.dispatch(setOnlineUserAction({ key: accessory.userId, ...accessory }));
      if (!!room) {
        const emptyRoom = new Room(room);
        store.dispatch(setEmptyRoomAction(emptyRoom));
      }
      break;
    }
    case 'changeLocation' : {
      const { accessory } = item;
      if (!accessory) {
        break;
      }
      store.dispatch(setOnlineUserAction({ key: accessory.userId, ...accessory }));
      break;
    }
    case 'emptyRoomRepeat': {
      const emptyRoom = new Room(room);
      store.dispatch(setEmptyRoomAction(emptyRoom));
      break;
    }
    case 'removeAgentsRooms': {
      store.dispatch(removeRoomAgentsAction(item));
      break;
    }
  }
};

export default socketController;
