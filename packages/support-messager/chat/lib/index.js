var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import App from './components/ShowChat';
import store from './redux/store';
import { Provider } from 'react-redux';
import { useEffect, useState } from 'react';
import ImageView from './components/ImageView';
import { getCookie, setCookie } from './tools/cookie';
import { v4 as uuidv4 } from 'uuid';
import WebSocketComponent from './sokets/newSocket';
import { Subject } from 'rxjs';
var ShowChat = function (props) {
    var redirectInfo = props.redirectInfo;
    var subjects = useState({
        InitEvent: new Subject(),
        OpenEvent: new Subject(),
        CloseEvent: new Subject(),
    })[0];
    useEffect(function () {
        var oldUuid = getCookie('uuid');
        if (!oldUuid) {
            setCookie('uuid', uuidv4());
        }
        console.log('Init web sockets');
        subjects.InitEvent.next();
        subjects.OpenEvent.subscribe(function (open) { return console.log('open', open); });
        subjects.CloseEvent.subscribe(function (close) { return console.log('close', close); });
    }, []);
    return (_jsxs(Provider, __assign({ store: store }, { children: [_jsx(ImageView, {}, void 0), _jsx(WebSocketComponent, __assign({}, subjects), void 0), _jsx(App, { redirectInfo: redirectInfo }, void 0)] }), void 0));
};
export default ShowChat;
//# sourceMappingURL=index.js.map