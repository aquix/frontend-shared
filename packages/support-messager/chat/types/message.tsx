import getImage from '../components/getImage';

export interface IMessage {
  id: number,
  message: string,
  read: boolean,
  skip: boolean,
  roomId: number
  author: string
  type: string
  agentId: number | string,
  clientId: number | string
  createdAt: Date
}

export class Message implements IMessage {
  public readonly id: number;
  public readonly roomId: number;
  public agentId: number | string;
  public clientId: number | string;
  public message: string;
  public author: string;
  public type: string;
  public read: boolean;
  public skip: boolean;
  public createdAt: Date;

  constructor(obj: IMessage) {
    this.id = obj.id;
    this.agentId = obj.agentId;
    this.clientId = obj.clientId;
    this.roomId = obj.roomId;
    this.message = obj.message;
    this.author = obj.author;
    this.type = obj.type;
    this.read = obj.read;
    this.skip = obj.skip;
    this.createdAt = new Date(obj.createdAt);
  }

  getPosition() {
    return this.agentId ? 'right' : 'left';
  }

  getType() {
    if (this.clientId) {
      return 'user';
    }

    if (this.agentId) {
      return 'support';
    }

    if (this.type == 'system') {
      return this.type;
    }
  }

  getMessage(openImageView: (key: string) => void) {
    if (this.type == 'image') {
      return <img
        // lowsrc={getImage(100, 100, this.message)}
        onClick={() => openImageView(this.message)}
        src={getImage(800, 800, this.message)}
        style={{ padding: '3px', maxHeight: '200px', maxWidth: '200px' }}
      />;
    }

    return this.message;
  }
}

export type MessagesList = { [roomId: number]: Message[], }

export interface IMessageReducer {
  list: MessagesList;
  needRead: number[];
  count: number;
}

export interface ILoadMessageByRoom {
  roomId: number,
  messages: Message[]
}


export interface IMessageType {
  type?: string,
  message?: string,
  roomId: number
}

export interface IUpdateCurrentRoom {
  agentId: number,
  roomId: number
}

export interface INewMessage {
  message: Message,
  agentId: number
}

export interface ISetRead {
  id: number;
  roomId: number;
}
