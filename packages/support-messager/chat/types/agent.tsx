export interface ISupportUser {
  id: number,
  outerId: number,
  userId: number,
  info: object
  name: string,
  email: string,
  image: string,
  user?: ISupportUser,
  phone: string,
  messagerId: string
}

export class SupportUser implements ISupportUser {
  id: number;
  email: string;
  name: string;
  userId: number;
  image: string;
  phone: string;
  user?: ISupportUser;
  messagerId: string;
  outerId: number;
  info: object;

  constructor(obj: ISupportUser) {
    this.id = obj.id;
    this.email = obj.email;
    this.name = obj.name;
    this.userId = obj.userId;
    this.image = obj.image;
    this.phone = obj.phone;
    this.user = obj.user && new SupportUser(obj.user) || undefined;
    this.outerId = obj.outerId;
    this.messagerId = obj.messagerId;
    this.info = obj.info;
  }
}

export interface IAgentReducer {
  agent?: ISupportUser;
  active: boolean;
}

export interface IResLoadAgentByUUid {
  user: ISupportUser;
  active: boolean;
}



