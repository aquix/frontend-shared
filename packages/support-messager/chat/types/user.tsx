export interface IUserReducer {
  [key: number]: IActionUserNames;
}

export interface IActionUser extends IActionUserNames{
  key: number,
}



export interface IActionUserNames {
  surname: string
  middleName: string,
  firstName: string,
}
