import { IMessage, Message } from './message';
import { ISupportUser, SupportUser } from './agent';
import { ICount } from './common';
import getImage from '../components/getImage';

export interface IChat {
  id: number,
  title: string
  privateKey: string
}


export interface IRoom {
  id: number,
  agentId: number,
  agent: ISupportUser,
  clientId: number,
  client: ISupportUser,
  chatId: number,
  chat: IChat,
  open: boolean,
  typing: ITypingRoom,
  pending: boolean
  isEmpty: boolean
  countNotify: number
  lastMessage: IMessage
  createdAt: Date
  lastUpdate?: Date
  online?: boolean
}

export class Room implements IRoom {
  public readonly id: number;
  public agentId: number;
  public clientId: number;
  public client: ISupportUser;
  public chatId: number;
  public chat: IChat;
  public agent: ISupportUser;
  public countNotify: number;
  public lastMessage: IMessage;
  public typing: ITypingRoom;
  public open: boolean;
  public isEmpty: boolean;
  public pending: boolean;
  public createdAt: Date;
  public lastUpdate?: Date;
  public online?: boolean;

  constructor(obj: IRoom) {
    this.id = obj.id;
    this.agentId = obj.agentId;
    this.chatId = obj.chatId;
    this.chat = obj.chat;
    this.agent = obj.agent && new SupportUser(obj.agent) || null;
    this.clientId = obj.clientId;
    this.client = obj.client && new SupportUser(obj.client) || null;
    this.open = obj.open;
    this.online = obj.online || false;
    this.isEmpty = obj.isEmpty;
    this.typing = obj.typing && obj.typing || { typing: false, type: 'text' } as ITypingRoom;
    this.countNotify = obj.countNotify && Number(obj.countNotify) || 0;
    this.pending = obj.pending;
    this.createdAt = new Date(obj.createdAt);
    this.lastUpdate = obj.lastUpdate && new Date(obj.lastUpdate);
    this.lastMessage = obj.lastMessage && new Message(obj.lastMessage) || undefined;
  }


  getClientEmail() {
    if (!this || !this.client) {
      return;
    }

    if (this.client.email === 'firstLoad') {
      return null;
    }

    return this.client.email;
  }

  getLastMessage() {
    if (!this || !this.lastMessage) {
      return 'No messages';
    }

    if (this.lastMessage.type === 'image') {
      return 'Photo';
    }

    return this.lastMessage.message;
  }

  getClientImage() {
    if (!this || !this.client) {
      return;
    }

    return getImage(200, 200, this.client.image, 'https://aquix-images.s3.eu-north-1.amazonaws.com/logo_aquix.png');
  }

  // getLastTimeMessage() {
  //   return this.lastMessage && this.lastMessage.createdAt || '-';
  // }

  getTyping() {
    if (this.typing.typing) {
      if (this.typing.type == 'text') {
        return <span>Печатает</span>;
        // return (
        //   <span className="text-write">
        //     <span>Typing</span>
        //     <span className="dotteds">
        // <span className="dotted-1">.</span>
        // <span className="dotted-2">.</span>
        // <span className="dotted-3">.</span>
        // </span>
        // </span>
        // );
      }

      if (this.typing.type == 'image') {
        return <span>Отправляет фото</span>;
        // return (
        //   <span className="text-write">
        //     <span>Sending Photo</span>
        // <span className="dotteds">
        // <span className="dotted-1">.</span>
        // <span className="dotted-2">.</span>
        // <span className="dotted-3">.</span>
        // </span>
        // </span>
        // );
      }
    }
    return this.getLastMessage();
  }

}

export interface IRoomCategoryValue extends ICount {
  items: Room[];
  currentPage: number;
}

export interface IRoomReducer {
  list: ICategoryRoom,
  current?: Room
}

export interface ICategoryRoom {
  my: IRoomCategoryValue,
  new: IRoomCategoryValue,
  archive: IRoomCategoryValue,
  empty: IRoomCategoryValue,
}

export interface ILoadRoomOrCount {
  slug: string,
  currentPage?: number
  list?: Room[],
  count?: number
}


export interface IRoomAction {
  room: Room,
  agentId?: number
}


export interface ITypingRoom {
  roomId?: number,
  type: string,
  typing: boolean
  agentId?: number
  clientId?: number
}


export interface IReadMessages {
  count: number;
  roomId: number;
  slug: string;
}

export interface IUpdateMessagesInRoom {
  count: number;
  roomId: number;
  slug: string;
  method: string;
}

//
// export interface IAuthChatResp extends BaseCommandProps{
//   room: IRoom,
//   uuid: string
// }
