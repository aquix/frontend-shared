import { WebSocketSubject } from 'rxjs/webSocket';
import { Message } from '../sokets/newSocket';

export interface ISocketReducer {
  socket?: WebSocket;
}

export interface ISendMessageSocket {
  action: string,
  topic: string,
  message: any
}
