import { IRoomReducer } from './room';
import { IMessageReducer } from './message';
import { IAgentReducer } from './agent';
import { ICategory, ICategoryReducer } from './category';
import { IUserReducer } from './user';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { Action, AnyAction } from 'redux';
import { CommonReducer } from '../module/common/types';
import { IChatReducer } from '../redux/action/chat';
import { ISocketReducer } from './socket';
import { IOnlineReducer, IOnlineUser } from './online';

export interface ISupportMessagerReducer {
  room: IRoomReducer;
  message: IMessageReducer;
  agent: IAgentReducer;
  category: ICategoryReducer;
  user: IUserReducer;
  online: IOnlineReducer;
  common: CommonReducer;
  chat: IChatReducer;
  socket: ISocketReducer
}

export type ThunkResult<R> = ThunkAction<R, ISupportMessagerReducer, null, Action<Promise<R>>>;
export type AppDispatch<R> = ThunkDispatch<ISupportMessagerReducer, {}, Action<Promise<R>> | AnyAction>

export interface BaseCommandProps {
  success: string;
  statusCode?: number;
  message?: string;
}
