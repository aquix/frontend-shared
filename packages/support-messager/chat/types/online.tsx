export interface IOnlineReducer {
  [key: number]: IActionOnline;
}

export interface IOnlineUser extends IActionOnline{
  key: number,
}

export interface IActionOnline {
  online: string
  location: string,
  lastVisit: string,
  timeChangeLocation: string
}
