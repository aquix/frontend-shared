export interface ICategory {
  title: string,
  slug: string,
  notify: number
}

export interface ICategoryReducer {
  list: ICategory[],
  current: string
  allNotify: number
}

export interface ISetCategoryNotify {
  slug: string,
  count: number
}
