import { Provider } from 'react-redux';
import React, { FunctionComponent, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import WebSocketComponent from './sokets/newSocket';
import { getCookie, setCookie } from './tools/cookie';
import ImageView from './components/ImageView';
import App from './components/ShowChat';
import store from './redux/store';

export interface IShowChatApp {
  redirectInfo?: (value: string | number) => void;
}

const ShowChat: FunctionComponent<IShowChatApp> = (props) => {
  const { redirectInfo } = props;

  useEffect(() => {
    const oldUuid = getCookie('uuid');
    if (!oldUuid) {
      setCookie('uuid', uuidv4());
    }
  }, []);

  return (
    <Provider store={store}>
      <ImageView />
      <WebSocketComponent />
      <App redirectInfo={redirectInfo} />
    </Provider>
  );

};


export default ShowChat;

