import { apiCall } from './httpClient';
import { AppDispatch, BaseCommandProps, ISupportMessagerReducer } from '../types/reducer';
import { IRoom, Room } from '../types/room';
import {
  closeAppealAction,
  loadRoomsAction, loadRoomsCountAction, takeOverAction, takeOverEmptyAction,
} from '../redux/action/room';
import { IMessageType } from '../types/message';
import getNameCatalog from '../tools/getNameCatalog';
import {
  refillNotifyInCategoryAction, setAllNotifyAction,
  setCurrentCategoryAction,
  setNotifyCategoryAction, withdrawalNotifyInCategoryAction,
} from '../redux/action/category';
import { socketSendAction } from '../redux/action/socket';


export const loadRooms = (slug: string, currentPage: number, pageSize: number) => (dispatch: AppDispatch<any>) => {
  return dispatch(apiCall<object, any>(`/messenger/room/list/${getNameCatalog(slug)}`, {
    opts: {
      limit: pageSize,
      offset: currentPage * pageSize,
      // order: '"support-messenger"."created_at" desc',
    },
  }, { method: 'post' }))
    .then(({ list }) => {
      const items = (list || []).map((i: IRoom) => new Room(i));
      dispatch(loadRoomsAction({ slug, list: items, currentPage }));
    });
};

export const loadCountRooms = (slug: string) => (dispatch: AppDispatch<IMessageType>) => {
  return dispatch(apiCall<object, { count: number } & BaseCommandProps>(`/messenger/room/count/${getNameCatalog(slug)}`, undefined, { method: 'get' }))
    .then(({ count }) => {
      dispatch(loadRoomsCountAction({ slug, count }));
    });
};

export const takeOver = (slug: string, roomId: number) => (dispatch: AppDispatch<any>, getState: () => ISupportMessagerReducer) => {
  const state: ISupportMessagerReducer = getState();
  const agentId = state.agent.agent?.userId;
  return dispatch(apiCall<object, any>(`/messenger/chat/take-over/${roomId}`, undefined, { method: 'get' }))
    .then(({ item }) => {
      const room = new Room(item);
      if (slug == "empty") {
        dispatch(takeOverEmptyAction({ room, agentId }));
        dispatch(setCurrentCategoryAction('my'));
      } else {
        dispatch(takeOverAction({ room, agentId }));
        dispatch(setCurrentCategoryAction('my'));
        dispatch(refillNotifyInCategoryAction({ slug: 'my', count: room.countNotify }));
        dispatch(withdrawalNotifyInCategoryAction({ slug: 'new', count: room.countNotify }));
        dispatch(socketSendAction({ action: 'takeOver', topic: 'room', message: room }));
      }
    });
};

export const closeAppeal = (roomId: number) => (dispatch: AppDispatch<any>, getState: () => ISupportMessagerReducer) => {
  const state: ISupportMessagerReducer = getState();
  const agentId = state.agent.agent?.userId;
  return dispatch(apiCall<object, any>(`/messenger/chat/close-appeal/${roomId}`, undefined, { method: 'get' }))
    .then(({ item }) => {
      const room = new Room(item);
      dispatch(closeAppealAction({ room, agentId }));
      dispatch(setCurrentCategoryAction('archive'));
      dispatch(withdrawalNotifyInCategoryAction({ slug: 'my', count: room.countNotify }));
      dispatch(socketSendAction({ action: 'closeAppeal', topic: 'room', message: room }));
    });
};

export const countNotify = () => (dispatch: AppDispatch<any>, getState: () => ISupportMessagerReducer) => {
  dispatch(apiCall<object, any>(`/messenger/room/count-notify/${getNameCatalog('my')}`, undefined, { method: 'get' }))
    .then(({ count }) => {
      dispatch(setNotifyCategoryAction({ slug: 'my', count }));
      dispatch(apiCall<object, any>(`/messenger/room/count-notify/${getNameCatalog('archive')}`, undefined, { method: 'get' }))
        .then(({ count }) => {
          dispatch(setNotifyCategoryAction({ slug: 'archive', count: count || 0 }));
          dispatch(apiCall<object, any>(`/messenger/room/count-notify/${getNameCatalog('new')}`, undefined, { method: 'get' }))
            .then(({ count }) => {
              dispatch(setNotifyCategoryAction({ slug: 'new', count: count || 0 }));
              const state: ISupportMessagerReducer = getState();
              const countAll = (state.category.list || []).reduce((acc, cur) => acc += cur.notify, 0);
              dispatch(setAllNotifyAction(countAll));
            });
        });
    });
};
