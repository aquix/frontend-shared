import { AppDispatch, ISupportMessagerReducer } from '../types/reducer';
import { apiCall } from './httpClient';
import {
  loadMessageByRoomAction, setCountMessageAction, setNewMessageAction, updateMessageAction,
} from '../redux/action/message';
import { IMessage, Message } from '../types/message';
import { withdrawalNotifyInCategoryAction } from '../redux/action/category';
import { updateLastMessageInRoomAction, updateRoomNotifyAction } from '../redux/action/room';
import store from '../redux/store';

export const loadMessageByRoomId = (roomId: number, currentPage: number, pageSize: number) => (dispatch: AppDispatch<any>) => {
  return dispatch(apiCall<object, any>('/messenger/message/list', {
    roomId,
    opts: {
      offset: currentPage * pageSize,
      limit: pageSize,
      order: 'id desc',
    },
  }, { method: 'post' }))
    .then(({ list }) => {
      const messages = (list || []).map((i: IMessage) => new Message(i));
      dispatch(loadMessageByRoomAction({ messages, roomId }));
    });
};

export const countMessages = (roomId: number) => (dispatch: AppDispatch<any>) => {
  return dispatch(apiCall<object, any>(`/messenger/message/count/${roomId}`, undefined, { method: 'get' }))
    .then(({ count }) => {
      dispatch(setCountMessageAction(count));
    });
};

export const sendMessage = (message: string, roomId: number, type: string) => (dispatch: AppDispatch<any>) => {
  return dispatch(apiCall<object, any>(`/messenger/message/send`, { message, roomId, type }, { method: 'post' }))
    .then(({ item }) => {
      const message = new Message(item);
      store.dispatch(setNewMessageAction(message));
      store.dispatch(updateLastMessageInRoomAction(message));
    });
};


export const readMessages = () => (dispatch: AppDispatch<any>, getState: () => ISupportMessagerReducer) => {
  const state: ISupportMessagerReducer = getState();
  const ids = state.message.needRead;
  dispatch(apiCall<object, any>(`/messenger/message/read`, { ids }, { method: 'put' }));
};

export const readMessagesLocal = (id: number, slug: string, roomId: number) => (dispatch: AppDispatch<any>) => {
  if (slug == 'my') {
    dispatch(updateMessageAction({ id, roomId }));
    dispatch(withdrawalNotifyInCategoryAction({ count: 1, slug }));
    dispatch(updateRoomNotifyAction({ count: 1, roomId, slug, method: 'withdrawal' }));
  }
};
