import { apiCall } from './httpClient';
import { loadAgentAction, toggleShiftAction } from '../redux/action/agent';
import { setUserNameAction } from '../redux/action/user';
import { AppDispatch, ISupportMessagerReducer } from '../types/reducer';
import { setOnlineUserAction } from '../redux/action/online';

export const openShift = () => (dispatch: AppDispatch<any>) => {
  return dispatch(apiCall<object, any>('/messenger/agent/shift/open', undefined, { method: 'get' }))
    .then(({ item }) => {
      const { agent, active } = item;
      dispatch(toggleShiftAction(active));
      dispatch(loadAgentAction(agent));
    });
};

export const closeShift = () => (dispatch: AppDispatch<any>) => {
  return dispatch(apiCall<object, any>('/messenger/agent/shift/close', undefined, { method: 'get' }))
    .then(({ item: { active } }) => {
      dispatch(toggleShiftAction(active));
    });
};

export const loadAgent = () => (dispatch: AppDispatch<any>) => {
  return dispatch(apiCall<object, any>('/messenger/agent/get', undefined, { method: 'get' }))
    .then(({ active, agent }) => {
      dispatch(toggleShiftAction(active));
      dispatch(loadAgentAction(agent));
    });
};

export const loadFullName = (id: number) => (dispatch: AppDispatch<any>, getState: () => ISupportMessagerReducer) => {
  const state: ISupportMessagerReducer = getState();
  const name = id && state.user[id];

  if (!name) {
    dispatch(apiCall<object, any>(`/messenger/name/${id}`, undefined, { method: 'get' }))
      .then(({ item }) => {
        dispatch(setUserNameAction({ key: id, ...item }));
      });
  }
};

export const loadOnline = (id: number) => (dispatch: AppDispatch<any>, getState: () => ISupportMessagerReducer) => {
  const state: ISupportMessagerReducer = getState();
  const name = id && state.online[id];

  if (!name) {
    dispatch(apiCall<object, any>(`/messenger/online/${id}`, undefined, { method: 'get' }))
      .then(({ item }) => {
        dispatch(setOnlineUserAction({ key: id, ...item }));
      });
  }
};
