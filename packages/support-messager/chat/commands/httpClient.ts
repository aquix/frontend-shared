import { Action, AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import Router from 'next/router';
// import appConfig from '@configs/app';
import { AppDispatch, BaseCommandProps, ISupportMessagerReducer } from '../types/reducer';
import { ApiClientOptions, simpleApiClient } from '../module/apiClient';
import { catchError, removeLoadingContext, setLoadingContext } from '../module/common/actions';
import StatusError from '../module/apiClient/StatusError';
import { INotifMessage } from '../module/common/types';
import { getCookie } from '../tools/cookie';

const httpClient = <P extends object | null, T extends BaseCommandProps>(url: string, params?: P, options?: ApiClientOptions) =>
  (dispatch: ThunkDispatch<ISupportMessagerReducer, {}, Action<Promise<P>> | AnyAction>) => {
    const uuid = getCookie('uuid');
    const originToken = getCookie('originToken');
    const token = getCookie('token');
    const { context, asImpersonate } = options || { context: '' };
    const accessToken = asImpersonate ? { accessToken: `Bearer ${originToken}` } : {};
    const headers = {
      ...(options?.headers || {}),
      'Content-Type': 'application/json',
      'authorization': `Bearer ${token}`,
      uuid,
      ...accessToken,
    };

    const clientOptions = { ...(options || {}), headers };

    if (context) {
      dispatch(setLoadingContext(context));
    }

    return simpleApiClient<P, T>(url, params, clientOptions)
      .then((res: T) => {
        if (options?.isFile) {
          return res;
        }

        const { success } = res;

        if (context) {
          dispatch(removeLoadingContext(context));
        }

        if (!success) {
          const { message } = res;
          if (process.env.NODE_ENV === 'production') {
            throw new StatusError(message ? `${message}` : `No success`, res.statusCode || 200);
          } else {
            throw new StatusError(message ? `${message} url: /api${url}` : `No success, url: /api${url}`, res.statusCode || 200);
          }
        }

        return res;
      })
      .catch((e: StatusError) => {
        if (context) {
          dispatch(removeLoadingContext(context));
        }

        const msg: INotifMessage = {
          message: e.message,
        };
        dispatch(catchError(msg));

        // if (e.status === 405) {
        //   Router.push(appConfig.public.redirectPage);
        //   dispatch(clearData());
        // }

        throw e;
      });
  };


export const apiCall = <P extends object | null, T extends BaseCommandProps>(url: string, params?: P, options?: ApiClientOptions) =>
  (dispatch: AppDispatch<P>) =>
    dispatch(httpClient<P, T>(url, params, options))
      .then((res: T | void) => res as T);

export default httpClient;
