import {
  chatAction,
  IChatReducer,
  IMAGE_UPLOAD, IMAGE_VIEW, ImageUploadActionType, ImageViewType,
  SET_VISIBLE_CHAT,
  SET_VISIBLE_STYLE_CHAT,
  setVisibleChatStyleType,
  setVisibleChatType,
} from '../action/chat';

const CharReducer = (
  state: IChatReducer = { visible: false, visibleStyle: false, upload: false, img: undefined },
  action: chatAction,
): IChatReducer => {
  if (action.type === SET_VISIBLE_CHAT) {
    return {
      ...state,
      visible: (action as setVisibleChatType).payload,
    };
  }

  if (action.type === SET_VISIBLE_STYLE_CHAT) {
    return {
      ...state,
      visibleStyle: (action as setVisibleChatStyleType).payload,
    };
  }

  if (action.type === IMAGE_UPLOAD) {
    return {
      ...state,
      upload: (action as ImageUploadActionType).payload,
    };
  }

  if (action.type === IMAGE_VIEW) {
    return {
      ...state,
      img: (action as ImageViewType).payload,
    };
  }

  return state;
};

export default CharReducer;
