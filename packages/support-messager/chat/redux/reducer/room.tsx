import { ICategoryRoom, IRoom, IRoomReducer, ITypingRoom, Room } from '../../types/room';
import {
  CLOSE_APPEAL,
  closeAppealType,
  LOAD_ROOMS,
  LOAD_ROOMS_COUNT,
  loadRoomsType, MOVE_ROOM_ARCHIVE_TO_MY, moveRoomArchiveToMyType, REMOVE_ROOMS_AGENTS, removeRoomAgentsType,
  RoomAction,
  SET_CURRENT_ROOM, SET_EMPTY_ROOM,
  SET_NEW_ROOM, SET_TYPING,
  setCurrentRoomType, setEmptyRoomType, setNewRoomType, setTypingType,
  TAKE_OVER, TAKE_OVER_EMPTY, takeOverEmptyType,
  takeOverType, UPDATE_LAST_MESSAGE,
  UPDATE_ROOM_NOTIFY, updateLastMessageInRoomType,
  updateRoomNotifyType,

} from '../action/room';
import { uniqBy } from 'lodash';
import { Message } from '../../types/message';

const defaultState: IRoomReducer = {
  list: {
    empty: { count: 0, items: [], currentPage: 0 },
    my: { count: 0, items: [], currentPage: 0 },
    archive: { count: 0, items: [], currentPage: 0 },
    new: { count: 0, items: [], currentPage: 0 },
  },
  current: undefined,
};


const updateLastMessage = (message: Message, list: ICategoryRoom) => {
  for (const [key, value] of Object.entries(list)) {
    const item = value.items.find((i: Room) => i.id === message.roomId);
    if (!item) {
      continue;
    }

    const index = value.items.findIndex((i: Room) => i.id === message.roomId);
    const items = list[key as keyof ICategoryRoom].items;
    list[key as keyof ICategoryRoom].items = [
      ...items.slice(0, index),
      new Room({ ...item, lastMessage: message }),
      ...items.slice(index + 1),
    ];
  }

  return list;
};

const setTypingRoom = (item: ITypingRoom, list: ICategoryRoom) => {
  for (const [key, value] of Object.entries(list)) {
    const room = value.items.find((i: Room) => i.id === item.roomId);
    if (!room) {
      continue;
    }
    const index = value.items.findIndex((i: Room) => i.id === item.roomId);
    const items = list[key as keyof ICategoryRoom].items;
    list[key as keyof ICategoryRoom].items = [
      ...items.slice(0, index),
      new Room({ ...room, typing: item }),
      ...items.slice(index + 1),
    ];
  }

  return list;
};

const RoomReducer = (
  state: IRoomReducer = defaultState,
  action: RoomAction,
): IRoomReducer => {
  if (action.type === LOAD_ROOMS) {
    const { slug, list, currentPage } = (action as loadRoomsType).payload;
    const slugValue = slug as keyof ICategoryRoom;
    return {
      ...state,
      list: {
        ...state.list,
        [slugValue]: {
          ...(state.list[slugValue] || {}),
          items: uniqBy([
            ...(state.list[slugValue].items || []),
            ...(list || []),
          ], 'id'),
          currentPage,
        },
      },
    };
  }

  if (action.type === UPDATE_LAST_MESSAGE) {
    const message = (action as updateLastMessageInRoomType).payload;
    const list = state.list;

    return {
      ...state,
      list: {
        ...list,
        ...updateLastMessage(message, list),
      },
    };
  }

  if (action.type === SET_NEW_ROOM) {
    const room = (action as setNewRoomType).payload;
    return {
      ...state,
      list: {
        ...state.list,
        ['new']: {
          ...(state.list['new'] || {}),
          items: uniqBy([
            room,
            ...(state.list['new'].items || []),
          ], 'id'),
        },
      },
    };
  }

  if (action.type === SET_EMPTY_ROOM) {
    const room = (action as setEmptyRoomType).payload;
    return {
      ...state,
      list: {
        ...state.list,
        ['empty']: {
          ...(state.list['empty'] || {}),
          count: state.list['empty'].count + 1,
          items: uniqBy([
            room,
            ...(state.list['empty'].items || []),
          ], 'id'),
        },
      },
    };
  }

  if (action.type === LOAD_ROOMS_COUNT) {
    const { slug, count } = (action as loadRoomsType).payload;
    const slugValue = slug as keyof ICategoryRoom;
    return {
      ...state,
      list: {
        ...state.list,
        [slugValue]: {
          ...(state.list[slugValue] || {}),
          count,
        },
      },
    };
  }

  if (action.type === SET_CURRENT_ROOM) {
    return {
      ...state,
      current: (action as setCurrentRoomType).payload,
    };
  }

  if (action.type === TAKE_OVER) {
    const { room, agentId } = (action as takeOverType).payload;
    const item = state.list['new'].items.find(i => i.id == room.id);
    const index = state.list['new'].items.findIndex(i => i.id == room.id);
    if (!item) {
      return state;
    }

    if (room.agentId != agentId) {
      return {
        ...state,
        list: {
          ...(state.list || {}),
          ['new']: {
            ...(state.list['new'] || {}),
            count: state.list['new'].count - 1,
            items: [
              ...state.list['new'].items.slice(0, index),
              ...state.list['new'].items.slice(index + 1),
            ],
          },
        },
      };
    }

    return {
      ...state,
      current: room,
      list: {
        ...(state.list || {}),
        ['my']: {
          ...(state.list['my'] || {}),
          count: state.list['my'].count + 1,
          items: uniqBy([
            room,
            ...(state.list['my'].items || []),
          ], 'id'),
        },
        ['new']: {
          ...(state.list['new'] || {}),
          count: state.list['new'].count - 1,
          items: [
            ...state.list['new'].items.slice(0, index),
            ...state.list['new'].items.slice(index + 1),
          ],
        },
      },
    };
  }

  if (action.type === TAKE_OVER_EMPTY) {
    const { room, agentId } = (action as takeOverEmptyType).payload;
    const item = state.list['empty'].items.find(i => i.id == room.id);
    const index = state.list['empty'].items.findIndex(i => i.id == room.id);
    if (!item) {
      return state;
    }

    if (room.agentId != agentId) {
      return {
        ...state,
        list: {
          ...(state.list || {}),
          ['empty']: {
            ...(state.list['empty'] || {}),
            count: state.list['empty'].count - 1,
            items: [
              ...state.list['empty'].items.slice(0, index),
              ...state.list['empty'].items.slice(index + 1),
            ],
          },
        },
      };
    }

    return {
      ...state,
      current: room,
      list: {
        ...(state.list || {}),
        ['my']: {
          ...(state.list['my'] || {}),
          count: state.list['my'].count + 1,
          items: uniqBy([
            room,
            ...(state.list['my'].items || []),
          ], 'id'),
        },
        ['empty']: {
          ...(state.list['empty'] || {}),
          count: state.list['empty'].count - 1,
          items: [
            ...state.list['empty'].items.slice(0, index),
            ...state.list['empty'].items.slice(index + 1),
          ],
        },
      },
    };
  }

  if (action.type === CLOSE_APPEAL) {
    const { room, agentId } = (action as closeAppealType).payload;
    const item = state.list['my'].items.find(i => i.id == room.id);
    const index = state.list['my'].items.findIndex(i => i.id == room.id);

    if (room.agentId != agentId) {
      return {
        ...state,
        list: {
          ...(state.list || {}),
          ['archive']: {
            ...(state.list['archive'] || {}),
            count: state.list['archive'].count + 1,
            items: uniqBy([
              room,
              ...(state.list['archive'].items || []),
            ], 'id'),
          },
        },
      };
    }

    if (!item) {
      return state;
    }

    return {
      ...state,
      current: room,
      list: {
        ...(state.list || {}),
        ['archive']: {
          ...(state.list['archive'] || {}),
          count: state.list['archive'].count + 1,
          items: uniqBy([
            room,
            ...(state.list['archive'].items || []),
          ], 'id'),
        },
        ['my']: {
          ...(state.list['my'] || {}),
          count: state.list['my'].count - 1,
          items: [
            ...state.list['my'].items.slice(0, index),
            ...state.list['my'].items.slice(index + 1),
          ],
        },
      },
    };
  }

  if (action.type === UPDATE_ROOM_NOTIFY) {
    const { count, roomId, slug, method } = (action as updateRoomNotifyType).payload;
    const item = state.list[slug as keyof ICategoryRoom].items.find(i => i.id == roomId);
    const index = state.list[slug as keyof ICategoryRoom].items.findIndex(i => i.id == roomId);
    if (!item) {
      return state;
    }

    const countNotify = method == 'refill' ? item.countNotify + count : method == 'withdrawal' ? item.countNotify - count : item.countNotify;

    return {
      ...state,
      list: {
        ...(state.list || {}),
        [slug]: {
          ...(state.list[slug as keyof ICategoryRoom] || {}),
          items: [
            ...(state.list[slug as keyof ICategoryRoom].items || []).slice(0, index),
            new Room({ ...item, countNotify }),
            ...(state.list[slug as keyof ICategoryRoom].items || []).slice(index + 1),
          ],
        },
      },
    };
  }

  if (action.type === MOVE_ROOM_ARCHIVE_TO_MY) {
    const { room, agentId } = (action as moveRoomArchiveToMyType).payload;
    const item = state.list['archive'].items.find(i => i.id == room.id);
    if (!item) {
      return state;
    }

    const index = state.list['archive'].items.findIndex(i => i.id == room.id);
    if (room.agentId != agentId) {
      if (state.current?.id == room.id) {
        state.current = undefined;
      }
      return {
        ...state,
        list: {
          ...(state.list || {}),
          ['archive']: {
            ...(state.list['archive'] || {}),
            count: state.list['archive'].count - 1,
            items: [
              ...state.list['archive'].items.slice(0, index),
              ...state.list['archive'].items.slice(index + 1),
            ],
          },
        },
      };
    }

    return {
      ...state,
      current: room.id == state.current?.id ? room : state.current,
      list: {
        ...(state.list || {}),
        ['my']: {
          ...(state.list['my'] || {}),
          count: state.list['my'].count + 1,
          items: [
            room,
            ...(state.list['my'].items || []),
          ],
        },
        ['archive']: {
          ...(state.list['archive'] || {}),
          count: state.list['archive'].count - 1,
          items: [
            ...state.list['archive'].items.slice(0, index),
            ...state.list['archive'].items.slice(index + 1),
          ],
        },
      },
    };
  }

  if (action.type === SET_TYPING) {
    const item = (action as setTypingType).payload;
    if (state.current?.id == item.roomId) {
      state.current = new Room({ ...(state.current || {} as IRoom), typing: item });
    }

    return {
      ...state,
      list: {
        ...state.list,
        ...setTypingRoom(item, state.list),
      },
    };
  }

  if (action.type === REMOVE_ROOMS_AGENTS) {
    const ids = (action as removeRoomAgentsType).payload;

    const keys = Object.keys(state.list);
    let newStateList = state.list;

    keys.forEach(category => {
      let list = newStateList[category];
      if (!list) {
        return;
      }

      if (!list.items.length) {
        return;
      }

      ids.forEach(id => {
        const item = list.items.find(i => i.id === id);
        if (!item) {
          return;
        }

        const index = list.items.findIndex(i => i.id === id);
        list = {
          ...list,
          count: list.count - 1,
          items: [
            ...list.items.slice(0, index),
            ...list.items.slice(index + 1),
          ],
        };
      });

      newStateList[category] = list;
    });

    return {
      ...state,
      list: Object.assign({}, newStateList),
    };
  }

  return state;
};

export default RoomReducer;
