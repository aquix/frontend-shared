import { IUserReducer } from '../../types/user';
import { SET_USER_NAME, setUserNameType, UserAction } from '../action/user';

const UserReducer = (
  state: IUserReducer = {},
  action: UserAction,
): IUserReducer => {
  if (action.type === SET_USER_NAME) {
    const { key, ...fields } = (action as setUserNameType).payload;
    return {
      ...state,
      [key]: fields,
    };
  }

  return state;
};

export default UserReducer;
