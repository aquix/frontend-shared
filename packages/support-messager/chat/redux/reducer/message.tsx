import {
  CLEAR_READ_PATH,
  COUNT_MESSAGE,
  LOAD_MESSAGE_BY_ROOM,
  loadMessageByRoomType,
  MessageAction,
  SET_NEW_MESSAGE, SET_READ_PATH,
  setCountMessageType,
  setNewMessageType, setReadPathType, UPDATE_MESSAGE, updateMessageType,
} from '../action/message';
import { IMessageReducer, Message } from '../../types/message';
import { uniqBy, uniq } from 'lodash';

const defaultState: IMessageReducer = {
  list: {},
  count: 0,
  needRead: [],
};

const MessageReducer = (
  state: IMessageReducer = defaultState,
  action: MessageAction,
): IMessageReducer => {
  if (action.type === LOAD_MESSAGE_BY_ROOM) {
    const { messages, roomId } = (action as loadMessageByRoomType).payload;

    return {
      ...state,
      list: {
        ...(state.list || {}),
        [roomId]: uniqBy([
          ...(state.list[roomId] || []),
          ...messages,
        ], 'id'),
      },
    };
  }

  if (action.type === COUNT_MESSAGE) {
    return {
      ...state,
      count: (action as setCountMessageType).payload || 0,
    };
  }

  if (action.type === SET_READ_PATH) {
    const messageId = (action as setReadPathType).payload;
    state.needRead.push(messageId);

    return {
      ...state,
      needRead: uniq(state.needRead),
    };
  }

  if (action.type === CLEAR_READ_PATH) {
    return {
      ...state,
      needRead: [],
    };
  }

  if (action.type === UPDATE_MESSAGE) {
    const { id, roomId } = (action as updateMessageType).payload;
    const item = state.list[roomId].find(i => i.id == id);
    if (!item || item.read) {
      return state;
    }
    const index = state.list[roomId].findIndex(i => i.id == id);

    return {
      ...state,
      list: {
        ...(state.list || {}),
        [roomId]: [
          ...(state.list[roomId] || []).slice(0, index),
          new Message({ ...item, read: true }),
          ...(state.list[roomId] || []).slice(index + 1),
        ],
      },
    };
  }


  if (action.type === SET_NEW_MESSAGE) {
    const item = (action as setNewMessageType).payload;
    return {
      ...state,
      list: {
        ...(state.list || {}),
        [item.roomId]: uniqBy([
          item,
          ...(state.list[item.roomId] || []),
        ], 'id'),
      },
    };
  }
  return state;
};

export default MessageReducer;
