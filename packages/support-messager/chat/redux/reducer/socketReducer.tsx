import { ISocketReducer } from '../../types/socket';
import {
  ON_CONNECT_SOCKET_CHAT,
  onConnectSocketType,
  SOCKET_JOIN_ROOM_CHAT, SOCKET_LEAVE_ROOM_CHAT, SOCKET_SEND_CHAT,
  SocketAction,
  SocketJoinRoomType, SocketLeaveRoomType, SocketSendType,
} from '../action/socket';

const SocketReducer = (
  state: ISocketReducer = {
    socket: undefined,
  },
  action: SocketAction,
): ISocketReducer => {
  if (action.type === ON_CONNECT_SOCKET_CHAT) {
    const socket = (action as onConnectSocketType).payload;
    return {
      socket,
    };
  }


  if (action.type === SOCKET_JOIN_ROOM_CHAT) {
    const topic = (action as SocketJoinRoomType).payload;
    const { socket } = state;
    const payload = { action: 'subscribe', topic };
    socket?.send(JSON.stringify(payload));
    return state;
  }

  if (action.type === SOCKET_LEAVE_ROOM_CHAT) {
    const topic = (action as SocketLeaveRoomType).payload;
    const { socket } = state;

    const payload = { action: 'unsubscribe', topic };
    socket?.send(JSON.stringify(payload));
    return state;
  }

  if (action.type === SOCKET_SEND_CHAT) {
    const { message, topic, action: actionSocket } = (action as SocketSendType).payload;
    const { socket } = state;

    const payload = { action: actionSocket, topic, message };
    socket?.send(JSON.stringify(payload));
    return state;
  }

  return state;
};

export default SocketReducer;
