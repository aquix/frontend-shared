import { ICategory, ICategoryReducer } from '../../types/category';
import {
  CategoryAction, REFILL_COUNT_NOTIFY_IN_CATEGORY, refillNotifyInCategoryType, SET_ALL_NOTIFY, SET_CURRENT_CATEGORY,
  SET_NOTIFY_CATEGORY, setAllNotifyType, setCurrentCategoryType,
  setNotifyCategoryType, WITHDRAWAL_COUNT_NOTIFY_IN_CATEGORY, withdrawalNotifyInCategoryType,
} from '../action/category';

const defaultState: ICategoryReducer = {
  list: [
    {
      title: 'На сайте',
      slug: 'empty',
      notify: 0,
    },
    {
      title: 'Новые',
      slug: 'new',
      notify: 0,
    },
    {
      title: 'Мои',
      slug: 'my',
      notify: 0,
    },
    {
      title: 'Архив',
      slug: 'archive',
      notify: 0,
    },
  ],
  current: 'empty',
  allNotify: 0,
};

const CategoryReducer = (
  state: ICategoryReducer = defaultState,
  action: CategoryAction,
): ICategoryReducer => {
  if (action.type == SET_NOTIFY_CATEGORY) {
    const { count, slug } = (action as setNotifyCategoryType).payload;
    const item = state.list.find(i => i.slug == slug);
    const index = state.list.findIndex(i => i.slug == slug);

    if (!item) {
      return state;
    }

    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        { ...item, notify: count },
        ...state.list.slice(index + 1),
      ],
    };
  }

  if (action.type == SET_CURRENT_CATEGORY) {
    const current = (action as setCurrentCategoryType).payload;
    return {
      ...state,
      current,
    };
  }

  if (action.type == REFILL_COUNT_NOTIFY_IN_CATEGORY) {
    const { count, slug } = (action as refillNotifyInCategoryType).payload;
    const item = state.list.find(i => i.slug == slug);
    const index = state.list.findIndex(i => i.slug == slug);
    if (!item) {
      return state;
    }

    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        { ...item, notify: item.notify + count },
        ...state.list.slice(index + 1),
      ],
      allNotify: state.allNotify + count,
    };
  }

  if (action.type == WITHDRAWAL_COUNT_NOTIFY_IN_CATEGORY) {
    const { count, slug } = (action as withdrawalNotifyInCategoryType).payload;
    const item = state.list.find(i => i.slug == slug);
    const index = state.list.findIndex(i => i.slug == slug);

    if (!item) {
      return state;
    }


    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        { ...item, notify: item.notify - count },
        ...state.list.slice(index + 1),
      ],
      allNotify: state.allNotify - count,
    };
  }

  if (action.type == SET_ALL_NOTIFY) {
    const count = (action as setAllNotifyType).payload;

    return {
      ...state,
      allNotify: count,
    };
  }

  return state;
};

export default CategoryReducer;
