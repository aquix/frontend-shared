import { IOnlineReducer } from '../../types/online';
import { OnlineAction, SET_ONLINE_USER, setOnlineUserActionType } from '../action/online';

const OnlineReducer = (
  state: IOnlineReducer = {},
  action: OnlineAction,
): IOnlineReducer => {
  if (action.type === SET_ONLINE_USER) {
    const { key, ...fields } = (action as setOnlineUserActionType).payload;
    return {
      ...state,
      [key]: fields,
    };
  }

  return state;
};

export default OnlineReducer;
