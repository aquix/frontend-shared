import { combineReducers } from 'redux';
import { ISupportMessagerReducer } from '../../types/reducer';
import RoomReducer from './room';
import MessageReducer from './message';
import AgentReducer from './agent';
import CategoryReducer from './category';
import UserReducer from './userReducer';
import commonReducer from '../../module/common/reducers';
import chatReducer from './chatReducer';
import SocketReducer from './socketReducer';
import OnlineReducer from './onlineReducer';

export const SupportMessagerReducer = combineReducers<ISupportMessagerReducer>({
  room: RoomReducer,
  message: MessageReducer,
  agent: AgentReducer,
  category: CategoryReducer,
  user: UserReducer,
  common: commonReducer,
  chat: chatReducer,
  socket: SocketReducer,
  online: OnlineReducer
});


