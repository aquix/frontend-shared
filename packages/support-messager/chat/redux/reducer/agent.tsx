import {
  AgentAction,
  LOAD_AGENT,
  loadAgentType,
  TOGGLE_SHIFT, toggleShiftType,
} from '../action/agent';
import { IAgentReducer } from '../../types/agent';

const defaultState: IAgentReducer = {
  agent: undefined,
  active: false,
};

const AgentReducer = (
  state: IAgentReducer = defaultState,
  action: AgentAction,
): IAgentReducer => {
  if (action.type === LOAD_AGENT) {
    const agent = (action as loadAgentType).payload;
    return {
      ...state,
      agent: agent,
    };
  }

  if (action.type === TOGGLE_SHIFT) {
    const active = (action as toggleShiftType).payload;
    return {
      ...state,
      active,
    };
  }

  return state;
};

export default AgentReducer;
