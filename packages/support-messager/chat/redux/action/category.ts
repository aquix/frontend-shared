import { ISetCategoryNotify } from '../../types/category';
import { CommonAction } from '../../module/common/actions';

export const SET_NOTIFY_CATEGORY = 'SET_NOTIFY_CATEGORY';
export const SET_CURRENT_CATEGORY = 'SET_CURRENT_CATEGORY';
export const REFILL_COUNT_NOTIFY_IN_CATEGORY = 'REFILL_COUNT_NOTIFY_IN_CATEGORY';
export const WITHDRAWAL_COUNT_NOTIFY_IN_CATEGORY = 'WITHDRAWAL_COUNT_NOTIFY_IN_CATEGORY';
export const SET_ALL_NOTIFY = 'SET_ALL_NOTIFY';

export const setNotifyCategoryAction = CommonAction<ISetCategoryNotify>(SET_NOTIFY_CATEGORY);
export const setCurrentCategoryAction = CommonAction<string>(SET_CURRENT_CATEGORY);
export const setAllNotifyAction = CommonAction<number>(SET_ALL_NOTIFY);
export const refillNotifyInCategoryAction = CommonAction<ISetCategoryNotify>(REFILL_COUNT_NOTIFY_IN_CATEGORY);
export const withdrawalNotifyInCategoryAction = CommonAction<ISetCategoryNotify>(WITHDRAWAL_COUNT_NOTIFY_IN_CATEGORY);

export type setNotifyCategoryType = ReturnType<typeof setNotifyCategoryAction>;
export type setCurrentCategoryType = ReturnType<typeof setCurrentCategoryAction>;
export type refillNotifyInCategoryType = ReturnType<typeof refillNotifyInCategoryAction>;
export type withdrawalNotifyInCategoryType = ReturnType<typeof withdrawalNotifyInCategoryAction>;
export type setAllNotifyType = ReturnType<typeof setAllNotifyAction>;


export type CategoryAction =
  setNotifyCategoryType
  | setCurrentCategoryType
  | refillNotifyInCategoryType
  | withdrawalNotifyInCategoryType
  | setAllNotifyType
