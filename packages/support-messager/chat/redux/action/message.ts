import { CommonAction } from '../../module/common/actions';
import { ILoadMessageByRoom, ISetRead, Message } from '../../types/message';

export const LOAD_MESSAGE_BY_ROOM = 'LOAD_MESSAGE_BY_ROOM';
export const SET_NEW_MESSAGE = 'SET_NEW_MESSAGE';
export const COUNT_MESSAGE = 'COUNT_MESSAGE';
export const SET_READ_PATH = 'SET_READ_PATH';
export const CLEAR_READ_PATH = 'CLEAR_READ_PATH';
export const UPDATE_MESSAGE = 'UPDATE_MESSAGE';

export const loadMessageByRoomAction = CommonAction<ILoadMessageByRoom>(LOAD_MESSAGE_BY_ROOM);
export const setNewMessageAction = CommonAction<Message>(SET_NEW_MESSAGE);
export const setCountMessageAction = CommonAction<number>(COUNT_MESSAGE);
export const setReadPathAction = CommonAction<number>(SET_READ_PATH);
export const updateMessageAction = CommonAction<ISetRead>(UPDATE_MESSAGE);
export const clearReadPathAction = CommonAction<ISetRead>(CLEAR_READ_PATH);

export type loadMessageByRoomType = ReturnType<typeof loadMessageByRoomAction>;
export type setNewMessageType = ReturnType<typeof setNewMessageAction>;
export type setCountMessageType = ReturnType<typeof setCountMessageAction>;
export type setReadPathType = ReturnType<typeof setReadPathAction>;
export type updateMessageType = ReturnType<typeof updateMessageAction>;
export type clearReadPathAction = ReturnType<typeof clearReadPathAction>;

export type MessageAction =
  loadMessageByRoomType
  | setNewMessageType
  | setCountMessageType
  | setReadPathType
  | updateMessageType
  | clearReadPathAction;

