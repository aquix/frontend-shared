import { CommonAction } from '../../module/common/actions';
import { ISendMessageSocket } from '../../types/socket';

export const ON_CONNECT_SOCKET_CHAT = 'ON_CONNECT_SOCKET_CHAT';
export const SOCKET_JOIN_ROOM_CHAT = 'SOCKET_JOIN_ROOM_CHAT';
export const SOCKET_LEAVE_ROOM_CHAT = 'SOCKET_LEAVE_ROOM_CHAT';
export const SOCKET_SEND_CHAT = 'SOCKET_SEND_CHAT';

export const onConnectSocketAction = CommonAction<WebSocket | undefined>(ON_CONNECT_SOCKET_CHAT);
export const socketJoinRoomAction = CommonAction<string>(SOCKET_JOIN_ROOM_CHAT);
export const socketLeaveRoomAction = CommonAction<string>(SOCKET_LEAVE_ROOM_CHAT);
export const socketSendAction = CommonAction<ISendMessageSocket>(SOCKET_SEND_CHAT);

export type onConnectSocketType = ReturnType<typeof onConnectSocketAction>;
export type SocketJoinRoomType = ReturnType<typeof socketJoinRoomAction>;
export type SocketLeaveRoomType = ReturnType<typeof socketLeaveRoomAction>;
export type SocketSendType = ReturnType<typeof socketSendAction>;

export type SocketAction = onConnectSocketType | SocketLeaveRoomType | SocketJoinRoomType | SocketSendType
