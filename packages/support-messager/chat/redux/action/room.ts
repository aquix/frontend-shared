import {
  ILoadRoomOrCount,
  IReadMessages,
  IRoomAction,
  ITypingRoom,
  IUpdateMessagesInRoom,
  Room,
} from '../../types/room';
import { Message } from '../../types/message';
import { CommonAction } from '../../module/common/actions';

export const LOAD_ROOMS = 'LOAD_ROOMS';
export const UPDATE_LAST_MESSAGE = 'UPDATE_LAST_MESSAGE';
export const LOAD_ROOMS_COUNT = 'LOAD_ROOMS_COUNT';
export const SET_CURRENT_ROOM = 'SET_CURRENT_ROOM';
export const TAKE_OVER = 'TAKE_OVER';
export const TAKE_OVER_EMPTY = 'TAKE_OVER_EMPTY';
export const CLOSE_APPEAL = 'CLOSE_APPEAL';
export const UPDATE_ROOM_NOTIFY = 'UPDATE_ROOM_NOTIFY';
export const SET_NEW_ROOM = 'SET_NEW_ROOM';
export const SET_EMPTY_ROOM = 'SET_EMPTY_ROOM';
export const MOVE_ROOM_ARCHIVE_TO_MY = 'MOVE_ROOM_ARCHIVE_TO_MY';
export const SET_TYPING = 'SET_TYPING';
export const REMOVE_ROOMS_AGENTS = 'REMOVE_ROOMS_AGENTS';

export const loadRoomsAction = CommonAction<ILoadRoomOrCount>(LOAD_ROOMS);
export const loadRoomsCountAction = CommonAction<ILoadRoomOrCount>(LOAD_ROOMS_COUNT);
export const updateLastMessageInRoomAction = CommonAction<Message>(UPDATE_LAST_MESSAGE);
export const setNewRoomAction = CommonAction<Room>(SET_NEW_ROOM);
export const setEmptyRoomAction = CommonAction<Room>(SET_EMPTY_ROOM);
export const setCurrentRoomAction = CommonAction<Room>(SET_CURRENT_ROOM);
export const takeOverAction = CommonAction<IRoomAction>(TAKE_OVER);
export const takeOverEmptyAction = CommonAction<IRoomAction>(TAKE_OVER_EMPTY);
export const closeAppealAction = CommonAction<IRoomAction>(CLOSE_APPEAL);
export const moveRoomArchiveToMyAction = CommonAction<IRoomAction>(MOVE_ROOM_ARCHIVE_TO_MY);
export const updateRoomNotifyAction = CommonAction<IUpdateMessagesInRoom>(UPDATE_ROOM_NOTIFY);
export const setTypingAction = CommonAction<ITypingRoom>(SET_TYPING);
export const removeRoomAgentsAction = CommonAction<number[]>(REMOVE_ROOMS_AGENTS);

export type loadRoomsType = ReturnType<typeof loadRoomsAction>;
export type setTypingType = ReturnType<typeof setTypingAction>;
export type setNewRoomType = ReturnType<typeof setNewRoomAction>;
export type setEmptyRoomType = ReturnType<typeof setEmptyRoomAction>;
export type updateLastMessageInRoomType = ReturnType<typeof updateLastMessageInRoomAction>;
export type loadRoomsCountType = ReturnType<typeof loadRoomsCountAction>;
export type setCurrentRoomType = ReturnType<typeof setCurrentRoomAction>;
export type takeOverType = ReturnType<typeof takeOverAction>;
export type takeOverEmptyType = ReturnType<typeof takeOverEmptyAction>;
export type moveRoomArchiveToMyType = ReturnType<typeof moveRoomArchiveToMyAction>;
export type closeAppealType = ReturnType<typeof closeAppealAction>;
export type updateRoomNotifyType = ReturnType<typeof updateRoomNotifyAction>;
export type removeRoomAgentsType = ReturnType<typeof removeRoomAgentsAction>;

export type RoomAction =
  loadRoomsType
  | loadRoomsCountType
  | setCurrentRoomType
  | takeOverType
  | closeAppealType
  | updateRoomNotifyType
  | setNewRoomType
  | updateLastMessageInRoomType
  | moveRoomArchiveToMyType
  | setTypingType
  | setEmptyRoomType
  | takeOverEmptyType
  | removeRoomAgentsType
