import { ISupportUser, SupportUser } from '../../types/agent';
import { CommonAction } from '../../module/common/actions';

export const LOAD_AGENT = 'LOAD_AGENT';
export const TOGGLE_SHIFT = 'TOGGLE_SHIFT';
export const LOAD_LIST_AGENT = 'LOAD_LIST_AGENT';

export const loadAgentAction = CommonAction<ISupportUser | undefined>(LOAD_AGENT);
export const toggleShiftAction = CommonAction<boolean>(TOGGLE_SHIFT);
export const loadAgentListAction = CommonAction<SupportUser[]>(LOAD_LIST_AGENT);

export type loadAgentType = ReturnType<typeof loadAgentAction>;
export type toggleShiftType = ReturnType<typeof toggleShiftAction>;
export type loadAgentListType = ReturnType<typeof loadAgentListAction>;

export type AgentAction = loadAgentType | toggleShiftType | loadAgentListType;
