import { CommonAction } from '../../module/common/actions';
import { IOnlineUser } from '../../types/online';

export const SET_ONLINE_USER = 'SET_ONLINE_USER';

export const setOnlineUserAction = CommonAction<IOnlineUser>(SET_ONLINE_USER);

export type setOnlineUserActionType = ReturnType<typeof setOnlineUserAction>;

export type OnlineAction = setOnlineUserActionType
