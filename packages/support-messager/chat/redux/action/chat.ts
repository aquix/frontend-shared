import { CommonAction } from '../../module/common/actions';

export const SET_VISIBLE_CHAT = 'SET_VISIBLE_CHAT';
export const SET_VISIBLE_STYLE_CHAT = 'SET_VISIBLE_STYLE_CHAT';
export const IMAGE_UPLOAD = 'IMAGE_UPLOAD';
export const IMAGE_VIEW = 'IMAGE_VIEW';

export const setVisibleChatAction = CommonAction<boolean>(SET_VISIBLE_CHAT);
export const setVisibleChatStyleAction = CommonAction<boolean>(SET_VISIBLE_STYLE_CHAT);
export const ImageUploadAction = CommonAction<boolean>(IMAGE_UPLOAD);
export const ImageViewAction = CommonAction<string | undefined>(IMAGE_VIEW);

export type setVisibleChatType = ReturnType<typeof setVisibleChatAction>;
export type setVisibleChatStyleType = ReturnType<typeof setVisibleChatStyleAction>;
export type ImageUploadActionType = ReturnType<typeof ImageUploadAction>;
export type ImageViewType = ReturnType<typeof ImageViewAction>;

export type chatAction = setVisibleChatType | setVisibleChatStyleType | ImageUploadActionType | ImageViewType

export interface IChatReducer {
  visible: boolean;
  visibleStyle: boolean;
  upload: boolean;
  img: string | undefined;
}
