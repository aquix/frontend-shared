import { IActionUser } from '../../types/user';
import { CommonAction } from '../../module/common/actions';

export const SET_USER_NAME = 'SET_USER_NAME';

export const setUserNameAction = CommonAction<IActionUser>(SET_USER_NAME);

export type setUserNameType = ReturnType<typeof setUserNameAction>;

export type UserAction = setUserNameType
