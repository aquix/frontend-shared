import { createStore, applyMiddleware, AnyAction } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { SupportMessagerReducer } from './reducer/SupportMessagerReducer';
import { ISupportMessagerReducer } from '../types/reducer';

const loggerMiddleware = createLogger();
const initialState = {};

let middleware;

if (process.env.NODE_ENV === 'production') {
  middleware = applyMiddleware(thunkMiddleware);
} else {
  middleware = applyMiddleware(thunkMiddleware, loggerMiddleware);
}

export default createStore<ISupportMessagerReducer, AnyAction, {}, {}>(SupportMessagerReducer, initialState as ISupportMessagerReducer, middleware);
