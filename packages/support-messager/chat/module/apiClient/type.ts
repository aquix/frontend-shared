import { BaseCommandProps } from '@app-types/CommonTypes';

export interface ResultListRes<P> extends BaseCommandProps {
  list: P
}

export interface ResultItemRes<P> extends BaseCommandProps {
  item: P
}
