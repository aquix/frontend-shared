export const setCookie = (key: string, value: string) => {
  if (!document || !value) {
    return;
  }

  document.cookie = `${key}=${value}; path=/;Max-Age=${parseInt('2629743', 10)}`;
};

export const removeCookie = (key: string) => {
  if (!document) {
    return;
  }

  document.cookie = `${key}=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT`;
};

export const getCookie = (key: string) => {
  if (!document || !key) {
    return;
  }
  const results = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');

  if (results) {
    return (unescape(results[2]));
  } else {
    return;
  }
};
