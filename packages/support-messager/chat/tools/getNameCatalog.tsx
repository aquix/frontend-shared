type HandlerType = { [key: string]: string }

const handler: HandlerType = {
  'my': 'my-room',
  'archive': 'archive',
  'new': 'new',
  'empty': 'empty',
};

const getNameCatalog = (slug: string): string => handler[slug];

export default getNameCatalog;
