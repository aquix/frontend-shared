import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import mapValues from 'lodash/mapValues';
import axios from 'axios';


interface IImageListState {
  index: number
  file: File,
  dataUrl: string | ArrayBuffer | null
}

interface IImageUploader {
  sendMessage: (type: string, message: string) => void;
  icon?: React.ReactElement<HTMLSpanElement>;
  typing?: (type: string) => void;
}

const ImageUploader: FunctionComponent<IImageUploader> = (props) => {
  const { icon, sendMessage, typing } = props;
  const [imageList, setImageList] = useState<IImageListState[]>([]);
  const [indexItem, setIndexItem] = useState<number | undefined>(undefined);
  const [multiple, setMultiple] = useState<boolean>(true);
  const ref = useRef(null);

  const cancel = () => {
    setImageList([]);
  };

  const deleteItem = (item: IImageListState) => {
    const index = imageList.findIndex(i => i.index == item.index);
    setImageList([
      ...imageList.slice(0, index),
      ...imageList.slice(index + 1),
    ]);
  };

  const onChange = (files: FileList | null) => {
    if (!files) {
      return;
    }
    if (indexItem) {
      const index = imageList.findIndex(i => i.index == indexItem);
      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onloadend = function () {
        const item = {
          index,
          dataUrl: reader.result,
          file: files[0],
        };

        const payload = [
          ...imageList.slice(0, index),
          item,
          ...imageList.slice(index + 1),
        ];
        setImageList(payload);
        setMultiple(true);
      };
    } else {
      mapValues(files, (file: File, index: string) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function () {
          const payload = {
            index: parseInt(index),
            dataUrl: reader.result,
            file,
          };
          setImageList(prev => [...prev, payload]);
        };
      });
    }
  };


  const send = () => {
    imageList.forEach(async i => {
      typing && typing('image');
      const formData = new FormData();
      formData.append(
        'file',
        i.file,
        i.file.name,
      );
      //TODO: env url
      axios.post(`${process.env.API_URL_CLIENT}/v1/image/image-upload`, formData)
        .then((res) => {
          console.log('res', res)
          const { data } = res;
          if (data) {
            const { key } = data;
            sendMessage('image', key);
          }
        });
    });
    cancel();
  };


  const click = (index?: number) => {
    if (index) {
      setMultiple(false);
      setIndexItem(index);
    }
    //@ts-ignore
    ref?.current?.click();
  };

  return (
    <span>
    <label htmlFor="file-upload" className="custom-file-upload">
        {icon}
      </label>
      <input
        ref={ref}
        id="file-upload"
        type="file"
        style={{ display: 'none' }}
        multiple={multiple}
        onChange={({ target }) => onChange(target.files)}
      />
      {!!imageList.length &&
      <>
        <div className="upload-image-container-wrapper" onClick={() => cancel()} />
        <div className="upload-image-container">
          <p className="upload-image-title">Выбрано {imageList.length} файла</p>
          <div className="upload-image-items">
            {imageList.map((i) => {
              return (
                <div className="upload-image-item">
                  <img
                    className="upload-image-item-image"
                    src={i.dataUrl as string}
                    alt=""
                    width={100}
                    height={100}
                    key={i.index}
                  />
                  <div className="upload-image-item-text-container">
                    <p className="upload-image-item-name">{i.file.name}</p>
                    <p className="upload-image-item-size">{(i.file.size / 1024 / 1024).toFixed(2)}</p>
                  </div>
                  <div className="upload-image-item-buttons">
                    <span className="json-chat icon-loop" onClick={() => click(i.index)} />
                    <span className="json-chat icon-bin" onClick={() => deleteItem(i)} />
                  </div>
                </div>
              );
            })}
          </div>
          <div className="upload-image-buttons-container">
            <p>Добавить</p>
            <div className="upload-image-buttons-right-side">
              <p onClick={() => cancel()}>Отмена</p>
              <p onClick={() => send()}>Отправить</p>
            </div>
          </div>
        </div>
      </>
      }
    </span>
  );
};

export default ImageUploader;
