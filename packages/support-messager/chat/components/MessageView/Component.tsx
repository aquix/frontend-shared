import React, { FunctionComponent, useEffect, useMemo, useRef, useState } from 'react';
import { MessagesViewProps } from './redux';
//TODO: delete antd
import { Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroll-component';
import UserFullName from '../UserFullName';
import SendMessage from '../SendMessage';
import VisibilitySensor from 'react-visibility-sensor';
import UserOnlineAndLocation from '../UserOnlineAndLocation';
import MessageEmpty from './MessageEmpty';

const MessageView: FunctionComponent<MessagesViewProps> = (props) => {
  const {
    currentRoom,
    redirectInfo,
    loadMessages,
    messages,
    takeOver,
    agent,
    closeAppeal,
    count,
    currentCategory,
    countMessages,
    readMessage,
    readMessagesLocal,
    openMessage,
    close,
    openImageView,
    allNotify,
    active,
  } = props;
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [pageSize, setPageSize] = useState<number>(10);
  const [hasMore, setHasMore] = useState<boolean>(true);
  const messagesList = useMemo(() => messages[currentRoom?.id as number] || [], [currentRoom, messages]);
  useEffect(() => {
    if (currentRoom) {
      loadMessages(currentRoom.id, currentPage, pageSize);
    }
  }, [currentRoom, currentPage]);

  useEffect(() => {
    if (currentRoom) {
      countMessages(currentRoom.id);
    }
  }, [currentRoom]);

  useEffect(() => {
    if (messagesList?.length == count) {
      setHasMore(false);
    } else {
      setHasMore(true);
    }
  }, [currentRoom, messages, count]);


  const searchUserInSupportDB = () => {
    const value = currentRoom?.client?.outerId || currentRoom?.client?.email;
    if (!!value) {
      redirectInfo && redirectInfo(value);
    }
  };

  const messageItem = useRef<HTMLDivElement>(null);

  return (
    <div
      className={`chat-correspondence-container ${openMessage ? 'chat-correspondence-container-show' : 'chat-correspondence-container-hide'}`}>
      {currentRoom && (
        <div className="chat-correspondence-user">
          <div
            onClick={() => close()}
            className="chat-button-close-messages-wrapper"
          >
            <div className="chat-button-close-messages">
              {active && !!allNotify && <div className="chat-button-close-messages-bage">{allNotify}</div>}
              <span className="json-chat icon-close-messages chat-button-close-messages-icon" />
            </div>
          </div>
          <img
            className="chat-correspondence-user-img"
            src={currentRoom.getClientImage()}
            alt=""
          />
          <div className="chat-correspondence-user-text-wrapper">
            <div className="chat-correspondence-user-text-container">
              <p className="chat-correspondence-user-name">
                <UserFullName
                  userId={currentRoom.clientId}
                  defaultValue={(currentRoom?.isEmpty && !currentRoom?.client.name) && `# ${currentRoom?.id}` || currentRoom?.client.name}
                />
              </p>
              {!!currentRoom?.getClientEmail() && (
                <p className="chat-correspondence-user-email">{currentRoom?.getClientEmail()}</p>
              )}
              <p className="chat-correspondence-user-status">
                <UserOnlineAndLocation
                  userId={currentRoom?.clientId}
                  isOnline
                />
              </p>
              <p className="chat-correspondence-user-status">
                <UserOnlineAndLocation
                  userId={currentRoom?.clientId}
                  isLocation
                  isLastVisit
                />
              </p>
              {!currentRoom?.isEmpty && (
                <div
                  className="chat-correspondence-user-status_btn chat-correspondence-user-status_btn-mobile"
                  onClick={() => takeOver(currentCategory, currentRoom?.id)}
                >Взять
                  диалог
                  на себя
                </div>
              )}
            </div>
          </div>
          <div className="chat-correspondence-user-info-container">
            <div className="chat-correspondence-user-info">
              {(currentRoom?.agentId == agent?.user?.id) && currentRoom?.open && (
                <span className="json-chat icon-active-chat" onClick={() => closeAppeal(currentRoom?.id)} />
              )}
              {(!!currentRoom?.client?.outerId) && (
                <span className="json-chat icon-info" onClick={() => searchUserInSupportDB()} />
              )}
            </div>
            {!currentRoom.isEmpty && currentCategory !== 'my' && (
              <div
                className="chat-correspondence-user-status_btn chat-correspondence-user-status_btn-desctop"
                onClick={() => !currentRoom?.agent && takeOver(currentCategory, currentRoom?.id)}
              >
                {!currentRoom?.agent ? (<span>Взять диалог на себя</span>) : (
                  <UserFullName userId={currentRoom.agentId} />)}
              </div>
            )}
          </div>
        </div>
      )}
      <div
        id="chat-correspondence-content"
        className={`chat-correspondence-content ${!messagesList || !messagesList?.length && 'chat-messages-wrapper-empty'}`}>
        <MessageEmpty isMessages={!messagesList || !messagesList?.length} room={currentRoom} />
        <InfiniteScroll
          className="chat-messages-wrapper"
          scrollableTarget="chat-correspondence-content"
          dataLength={messagesList?.length || 0}
          next={() => {
            setCurrentPage(currentPage + 1);
          }}
          hasMore={hasMore}
          loader={<Spin />}
          refreshFunction={() => console.log('refreshFunction')}
          inverse={true}
        >
          {currentRoom?.typing.typing && (
            <div className={`chat-messages-left-wrapper`}>
              <div className={`chat-messages-left chat-messages-user`}>
                <p className="chat-messages-text">
                  {currentRoom.getTyping()}
                </p>
              </div>
            </div>
          )}
          {(messages[currentRoom?.id as number] || []).map((i) => {
            return (
              <VisibilitySensor
                key={i.id}
                containment={messageItem.current?.children[0].children[0]}
              >
                {({ isVisible }) => {
                  if ((isVisible && !i.agentId && !!i.clientId && (currentRoom?.agentId == agent?.userId) && !i.read)) {
                    readMessagesLocal(i.id, 'my', currentRoom?.id);
                    readMessage(i.id);
                  }
                  return (
                    <div className={`chat-messages-${i.getPosition()}-wrapper`}>
                      <div className={`chat-messages-${i.getPosition()} chat-messages-${i.getType()}`}>
                        {!!i.author && (
                          <p className="chat-messages-title">
                            {i.author}
                          </p>
                        )}
                        <p className="chat-messages-text">
                          {i.getMessage(openImageView)}
                        </p>
                      </div>
                    </div>
                  );
                }}
              </VisibilitySensor>
            );
          })}
        </InfiniteScroll>
      </div>
      <SendMessage />
    </div>
  );
};

export default MessageView;
