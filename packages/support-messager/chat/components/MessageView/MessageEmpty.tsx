import { Room } from '../../types/room';
import { FunctionComponent } from 'react';

interface IMessageEmpty {
  room?: Room;
  isMessages: boolean;
}

const MessageEmpty: FunctionComponent<IMessageEmpty> = (props) => {
  const { isMessages, room } = props;

  if (!isMessages) {
    return null;
  }

  let content = <p>Нет сообщений</p>;

  if (!room) {
    content = <p>Выберите или начните диалог чтобы увидеть сообщения</p>;
  }

  if (room?.isEmpty) {
    content = <p>Напишите пользователю, тем самым вы прикрепите его к себе до завершения диалога</p>;
  }

  return (
    <div className="message-empty_block">
      {content}
    </div>
  );
};

export default MessageEmpty;
