import { ThunkDispatch } from 'redux-thunk';
import { countMessages, loadMessageByRoomId, readMessages, readMessagesLocal } from '../../commands/message';
import { closeAppeal, takeOver } from '../../commands/room';
import { ISupportMessagerReducer } from '../../types/reducer';
import { debounce } from 'lodash';
import { setReadPathAction } from '../../redux/action/message';
import { IShowChatApp } from '../../index';
import { ImageViewAction } from '../../redux/action/chat';
import { AnyAction } from 'redux';

interface IMessagesViewProps {
  openMessage: boolean;

  close(): void;
}

export const mapStateToProps = (state: ISupportMessagerReducer) => ({
  currentRoom: state.room.current,
  currentCategory: state.category.current,
  messages: state.message.list,
  count: state.message.count,
  agent: state.agent.agent,
  active: state.agent.active,
  allNotify: state.category.allNotify
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => {
  const readMessagePath = debounce(() => {
    dispatch(readMessages());
  }, 1000);
  return ({
    loadMessages: (roomId: number, currentPage: number, pageSize: number) => dispatch(loadMessageByRoomId(roomId, currentPage, pageSize)),
    countMessages: (roomId: number) => dispatch(countMessages(roomId)),
    takeOver: (slug: string, roomId: number) => dispatch(takeOver(slug,roomId)),
    closeAppeal: (roomId: number) => dispatch(closeAppeal(roomId)),
    readMessagesLocal: (id: number, slug: string, roomId?: number) => roomId && dispatch(readMessagesLocal(id, slug, roomId)),
    readMessage: (id: number) => {
      dispatch(setReadPathAction(id));
      readMessagePath();
    },
    openImageView: (key: string) => dispatch(ImageViewAction(key)),
  });
};

export type MessagesViewProps =
  IShowChatApp
  & IMessagesViewProps
  & ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>;
