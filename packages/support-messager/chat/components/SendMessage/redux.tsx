import { ThunkDispatch } from 'redux-thunk';
import { ISupportMessagerReducer } from '../../types/reducer';
import { sendMessage } from '../../commands/message';
import { debounce } from 'lodash';
import { ImageUploadAction } from '../../redux/action/chat';
import { socketSendAction } from '../../redux/action/socket';
import { ISendMessageSocket } from '../../types/socket';
import { AnyAction } from 'redux';
import { takeOver } from '../../commands/room';

export const mapStateToProps = (state: ISupportMessagerReducer) => ({
  currentRoom: state.room.current,
  agent: state.agent.agent,
  currentCategory: state.category.current,
});


export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => {
  const stopTyping = debounce((topic, message, func) => {
    dispatch(socketSendAction({ action: 'stopTyping', topic, message }));
    func();
  }, 3000);

  return ({
    stopTyping,
    takeOver: (slug: string, roomId: number) => dispatch(takeOver(slug,roomId)),
    sendMessage: (message: string, roomId: number, type: string) => dispatch(sendMessage(message, roomId, type)),
    upload: (value: boolean) => dispatch(ImageUploadAction(value)),
    socketSend: (value: ISendMessageSocket) => dispatch(socketSendAction(value)),
  });
};

export type SendMessageProps =
  & ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>;
