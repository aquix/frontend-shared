import React, { FunctionComponent, useState } from 'react';
import { SendMessageProps } from './redux';
import ImageUploder from '../ImageUploader';
import { ITypingRoom } from '../../types/room';

const SendMessage: FunctionComponent<SendMessageProps> = (props) => {
  const { currentRoom, sendMessage, stopTyping, socketSend, takeOver, currentCategory } = props;
  const [value, setValue] = useState<string>('');
  const [typingMessage, setTypingMessage] = useState<boolean>(false);

  const typingHandler = (type: string = 'text') => {
    if (currentRoom) {
      if (!typingMessage) {
        socketSend({
          topic: `room/${currentRoom.clientId}`,
          action: 'typing',
          message: { to: currentRoom.clientId, type, typing: true, roomId: currentRoom.id },
        });
        setTypingMessage(true);
        stopTyping(`room/${currentRoom.clientId}`, {
          to: currentRoom.clientId,
          type,
          typing: false,
          roomId: currentRoom.id,
        } as ITypingRoom, () => setTypingMessage(false));
      } else {
        stopTyping(`room/${currentRoom.clientId}`, {
          to: currentRoom.clientId,
          type,
          typing: false,
          roomId: currentRoom.id,
        } as ITypingRoom, () => setTypingMessage(false));
      }
    }
  };

  const sendMessageHandler = (type: string = 'text', message?: string) => {
    if (!currentRoom) {
      return;
    }

    if (!message && !value) {
      return;
    }
    if (currentRoom.isEmpty) {
      takeOver(currentCategory, currentRoom.id).then(() => {
        sendMessage(message || value as string, currentRoom.id, type).then(() => {
          setValue('');
        });
      });
      return;
    }

    sendMessage(message || value as string, currentRoom.id, type).then(() => {
      setValue('');
      socketSend({
        action: 'stopTyping',
        topic: `room/${currentRoom.clientId}`,
        message: {
          to: currentRoom.clientId,
          roomId: currentRoom.id,
          type,
          typing: false,
        },
      });
    });
  };

  const disableInput = (currentRoom?.isEmpty ? !currentRoom?.isEmpty : (!currentRoom?.open || !currentRoom.agentId))
  return (
    <>
      <div className="chat-input-wrapper">
        <div className="chat-input">
          <input
            autoComplete="off"
            onKeyPress={(e) => {
              if (e.key == 'Enter') {
                sendMessageHandler();
              }
            }}
            value={value}
            placeholder="Введите ваше сообщение"
            onChange={({ target: { value } }) => {
              setValue(value);
              if (!currentRoom?.isEmpty) {
                typingHandler();
              }
            }}
            disabled={disableInput}
          />
        </div>
        <div className="chat-input-icons">
          {/*<span className="json-chat icon-attach-file" onClick={() => upload(true)} />*/}
          <ImageUploder
            icon={<span className="json-chat icon-attach-file" />}
            sendMessage={sendMessageHandler}
            typing={typingHandler}
          />
          {/*<span className="json-chat icon-emoticons" />*/}
          <span className="json-chat icon-send" onClick={() => sendMessageHandler()} />
        </div>
      </div>
    </>
  );
};

export default SendMessage;
