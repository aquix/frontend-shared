import React from 'react';
import { Upload, message } from 'antd';
import { RcFile, UploadChangeParam } from 'antd/es/upload/interface';
// import { sendSocket } from '../sokets';

interface IUploadImage {
  roomId: number
  onChange: (type: string, key: string) => void,
  onDisabled: (item: boolean) => void,
  value?: string,
  uploadClass?: string,
  text?: string,
  disabled?: boolean,
}

class UploadImage extends React.Component<IUploadImage> {
  state = {
    loading: false,
  };

  beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt5M = file.size / 1024 / 1024 < 10;
    if (!isLt5M) {
      message.error('Image must smaller than 10MB!');
    }
    return isJpgOrPng && isLt5M;
  };

  handleChange = (info: UploadChangeParam) => {
    const { onChange, onDisabled, roomId } = this.props;
    if (info.file.status === 'uploading') {
      sendSocket({
        topic: `room/${roomId}`,
        value: {},
        name: 'sending_photo',
      });
      onDisabled(true);
      return;
    }
    if (info.file.status === 'done') {
      const { response } = info.file;

      if (!response) {
        return;
      }

      const { key } = response;
      // sendSocket({
      //   topic: `room/${roomId}`,
      //   value: {},
      //   name: 'stop_sending_photo',
      // });
      onChange('image', key);
      onDisabled(false);
      this.setState({ loading: false });
    }
  };

  render() {
    const { uploadClass, disabled } = this.props;

    return (
      <React.Fragment>
        <Upload
          name="image"
          className={uploadClass}
          showUploadList={false}
          action={`${process.env.API_URL_CLIENT}/v1/image/image-upload`}
          beforeUpload={this.beforeUpload}
          onChange={this.handleChange}
          disabled={disabled}
          style={{ display: 'flex', justifyContent: 'center' }}
        >
          <span className="json icon-upload fs-25" />
        </Upload>
      </React.Fragment>
    );
  }
}

export default UploadImage;
