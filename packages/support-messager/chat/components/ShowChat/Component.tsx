import React, { FunctionComponent, useEffect } from 'react';
import { Badge } from 'antd';
import LayoutChat from '../Layout';
import { ShowChatProps } from './redux';
import debounce from 'lodash/debounce';

const Component: FunctionComponent<ShowChatProps> = (props) => {
  const {
    loadAgent,
    allNotify,
    visible,
    active,
    redirectInfo,
    visibleStyle,
    setVisible,
    setVisibleStyle,
    countNotify,
    joinSocket,
    wss,
    agent,
  } = props;

  useEffect(() => {
    if (wss && agent) {
      joinSocket('room');
      joinSocket(`room/${agent?.user?.id}`);
    }
  }, [wss, agent]);

  useEffect(() => {
    loadAgent()
  }, []);

  useEffect(() => {
    if (active) {
      countNotify();
    }
  }, [active]);

  const visibleSide = (mobile: boolean) => {
    const setVisibleMessenger = debounce(() => !visible ? setVisibleStyle(true) : setVisible(false), mobile ? 600 : 300);
    if (!visible) {
      setVisible(true);
      setVisibleMessenger();
    } else {
      setVisibleStyle(false);
      setVisibleMessenger();
    }
  };

  return (
    <span style={{ marginRight: !!allNotify && active && '25px' || '15px' }}>
      <span
        onClick={() => visibleSide(false)}
      >
          <Badge count={!!allNotify && active && allNotify || 0}>
            <span className="json-chat icon-chat_small" />
          </Badge>
        </span>
      {visible && <LayoutChat closeMessenger={() => visibleSide(true)} visibleMessenger={visibleStyle}
                              redirectInfo={redirectInfo} />}
      {/*<ImageUploader*/}
      {/*  sendMessage={console.log}*/}
      {/*/>*/}
    </span>
  );
};

export default Component;
