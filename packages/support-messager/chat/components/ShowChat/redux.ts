import { ThunkDispatch } from 'redux-thunk';
import { loadAgent } from '../../commands/agent';
import { ISupportMessagerReducer } from '../../types/reducer';
import { setVisibleChatAction, setVisibleChatStyleAction } from '../../redux/action/chat';
import { countNotify } from '../../commands/room';
import { AnyAction } from 'redux';
import { IShowChatApp } from '../../index';
import { socketJoinRoomAction } from '../../redux/action/socket';

interface IShowChatProps {
  dropdownClass?: string;
}

export const mapStateToProps = (state: ISupportMessagerReducer) => ({
  agent: state.agent.agent,
  active: state.agent.active,
  visible: state.chat.visible,
  visibleStyle: state.chat.visibleStyle,
  allNotify: state.category.allNotify,
  wss: state.socket.socket
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => ({
  loadAgent: () => dispatch(loadAgent()),
  setVisible: (value: boolean) => dispatch(setVisibleChatAction(value)),
  setVisibleStyle: (value: boolean) => dispatch(setVisibleChatStyleAction(value)),
  countNotify: () => dispatch(countNotify()),
  joinSocket: (topic: string) => dispatch(socketJoinRoomAction(topic)),
});

export type ShowChatProps =
  IShowChatApp
  & IShowChatProps
  & ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>
