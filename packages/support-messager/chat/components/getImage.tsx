const getImage = (width: number, height: number, key?: string, defaultImg: string = '/newsEvents0.jpg') => {
  if (!key) {
    return defaultImg;
  }
  return `${process.env.API_URL_CLIENT}/v1/image/${width}/${height}/${encodeURIComponent(key)}`;
};

export default getImage;
