import { ThunkDispatch } from 'redux-thunk';
import { closeShift, openShift } from '../../commands/agent';
import { ISupportMessagerReducer } from '../../types/reducer';
import { AnyAction } from 'redux';


export const mapStateToProps = (state: ISupportMessagerReducer) => ({
  active: state.agent.active,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => ({
  openShift: () => dispatch(openShift()),
  closeShift: () => dispatch(closeShift()),
});

export type SwitchShiftProps =
  ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>;
