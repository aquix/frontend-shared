import React, { FunctionComponent } from 'react';
import { SwitchShiftProps } from './redux';
import debounce from 'lodash/debounce';

const SwitchShift: FunctionComponent<SwitchShiftProps> = (props) => {
  const { openShift, closeShift, active } = props;

  const switchHandler = debounce((activeValue: boolean) => {
    if (!activeValue) {
      closeShift();
    } else {
      openShift();
    }
  }, 500);

  return (
    <div className="chat-dialogues-switch">
      <label className="chat-dialogues-switch-element">
        <input
          defaultChecked={active}
          className="chat-dialogues-switch-input"
          type="checkbox"
          onChange={({ target: { checked } }) => switchHandler(checked)} />
        <span className="chat-dialogues-switch-slider" />
      </label>
      <p className="chat-dialogues-switch-text">На смене</p>
    </div>
  );
};

export default SwitchShift;
