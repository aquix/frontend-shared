import { Room } from '../../types/room';

const sortingRooms = (slug: string, rooms: Room[]) => {
  if (slug == 'empty') {
    // @ts-ignore
    rooms.sort((a: Room, b: Room) => {
      // @ts-ignore
      const dateA = new Date(a?.lastUpdate).getTime()
      // @ts-ignore
      const dateB = new Date(b?.lastUpdate).getTime()

      return dateB > dateA && !b.online && a.online
    })
  }

  // @ts-ignore
  return rooms.sort((a, b) => {
    return new Date(b?.lastMessage?.createdAt).getTime() > new Date(a?.lastMessage?.createdAt).getTime()
  })
}

export default sortingRooms
