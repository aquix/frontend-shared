import { ThunkDispatch } from 'redux-thunk';
import { loadCountRooms, loadRooms } from '../../commands/room';
import { Room } from '../../types/room';
import { setCurrentRoomAction } from '../../redux/action/room';
import { ISupportMessagerReducer } from '../../types/reducer';
import { setCurrentCategoryAction } from '../../redux/action/category';
import { AnyAction } from 'redux';

interface IDialogsViewProps {
  open(): void;
}

export const mapStateToProps = (state: ISupportMessagerReducer) => ({
  category: state.category.list,
  currentCategory: state.category.current,
  rooms: state.room.list,
  active: state.agent.active,
  currentRoom: state.room.current,
});


export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => ({
  loadRooms: (slug: string, currentPage: number, pageSize: number) => dispatch(loadRooms(slug, currentPage, pageSize)),
  loadCountRooms: (slug: string) => dispatch(loadCountRooms(slug)),
  setCurrentRoom: (room: Room) => dispatch(setCurrentRoomAction(room)),
  setCurrentCategory: (slug: string) => dispatch(setCurrentCategoryAction(slug)),
});

export type DialogsViewProps =
  & IDialogsViewProps
  & ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>;
