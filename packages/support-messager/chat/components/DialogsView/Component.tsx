import React, { FunctionComponent, useEffect, useMemo, useRef, useState } from 'react';
import { DialogsViewProps } from './redux';
import { ICategory } from '../../types/category';
import { ICategoryRoom } from '../../types/room';
import SwitchShift from '../SwitchShift';
//TODO: Deleted antd
import { Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroll-component';
import UserFullName from '../UserFullName';
import UserOnlineAndLocation from '../UserOnlineAndLocation';
import sortingRooms from './sorting';

const DialogsView: FunctionComponent<DialogsViewProps> = (props) => {
  const dialogBox = useRef<HTMLDivElement>(null);
  const {
    category,
    loadRooms,
    loadCountRooms,
    rooms,
    setCurrentRoom,
    setCurrentCategory,
    currentCategory,
    open,
    active,
    currentRoom,
  } = props;
  const [hasMore, setHasMore] = useState<boolean>(true);
  const roomsBySlug = useMemo(() => (rooms[currentCategory as keyof ICategoryRoom] || []), [rooms, currentCategory]);
  const [currentPage, setCurrentPage] = useState<number>(0);

  useEffect(() => {
    if (active) {
      loadCountRooms(currentCategory);
    }
  }, [currentCategory, active]);

  useEffect(() => {
    if (active) {
      loadRooms(currentCategory, currentPage, Math.ceil((dialogBox?.current?.offsetHeight || 0) / 82) + 1);
    }
  }, [currentCategory, currentPage, active]);

  useEffect(() => {
    if (roomsBySlug?.items?.length == roomsBySlug?.count) {
      setHasMore(false);
    } else {
      setHasMore(true);
    }
  }, [currentCategory, rooms]);

  const readyRooms = useMemo(() => sortingRooms(currentCategory, roomsBySlug?.items || []), [roomsBySlug, currentCategory]);

  return (
    <div className="chat-dialogues-container">
      <div className="chat-dialogues-header">
        <input placeholder="Найти..." className="chat-dialogues-header-search" type="text" />
        <div className="chat-dialogues-header-dialogue_tabs">
          {category.map((i: ICategory) => (
            <div
              key={i.slug}
              onClick={() => {
                setCurrentPage(rooms[i.slug as keyof ICategoryRoom].currentPage);
                setCurrentCategory(i.slug);
              }}
              className={`chat-dialogues-header-dialogue_tabs-item-wrapper ${currentCategory === i.slug ? 'chat-dialogues-header-dialogue_tabs-item-active' : {}}`}
            >
              <span className="chat-dialogues-header-dialogue_tabs-item">
                {i.title}
              </span>
              {!!i.notify &&
              <span className="chat-dialogues-header-dialogue_tabs-item-notify">{i.notify}</span>
              }
            </div>
          ))}
        </div>
      </div>
      <div
        ref={dialogBox}
        id="chat-dialogues-content"
        className="chat-dialogues-content"
      >
        {!roomsBySlug?.items?.length && <p className="chat-dialogues-empty">Нет диалогов</p>}
        <InfiniteScroll
          scrollableTarget="chat-dialogues-content"
          dataLength={roomsBySlug?.items.length}
          next={() => {
            setCurrentPage(currentPage + 1);
          }}
          hasMore={hasMore}
          loader={<Spin />}
          pullDownToRefresh={true}
          refreshFunction={console.log}
          pullDownToRefreshThreshold={5}
        >
          {/*@ts-ignore*/}
          {readyRooms.map((i, index) => (
            <div
              key={i.id}
              onClick={() => {
                setCurrentRoom(i);
                open();
              }}
              className={`chat-dialogues-item ${currentRoom?.id === i.id ? 'chat-dialogues-item-active' : ''}`}
            >
              <img
                className="chat-dialogues-item-image"
                src={i.getClientImage()}
                alt={`dialog_image_${index}`}
              />
              <div className="chat-dialogues-item-text-wrapper">
                <div className="chat-dialogues-item-text-container">
                  <p className="chat-dialogues-item-title">{i.chat?.title} #{i.id}</p>
                  <p className="chat-dialogues-item-name">
                    {!!i.client.outerId &&
                    <UserFullName
                      userId={i.clientId}
                      defaultValue={String(i.client.outerId)} /> || `${!!i.client.name && (i.client.name + ' /') || ''}`} {!i.isEmpty && i.getClientEmail() || ''}
                  </p>
                  <p className="chat-dialogues-item-online">
                    <UserOnlineAndLocation userId={i.clientId} isOnline />
                  </p>
                  <p className="chat-dialogues-item-online">
                    <UserOnlineAndLocation userId={i.clientId} isLocation />
                  </p>
                  <p className="chat-dialogues-item-message">{i.isEmpty && (
                    <UserOnlineAndLocation userId={i.clientId} isLastVisit />
                  ) || i.getTyping()}</p>
                </div>
              </div>
              {!!i?.agent?.outerId && currentCategory == 'archive' && (
                <div>
                  <div
                    className="chat-correspondence-user-status_btn chat-correspondence-user-status_btn-desctop"
                  >
                    <UserFullName userId={i.agentId} defaultValue={String(i.agent.outerId)} />
                  </div>
                </div>
              )}
              {!!i.countNotify && i.open &&
              <span className="chat-dialogues-item-notify">{i.countNotify}</span>
              }
            </div>
          ))}
        </InfiniteScroll>
      </div>
      <SwitchShift />
    </div>
  );
};

export default DialogsView;
