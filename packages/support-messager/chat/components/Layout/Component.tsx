import React, { FunctionComponent, useEffect, useState } from 'react';
import MessageView from '../MessageView';
import DialogsView from '../DialogsView';
import { LayoutChatProps } from './redux';

const Layout: FunctionComponent<LayoutChatProps> = (props) => {
  const { visibleMessenger, closeMessenger, active, agent, redirectInfo, leaveSocket, joinSocket } = props;
  const [openMessage, setOpenMessage] = useState<boolean>(false);

  const open = () => setOpenMessage(true);
  const close = () => setOpenMessage(false);

  return (
    <>
      <div className={`chat-container ${visibleMessenger ? 'chat-container-show' : 'chat-container-hide'}`}>
        <div className={`chat-wrapper ${visibleMessenger ? 'chat-wrapper-show' : 'chat-wrapper-hide'}`}>
          <div
            onClick={() => closeMessenger()}
            className="chat-close-button"
          >
            <span className="json-chat icon-close-chat" />
          </div>
          <DialogsView open={open} />
          <MessageView close={close} openMessage={openMessage} redirectInfo={redirectInfo} />
        </div>
      </div>
    </>
  );
};

export default Layout;
