import { ISupportMessagerReducer } from '../../types/reducer';
import { ThunkDispatch } from 'redux-thunk';
import { socketJoinRoomAction, socketLeaveRoomAction } from '../../redux/action/socket';
import { AnyAction } from 'redux';
import { IShowChatApp } from '../../index';

interface ILayoutChatProps {
  visibleMessenger: boolean;

  closeMessenger(): void;
}

export const mapStateToProps = (state: ISupportMessagerReducer) => ({
  active: state.agent.active,
  agent: state.agent.agent,
});


export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => ({
  leaveSocket: (topic: string) => dispatch(socketLeaveRoomAction(topic)),
  joinSocket: (topic: string) => dispatch(socketJoinRoomAction(topic)),
});

export type LayoutChatProps =
  IShowChatApp
  & ILayoutChatProps
  & ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>
