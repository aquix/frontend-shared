import moment from 'moment';
import { displayDateUnit } from './dateHelper';
import { useState } from 'react';
import useInterval from './useInterval';

export type TimeOutHandler = (days: string | number, hours: string | number, minutes: string | number, seconds: string | number) => string | JSX.Element

const defaultHandler: TimeOutHandler = (days: string | number, hours: string | number, minutes: string | number, seconds: string | number) => `${days}:${hours}:${minutes}:${seconds}`;

const calculateDuration = (toDate: string | Date, handler: TimeOutHandler) => {
  const diff = moment().utc().diff(moment(toDate).utc());
  const duration = moment.duration(diff);
  const days = displayDateUnit(Math.floor(duration.asDays()));
  const hours = displayDateUnit(duration.hours());
  const minutes = displayDateUnit(duration.minutes());
  const seconds = displayDateUnit(duration.seconds());
  return handler(days, hours, minutes, seconds);
};

const timeOutHook = (toDate: string | Date, handler: TimeOutHandler = defaultHandler) => {
  const time = calculateDuration(toDate, handler);
  const [currentTime, setCurrentTime] = useState(time);

  useInterval(() => {
    const val = calculateDuration(toDate, handler);
    setCurrentTime(val);
  }, 1000);

  return [time, currentTime];
}

export default timeOutHook;
