import { FunctionComponent } from 'react';
import timeOutHook, { TimeOutHandler } from './timeOutHook';

interface TimerProps {
  date: string | Date
  handler?: TimeOutHandler
}

const Timer: FunctionComponent<TimerProps> = (props) => {
  const [time] = timeOutHook(props.date, props.handler);
  return <>{time}</>;
}

export default Timer;
