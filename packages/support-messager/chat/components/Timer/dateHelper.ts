export const displayDateUnit = (unit: number, min = 0) => unit <= 9 ? `0${Math.max(unit, min)}` : unit;
