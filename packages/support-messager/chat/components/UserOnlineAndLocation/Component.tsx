import React, { FunctionComponent, useEffect } from 'react';
import { IUserOnlineProps } from './redux';
import isEmpty from 'lodash/isEmpty';
import Timer from '../Timer';

const Component: FunctionComponent<IUserOnlineProps> = (props) => {
  const {
    load,
    userId,
    online,
    isLocation,
    isOnline,
    isLastVisit,
  } = props;

  useEffect(() => {
    load();
  }, []);

  if (isEmpty(online)) {
    return <span>{userId}</span>;
  }
  return (
    <span>
      {/*{childrenName.surname || ''}*/}
      {!!isOnline && (!!online.online ? <span style={{ color: 'green' }}>В сети </span> :
        <span style={{ color: 'red' }}>Не в сети </span>)}
      {!!isLocation && <span>{online.location} </span>}

      {!!online.online && !!isLastVisit && <Timer
        date={online.timeChangeLocation}
        handler={(days: string | number, hours: string | number, minutes: string | number, seconds: string | number) => {
          if (!!parseInt(days as string, 10)) {
            return `${parseInt(days as string, 10)} Дней`;
          }
          if (!!parseInt(hours as string, 10)) {
            return `${parseInt(hours as string, 10)} Часов`;
          }

          if (!!parseInt(minutes as string, 10)) {
            return `${parseInt(minutes as string , 10)} Минуты`;
          }

          return `${seconds} Секунду`;
        }}
      />}
      {/*{childrenName.middleName || ''}*/}
    </span>
  );
};

export default Component;
