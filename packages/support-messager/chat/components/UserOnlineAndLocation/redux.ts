import { ThunkDispatch } from 'redux-thunk';
import { loadFullName, loadOnline } from '../../commands/agent';
import { ISupportMessagerReducer } from '../../types/reducer';
import { AnyAction } from 'redux';

export interface IUserOnline {
  userId: number,
  isOnline?: boolean
  isLocation?: boolean
  isLastVisit?: boolean
}

export const mapStateToProps = (state: ISupportMessagerReducer, props: IUserOnline) => {
  const { userId } = props;
  return ({
    online: state.online[userId],
  });
};

export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, null, AnyAction>, props: IUserOnline) => ({
  load: () => dispatch(loadOnline(props.userId)),
});

export type IUserOnlineProps = IUserOnline & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
