import { ThunkDispatch } from 'redux-thunk';
import { ISupportMessagerReducer } from '../../types/reducer';
import { ImageViewAction } from '../../redux/action/chat';
import { AnyAction } from 'redux';

export const mapStateToProps = (state: ISupportMessagerReducer) => ({
  img: state.chat.img,
});

export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, void, AnyAction>) => ({
  close: () => dispatch(ImageViewAction(undefined)),
});

export type ImageViewProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
