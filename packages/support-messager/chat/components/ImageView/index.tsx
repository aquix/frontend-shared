import React from 'react';
import Component from './Component';
import { connect } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from './redux';

export default connect(mapStateToProps, mapDispatchToProps)(Component);
