import React, { FunctionComponent } from 'react';
import { ImageViewProps } from './redux';
import getImage from '../getImage';


const ImageView: FunctionComponent<ImageViewProps> = (props) => {
  const { img, close } = props;

  if (!img) {
    return null;
  }

  return (
    <div className="image-view-box">
      <div className="image-view-mask" onClick={() => close()} />
      <div className="json-chat icon-chat_small image-view-image-cross" onClick={() => close()} />
      <div className="image-view-image-wrapper">
        <img
          className="image-view-image"
          src={getImage(1200, 1200, img)}
          alt="img"
        />
      </div>
    </div>
  );
};

export default ImageView;
