import { connect } from 'react-redux';
import Component from './Component';
import { mapDispatchToProps, mapStateToProps } from './redux';

export default connect(mapStateToProps, mapDispatchToProps)(Component);
