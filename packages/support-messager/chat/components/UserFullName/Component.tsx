import React, { FunctionComponent, useEffect } from 'react';
import { IUserNameProps } from './redux';
import isEmpty from 'lodash/isEmpty';

const Component: FunctionComponent<IUserNameProps> = (props) => {
  const {
    load,
    userId,
    childrenName,
    defaultValue
  } = props;

  useEffect(() => {
    load();
  }, []);
  if (isEmpty(childrenName)) {
    return <span>{defaultValue || userId}</span>;
  }

  return (
    <span>
      {/*{childrenName.surname || ''}*/}
      {childrenName.firstName || ''}
      {/*{childrenName.middleName || ''}*/}
    </span>
  );
};

export default Component;
