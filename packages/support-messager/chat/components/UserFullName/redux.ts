import { ThunkDispatch } from 'redux-thunk';
import { loadFullName } from '../../commands/agent';
import { ISupportMessagerReducer } from '../../types/reducer';
import { AnyAction } from 'redux';

export interface IUserFullName {
  userId: number,
  includeId?: boolean,
  includeEmail?: boolean,
  defaultValue?: string,
}

export const mapStateToProps = (state: ISupportMessagerReducer, props: IUserFullName) => {
  const { userId } = props;
  return ({
    childrenName: state.user[userId] || userId,
  });
};

export const mapDispatchToProps = (dispatch: ThunkDispatch<ISupportMessagerReducer, null, AnyAction>, props: IUserFullName) => ({
  load: () => dispatch(loadFullName(props.userId)),
});

export type IUserNameProps = IUserFullName & ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;
