# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.0.13](https://gitlab.com/aquix/frontend-shared/compare/v3.0.12...v3.0.13) (2023-05-08)

**Note:** Version bump only for package frontend-shared





## [3.0.12](https://gitlab.com/aquix/frontend-shared/compare/v3.0.11...v3.0.12) (2023-05-08)

**Note:** Version bump only for package frontend-shared





## [3.0.11](https://gitlab.com/aquix/frontend-shared/compare/v0.0.7...v3.0.11) (2023-05-08)

**Note:** Version bump only for package frontend-shared





## [1.2.7](https://gitlab.com/aquix/frontend-shared/compare/v1.2.6...v1.2.7) (2022-05-11)

**Note:** Version bump only for package frontend-shared





## [1.2.6](https://gitlab.com/aquix/frontend-shared/compare/v1.2.5...v1.2.6) (2022-05-11)

**Note:** Version bump only for package frontend-shared





## [1.2.5](https://gitlab.com/aquix/frontend-shared/compare/v1.1.19...v1.2.5) (2022-05-11)

**Note:** Version bump only for package frontend-shared





## [1.1.14-alpha.4](https://gitlab.com/aquix/frontend-shared/compare/v1.1.14-alpha.3...v1.1.14-alpha.4) (2021-09-29)

**Note:** Version bump only for package frontend-shared





## [1.1.14-alpha.3](https://gitlab.com/aquix/frontend-shared/compare/v1.1.14-alpha.2...v1.1.14-alpha.3) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.14-alpha.2](https://gitlab.com/aquix/frontend-shared/compare/v1.1.14-alpha.1...v1.1.14-alpha.2) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.14-alpha.1](https://gitlab.com/aquix/frontend-shared/compare/v1.1.14-alpha.0...v1.1.14-alpha.1) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.14-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.13-alpha.0...v1.1.14-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.13-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.12-alpha.0...v1.1.13-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.12-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.11-alpha.0...v1.1.12-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.11-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.10-alpha.0...v1.1.11-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.10-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.9-alpha.0...v1.1.10-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.9-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.8-alpha.0...v1.1.9-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.8-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.7-alpha.0...v1.1.8-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.7-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.6-alpha.0...v1.1.7-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.6-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.5-alpha.0...v1.1.6-alpha.0) (2021-09-28)

**Note:** Version bump only for package frontend-shared





## [1.1.5-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.4-alpha.0...v1.1.5-alpha.0) (2021-09-21)

**Note:** Version bump only for package frontend-shared





## [1.1.4-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.3-alpha.0...v1.1.4-alpha.0) (2021-09-21)

**Note:** Version bump only for package frontend-shared





## [1.1.3-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.2-alpha.0...v1.1.3-alpha.0) (2021-09-21)

**Note:** Version bump only for package frontend-shared





## [1.1.2-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.1-alpha.0...v1.1.2-alpha.0) (2021-09-21)

**Note:** Version bump only for package frontend-shared





## [1.1.1-alpha.0](https://gitlab.com/aquix/frontend-shared/compare/v1.1.0...v1.1.1-alpha.0) (2021-09-13)

**Note:** Version bump only for package frontend-shared





# [1.1.0](https://gitlab.com/aquix/frontend-shared/compare/v1.0.4...v1.1.0) (2021-09-04)

**Note:** Version bump only for package frontend-shared
